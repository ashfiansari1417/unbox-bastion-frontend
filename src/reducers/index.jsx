import { combineReducers } from 'redux';
import EmailApp from './email.reducer';
import ChatApp from './chat.reducer';
import EcommerceApp from './ecommerce.reducer';
import WishlistApp from './wishlist.reducer';
import Filters from './filters.reducer';
import Customizer from './customizer.reducer';
import getAccounts from './getAccounts';
import loginUser from './loginUser';
import transactions from './transactions';
import beneficiary from './beneficiaryReducer';
import adminReducer from './adminReducer';
import loadingReducer from './loaderReducer';
import passwordReducer from './passwordReducer';

const reducers = combineReducers({
  EmailApp,
  ChatApp,
  data: EcommerceApp,
  WishlistApp,
  filters: Filters,
  Customizer,
  getAccounts,
  loginUser,
  transactions,
  beneficiary,
  adminReducer,
  loadingReducer,
  passwordReducer,
});

export default reducers;
