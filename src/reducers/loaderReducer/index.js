import {
  LOADING_START,
  LOADING_STOP,
  TRANSFER_LOADING_START,
  TRANSFER_LOADING_STOP,
} from '../../constant/actionTypes';

const initialState = {
  isloading: false,
  isTransactionLoading: false,
  transferLoader: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING_START:
      return {
        ...state,
        isloading: true,
      };
    case LOADING_STOP:
      return {
        ...state,
        isloading: false,
      };
    case TRANSFER_LOADING_START:
      return {
        ...state,
        transferLoader: true,
      };
    case TRANSFER_LOADING_STOP:
      return {
        ...state,
        transferLoader: false,
      };
    default:
      return state;
  }
}
