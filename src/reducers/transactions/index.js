import {
  ON_GET_TRANSACTIONS,
  ON_EXCHANGE_CURRENCY,
  TRANSACTION_OUTGOING,
  TRANSACTION_INTERNAL,
  GET_EXCHANGE_RATE,
} from '../../constant/actionTypes';

const initialState = {
  transactionData: {},
  exchangeCurrency: {},
  transactionMessage: null,
  internalTransactionMessage: null,
  exchangeRate: null,
  exchangeSuccessMessage: '',
  error: false,
  called: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ON_GET_TRANSACTIONS:
      const { error } = action.payload;
      return {
        ...state,
        transactionData: action.payload,
        error,
        called: true,
      };
    case TRANSACTION_OUTGOING:
      return { ...state, transactionMessage: action.payload };
    case TRANSACTION_INTERNAL:
      return { ...state, internalTransactionMessage: action.payload };
    case GET_EXCHANGE_RATE:
      return { ...state, exchangeRate: action.payload };
    case ON_EXCHANGE_CURRENCY:
      return { ...state, exchangeSuccessMessage: action.payload };

    default:
      return { ...state, called: false };
  }
}
