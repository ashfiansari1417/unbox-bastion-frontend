import { ON_GET_ACCOUNTS } from '../../constant/actionTypes';

const initialState = {
  accountData: [],
  error: false,
  called: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_GET_ACCOUNTS:
      const { error } = action.payload;
      return {
        ...state,
        accountData: action.payload,
        error,
        called: true,
      };
    default:
      return { ...state, called: false };
  }
};
