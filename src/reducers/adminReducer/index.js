import {
  GET_SWIFT_OUTGOING_PENDING,
  SWIFT_ADMIN_APPROVAL,
  ON_GET_ADMIN_BANK_ACCOUNTS,
  GET_ACTIVE_CLIENTS,
  GET_CLIENT_TRANSACTION,
} from '../../constant/actionTypes';

const initialState = {
  //bank balance
  adminBankBalance: [],
  //outgoing internal and external transfer
  swiftOutgoingPendingTransactions: null,
  swiftAdminApprovalMessage: null,
  // client account
  activeClients: null,
  clientTransaction: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ON_GET_ADMIN_BANK_ACCOUNTS:
      return { ...state, adminBankBalance: action.payload };
    case GET_SWIFT_OUTGOING_PENDING:
      return { ...state, swiftOutgoingPendingTransactions: action.payload };
    case SWIFT_ADMIN_APPROVAL:
      return { ...state, swiftAdminApprovalMessage: action.payload };
    case GET_ACTIVE_CLIENTS:
      return { ...state, activeClients: action.payload };
    case GET_CLIENT_TRANSACTION:
      return { ...state, clientTransaction: action.payload };
    default:
      return state;
  }
}
