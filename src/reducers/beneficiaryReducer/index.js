import {
  ADD_BENEFICIARY,
  GET_BENEFICIARY,
  DELETE_BENEFICIARY,
} from '../../constant/actionTypes';

const initialState = {
  allBeneficiaries: null,
  beneficiaryMessage: null,
  deleteBeneficiaryMsg: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_BENEFICIARY:
      return {
        ...state,
        beneficiaryMessage: action.payload,
      };
    case GET_BENEFICIARY:
      return { ...state, allBeneficiaries: action.payload };
    case DELETE_BENEFICIARY:
      return { ...state, deleteBeneficiaryMsg: action.payload };
    default:
      return state;
  }
}
