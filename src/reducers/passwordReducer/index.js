import { RESET_PASSWORD } from '../../constant/actionTypes';

const initialState = {
  resetPassword: {},
  error: false,
  called: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case RESET_PASSWORD:
      return { ...state, resetPassword: action.payload };
    default:
      return { ...state };
  }
}
