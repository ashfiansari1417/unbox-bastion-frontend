import { ON_LOGIN, ON_ADMIN_LOGIN } from '../../constant/actionTypes';

const initialState = {
  data: null,
  error: false,
  called: false,
  adminData: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_LOGIN:
      const { error } = action.payload || {};
      return {
        ...state,
        data: action.payload,
        error,
        called: true,
      };
    case ON_ADMIN_LOGIN:
      return {
        ...state,
        adminData: action.payload,
        called: true,
      };
    default:
      return { ...state, called: false };
  }
};
