import { Home, Airplay } from 'react-feather';
import bankLogo from '../assets/images/sidebar-image/bank.png';
import usersLogo from '../assets/images/sidebar-image/users.png';
import swiftLogo from '../assets/images/sidebar-image/swift.png';
import sepaLogo from '../assets/images/sidebar-image/sepa.png';
import homeLogo from '../assets/images/headerLogoRed.png';
import logoutIcon from '../assets/images/sidebar-logout.png';
import customerLogo from '../assets/images/sidebar-image/customer.png';
import creditLogo from '../assets/images/sidebar-image/credit.png';
import merchantLogo from '../assets/images/sidebar-image/merchant.png';
const admin = localStorage.getItem('admin');
const userID = JSON.parse(admin);

let MENUITEMS = [];
window.location.href.includes('admin')
  ? (MENUITEMS = [
      {
        title: 'Home',
        icon: homeLogo,
        type: 'link',
        path: '/admin/home',
      },
      {
        title: 'Client Accounts',
        icon: usersLogo,
        type: 'sub',
        active: false,
        children: [
          {
            path: '/admin/clients-active',
            title: 'Active Clients',
            type: 'link',
            active: false,
          },
          {
            path: '/admin/clients-underCompliance',
            title: 'Under Compliance',
            type: 'link',
            active: false,
          },
          {
            path: '/admin/clients-closed',
            title: 'Closed Clients',
            type: 'link',
            active: false,
          },
        ],
      },
      {
        title: 'SWIFT Transfers',
        icon: swiftLogo,
        type: 'sub',
        active: false,
        children: [
          {
            path: '/admin/swift-outgoing',
            title: 'Outgoing',
            type: 'link',
            active: false,
          },
          {
            path: '/admin/swift-incoming',
            title: 'Incoming',
            type: 'link',
            active: false,
          },
          {
            path: '/admin/swift-internal',
            title: 'Internal',
            type: 'link',
            active: false,
          },
          {
            path: '/admin/swift-internal',
            title: 'MT Messages',
            type: 'link',
            active: false,
          },
        ],
      },
      // {
      //   title: 'SEPA Transfers',
      //   icon: sepaLogo,
      //   type: 'sub',
      //   active: false,
      //   children: [
      //     {
      //       path: '/admin/sepa-outgoing',
      //       title: 'Outgoing',
      //       type: 'link',
      //       active: false,
      //     },
      //     {
      //       path: '/admin/sepa-incoming',
      //       title: 'Incoming',
      //       type: 'link',
      //       active: false,
      //     },
      //     {
      //       path: '/admin/sepa-internal',
      //       title: 'Internal',
      //       type: 'link',
      //       active: false,
      //     },
      //   ],
      // },
      {
        title: 'Logout',
        icon: logoutIcon,
        type: 'logout',
        path: '/admin-login',
      },
      // {
      //     title: 'Currency Exchange', icon: merchantLogo, type: 'sub', active: false,
      //     children: [
      //         { path: '/admin/exchange-swift', title: 'SWIFT', type: 'link', active: false },
      //     ]
      // }
    ])
  : (MENUITEMS = [
      {
        title: 'Users',
        icon: usersLogo,
        type: 'sub',
        badgeType: 'primary',
        active: true,
        children: [
          { path: '/dashboard/my-account', title: 'My Account', type: 'link' },
          {
            path: '/dashboard/all-transaction',
            title: 'Payment History',
            type: 'link',
          },
          {
            path: '/dashboard/currency-exchange',
            title: 'Currency Exchange',
            type: 'link',
          },
          { path: '/dashboard/user-setting', title: 'Settings', type: 'link' },
          {
            path: '/dashboard/user-information',
            title: 'Client Information',
            type: 'link',
          },
          { path: '/dashboard/user-contacts', title: 'Contacts', type: 'link' },
        ],
      },
    ]);
export { MENUITEMS };
