/* TODO APP*/
export const GET_LIST = 'GET_LIST';
export const GET_LIST_SUCCESS = 'GET_LIST_SUCCESS';
export const MARK_ALL_ITEMS = 'MARK_ALL_ITEMS';
export const ADD_NEW_ITEM = 'ADD_NEW_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const SELECTED_ITEM = 'SELECTED_ITEM';

/* TODO FIREBASE APP*/
export const GET_LIST_FIREBASE = 'GET_LIST';
export const GET_LIST_SUCCESS_TASK = 'GET_LIST_SUCCESS_TASK';
export const MARK_ALL_TASK = 'MARK_ALL_TASK';
export const ADD_NEW_TASK = 'ADD_NEW_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';
export const SELECTED_TASK = 'SELECTED_TASK';

/* ECOMMERRCE APP */
export const GET_ALL_PRODUCT = 'GET_ALL_PRODUCT';
export const SELECTED_PRODUCT = 'SELECTED_PRODUCT';
export const CHANGE_CURRENCY = 'CHANGE_CURRENCY';
export const SEARCH_MEMBER_PRODUCT = 'SEARCH_MEMBER_PRODUCT';

// Cart
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const INCREMENT_QTY = 'INCREMENT_QTY';
export const DECREMENT_QTY = 'DECREMENT_QTY';
export const GET_SINGLE_ITEM = 'GET_SINGLE_ITEM';

// WishList
export const ADD_TO_WISHLIST = 'ADD_TO_WISHLIST';
export const REMOVE_FROM_WISHLIST = 'REMOVE_FROM_WISHLIST';

// CheckOut Process
export const CHECKOUT_REQUEST = 'CHECKOUT_REQUEST';
export const CHECKOUT_SUCCESS = 'CHECKOUT_SUCCESS';
export const CHECKOUT_FAILURE = 'CHECKOUT_FAILURE';

// FILTERS
export const FILTER_BRAND = 'FILTER_BRAND';
export const FILTER_COLOR = 'FILTER_COLOR';
export const FILTER_PRICE = 'FILTER_PRICE';
export const SORT_BY = 'SORT_BY';
export const SEARCH_BY = 'SEARCH_BY';

/* CHAT APP*/
export const GET_MEMBERS = 'GET_MEMBERS';
export const GET_MEMBERS_SUCCESS = 'GET_MEMBERS_SUCCESS';
export const GET_CHATS = 'GET_CHATS';
export const GET_CHATS_SUCCESS = 'GET_CHATS_SUCCESS';
export const GET_CHATS_ERROR = 'GET_CHATS_ERROR';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEARCH_MEMBER = 'SEARCH_MEMBER';
export const CHANGE_CHAT = 'CHANGE_CHAT';
export const CREATE_CHAT = 'CREATE_CHAT';
export const UPDATE_STATUS = 'UPDATE_STATUS';
export const UPDATE_SELECTED_USER = 'UPDATE_SELECTED_USER';
export const REPLY_BY_SELECTED_USER = 'REPLY_BY_SELECTED_USER';

// EMAIL APP
export const GET_EMAILS = 'GET_EMAILS';
export const GET_ALL_EMAILS = 'GET_ALL_EMAILS';
export const GET_EMAIL_TYPES = 'GET_EMAIL_TYPES';
export const UPDATE_EMAIL_TYPES = 'UPDATE_EMAIL_TYPES';

// AUTHENTICATION APP
export const LOGIN = 'LOGIN';
export const INITIALIZE_FIREBASE = 'INITIALIZE_FIREBASE';

//CUSTOMIZER
export const ADD_COSTOMIZER = 'ADD_COSTOMIZER';
export const ADD_LAYOUT = 'ADD_LAYOUT';
export const ADD_SIDEBAR_TYPES = 'ADD_SIDEBAR_TYPES';
export const ADD_SIDEBAR_SETTINGS = 'ADD_SIDEBAR_SETTINGS';
export const ADD_COLOR = 'ADD_COLOR';
export const ADD_MIXlAYOUT = 'ADD_MIXlAYOUT';

// export const CONSTANT_URL = `http://localhost:4100/`;
export const CONSTANT_URL = `https://sterfex.com:4200/`;

// FINCO APP
export const ADMIN_LOGIN_APP = CONSTANT_URL + `adminLogin`;
export const SIGN_UP_APP = CONSTANT_URL + `sigunp`;
export const LOGIN_APP = CONSTANT_URL + `login`;
export const GET_USER_ACCOUNT_DETAILS = CONSTANT_URL + `getUserAccountDetails`;
export const TRANSFER_REQUEST = CONSTANT_URL + `addTransferDetails`;
export const GET_TRANSFER_REQUEST = CONSTANT_URL + `getTransferDetails`;
export const APPROVE_TRANSFER_REQUEST = CONSTANT_URL + `approveTransferStatus`;
export const GET_APPROVE_REQUEST = CONSTANT_URL + `getApproveTransferRequest`;
export const GET_REJECT_REQUEST = CONSTANT_URL + `getRejectTransferRequest`;
export const GET_ALL_TRANSACTION_REQUEST =
  CONSTANT_URL + `getAllRequestTransaction`;
export const GET_TOTAL_BALANCE = CONSTANT_URL + `getTotalBalance`;

export const GET_FinIncoming = CONSTANT_URL + `getFinIncoming`;
export const GET_FinIncomingApprovedRequest =
  CONSTANT_URL + `getFinIncomingApprovedRequest`;
export const GET_FinIncomingRejectRequest =
  CONSTANT_URL + `getFinIncomingRejectRequest`;
export const ApproveTransferAccountStatusFinIncoming =
  CONSTANT_URL + `approveTransferAccountStatusFinIncoming`;

export const Get_PendingCurrencyExchange =
  CONSTANT_URL + `getPendingCurrencyExchange`;
export const Get_ApprovedCurrencyExchange =
  CONSTANT_URL + `getApprovedCurrencyExchange`;
export const Get_RjectedCurrencyExchange =
  CONSTANT_URL + `getRjectedCurrencyExchange`;

export const ExchangeApproveDisapprove = CONSTANT_URL + `getExchangeRequest`;
export const setExchangeRate = CONSTANT_URL + `setExchangeRate`;
export const getExchangeRates = CONSTANT_URL + `getExchangeRates`;

export const SendGetRateOtehrInfo = CONSTANT_URL + `getexchangeUserDetail`;
export const SendChangeingAmount = CONSTANT_URL + `getExchangeCalculator`;
export const SaveExchangedAmount = CONSTANT_URL + `setExchangeDataInsert`;

export const GetNewUserSignupDetails = `https://130642xi7h.execute-api.us-west-2.amazonaws.com/default/get_new_user_signup_details`;
export const NewUserRequest = `https://57u8vsrbmg.execute-api.us-west-2.amazonaws.com/default/new_user_request`;

export const updatePassword = `https://ghlmox95na.execute-api.us-west-2.amazonaws.com/default/update_user_password`;

export const forgotPasswordLink = `https://sterfex.com:4200/forgot-password`;
export const resetPassword = `https://sterfex.com:4200/reset-password`;

// *************************************************************************************************************************************************************************

export const ON_LOGIN = 'ON_LOGIN';
export const ON_GET_ACCOUNTS = 'ON_GET_ACCOUNTS';
export const ON_ADMIN_LOGIN = 'ON_ADMIN_LOGIN';

//BENEFICIARIES
export const ADD_BENEFICIARY = 'ADD_BENEFICIARY';
export const GET_BENEFICIARY = 'GET_BENEFICIARY';
export const DELETE_BENEFICIARY = 'DELETE_BENEFICIARY';

//transactions
export const ON_GET_TRANSACTIONS = 'ON_GET_TRANSACTIONS';
export const EXCHANGE_CURRENCY = 'EXCHANGE_CURRENCY';
export const TRANSACTION_OUTGOING = 'TRANSACTION_OUTGOING';
export const TRANSACTION_INTERNAL = 'TRANSACTION_INTERNAL';
export const GET_EXCHANGE_RATE = 'GET_EXCHANGE_RATE';
export const ON_EXCHANGE_CURRENCY = 'ON_EXCHANGE_CURRENCY';

// admin
export const GET_SWIFT_OUTGOING_PENDING = 'GET_SWIFT_OUTGOING_PENDING';
export const SWIFT_ADMIN_APPROVAL = 'SWIFT_ADMIN_APPROVAL';
export const ON_GET_ADMIN_BANK_ACCOUNTS = 'ON_GET_ADMIN_BANK_ACCOUNTS';
export const GET_ACTIVE_CLIENTS = 'GET_ACTIVE_CLIENTS';
export const GET_CLIENT_TRANSACTION = 'GET_CLIENT_TRANSACTION';

// Loader
export const LOADING_START = 'LOADING_START';
export const LOADING_STOP = 'LOADING_STOP';
export const TRANSFER_LOADING_START = 'TRANSFER_LOADING_START';
export const TRANSFER_LOADING_STOP = 'TRANSFER_LOADING_STOP';

// password
export const RESET_PASSWORD = 'RESET_PASSWORD';
