import React, { useState, useEffect, Fragment } from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
// to implement recaptcha
// import { loadReCaptcha } from 'react-google-recaptcha'

import { createBrowserHistory } from 'history';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

// ** Import custom components for redux**
import { Provider } from 'react-redux';
import store from './store/index';
import App from './components/app';
// Import custom Components

import MyAccount from './components/dashboard/defaultCompo/myAccount';
import AllTransactions from './components/dashboard/defaultCompo/allTransaction';
import HomeComponent from './components/dashboard/defaultCompo/home';
import userCurrencyExch from './components/dashboard/defaultCompo/userCurrencyExchange';

import TransferForm from './components/dashboard/defaultCompo/transferForm';
import UserInformations from './components/dashboard/defaultCompo/user-information';
import UserContacts from './components/dashboard/defaultCompo/user-contacts';
import UserSettings from './components/dashboard/defaultCompo/user-setting';
import UserMtMessage from './components/dashboard/defaultCompo/mt-message';

import Signin from './auth/signin';
// import SignUp from "./auth/signup";
import SignUp from './auth/SignUp2';
import AdminSignIn from './auth/adminSignin';
import ForgotPassword from './auth/forgotPassword';

//Admin Data
import AdminHome from './components/dashboard/admin/AdminHome';
import OutPending from './components/dashboard/admin/fin-outgoing/out-pending';
import OutApproveRequest from './components/dashboard/admin/fin-outgoing/out-approve-req';
import OutRejectRequest from './components/dashboard/admin/fin-outgoing/out-reject-req';
import InPending from './components/dashboard/admin/fin-imcoming/pending-req';
import InApproved from './components/dashboard/admin/fin-imcoming/approve-req';
import InDisapproved from './components/dashboard/admin/fin-imcoming/reject-req';
import ExcApproved from './components/dashboard/admin/exchange/exchange-approved';
import ExcDisapproved from './components/dashboard/admin/exchange/exchange-disapproved';
import ExcPending from './components/dashboard/admin/exchange/exchange-pending';
import ExcSetRate from './components/dashboard/admin/exchange/exchange-setExchRate';
import TransactionLog from './components/dashboard/admin/exchange/exchange-history';
import UaPending from './components/dashboard/admin/client-accounts/useracc-pending';
import UaApproved from './components/dashboard/admin/client-accounts/useracc-approved';
import UaDisapproved from './components/dashboard/admin/client-accounts/useracc-disapproved';
import UaActive from './components/dashboard/admin/client-accounts/useracc-active';
import UaClosedacc from './components/dashboard/admin/client-accounts/useracc-closedAcc';
import InternalTransfers from './components/dashboard/admin/internal-transfers';
import Commissions from './components/dashboard/admin/commissions';
import TotalBalance from './components/dashboard/admin/total-balance';
import MtMessage from './components/dashboard/admin/mt-message';
import SwiftOutgoing from './components/dashboard/admin/swift/SwiftOutgoing';
import SwiftIncoming from './components/dashboard/admin/swift/SwiftIncoming';
import SwiftInternal from './components/dashboard/admin/swift/SwiftInternal';
import SepaOutgoing from './components/dashboard/admin/sepa/SepaOutgoing';
import SepaIncoming from './components/dashboard/admin/sepa/SepaIncoming';
import SepaInternal from './components/dashboard/admin/sepa/SepaInternal';
import ExchangeSwift from './components/dashboard/admin/exchange/exchangeSwift';
import EmptyCompoent from './components/EmptyCompoent';
import Otp from './Otp';
import AddBeneficiary from './components/dashboard/defaultCompo/AddBeneficiary';
import ResetPassword from './auth/resetPassword';
import AddressBook from './components/dashboard/defaultCompo/AddressBook';
import UserSetting from './components/dashboard/defaultCompo/UserSettings';
import EditUserProfile from './components/dashboard/defaultCompo/EditUserProfile';

const hist = createBrowserHistory();

function Root() {
  const abortController = new window.AbortController();
  const [currentUser, setCurrentUser] = useState('true');
  const [, setSuperUser] = useState('true');

  useEffect(() => {
    // const admin = localStorage.getItem('user');
    // const suAdmin = localStorage.getItem('admin');
    // loadReCaptcha();

    const themeColor = localStorage.getItem('theme-color');
    const layout = localStorage.getItem('layout_version');
    // {
    //   admin ? setCurrentUser(true) : setCurrentUser(false);
    // }
    // {
    //   suAdmin ? setSuperUser(true) : setSuperUser(false);
    // }
    document
      .getElementById('color')
      .setAttribute(
        'href',
        `${process.env.PUBLIC_URL}/assets/css/${themeColor}.css`
      );
    document.body.classList.add(layout);

    return function cleanup() {
      abortController.abort();
    };
  }, []);

  console.log(currentUser, 'currentUser');

  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter history={hist} basename={`/`}>
          {/* <ScrollContext> */}
          <Switch>
            <Route exact path="/Otp" component={Otp} />

            <Route exact path="/">
              <Redirect to="/login" />
            </Route>
            {/* Admin Route */}
            <Route exact path="/admin-login" component={AdminSignIn} />
            <Route exact path="/login" component={Signin} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/EmptyCompoent" component={EmptyCompoent} />
            <Route exact path="/forgot-password" component={ForgotPassword} />
            <Route exact path="/reset-password" component={ResetPassword} />
            <Route exact path="/home" component={HomeComponent} />
            <Route exact path="/beneficiary" component={AddBeneficiary} />
            <Route exact path="/address-book" component={AddressBook} />
            <Route exact path="/user-setting" component={UserSetting} />
            <Route exact path="/user-profile" component={EditUserProfile} />
            <Route
              path="/:slug"
              exact
              strict
              render={(props) => {
                if (
                  /^(?=.*\d.*\d)(?=.*[A-Z].*[A-Z])(?=.*[!@#$%<>_+^&*()].*[!@#$%<>_+^&*()]).{8,15}$/.test(
                    props.match.params.slug
                  )
                ) {
                  return <AdminSignIn {...props} />;
                } else {
                  return (
                    <Redirect
                      to={{
                        pathname: '/login',
                        state: { from: props.location },
                      }}
                      {...props}
                    />
                  );
                }
              }}
            />
            <Route
              exact
              path="/dashboard/all-transaction"
              component={AllTransactions}
            />
            <Route exact path="/admin/out-pending" component={OutPending} />
            {
              //  currentUser?
              <Fragment>
                <App>
                  <Route
                    exact
                    path="/dashboard/my-account"
                    component={MyAccount}
                  />
                  <Route
                    exact
                    path="/dashboard/all-transaction"
                    component={AllTransactions}
                  />
                  <Route
                    exact
                    path="/dashboard/transfer-form"
                    component={TransferForm}
                  />
                  <Route
                    exact
                    path="/dashboard/currency-exchange"
                    component={userCurrencyExch}
                  />
                  <Route
                    exact
                    path="/dashboard/user-information"
                    component={UserInformations}
                  />
                  <Route
                    exact
                    path="/dashboard/user-setting"
                    component={UserSettings}
                  />

                  <Route
                    exact
                    path="/dashboard/user-contacts"
                    component={UserContacts}
                  />
                  <Route
                    exact
                    path="/dashboard/usermt-message"
                    component={UserMtMessage}
                  />
                  {/* </App> */}
                  {/* </Fragment> */}
                  {/* : */}

                  {/* superUser ? */}
                  {/* <Fragment> */}
                  {/* <App> */}
                  <Route exact path="/admin/home" component={AdminHome} />
                  <Route
                    exact
                    path="/admin/out-approved"
                    component={OutApproveRequest}
                  />
                  <Route
                    exact
                    path="/admin/out-disapproved"
                    component={OutRejectRequest}
                  />
                  <Route
                    exact
                    path="/admin/incoming-pending"
                    component={InPending}
                  />
                  <Route
                    exact
                    path="/admin/incoming-approved"
                    component={InApproved}
                  />
                  <Route
                    exact
                    path="/admin/incoming-disapproved"
                    component={InDisapproved}
                  />
                  <Route
                    exact
                    path="/admin/exchange-pending"
                    component={ExcPending}
                  />
                  <Route
                    exact
                    path="/admin/exchange-approved"
                    component={ExcApproved}
                  />
                  <Route
                    exact
                    path="/admin/exchange-disapproved"
                    component={ExcDisapproved}
                  />
                  <Route
                    exact
                    path="/admin/exchange-setExchRate"
                    component={ExcSetRate}
                  />
                  <Route
                    exact
                    path="/admin/exchange-history"
                    component={TransactionLog}
                  />

                  <Route
                    exact
                    path="/admin/clients-underCompliance"
                    component={UaPending}
                  />
                  <Route
                    exact
                    path="/admin/clients-active"
                    component={UaActive}
                  />
                  <Route
                    exact
                    path="/admin/clients-closed"
                    component={UaClosedacc}
                  />
                  <Route
                    exact
                    path="/admin/swift-outgoing"
                    component={SwiftOutgoing}
                  />
                  <Route
                    exact
                    path="/admin/swift-incoming"
                    component={SwiftIncoming}
                  />
                  <Route
                    exact
                    path="/admin/swift-internal"
                    component={SwiftInternal}
                  />
                  <Route
                    exact
                    path="/admin/sepa-outgoing"
                    component={SepaOutgoing}
                  />
                  <Route
                    exact
                    path="/admin/sepa-incoming"
                    component={SepaIncoming}
                  />
                  <Route
                    exact
                    path="/admin/sepa-internal"
                    component={SepaInternal}
                  />
                  <Route
                    exact
                    path="/admin/exchange-swift"
                    component={ExchangeSwift}
                  />

                  <Route
                    exact
                    path="/admin/useracc-approved"
                    component={UaApproved}
                  />
                  <Route
                    exact
                    path="/admin/useracc-disapproved"
                    component={UaDisapproved}
                  />
                  <Route
                    exact
                    path="/admin/internal-transfers"
                    component={InternalTransfers}
                  />
                  <Route
                    exact
                    path="/admin/commissions"
                    component={Commissions}
                  />
                  <Route
                    exact
                    path="/admin/total-balance"
                    component={TotalBalance}
                  />
                  <Route exact path="/admin/mt-message" component={MtMessage} />
                </App>
              </Fragment>
              // :
              // null
            }
          </Switch>
          {/* </ScrollContext> */}
        </BrowserRouter>
      </Provider>
    </div>
  );
}

ReactDOM.render(<Root />, document.getElementById('root'));
