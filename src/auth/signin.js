import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { OnLoginUser } from '../actions';
import ArrowBackIcon from '../assets/images/arrow_back.png';
import ArrowHeadIcon from '../assets/images/arrow_head.png';
import '../assets/scss/auth/signin.scss';
import Footer from '../components/common/footer';
import Header from '../components/common/header-component/signInHeader';
import Loader from '../components/common/loader';
import '../components/dashboard/defaultCompo/styles/ForgotPassword.css';

class Signin extends Component {
  state = {
    isDisabled: false,

    // username: 'denis.grati@list.ru',
    // password: 'denis.grati@list.ru123',
    username: '',
    password: '',
    pin: '',
    text: '',
    loading: false,
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.loginUser !== prevProps.loginUser &&
      this.props.loginUser.called
    ) {
      const { error } = this.props.loginUser || {};
      // console.log(error);
      console.log(this.props.loginUser.data);
      if (this.props.loginUser.data.QR) {
        this.setState({ loading: false });
      } else if (this.props.loginUser.data.token) {
        this.setState({ loading: false });
      } else if (error) {
        this.setState({ loading: false });
        // alert('User Account not active,please contact the bank');
        alert(this.props.loginUser.data.message);
        // this.props.history.push("EmptyCompoent");
      }
    }
  }

  handlePinChange = (event) => {
    this.setState({
      pin: event.target.value,
    });
  };

  handlePasswordChange = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  handleEmailChange = (e) => {
    this.setState({ username: e.target.value });
  };

  loginUser = (e) => {
    const { username, password, pin } = this.state;
    e.preventDefault();
    if (!username) {
      alert('Username cannot be empty');
    } else if (!password) {
      alert('Password cannot be empty');
    }
    // else if (!pin) {
    //   alert('Google Authenticator pin required');
    // }
    else {
      this.setState({ loading: true });
      this.props.OnLoginUser(this.state, this.props.history);
    }
  };

  render() {
    return (
      <Fragment>
        <Header />
        {/* <img
          src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=otpauth://totp/Secure%20App:Paras%40unboxinnovations.com?secret=PEJUKD46M62J7UOO&issuer=Secure%20App"
          alt=""
        /> */}
        {this.state.loading ? (
          <Loader />
        ) : (
          <div className="page-wrapper">
            <div className="container-fluid p-0">
              {/* <!-- login page start--> */}
              <div
                className="authentication-main"
                style={{ paddingTop: 0, paddingBottom: 0 }}
              >
                <div className="row">
                  <div className="col-md-12">
                    <div className="auth-innerright">
                      <div className="authentication-box login-page">
                        <div className="mt-4">
                          <div className="text-center">
                            {/* <div className="auth-logo"><img src={logo} alt="logo"/></div>
                             */}
                            <div
                              className="auth-logo"
                              style={{ marginTop: -22 }}
                            >
                              Welcome Back
                            </div>
                            <div
                              className="auth-logo"
                              style={{ marginTop: -20 }}
                            >
                              Sign In
                            </div>
                          </div>
                          <div className="text-center">
                            {/* <h4>LOGIN</h4>
                                                    <h6>Enter your Email and Password </h6> */}
                          </div>
                          <div className="card-body card-body-inner">
                            <div className="col-md-6 mx-auto">
                              <form
                                onSubmit={this.loginUser}
                                className="theme-form"
                              >
                                <div className="form-group">
                                  <label className="col-form-label pt-0">
                                    Email
                                  </label>
                                  <input
                                    className="form-control"
                                    type="username"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.handleEmailChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.handlePasswordChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Enter your google authenticator
                                  </label>
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="pin"
                                    value={this.state.pin}
                                    onChange={this.handlePinChange}
                                  />
                                </div>
                                <br />
                                <div className="form-group mb-0">
                                  <button
                                    disabled={this.state.isDisabled}
                                    className="btn btn-primary btn-block"
                                    type="submit"
                                    onClick={this.loginUser}
                                  >
                                    Login
                                    <img className="img1" src={ArrowBackIcon} />
                                    <img className="img2" src={ArrowHeadIcon} />
                                  </button>
                                </div>
                              </form>
                            </div>
                            <div className="col-md-12 px-4 my-4">
                              <div
                                className="form-group"
                                style={{
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                }}
                                // style={{
                                //   display: 'flex',
                                //   justifyContent: 'center',
                                // }}
                              >
                                <div className="newAuthLabel ">
                                  <a href="/signup">Open a bank account</a>
                                </div>
                                <div className="newAuthLabel">
                                  <a href="/forgot-password">
                                    Forgot Password ?
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="container-fluid">
                            <div
                              className="spsl-wrapper"
                              style={{ marginTop: 22 }}
                            >
                              <h4>Sterling Payment Services Limited</h4>
                              <p>
                                Unit D, 9/F, Neich Tower, 128 Gloucester Road,
                                Wanchai, Hong Kong
                              </p>
                              <p>
                                <a href="">https://sterlingsafepayment.com</a>|
                                <span>| info@sterlingsafepayment.com</span>
                              </p>
                              <p>
                                <span>Phone: +852 5801 4396</span> |
                                <span>BIC/SWIFT code: STPVHKHH</span>
                              </p>
                            </div>
                          </div>
                          {/* <div className="copy-right"><p>Copyright © 2020 - Fincofex. All Rights Reserved. <span>Internet banking Software provided by Fincofex Limited (UK)</span></p></div> */}
                          {/* </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ToastContainer />
              {/* <!-- login page end--> */}
            </div>
          </div>
        )}
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const { loginUser } = state;
  return { loginUser };
};

export default connect(mapStateToProps, { OnLoginUser })(Signin);
