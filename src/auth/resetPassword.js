import React, { Component, Fragment } from 'react';
import { ToastContainer, toast } from 'react-toastify';

import ArrowBackIcon from '../assets/images/arrow_back.png';
import ArrowHeadIcon from '../assets/images/arrow_head.png';

import Footer from '../components/common/footer';
import Header from '../components/common/header-component/header';

import { resetPassword } from '../actions/PasswordActions/PasswordActions';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';

import '../components/dashboard/defaultCompo/styles/ForgotPassword.css';
import '../components/dashboard/defaultCompo/styles/CustomLoader.css';

import 'react-toastify/dist/ReactToastify.css';

class ResetPassword extends Component {
  state = {
    oldPassword: '',
    newPassword: '',
    pin: '',
  };

  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });

      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }

  //   componentDidUpdate(prevProps) {
  //     if (
  //       this.props.resetPwd !== prevProps.resetPwd &&
  //       this.props.resetPwd.called
  //     ) {
  //       const { error } = this.props.resetPwd || {};
  //       // console.log(error);
  //       console.log(this.props.resetPwd.data);
  //       if (this.props.resetPwd.data.QR) {
  //         this.setState({ loading: false });
  //       } else if (this.props.resetPwd.data.token) {
  //         this.setState({ loading: false });
  //       } else if (error) {
  //         this.setState({ loading: false });
  //         // alert('User Account not active,please contact the bank');
  //         alert(this.props.resetPwd.data.message);
  //         // this.props.history.push("EmptyCompoent");
  //       }
  //     }
  //   }

  handlePinChange = (event) => {
    this.setState({
      pin: event.target.value,
    });
  };

  handleNewPasswordChange = (event) => {
    this.setState({
      newPassword: event.target.value,
    });
  };

  handleOldPasswordChange = (e) => {
    this.setState({ oldPassword: e.target.value });
  };

  resetPwd = (e) => {
    const { oldPassword, newPassword, pin } = this.state;
    const decode = jwt_decode(localStorage.getItem('access'));
    const data = {
      user_id: decode.user_id,
      pin,
      old_password: oldPassword,
      new_password: newPassword,
    };
    e.preventDefault();
    if (!oldPassword) {
      alert('Old Password cannot be empty');
    } else if (!newPassword) {
      alert('New Password cannot be empty');
    } else if (!pin) {
      alert('Google Authenticator pin required');
    } else {
      // this.setState({ loading: true });
      this.props.resetPassword(
        data,
        localStorage.getItem('access'),
        this.props.history
      );
    }
  };

  render() {
    return (
      <Fragment>
        <Header />
        {/* <img
              src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=otpauth://totp/Secure%20App:Paras%40unboxinnovations.com?secret=PEJUKD46M62J7UOO&issuer=Secure%20App"
              alt=""
            /> */}
        {this.props.isloading ? (
          <div style={{ height: '100vh' }}>
            <div className="customLoader-ring" id="spinner-id">
              <div></div>
              <div></div>
              <p style={{ marginTop: '200px' }}>Setting your password</p>
            </div>
          </div>
        ) : (
          <div className="page-wrapper">
            <div className="container-fluid p-0">
              {/* <!-- login page start--> */}
              <div
                className="authentication-main"
                style={{ paddingTop: 0, paddingBottom: 0 }}
              >
                <div className="row">
                  <div className="col-md-12">
                    <div className="auth-innerright">
                      <div className="authentication-box login-page">
                        <div className="mt-4">
                          <div className="text-center">
                            {/* <div className="auth-logo"><img src={logo} alt="logo"/></div>
                             */}
                            <div
                              className="forgot-password-title"
                              style={{ marginTop: -20 }}
                            >
                              Reset password
                            </div>
                            {/* <div
                              className="auth-logo"
                              style={{ marginTop: -20 }}
                            >
                              Sign In
                            </div> */}
                          </div>

                          <div className="card-body card-body-inner">
                            <div className="col-md-6 mx-auto">
                              <form
                                onSubmit={this.resetPwd}
                                className="theme-form"
                              >
                                <div className="form-group">
                                  <label className="col-form-label pt-0">
                                    Old Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="oldPassword"
                                    value={this.state.oldPassword}
                                    onChange={this.handleOldPasswordChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    New Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="newPassword"
                                    value={this.state.newPassword}
                                    onChange={this.handleNewPasswordChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Enter your google authenticator
                                  </label>
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="pin"
                                    value={this.state.pin}
                                    onChange={this.handlePinChange}
                                  />
                                </div>
                                <br />
                                <div className="form-group mb-0">
                                  <button
                                    disabled={this.state.isDisabled}
                                    className="btn btn-primary btn-block"
                                    type="submit"
                                    onClick={this.resetPwd}
                                  >
                                    Change Password
                                    <img className="img1" src={ArrowBackIcon} />
                                    <img className="img2" src={ArrowHeadIcon} />
                                  </button>
                                </div>
                              </form>
                            </div>
                            <div className="col-md-8 mx-auto mt-3">
                              <div className="form-group">
                                <div className="back-to-login">
                                  <a href="/home">Back</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="container-fluid">
                            <div
                              className="spsl-wrapper"
                              // className="app-info-wrapper"
                              style={{ marginTop: 22 }}
                            >
                              <h4>Sterling Payment Services Limited</h4>
                              <p>
                                Unit D, 9/F, Neich Tower, 128 Gloucester Road,
                                Wanchai, Hong Kong
                              </p>
                              <p>
                                <a href="">https://sterlingsafepayment.com</a>

                                <span>| info@sterlingsafepayment.com</span>
                              </p>
                              <p>
                                <span>Phone: +852 5801 4396</span> |
                                <span>BIC/SWIFT code: STPVHKHH</span>
                              </p>
                            </div>
                          </div>
                          {/* <div className="copy-right"><p>Copyright © 2020 - Fincofex. All Rights Reserved. <span>Internet banking Software provided by Fincofex Limited (UK)</span></p></div> */}
                          {/* </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ToastContainer />
              {/* <!-- login page end--> */}
            </div>
          </div>
        )}
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  resetPassword: state.passwordReducer.resetPassword,
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToProps, {
  resetPassword,
})(ResetPassword);
