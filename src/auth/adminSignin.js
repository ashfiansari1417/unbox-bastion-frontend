import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import '../assets/scss/auth/signin.scss';
import { LOGIN_APP } from '../constant/actionTypes';
import ArrowHeadIcon from '../assets/images/arrow_head.png';
import ArrowBackIcon from '../assets/images/arrow_back.png';
import Header from '../components/common/header-component/signInHeader';
import Footer from '../components/common/footer';
import logo from '../assets/images/sterling-logo.46d43575.png';
import Loader from '../components/common/loader';
import { connect } from 'react-redux';
import { OnLoginAdmin } from '../actions';

import 'react-toastify/dist/ReactToastify.css';

class AdminSignin extends Component {
  state = {
    isDisabled: true,
    username: '',
    password: '',
    // username: 'abhishek2funds@gmail.com',
    // password: 'test@123',
    isLoading: false,
  };

  handlePasswordChange = (event) => {
    this.setState(
      {
        password: event.target.value,
      },
      (e) => this.handleError()
    );
  };

  handleEmailChange = (e) => {
    this.setState({ username: e.target.value }, (e) => this.handleError());
  };
  handleTextChange = (e) => {
    this.setState({ text: e.target.value }, (e) => this.handleError());
  };

  handleError = () => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{1,4})$/;
    if (
      this.state.password &&
      this.state.password.length >= 5 &&
      reg.test(this.state.email)
    ) {
      this.setState({ isDisabled: false });
    } else {
      this.setState({ isDisabled: true });
    }
  };

  loginUser = (e) => {
    e.preventDefault();
    this.setState({ isLoading: true });
    this.props.OnLoginAdmin(this.state, this.props.history);
  };
  componentDidUpdate(prevProps) {
    if (
      this.props.loginUser !== prevProps.loginUser &&
      this.props.loginUser.called
    ) {
      const { error } = this.props.loginUser.adminData || {};
      // console.log(error);
      console.log(this.props.loginUser.adminData);
      if (this.props.loginUser.adminData.QR) {
        this.setState({ isLoading: false });
      } else if (this.props.loginUser.adminData.token) {
        this.setState({ isLoading: false });
      } else if (error) {
        this.setState({ isLoading: false });
        // alert('User Account not active,please contact the bank');
        alert('Please try again');
        // this.props.history.push("EmptyCompoent");
      }
    }
  }

  render() {
    return (
      <Fragment>
        <Header />
        {this.state.isLoading ? (
          <Loader />
        ) : (
          <div className="page-wrapper">
            <div className="container-fluid p-0">
              {/* <!-- login page start--> */}
              <div className="authentication-main">
                <div className="row">
                  <div className="col-md-12">
                    <div className="auth-innerright">
                      <div className="authentication-box login-page">
                        <div className="mt-4">
                          <div className="text-center">
                            {/* <div className="auth-logo"><img src={logo} alt="logo"/></div>
                             */}
                            <div
                              className="auth-logo"
                              style={{ marginTop: -30 }}
                            >
                              Admin Signin
                            </div>
                          </div>
                          <div className="text-center">
                            {/* <h4>LOGIN</h4>
                                                    <h6>Enter your Email and Password </h6> */}
                          </div>
                          <div className="card-body card-body-inner mt-4  py-4">
                            <div className="col-md-6 mx-auto">
                              <form className="theme-form">
                                <div className="form-group">
                                  <label className="col-form-label pt-0">
                                    Username
                                  </label>
                                  <input
                                    className="form-control"
                                    type="email"
                                    name="email"
                                    value={this.state.username}
                                    onChange={this.handleEmailChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.handlePasswordChange}
                                  />
                                </div>
                                <br />
                                <div className="form-group mb-0">
                                  <button
                                    className="btn btn-primary btn-block"
                                    type="button"
                                    onClick={this.loginUser}
                                  >
                                    Login
                                    <img className="img1" src={ArrowBackIcon} />
                                    <img className="img2" src={ArrowHeadIcon} />
                                  </button>
                                </div>
                              </form>
                            </div>
                            {/* <div className="col-md-12 mx-auto mt-5">
                              <div className="form-group">
                                <div className="newAuthLabel">
                                <a href="/signup">Open a bank account</a>
                                </div>
                                <div className="newAuthLabel text-right mt-2">
                                  <a href="/forgot-password">
                                    Forgot Password ?
                                  </a>
                                </div>
                              </div>
                            </div> */}
                          </div>
                          <div className="container-fluid mt-4">
                            <div className="row ">
                              <div className="col-md-12">
                                <div
                                  className="spsl-wrapper"
                                  style={{ marginTop: 20 }}
                                >
                                  <h4>Sterling Payment Services Limited</h4>
                                  <p>
                                    6/F, Room A13, Overseas Trust Bank Building,
                                    160 Gloucester Road, Wanchai, Hong Kong
                                  </p>
                                  <p>
                                    <a href="">
                                      {' '}
                                      https://sterlingsafepayment.com
                                    </a>{' '}
                                    | <span>info@sterlingsafepayment.com</span>
                                  </p>
                                  <p>
                                    <span>Phone: +852 5801 4396</span> |{' '}
                                    <span>BIC/SWIFT code: STPVHKHH</span>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <div className="copy-right"><p>Copyright © 2020 - Fincofex. All Rights Reserved. <span>Internet banking Software provided by Fincofex Limited (UK)</span></p></div> */}
                          {/* </div> */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ToastContainer />
              {/* <!-- login page end--> */}
            </div>
          </div>
        )}
        <Footer />
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  const { loginUser } = state;
  return { loginUser };
};

export default connect(mapStateToProps, { OnLoginAdmin })(AdminSignin);
