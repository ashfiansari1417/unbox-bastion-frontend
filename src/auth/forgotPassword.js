import React, { Component, Fragment } from 'react';
import { ToastContainer, toast } from 'react-toastify';

import { forgotPassword } from '../actions/PasswordActions/PasswordActions';
import ArrowBackIcon from '../assets/images/arrow_back.png';
import ArrowHeadIcon from '../assets/images/arrow_head.png';

import Footer from '../components/common/footer';
import Header from '../components/common/header-component/signInHeader';
import Loader from '../components/common/loader';
import BackdropLoader from '../components/dashboard/defaultCompo/reusableComponents/LoaderBackdrop';
import { connect } from 'react-redux';

import '../components/dashboard/defaultCompo/styles/ForgotPassword.css';

import 'react-toastify/dist/ReactToastify.css';

class ForgotPassword extends Component {
  state = {
    isDisabled: false,
    email: '',

    newPassword: '',
    confirmPassword: '',
    pin: '',
    text: '',
  };

  handlePinChange = (event) => {
    this.setState({
      pin: event.target.value,
    });
  };

  handleNewPasswordChange = (event) => {
    this.setState({
      newPassword: event.target.value,
    });
  };

  handleEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };
  handleConfirmPasswordChange = (e) => {
    this.setState({ confirmPassword: e.target.value });
  };

  loginUser = (e) => {
    const { email, newPassword, confirmPassword, pin } = this.state;
    e.preventDefault();
    if (!email) {
      alert('Email cannot be empty');
    } else if (!newPassword) {
      alert('New Password cannot be empty');
    } else if (!confirmPassword) {
      alert('Confirm Password cannot be empty');
    } else if (!pin) {
      alert('Google Authenticator pin required');
    } else if (confirmPassword !== newPassword) {
      alert('Password does not match ,Please confirm your password');
    } else {
      this.setState({ loading: true });
      this.props.forgotPassword(
        { pin, new_password: newPassword, username: email },
        this.props.history
      );
    }
  };

  render() {
    return (
      <Fragment>
        <Header />

        {this.props.isloading ? (
          <BackdropLoader message="Setting your new password.Please wait.." />
        ) : (
          <div className="page-wrapper">
            <div className="container-fluid p-0">
              {/* <!-- login page start--> */}
              <div
                className="authentication-main"
                style={{ paddingTop: 0, paddingBottom: 0 }}
              >
                <div className="row">
                  <div className="col-md-12">
                    <div className="auth-innerright">
                      <div className="authentication-box login-page">
                        <div className="mt-4">
                          <div className="text-center">
                            <div
                              className="forgot-password-title"
                              style={{ marginTop: -20 }}
                            >
                              Forgot password
                            </div>
                          </div>

                          <div className="card-body card-body-inner">
                            <div className="col-md-6 mx-auto">
                              <form
                                onSubmit={this.loginUser}
                                className="theme-form"
                              >
                                <div className="form-group">
                                  <label className="col-form-label pt-0">
                                    Email
                                  </label>
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="email"
                                    value={this.state.email}
                                    onChange={this.handleEmailChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    New Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="newPassword"
                                    value={this.state.newPassword}
                                    onChange={this.handleNewPasswordChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Confirm Password
                                  </label>
                                  <input
                                    className="form-control"
                                    type="password"
                                    name="confirmPassword"
                                    value={this.state.confirmPassword}
                                    onChange={this.handleConfirmPasswordChange}
                                  />
                                </div>
                                <div className="form-group">
                                  <label className="col-form-label">
                                    Google Authenticator Pin
                                  </label>
                                  <input
                                    className="form-control"
                                    type="text"
                                    name="pin"
                                    value={this.state.pin}
                                    onChange={this.handlePinChange}
                                  />
                                </div>

                                <br />
                                <div className="form-group mb-0">
                                  <button
                                    disabled={this.state.isDisabled}
                                    className="btn btn-primary btn-block"
                                    type="submit"
                                    onClick={this.loginUser}
                                  >
                                    Change Password
                                    <img className="img1" src={ArrowBackIcon} />
                                    <img className="img2" src={ArrowHeadIcon} />
                                  </button>
                                </div>
                              </form>
                            </div>
                            <div className="col-md-8 mx-auto mt-3">
                              <div className="form-group">
                                <div className="back-to-login">
                                  <a href="/login">Back to login</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="container-fluid">
                            <div
                              className="spsl-wrapper"
                              // className="app-info-wrapper"
                              style={{ marginTop: 22, marginBottom: 36 }}
                            >
                              <h4>Sterling Payment Services Limited</h4>
                              <p>
                                Unit D, 9/F, Neich Tower, 128 Gloucester Road,
                                Wanchai, Hong Kong
                              </p>
                              <p>
                                <a href="">https://sterlingsafepayment.com</a>

                                <span>| info@sterlingsafepayment.com</span>
                              </p>
                              <p>
                                <span>Phone: +852 5801 4396</span> |
                                <span>BIC/SWIFT code: STPVHKHH</span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <ToastContainer />
            </div>
          </div>
        )}
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToProps, {
  forgotPassword,
})(ForgotPassword);
