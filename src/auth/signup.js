import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { CountryDropdown } from 'react-country-region-selector';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ToastContainer, toast } from 'react-toastify';
import { SIGN_UP_APP  } from '../constant/actionTypes';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import logo from '../assets/images/sterling-logo.46d43575.png';

class SignUp extends Component {
    state = { 
    currentTab:"corporate",
    docType: "passport",
    showSuggestion: true
    }
    televalue

    changeTab = (tab,e) =>{
        this.setState({currentTab:tab})
        this.setState({
            companyName : "",
            beneficiaryPassword : "",
            comapnyStreetNumber : "",
            companyStreetName : "",
            companyCity : "",
            CompanyState : "",
            companyCountry : "",
            comapnyPostCode : "",
            companyNameRegistrar : "",
            companyRegistrationNumber : "",
            companyRegistrationDate : "", 
            fName : "",
            mName : "",
            lName : "",
            passportNumber : "",
            passportcountry : "",
            countryCode : "",
            phone : "",
            beneficiaryEmail : "",
            beneficiaryCitizenship : "",
            beneficiaryResidenceAddress : "",
            beneficiaryTitle : "",
            typeOfBusiness : "",
            licenseRequirement : "",
            website : "",
            residentBusinessName : "",
            residentBusinessAddress : "",
            residentBusinessType : "",
            residentBusinessQuantity : "",
            residentBusinessDirectorName : "",
            residentBusinessShareholderName : "",
            EURCreditTotal : "",
            EURDebitTotal : "",
            EURNetBalence : "",
            EURAvgAmount : "",
            EURMonthlyVolume : "",
            USDCreditTotal : "",
            USDDebitTotal : "",
            USDNetBalence : "",
            USDAvgAmount : "",
            USDMonthlyVolume : "",
            GBPCreditTotal : "",
            GBPDebitTotal : "",
            GBPNetBalence : "",
            GBPAvgAmount : "",
            GBPMonthlyVolume : "",
            RUBCreditTotal : "",
            RUBDebitTotal : "",
            RUBNetBalence : "",
            RUBAvgAmount : "",
            RUBMonthlyVolume : "",
            partnerName : "",
            partnerCountry : "",
            partnerEstimatedTurnover : "",
            partnerCurrency : "",
            partnerPurposePayment : "",
        })    
    }

     handleInputChange =(telNumber, selectedCountry)=> {
        console.log('input changed. number: ', telNumber, 'selected country: ', selectedCountry)
    }
       
    handleInputBlur = (telNumber, selectedCountry)=> {
        console.log(
          'Focus off the ReactTelephoneInput component. Tel number entered is: ',
          telNumber,
          ' selected country is: ',
          selectedCountry
        )
      }

      handleChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    selectCompanyCountry (val) {
        this.setState({ companyCountry: val });
    }

    selectPartnerCountry (val) {
        this.setState({ partnerCountry: val });
    }

    selectPassportCountry (val) {
        this.setState({ passportcountry: val });
    }

    handleDateChange = date => {
        this.setState({companyRegistrationDate: date});
    }

    onChangeValue = (value) =>{   
        let country_code = value.split(" ")     
        let  number =  value.replace(country_code[0],'');
        this.setState({countryCode:country_code[0]})     
        this.setState({ phone:number })
    }

    handlePassword = e => {
        var reg = /^(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%<>_+^&*()]).{6,}$/;
        if (reg.test(e.target.value)) {
          this.setState({ beneficiaryPassword: e.target.value, showSuggestion: false });
        } else {
            this.setState({ showSuggestion : true, beneficiaryPassword: "" })
        } 
    };
    //  value = { iso2: 'cn', dialCode: '86', phone: '12345678901' };
 
    //  onChange = value => console.log(value);
    //  onReady = (instance, IntlTelInput) => console.log(instance, IntlTelInput)
    // inputProps = {
    //     placeholder: 'ReactIntlTelInput',
    //   };
     
    //  intlTelOpts = {
    //     preferredCountries: ['cn'],
    //   };
        createAccount = () =>{
        const { history } = this.props;
        axios.post( SIGN_UP_APP, {
            "company_type":this.state.currentTab,
            "company_name" : this.state.companyName,
            "password_hash" : this.state.beneficiaryPassword,
            "street_no":this.state.comapnyStreetNumber,
             "street_name":this.state.companyStreetName,
             "city":this.state.companyCity,
             "state":this.state.CompanyState,
             "company_country":this.state.companyCountry,
             "post_code":this.state.comapnyPostCode,
             "company_reg_name":this.state.companyNameRegistrar,
             "company_reg_no":this.state.companyRegistrationNumber,
             "company_reg_date":this.state.companyRegistrationDate, 
             "first_name":this.state.fName,
             "middle_name":this.state.mName,
             "last_name":this.state.lName,
             "id_document_type":this.state.docType,
             "id_document_num":this.state.passportNumber,
              "doc_country":this.state.passportcountry,
              "country_code":this.state.countryCode,
              "phone_number": this.state.phone,
              "email":this.state.beneficiaryEmail,
              "citizenship":this.state.beneficiaryCitizenship,
              "residential_address":this.state.beneficiaryResidenceAddress,
              "company_title":this.state.beneficiaryTitle,
              "type_of_business":this.state.typeOfBusiness,
              "licence_required":this.state.licenseRequirement,
              "website":this.state.website,
              "resident_business_company_name":this.state.residentBusinessName,
              "resident_business_company_address":this.state.residentBusinessAddress,
              "resident_business_type":this.state.residentBusinessType,
              "resident_business_company_employee_count":this.state.residentBusinessQuantity,
              "resident_business_company_director_full_name":this.state.residentBusinessDirectorName,
              "resident_business_company_shareholder_full_name":this.state.residentBusinessShareholderName,
              "estimate_credit_euro_monthly":this.state.EURCreditTotal,
              "estimate_debit_euro_monthly":this.state.EURDebitTotal,
              "estimate_net_balance_euro":this.state.EURNetBalence,
              "avg_amount_per_month_transaction_euro":this.state.EURAvgAmount,
              "monthly_volume_transaction_euro":this.state.EURMonthlyVolume,
              "estimate_credit_usd_monthly":this.state.USDCreditTotal,
              "estimate_debit_usd_monthly":this.state.USDDebitTotal,
              "estimate_net_balance_usd":this.state.USDNetBalence,
              "avg_amount_per_month_transaction_usd":this.state.USDAvgAmount,
              "monthly_volume_transaction_usd":this.state.USDMonthlyVolume,
              "estimate_credit_gbp_monthly":this.state.GBPCreditTotal,
              "estimate_debit_gbp_monthly":this.state.GBPDebitTotal,
              "estimate_net_balance_gbp":this.state.GBPNetBalence,
              "avg_amount_per_month_transaction_gbp":this.state.GBPAvgAmount,
              "monthly_volume_transaction_gbp":this.state.GBPMonthlyVolume,
              "estimate_credit_rub_monthly":this.state.RUBCreditTotal,
              "estimate_debit_rub_monthly":this.state.RUBDebitTotal,
              "estimate_net_balance_rub":this.state.RUBNetBalence,
              "avg_amount_per_month_transaction_rub":this.state.RUBAvgAmount,
              "monthly_volume_transaction_rub":this.state.RUBMonthlyVolume,
              "partner_name":this.state.partnerName,
              "partner_country":this.state.partnerCountry,
              "partner_turnover_monthly":this.state.partnerEstimatedTurnover,
              "partner_currency":this.state.partnerCurrency,
              "parter_payment_purpose":this.state.partnerPurposePayment,
              "status":0,
          })
          .then(function (response) {
            if (response.data.result.id !== null) {
                setTimeout(function() {
                    history.push('/login');
                }, 1500);
            } else {
                toast.error("Invalid email or password");
            }
          })
          .catch(function (error) {
              toast.error("Invalid email or password");
          })
    };

         createAccountForIndividual = () =>{
            const { history } = this.props;
        axios.post( SIGN_UP_APP, {
            //    username 
            "company_type":this.state.currentTab,
            "company_name" : this.state.companyName,
            "password_hash" : this.state.beneficiaryPassword,

            "street_no":this.state.comapnyStreetNumber,
             "street_name":this.state.companyStreetName,

             "city":this.state.companyCity,
             "state":this.state.CompanyState,
             "company_country":this.state.companyCountry,

             "post_code":this.state.comapnyPostCode,
             "company_reg_name":this.state.companyNameRegistrar,
             "company_reg_no":this.state.companyRegistrationNumber,
             "company_reg_date":this.state.companyRegistrationDate, 
             "first_name":this.state.fName,
             "middle_name":this.state.mName,
             "last_name":this.state.lName,
             "id_document_type":this.state.docType,
             "id_document_num":this.state.passportNumber,
              "doc_country":this.state.passportcountry,
            //   "country_code":this.state.,
            //   "phone_number":this.state.,
              "country_code":this.state.countryCode,
              "phone_number": this.state.phone,
              "email":this.state.beneficiaryEmail,
              "citizenship":this.state.beneficiaryCitizenship,
    // citizenship_two
              "residential_address":this.state.beneficiaryResidenceAddress,
              "company_title":this.state.beneficiaryTitle,
              "type_of_business":this.state.typeOfBusiness,
              "licence_required":this.state.licenseRequirement,
    
              "website":this.state.website,
              "resident_business_company_name":this.state.residentBusinessName,
              "resident_business_company_address":this.state.residentBusinessAddress,
              "resident_business_type":this.state.residentBusinessType,
              "resident_business_company_employee_count":this.state.residentBusinessQuantity,
              "resident_business_company_director_full_name":this.state.residentBusinessDirectorName,
              "resident_business_company_shareholder_full_name":this.state.residentBusinessShareholderName,
              "estimate_credit_euro_monthly":this.state.EURCreditTotal,

              "estimate_debit_euro_monthly":this.state.EURDebitTotal,
              "estimate_net_balance_euro":this.state.EURNetBalence,
              "avg_amount_per_month_transaction_euro":this.state.EURAvgAmount,
              "monthly_volume_transaction_euro":this.state.EURMonthlyVolume,
              "estimate_credit_usd_monthly":this.state.USDCreditTotal,
              "estimate_debit_usd_monthly":this.state.USDDebitTotal,
              "estimate_net_balance_usd":this.state.USDNetBalence,
              "avg_amount_per_month_transaction_usd":this.state.USDAvgAmount,
              "monthly_volume_transaction_usd":this.state.USDMonthlyVolume,
              "estimate_credit_gbp_monthly":this.state.GBPCreditTotal,
              "estimate_debit_gbp_monthly":this.state.GBPDebitTotal,
              "estimate_net_balance_gbp":this.state.GBPNetBalence,
              "avg_amount_per_month_transaction_gbp":this.state.GBPAvgAmount,
              "monthly_volume_transaction_gbp":this.state.GBPMonthlyVolume,
              "estimate_credit_rub_monthly":this.state.RUBCreditTotal,
              "estimate_debit_rub_monthly":this.state.RUBDebitTotal,
              "estimate_net_balance_rub":this.state.RUBNetBalence,
              "avg_amount_per_month_transaction_rub":this.state.RUBAvgAmount,
              "monthly_volume_transaction_rub":this.state.RUBMonthlyVolume,
              "partner_name":this.state.partnerName,
              "partner_country":this.state.partnerCountry,
              "partner_turnover_monthly":this.state.partnerEstimatedTurnover,
              "partner_currency":this.state.partnerCurrency,
              "parter_payment_purpose":this.state.partnerPurposePayment,
              "status":0,
          })
          .then(function (response) {
            if (response.data.result.id !== null) {
                setTimeout(function() {
                    history.push('/login');
                }, 1500);
            } else {
                toast.error("Invalid email or password");
            }
          })
          .catch(function (error) {
              toast.error("Invalid email or password");
          })
    };

    renderCorporateForm =  () => {       
        return(
            <Fragment>
                 <label className="font-weight-bold" htmlFor="exampleFormControlInput1">Company Information</label>
            <div className="form-group">
            <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Legal Name of your Company</span></span></span>
                <input required className="form-control"  type="text" name="companyName" placeholder ="Company Name" onChange={this.handleChange}/>
            </div> 
            <div className="form-group">
            <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Registered address of your company</span></span></span>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput1">Company Address</label>           
            </div>                    
            <div className="form-row">
            <div className="form-group col-md-6">
                <input required className="form-control" placeholder="Street Number" type="text" name="comapnyStreetNumber" onChange={this.handleChange}/>
            </div>
            <div className="form-group col-md-6">
                <input required className="form-control" placeholder="Street Name" id="exampleFormControlInput3" type="text" name="companyStreetName" onChange={this.handleChange}/>
            </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="City" type="text" name="companyCity" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="State" id="exampleFormControlInput5" type="text" name="CompanyState" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md-6">
                    <CountryDropdown className="form-control"  name="companyCountry" value={this.state.companyCountry} onChange={(val) => this.selectCompanyCountry(val)} />
                </div>
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="Post Code" type="text" name="comapnyPostCode" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">
            <div className="form-group col-md-6">
            <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Name of company registration authority</span></span></span>
                <input className="form-control" placeholder="Company Name Registrar" type="text" name="companyNameRegistrar" onChange={this.handleChange}/>
            </div>
            <div className="form-group col-md-6">
            <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Number of your company according to the registration document</span></span></span>
                <input className="form-control" placeholder="Company Registration Number" type="text" name="companyRegistrationNumber" onChange={this.handleChange}/>
            </div>
            </div>
            <div className="form-group">
                {/* <label>Company Registration Date</label>
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Date of registration according to the registration document</span></span></span> */}
                <DatePicker className="form-control" placeholderText="Company Registration Date" selected={this.state.companyRegistrationDate} onChange={this.handleDateChange}/>
            </div>
            </Fragment>
        )
    }
    
    renderIndividualForm = () =>{
   return(
    <Fragment>
        <div className="form-group">
        <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Beneficiary is an individual with significant control in the company for example Director/Shareholder</span></span></span>
        <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                Beneficiary information             
            </label>
        </div>
          
            <div className="form-row">
                    <div className="form-group col-md-4">
                    <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Legal first name as shown in a legal document ex. passport</span></span></span>
                        <input className="form-control" placeholder="First Name" type="text" name="fName" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-4">
                    <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Legal last name as shown in a legal document ex. passport</span></span></span>
                        <input className="form-control" placeholder="Middle Name" type="text" name="mName" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-4">
                    <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Optional. Legal middle name as shown in a legal document ex. passport</span></span></span>
                        <input className="form-control" placeholder="Last Name" type="text" name="lName" onChange={this.handleChange}/>
                    </div>                 
                </div>
                <div className="form-row">
                <div className="form-group col-md-12">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Type of identity verification document</span></span></span>
                    <select defaultValue={"passport"} className="selectpicker show-tick form-control"  id="id32" name="docType" onChange={this.handleChange}>
                        <option value="passport">Passport</option>
                        <option value="local">Local ID</option>                  
                    </select>
                </div>
                </div>
                { this.state.docType === "passport" && <Fragment>
                <label htmlFor="exampleFormControlInput7">Select Passport Country</label>
                <div className="form-row">
                <div className="form-group col-md-6">
                    <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Country of passport issuance</span></span></span>
                        <CountryDropdown className="form-control" placeholder="Select Country" name="passportCountry" value={this.state.passportcountry} onChange={(val) => this.selectPassportCountry(val)} />
                </div>
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="Passport Number" type="text" name="passportNumber" onChange={this.handleChange}/>
                </div>
                </div></Fragment> }

                { this.state.docType === "local" && <Fragment>
                <label htmlFor="exampleFormControlInput7">Select Local ID country</label>
                <div className="form-row">
                <div className="form-group col-md-6">
                    <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Country of local ID issuance</span></span></span>
                        <CountryDropdown className="form-control" placeholder="Select Country" name="passportCountry" value={this.state.passportcountry} onChange={(val) => this.selectPassportCountry(val)} />
                </div>
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="Local ID Number" type="text" name="passportNumber" onChange={this.handleChange}/>
                </div>
                </div></Fragment>}

                <div className="form-row">
                <div className="form-group col-md-6">                         
                 
                    <PhoneInput
                    country={'us'}
                    value={this.state.phone}
                    onChange={phone => 
                        this.onChangeValue(phone) 
                    }
                    />  

                 </div>
                 <div className="form-group  col-md-6">                  
                    <input className="form-control"  placeholder="Citizenship" type="text" name="beneficiaryCitizenship" onChange={this.handleChange}/>
                </div>
                 </div>
                 <div className="form-row">
                 <div className="form-group col-md-6"> 
                    <input className="form-control" placeholder="E-mail" type="email" name="beneficiaryEmail" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                    <input required className="form-control" placeholder="Password" type="password" name="beneficiaryPassword" onChange={this.handlePassword}/>
                { this.state.showSuggestion === true && <small id="emailHelp" className="form-text text-danger">( one digit, one capital letter, 1 special character and min lenght is 6)</small> }
                </div>
                 </div>
                 <div className="form-row">
                 <div className="form-group col-md-6">
                 <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Full residence address including postal code</span></span></span>
                    <input className="form-control" placeholder="Residence Address" type="email" name="beneficiaryResidenceAddress" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                    <select defaultValue={"1"}className="selectpicker show-tick form-control"  id="id33" name="beneficiaryTitle" onChange={this.handleChange} >
                        <option disabled value="1"> -- Title in company -- </option>
                        <option value="beneficiary">Beneficiary</option>
                        <option value="director">Director</option>
                        <option value="shareholder">Shareholder</option>                  
                    </select>
                </div>
                 </div>
                 <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                Description of commercial activities
                </label>
                <Fragment>
                <div className="form-row">            
                <div className="form-group col-md-6">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Precise nature of business activity ex. “Legal services”, “Trading” etc. </span></span></span>
                    <input className="form-control" placeholder="Type of business" type="text" name="typeOfBusiness" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>If your business activity is subject to licensing </span></span></span>
                    <select defaultValue={"1"}className="selectpicker show-tick form-control"  id="id34" name="licenseRequirement" onChange={this.handleChange}>
                        <option disabled value="1"> -- select licence requirement -- </option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                </div>
                <div className="form-row">            
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="Website" type="text" name="website" onChange={this.handleChange}/>
                </div>
                </div>
            </Fragment>
            <div className="form-group">
            <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Company with real office and employees in any country related to the Beneficiary</span></span></span>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
            Existence of a resident business:             
            </label>
            </div>
            
            <div className="form-row">            
                <div className="form-group col-md-6">      
                    <input className="form-control" id="exampleFormControlInput181" type="text" placeholder="company name" name="residentBusinessName" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">      
                    <input className="form-control" id="exampleFormControlInput182" type="text" placeholder="company address" name="residentBusinessAddress" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
                <div className="form-group col-md-6">
                    <input className="form-control" placeholder="Type of business" type="text" name="residentBusinessType" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                    <input className="form-control"  placeholder="Quantity of employees" type="text" name="residentBusinessQuantity" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
                <div className="form-group col-md-6">                   
                    <input className="form-control" id="exampleFormControlInput183" placeholder="Director full name" type="text" name="residentBusinessDirectorName" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">                 
                    <input className="form-control" id="exampleFormControlInput184" placeholder="shareholder full name" type="text" name="residentBusinessShareholderName" onChange={this.handleChange}/>
                </div>
            </div>
            <Fragment>
                <div className="form-group">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Incoming/outgoing volume, quantity of payments account balance per Month</span></span></span>
                <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
               Turnover information
                </label> 
                </div>
                  
            <Fragment>
            <label style={{"display":"block"}} className="font-weight-bold" htmlFor="exampleFormControlInput12">
                EUR account 
            </label>
            <div className="form-row">            
               <div className="form-group col-md-6">
               <label style={{"display":"block"}} className="font-weight-bold" htmlFor="exampleFormControlInput12">
                EUR account 
            </label> 
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of incoming payments within a month</span></span></span>                  
                    <input className="form-control" id="exampleFormControlInput185" placeholder="Estimated Credit Total Monthly" type="text" name="EURCreditTotal" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">  
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of outgoing payments within a month</span></span></span>               
                    <input className="form-control" id="exampleFormControlInput186" placeholder="Estimated Debit Total Monthly" type="text" name="EURDebitTotal" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Net Balance by the end of the month</span></span></span>                   
                    <input className="form-control" id="exampleFormControlInput187" placeholder="Estimated Net Balance" type="text" name="EURNetBalence" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">                 
                    <input className="form-control" id="exampleFormControlInput188" placeholder="Average Amount Per Transaction" type="text" name="EURAvgAmount" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">                   
                    <input className="form-control" id="exampleFormControlInput189" placeholder="Estimated Monthly volume of transactions" type="text" name="EURMonthlyVolume" onChange={this.handleChange}/>
                </div>
            </div>
            </Fragment>
            </Fragment>
            
            <Fragment>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
            USD account
            </label>  
                <div className="form-row">   
                         
               <div className="form-group col-md-6"> 
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of incoming payments within a month</span></span></span>                  
                    <input className="form-control" id="exampleFormControlInput1811" placeholder="Estimated Credit Total Monthly" type="text" name="USDCreditTotal" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6"> 
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of outgoing payments within a month</span></span></span>                
                    <input className="form-control" id="exampleFormControlInput1812" placeholder="Estimated Debit Total Monthly" type="text" name="USDDebitTotal" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">    
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Net Balance by the end of the month</span></span></span>               
                    <input className="form-control" id="exampleFormControlInput1813" placeholder="Estimated Net Balance" type="text" name="USDNetBalence" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">    
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average amount ex. 10 000 or 100 000</span></span></span>             
                    <input className="form-control" id="exampleFormControlInput1814" placeholder="Average Amount Per Transaction" type="text" name="USDAvgAmount" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6"> 
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Quantity of transactions ex. 10 or 100</span></span></span>                  
                    <input className="form-control" id="exampleFormControlInput1815" placeholder="Estimated Monthly volume of transactions" type="text" name="USDMonthlyVolume" onChange={this.handleChange}/>
                </div>
            </div>
            </Fragment>
            <Fragment>
             <div className="form-group">
             <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Note: Optional, Collapsable, if collapsed then all field are *</span></span></span>
             <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
            GBP account     </label>     
             </div>   
           
                <div className="form-row">            
               <div className="form-group col-md-6"> 
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of incoming payments within a month</span></span></span>                  
                    <input className="form-control" id="exampleFormControlInput1816" placeholder="Estimated Credit Total Monthly" type="text" name="GBPCreditTotal" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">    
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of outgoing payments within a month</span></span></span>             
                    <input className="form-control" id="exampleFormControlInput1817" placeholder="Estimated Debit Total Monthly" type="text" name="GBPDebitTotal" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">   
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Net Balance by the end of the month</span></span></span>                
                    <input className="form-control" id="exampleFormControlInput1818" placeholder="Estimated Net Balance" type="text" name="GBPNetBalence" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">   
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average amount ex. 10 000 or 100 000</span></span></span>              
                    <input className="form-control" id="exampleFormControlInput1819" placeholder="Average Amount Per Transaction" type="text" name="GBPAvgAmount" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">   
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Quantity of transactions ex. 10 or 100</span></span></span>                
                    <input className="form-control" id="exampleFormControlInput1820" placeholder="Estimated Monthly volume of transactions" type="text" name="GBPMonthlyVolume" onChange={this.handleChange}/>
                </div>
            </div>
            </Fragment>
            <Fragment>
                <div className="form-group">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Counterparties and counteragents you work with</span></span></span>
                <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                RUB account </label>
                </div>
             
                <div className="form-row">            
               <div className="form-group col-md-6">  
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of incoming payments within a month</span></span></span>                 
                    <input className="form-control" id="exampleFormControlInput1821" placeholder="Estimated Credit Total Monthly" type="text" name="RUBCreditTotal" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">     
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average total volume of outgoing payments within a month</span></span></span>            
                    <input className="form-control" id="exampleFormControlInput1822" placeholder="Estimated Debit Total Monthly" type="text" name="RUBDebitTotal" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">  
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Net Balance by the end of the month</span></span></span>                 
                    <input className="form-control" id="exampleFormControlInput1823" placeholder="Estimated Net Balance" type="text" name="RUBNetBalence" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6"> 
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average amount ex. 10 000 or 100 000</span></span></span>                
                    <input className="form-control" id="exampleFormControlInput1824" placeholder="Average Amount Per Transaction" type="text" name="RUBAvgAmount" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Quantity of transactions ex. 10 or 100</span></span></span>                   
                    <input className="form-control" id="exampleFormControlInput1825" placeholder="Estimated Monthly volume of transactions" type="text" name="RUBMonthlyVolume" onChange={this.handleChange}/>
                </div>
            </div>
            </Fragment>
            <Fragment>
                <div  className="form-group">
                <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Counterparties and counteragents you work with</span></span></span>
                <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                 Principal partners information:</label>  
                </div>
            
                <div className="form-row">            
               <div className="form-group col-md-6">  
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Counterparty Company name</span></span></span>                 
                    <input className="form-control" id="exampleFormControlInput1826" placeholder="Partner Name" type="text" name="partnerName" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6"> 
                    <CountryDropdown className="form-control" name="partnerCountry" value={this.state.partnerCountry} onChange={(val) => this.selectPartnerCountry(val)} />
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6">
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Average turnover volume of payments within a month</span></span></span>     
                    <input className="form-control" id="exampleFormControlInput1827" placeholder="Estimated Turnover Total Monthly" type="text" name="partnerEstimatedTurnover" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">                 
                <select defaultValue={"1"}className="selectpicker show-tick form-control"  id="id31" name="partnerCurrency" onChange={this.handleChange}>
                        <option disabled value="1"> -- Select Currency -- </option>
                        <option value="EUR">EUR</option>
                        <option value="USD">USD</option>
                        <option value="GBP">GBP</option>                  
                        <option value="RUB">RUB</option>
                    </select>
                </div>
            </div>
            <div className="form-row">            
               <div className="form-group col-md-6"> 
               <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>Precise nature of business activity ex. “Legal services”, “Trading” etc.</span></span></span>                   
                    <input className="form-control" id="exampleFormControlInput18" placeholder="Purpose of Payment" type="text" name="partnerPurposePayment" onChange={this.handleChange}/>
                </div>
            </div>
            </Fragment>
    </Fragment>        
    )
    }

    render(){
        const { currentTab, companyName, comapnyStreetNumber, companyStreetName, companyCity,
            CompanyState, companyCountry,comapnyPostCode,companyNameRegistrar,companyRegistrationNumber,
            companyRegistrationDate,fName,mName,lName,docType,passportcountry,passportNumber,
            beneficiaryCitizenship,beneficiaryEmail,beneficiaryPassword,beneficiaryResidenceAddress,
            beneficiaryTitle,typeOfBusiness,licenseRequirement,website,residentBusinessName,
            residentBusinessAddress,residentBusinessType,residentBusinessQuantity,
            residentBusinessDirectorName,residentBusinessShareholderName,EURCreditTotal,
            EURDebitTotal,EURNetBalence,EURAvgAmount,EURMonthlyVolume,USDCreditTotal,
            USDDebitTotal,USDNetBalence,USDAvgAmount,USDMonthlyVolume,GBPCreditTotal,
            GBPDebitTotal,GBPAvgAmount,GBPNetBalence,GBPMonthlyVolume,RUBCreditTotal,
            RUBDebitTotal,RUBAvgAmount,RUBNetBalence,RUBMonthlyVolume,partnerName,
            partnerCountry,partnerCurrency,partnerEstimatedTurnover,partnerPurposePayment,
            phone, showSuggestion
        } = this.state

        const currencyCheck = 
            ((EURCreditTotal && EURDebitTotal && EURNetBalence && EURAvgAmount && EURMonthlyVolume) ||
         (USDCreditTotal && USDDebitTotal && USDNetBalence && USDAvgAmount && USDMonthlyVolume) ||
         (GBPCreditTotal && GBPDebitTotal && GBPAvgAmount && GBPNetBalence && GBPMonthlyVolume) ||
         (RUBCreditTotal && RUBDebitTotal && RUBAvgAmount && RUBNetBalence && RUBMonthlyVolume))
        

        const enabled = currentTab === "corporate" ?
            currentTab && companyName &&  comapnyStreetNumber &&  companyStreetName &&  companyCity && 
            CompanyState &&  companyCountry && comapnyPostCode && companyNameRegistrar && companyRegistrationNumber && 
            companyRegistrationDate && fName && mName && lName && docType && passportcountry && passportNumber && 
            beneficiaryCitizenship && beneficiaryEmail && (beneficiaryPassword && beneficiaryPassword.length > 5) && beneficiaryResidenceAddress && 
            beneficiaryTitle && typeOfBusiness && licenseRequirement && website && residentBusinessName && 
            residentBusinessAddress && residentBusinessType && residentBusinessQuantity && 
            residentBusinessDirectorName && residentBusinessShareholderName && currencyCheck && partnerName && 
            partnerCountry && partnerCurrency && partnerEstimatedTurnover && partnerPurposePayment && phone && (showSuggestion === false)
        :
            currentTab && fName && mName && lName && docType && passportcountry && passportNumber && 
            beneficiaryCitizenship && beneficiaryEmail && (beneficiaryPassword && beneficiaryPassword.length > 5) && beneficiaryResidenceAddress && 
            beneficiaryTitle && typeOfBusiness && licenseRequirement && website && residentBusinessName && 
            residentBusinessAddress && residentBusinessType && residentBusinessQuantity && 
            residentBusinessDirectorName && residentBusinessShareholderName && currencyCheck && partnerName && 
            partnerCountry && partnerCurrency && partnerEstimatedTurnover && partnerPurposePayment && phone && (showSuggestion === false)

        
        return(
            <Fragment>
            <div className="page-wrapper">
                <div className="container-fluid">
                    {/* <!-- sign up page start--> */}
                    <div className="authentication-main">
                        <div className="row">
                            <div className="col-sm-12 p-0">
                                <div className="auth-innerright">
                                    <div className="authentication-box ">
                                        {/* <div className="text-center">
                                            <div className="auth-logo mt-1"><img src={logo} alt="logo"/></div>
                                        </div> */}
                                        <div className="card mt-4 p-4">
                                        <div className="card-body card-body-inner">
                                        {/* <div className="form-row text-center">
                                         <div className="col-md-6">
                                         <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>If you are opening an account for a company, Please select this option</span></span></span>
                                        <button class="btn btn-primary btn-lg" type="button" value="Corporate" onClick={()=>{ this.changeTab("Corporate")}}>Corporate</button>          
                                        </div>
                                        <div className="col-md-6">
                                        <span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>If you are opening an account for a individual, Please select this option</span></span></span>
                                        <button class="btn btn-primary btn-lg" type="button" value="individual" onClick={()=>{this.changeTab("individual")}}>Individual</button>             
                                        </div>
                                        </div> */}
                                        <div className="card-body card-body-inner">
                                            <form className="theme-form">

                                                <div className="row mb-4"><div className="switch-field switchlabelTabs-outer"><input type="radio" className="switch-one" id="corporate" name="currentTab" value="corporate" onClick={()=>{ this.changeTab("corporate")}} checked={this.state.currentTab === "corporate" ? true : false}  /><label htmlFor="corporate" className="switchlabelTabs">Corporate<span class="csTooltip"><span><i class="fa fa-info-circle"></i><span>If you are opening an account for a company, Please select this option</span></span></span></label><input type="radio" className="switch-two" id="individual" name="currentTab" value="individual" onClick={()=>{this.changeTab("individual")}} checked={this.state.currentTab === "individual" ? true : false}/><label htmlFor="individual" className="switchlabelTabs">Individual<span className="csTooltip"><span><i class="fa fa-info-circle"></i><span>If you are opening an account for a company, Please select this option</span></span></span></label></div></div>

                                                {this.state.currentTab === "corporate" ? 
                                                <React.Fragment> 
                                                {this.renderCorporateForm()}
                                                { this.renderIndividualForm()}
                                                </React.Fragment>
                                                :
                                                <React.Fragment> 
                                                {this.renderIndividualForm()}
                                                </React.Fragment>
                                            }
                                            <div className="form-group form-row mb-0">
                                                <button disabled={!enabled} className="btn btn-primary btn-block" type="button" onClick={this.state.currentTab === "corporate" ?  this.createAccount :this.createAccountForIndividual }>Create Account</button>
                                            </div>                                           
                                            </form>
                                        </div>
                                        </div>
                                        </div>

                                        <p class="newAuthLabel">Have an Account? <a href="/login">Login</a></p>
                                    
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copy-right"><p>Copyright © 2020 - Fincofex. All Rights Reserved.</p></div>
                    {/* <!-- sign up page ends--> */}
                </div>
            </div>
        </Fragment>
        )
    }
}
  
export default SignUp;