import React, { Component } from 'react';
import Header from '../components/common/header-component/signInHeader';
// import Footer from '../components/reusableComponents/Footer';
// import { register } from '../actions/UsersActions';
import { Link } from 'react-router-dom';
import { OnRegisterUser } from '../actions/index';
import { connect } from 'react-redux';
import LoaderBackdrop from '../components/dashboard/defaultCompo/reusableComponents/LoaderBackdrop';

import '../components/dashboard/defaultCompo/styles/SignUp.css';

export class SignUp2 extends Component {
  state = {
    isChecked: false,
    countries: {
      AF: 'AFGHANISTAN',
      AL: 'ALBANIA',
      DZ: 'ALGERIA',
      AU: 'AUSTRALIA ',
      IN: 'INDIA',
      US: 'UNITED STATES',
      GB: 'UNITED KINGDOM',
    },
    entityType: {
      travel: 'travel',
      chemical: 'chemical',
      fintech: 'fintech',
      accountancy: 'accountancy',
      aerospace: 'aerospace',
      mining: 'mining',
      agriculture: 'agriculture',
      'textiles/clothing': 'textiles/clothing',
      automotive: 'automotive',
      telecom: 'telecom',
      construction: 'construction',
      'digital/media/advertising': 'digital/media/advertising',
      ecommerce: 'ecommerce',
      education: 'education',
      electronics: 'electronics',
      energy: 'energy',
      entertainment: 'entertainment',
      fishing: 'fishing',
      food: 'food',
      healthcare: 'healthcare',
      hospitality: 'hospitality',
      humanresources: 'humanresources',
      informationtechnology: 'informationtechnology',
      internet: 'internet',
      luxurygoods: 'luxurygoods',
      manufacturing: 'manufacturing',
      marine: 'marine',
      payroll: 'payroll',
      pharmaceutical: 'pharmaceutical',
      publishing: 'publishing',
      pulpandpaper: 'pulpandpaper',
      retail: 'retail',
      steel: 'steel',
      timber: 'timber',
      tobacco: 'tobacco',
      transport: 'transport',
      water: 'water',
    },
    businessType: {
      partnership: 'partnership',
      public_limited_company: 'public_limited_company',
      sole_trader: 'sole_trader',
      limited_liability: 'limited_liability',
      joint_stock_company: 'joint_stock_company',
      charity: 'charity',
    },
    first_name: '',
    last_name: '',
    userEmail: '',
    userPassword: '',
    confirmPassword: '',
    companyName: '',
    address: '',
    countrySelect: '',
    entitySelect: '',
    businessSelect: '',
    registrationNumber: '',
    registrationDate: '2020-07-04', //datefield
    // businessType: '',
    registrationCertificate: '',
    turoverGBP: '0', //decimal field
    turnoverUSD: '0', //decimal field
    turnoverEUR: '0', //decimal field
    quantityOfPaymentsGBP: '',
    quantityOfPaymentsUSD: '',
    quantityOfPaymentsEUR: '',
    turnoverOther: '',
    volumeOfPaymentsOther: '',
    businessPartnerFullName_1: '',
    businessPartnerFullName_2: '',
    businessPartnerFullName_3: '',
    businessPartnerCountry_1: '',
    businessPartnerCountry_2: '',
    businessPartnerCountry_3: '',
    residenceCompanyName: '',
    residenceCountry: '',
    residenceBusinessType: '',
    directorFullName: '',
    directorCountry: '',
    directorDOB: '2020-07-04',
    directorDateOfIssue: '2020-07-04',
    directorPassportNumber: '',
    directorPassportScan: '',
    shareHolderFullName: '',
    shareHolderCountry: '',
    shareHolderDOB: '2020-07-04',
    shareHolderDateOfIssue: '2020-07-04',
    shareHolderPassportNumber: '',
    shareHolderPassportScan: '',
    beneficiaryFullName: '',
    beneficiaryCountry: '',
    beneficiaryDOB: '2020-07-04',
    beneficiaryDateOfIssue: '2020-07-04',
    beneficiaryPassportNumber: '',
    beneficiaryPassportScan: '',
    beneficiaryAddress: '',
    beneficiaryTelephone: '',
    benficiaryEmail: '',
    residenceBusinessQuantityOfPeople: '',
    residenceBusinessDirectorFullName: '',
    residenceBusinessShareHolderFullName: '',
  };

  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  onDropdownValueChange = (type, id) => {
    const { countries, entityType, businessType } = this.state;
    var val = document.getElementById(id).value;
    if (type === 'countries') {
      for (let key in countries) {
        if (countries.hasOwnProperty(key)) {
          if (countries[key] === val) this.setState({ countrySelect: key });
        }
      }
    } else if (type === 'residenceCountry') {
      for (let key in countries) {
        if (countries.hasOwnProperty(key)) {
          if (countries[key] === val) this.setState({ residenceCountry: key });
        }
      }
    } else if (type === 'entityType') {
      for (let key in entityType) {
        if (entityType.hasOwnProperty(key)) {
          if (entityType[key] === val) this.setState({ entitySelect: key });
        }
      }
    } else if (type === 'residenceEntityType') {
      for (let key in entityType) {
        if (entityType.hasOwnProperty(key)) {
          if (entityType[key] === val)
            this.setState({ residenceBusinessType: key });
        }
      }
    } else if (type === 'businessType') {
      for (let key in businessType) {
        if (businessType.hasOwnProperty(key)) {
          if (businessType[key] === val) this.setState({ businessSelect: key });
        }
      }
    }
  };
  toggleCheck = () => {
    this.setState({ isChecked: !this.state.isChecked });
  };

  onSubmit = () => {
    const {
      first_name,
      last_name,
      userEmail,
      userPassword,
      confirmPassword,
      isChecked,
      countries,
      entityType,
      countrySelect,
      entitySelect,
      companyName,
      address,
      registrationNumber,
      registrationCertificate,
      registrationDate,
      businessType,
      turoverGBP,
      turnoverUSD,
      turnoverEUR,
      quantityOfPaymentsGBP,
      quantityOfPaymentsUSD,
      quantityOfPaymentsEUR,
      turnoverOther,
      volumeOfPaymentsOther,
      businessPartnerFullName_1,
      businessPartnerFullName_2,
      businessPartnerFullName_3,
      businessPartnerCountry_1,
      businessPartnerCountry_2,
      businessPartnerCountry_3,
      residenceCompanyName,
      residenceCountry,
      residenceBusinessType,
      directorFullName,
      directorCountry,
      directorDOB,
      directorDateOfIssue,
      directorPassportNumber,
      directorPassportScan,
      shareHolderFullName,
      shareHolderCountry,
      shareHolderDOB,
      shareHolderDateOfIssue,
      shareHolderPassportNumber,
      shareHolderPassportScan,
      beneficiaryFullName,
      beneficiaryCountry,
      beneficiaryDOB,
      beneficiaryDateOfIssue,
      beneficiaryPassportNumber,
      beneficiaryAddress,
      beneficiaryPassportScan,
      beneficiaryTelephone,
      beneficiaryEmail,
      residenceBusinessQuantityOfPeople,
      residenceBusinessDirectorFullName,
      residenceBusinessShareHolderFullName,
      businessSelect,
    } = this.state;
    let formData = new FormData();

    //profile data
    let profile_data = {
      // registeration_certificate_url
      company_name: companyName, //done
      company_country: countrySelect, //done
      entity_type: entitySelect, //done
      company_address: address, //done
      registeration_number: registrationNumber, //done
      registeration_date: registrationDate, //date field //done
      business_type: businessSelect, //done
      turnover_gbp: parseFloat(turoverGBP), //decimal field //done
      quantity_of_payment_gbp: quantityOfPaymentsGBP, //done
      turnover_usd: parseFloat(turnoverUSD), //decimal field //done
      quantity_of_payment_usd: quantityOfPaymentsUSD, //done
      turnover_eur: parseFloat(turnoverEUR), //decimal field //done
      quantity_of_payment_eur: quantityOfPaymentsEUR, //done
      buiness_partner_1_full_name: businessPartnerFullName_1, //done
      business_partner_1_country: businessPartnerCountry_1, //done
      buiness_partner_2_full_name: businessPartnerFullName_2, //done
      business_partner_2_country: businessPartnerCountry_2, //done
      buiness_partner_3_full_name: businessPartnerFullName_3, //done
      business_partner_3_country: businessPartnerCountry_3, //done
      residence_business_company_name: residenceCompanyName, //done
      residence_business_company_country: residenceCountry, //done
      residence_business_company_type: residenceBusinessType, //done
      director_full_name: directorFullName, //done
      director_country: directorCountry, //done
      director_date_of_birth: directorDOB, //date field //done
      director_date_of_issue: directorDateOfIssue, //date field //done
      director_passport_number: directorPassportNumber, //done
      shareholder_full_name: shareHolderFullName, //done
      shareholder_country: shareHolderCountry, //done
      shareholder_date_of_birth: shareHolderDOB, //date field //done
      shareholder_date_of_issue: shareHolderDateOfIssue, //date field //done
      shareholder_passport_number: shareHolderPassportNumber, //done
      beneficiary_full_name: beneficiaryFullName,
      beneficiary_country: beneficiaryCountry,
      beneficiary_date_of_birth: beneficiaryDOB, //date field
      beneficiary_date_of_issue: beneficiaryDateOfIssue, //date field
      beneficiary_passport_number: beneficiaryPassportNumber,
      beneficiary_telephone: beneficiaryTelephone,
      beneficiary_email_id: beneficiaryEmail,
      residence_business_people_quantity_of_employee: residenceBusinessQuantityOfPeople,
      residence_business_people_director_full_name: residenceBusinessDirectorFullName,
      residence_business_people_shareholder_fullname: residenceBusinessShareHolderFullName,
      contactperson: 'Abhishek',
      registeredAddressPostalCode: '119',
      registeredAddressCity: 'Manchester',
    };

    if (!first_name) {
      alert('User Name field is required');
    } else if (!userEmail) {
      alert('User Email field is required');
    } else if (!userPassword) {
      alert('Password field is required');
    } else if (!confirmPassword) {
      alert('Confirm Password field is required');
    } else if (userPassword !== confirmPassword) {
      alert('User Password and Confirm Password doesnt match');
    } else if (!registrationCertificate) {
      alert('Please upload the registration certificate');
    } else if (!beneficiaryPassportScan) {
      alert('Please upload the beneficiary passport');
    } else if (!shareHolderPassportScan) {
      alert('Please upload the shareholder passport');
    } else if (!directorPassportScan) {
      alert('Please upload the director passport');
    } else if (!isChecked) {
      alert('Please Agree to terms anc conditions to continue');
    } else {
      //appending the form data values
      formData.append('first_name', first_name);
      formData.append('last_name', last_name);
      formData.append('password', userPassword);
      formData.append('password1', confirmPassword);
      formData.append('email', userEmail);
      formData.append('profile_data', JSON.stringify(profile_data));
      formData.append('file', registrationCertificate);
      formData.append('beneficiary_passport_scan', beneficiaryPassportScan);
      formData.append('shareholder_passport_scan', shareHolderPassportScan);
      formData.append('director_passport_scan', directorPassportScan);
      this.props.OnRegisterUser(formData, this.props.history);
    }
  };

  render() {
    const {
      first_name,
      last_name,
      userEmail,
      userPassword,
      confirmPassword,
      countries,
      entityType,
      companyName,
      address,
      registrationNumber,
      registrationDate,
      businessType,
      turoverGBP,
      turnoverUSD,
      turnoverEUR,
      quantityOfPaymentsGBP,
      quantityOfPaymentsUSD,
      quantityOfPaymentsEUR,
      turnoverOther,
      volumeOfPaymentsOther,
      businessPartnerFullName_1,
      businessPartnerFullName_2,
      businessPartnerFullName_3,
      businessPartnerCountry_1,
      businessPartnerCountry_2,
      businessPartnerCountry_3,
      residenceCompanyName,
      directorFullName,
      directorCountry,
      directorDOB,
      directorDateOfIssue,
      directorPassportNumber,
      shareHolderFullName,
      shareHolderCountry,
      shareHolderDOB,
      shareHolderDateOfIssue,
      shareHolderPassportNumber,
      beneficiaryFullName,
      beneficiaryCountry,
      beneficiaryDOB,
      beneficiaryDateOfIssue,
      beneficiaryPassportNumber,
      beneficiaryAddress,
      beneficiaryPassportScan,
      beneficiaryTelephone,
      beneficiaryEmail,
      residenceBusinessQuantityOfPeople,
      residenceBusinessDirectorFullName,
      residenceBusinessShareHolderFullName,
      businessSelect,
    } = this.state;
    const country = Object.values(countries);
    const entType = Object.values(entityType);
    const businesType = Object.values(businessType);
    return (
      <div>
        <Header />
        {this.props.isloading ? (
          <LoaderBackdrop message="Creating your account.Please wait..." />
        ) : (
          <div className="signUp-form-container" style={{ height: '100%' }}>
            <p className="Account-opening-Sign-Up-Form-title-text">
              Account opening Sign Up Form
            </p>
            <div className="company-information-box">User Details</div>
            <p className="company-title-text">User</p>
            <hr />
            <section className="user-section">
              <div className="fields-div">
                <label>First Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="first_name"
                  value={first_name}
                />
              </div>
              <div className="fields-div">
                <label>Last Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="last_name"
                  value={last_name}
                />
              </div>
              <div className="fields-div">
                <label>User Email</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="userEmail"
                  value={userEmail}
                />
              </div>
              <div className="fields-div">
                <label>Password</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="userPassword"
                  value={userPassword}
                  type="password"
                />
              </div>
              <div className="fields-div">
                <label>Confirm Password</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="confirmPassword"
                  value={confirmPassword}
                  type="password"
                />
              </div>
            </section>
            <div className="company-information-box">Company Information</div>
            <p className="company-title-text">Company</p>
            <hr />
            <section className="company-section">
              <div className="fields-div">
                <label>Company Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="companyName"
                  value={companyName}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  id="countryListInput"
                  className="signup-input"
                  onChange={() =>
                    this.onDropdownValueChange('countries', 'countryListInput')
                  }
                  list="countryList"
                />
                <datalist id="countryList">
                  {country.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              </div>
              <div className="fields-div">
                <label>Entity Type</label>
                <input
                  id="entityTypeListInput"
                  className="signup-input"
                  onChange={() =>
                    this.onDropdownValueChange(
                      'entityType',
                      'entityTypeListInput'
                    )
                  }
                  list="entityTypeList"
                />
                <datalist id="entityTypeList" style={{ position: 'fixed' }}>
                  {entType.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              </div>
              <div className="fields-div">
                <label>Address</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="address"
                  value={address}
                />
              </div>
              <div className="fields-div">
                <label>Registration Number</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="registrationNumber"
                  value={registrationNumber}
                />
              </div>
              <div className="fields-div">
                <label>Registration Date</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="registrationDate"
                  value={registrationDate}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Business Type</label>
                <input
                  id="businessTypeListInput"
                  className="signup-input"
                  onChange={() =>
                    this.onDropdownValueChange(
                      'businessType',
                      'businessTypeListInput'
                    )
                  }
                  list="businessTypeList"
                />
                <datalist id="businessTypeList" style={{ position: 'fixed' }}>
                  {businesType.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              </div>
              <div className="fields-div">
                <label>Registration Certificate</label>
                <input
                  className="signup-input"
                  onChange={(e) => {
                    const registrationCertificateFile = e.target.files[0];
                    this.setState({
                      registrationCertificate: registrationCertificateFile,
                    });
                  }}
                  type="file"
                />
              </div>
              <div></div>
              <p className="title-text">Volume Per Month</p>

              <div></div>
              <div></div>

              <div className="fields-div">
                <label>Turnover (GBP)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="turoverGBP"
                  value={turoverGBP}
                />
              </div>
              <div className="fields-div">
                <label>Turnover (USD)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="turnoverUSD"
                  value={turnoverUSD}
                />
              </div>
              <div className="fields-div">
                <label>Turnover (EUR)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="turnoverEUR"
                  value={turnoverEUR}
                />
              </div>
              <div className="fields-div">
                <label>Quantity of payments(GBP)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="quantityOfPaymentsGBP"
                  value={quantityOfPaymentsGBP}
                />
              </div>
              <div className="fields-div">
                <label>Quantity of payments(USD)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="quantityOfPaymentsUSD"
                  value={quantityOfPaymentsUSD}
                />
              </div>
              <div className="fields-div">
                <label>Quantity of payments(EUR)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="quantityOfPaymentsEUR"
                  value={quantityOfPaymentsEUR}
                />
              </div>
              <div className="fields-div">
                <label>Turnover (Other)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="turnoverOther"
                  value={turnoverOther}
                />
              </div>
              <div className="fields-div">
                <label>Volume of payments (Other)</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="volumeOfPaymentsOther"
                  value={volumeOfPaymentsOther}
                />
              </div>
              <div></div>
              <p className="title-text business-partners">Business Partners</p>

              <div></div>
              <div></div>

              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerFullName_1"
                  value={businessPartnerFullName_1}
                />
              </div>
              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerFullName_2"
                  value={businessPartnerFullName_2}
                />
              </div>
              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerFullName_3"
                  value={businessPartnerFullName_3}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerCountry_1"
                  value={businessPartnerCountry_1}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerCountry_2"
                  value={businessPartnerCountry_2}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="businessPartnerCountry_3"
                  value={businessPartnerCountry_3}
                />
              </div>
              <p className="title-text residence-business-company">
                Residence Business Company
              </p>
              <div></div>
              <div></div>
              <div className="fields-div">
                <label>Company Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="residenceCompanyName"
                  value={residenceCompanyName}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  id="residenceCountryListInput"
                  className="signup-input"
                  onChange={() =>
                    this.onDropdownValueChange(
                      'residenceCountry',
                      'residenceCountryListInput'
                    )
                  }
                  list="residenceCountryList"
                />
                <datalist id="residenceCountryList">
                  {country.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              </div>
              <div className="fields-div">
                <label>Type of business</label>
                <input
                  id="residenceEntityTypeListInput"
                  className="signup-input"
                  onChange={() =>
                    this.onDropdownValueChange(
                      'residenceEntityType',
                      'residenceEntityTypeListInput'
                    )
                  }
                  list="residenceEntityTypeList"
                />
                <datalist id="residenceEntityTypeList">
                  {entType.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              </div>
            </section>
            <div className="company-information-box">People Information</div>
            <p className="company-title-text">Director</p>
            <hr />
            <section className="people-information-section">
              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="directorFullName"
                  value={directorFullName}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input className="signup-input" onChange={this.onInputChange} />
              </div>
              <div className="fields-div">
                <label>Date of birth</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="directorDOB"
                  value={directorDOB}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Date of issue</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="directorDateOfIssue"
                  value={directorDateOfIssue}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Passport number</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="directorPassportNumber"
                  value={directorPassportNumber}
                />
              </div>
              <div className="fields-div">
                <label>Passport scan</label>
                <input
                  className="signup-input"
                  onChange={(e) => {
                    const directorPassportFile = e.target.files[0];
                    this.setState({
                      directorPassportScan: directorPassportFile,
                    });
                  }}
                  type="file"
                />
              </div>
              <p className="title-text shareholder">Shareholder</p>
              <div></div>
              <div></div>
              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="shareHolderFullName"
                  value={shareHolderFullName}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input className="signup-input" onChange={this.onInputChange} />
              </div>
              <div className="fields-div">
                <label>Date of birth</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="shareHolderDOB"
                  value={shareHolderDOB}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Date of issue</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="shareHolderDateOfIssue"
                  value={shareHolderDateOfIssue}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Passport number</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="shareHolderPassportNumber"
                  value={shareHolderPassportNumber}
                />
              </div>
              <div className="fields-div">
                <label>Passport scan</label>
                <input
                  className="signup-input"
                  onChange={(e) => {
                    const shareHolderPassportFile = e.target.files[0];
                    this.setState({
                      shareHolderPassportScan: shareHolderPassportFile,
                    });
                  }}
                  type="file"
                />
              </div>
              <p className="title-text account-beneficiary">
                Account Beneficiary
              </p>
              <div></div>
              <div></div>
              <div className="fields-div">
                <label>Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryFullName"
                  value={beneficiaryFullName}
                />
              </div>
              <div className="fields-div">
                <label>Country</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryCountry"
                  value={beneficiaryCountry}
                />
              </div>
              <div className="fields-div">
                <label>Date of birth</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryDOB"
                  value={beneficiaryDOB}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Date of issue</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryDateOfIssue"
                  value={beneficiaryDateOfIssue}
                  type="date"
                />
              </div>
              <div className="fields-div">
                <label>Passport number</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryPassportNumber"
                  value={beneficiaryPassportNumber}
                />
              </div>
              <div className="fields-div">
                <label>Address</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryAddress"
                  value={beneficiaryAddress}
                />
              </div>
              <div className="fields-div">
                <label>E-mail</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryEmail"
                  value={beneficiaryEmail}
                />
              </div>
              <div className="fields-div">
                <label>Telephone</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryTelephone"
                  value={beneficiaryTelephone}
                />
              </div>
              <div className="fields-div">
                <label>Passport scan</label>
                <input
                  className="signup-input"
                  onChange={(e) => {
                    const beneficiaryPassportFile = e.target.files[0];
                    this.setState({
                      beneficiaryPassportScan: beneficiaryPassportFile,
                    });
                  }}
                  type="file"
                />
              </div>
              {/* <div className="fields-div"></div> */}
              <p className="title-text residence-business-people">
                Residence Business People
              </p>
              <div></div>
              <div></div>
              <div className="fields-div">
                <label>Quantity of Employees</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="residenceBusinessQuantityOfPeople"
                  value={residenceBusinessQuantityOfPeople}
                />
              </div>
              <div className="fields-div">
                <label>Directors Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="residenceBusinessDirectorFullName"
                  value={residenceBusinessDirectorFullName}
                />
              </div>
              <div className="fields-div">
                <label>Shareholders Full Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="residenceBusinessShareHolderFullName"
                  value={residenceBusinessShareHolderFullName}
                />
              </div>
            </section>
            <div className="accept-terms-and-conditions">
              <div>
                <input
                  type="checkbox"
                  className="privacy-policy-check-box"
                  id="privacy-policy"
                  name="privacyPolicy"
                  checked={this.state.isChecked}
                  onChange={this.toggleCheck}
                />
                <label className="privacyPolicy">
                  Accept &nbsp;
                  <a
                    style={{
                      color: 'blue',
                      cursor: 'pointer',
                    }}
                    href="#privacy-policy"
                  >
                    Privacy Policy
                  </a>
                  &nbsp; and
                  <a style={{ color: 'blue', cursor: 'pointer' }} href="#t">
                    &nbsp; Terms
                  </a>
                  &nbsp; and
                  <a style={{ color: 'blue', cursor: 'pointer' }} href="#c">
                    &nbsp; Conditions
                  </a>
                </label>
              </div>
              <button className="signup-button" onClick={this.onSubmit}>
                Confirm
              </button>
            </div>
          </div>
        )}
        {/* <Footer /> */}
      </div>
    );
  }
}

const mapStateToPropes = (state) => ({
  // successMessage: state.userReducer.successMessage,
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToPropes, {
  OnRegisterUser,
})(SignUp2);
