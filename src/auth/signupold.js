import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { CountryDropdown } from 'react-country-region-selector';
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";

import { SIGN_UP_APP  } from '../constant/actionTypes';

import logo from '../assets/images/logo.png';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class SignUpOld extends Component {
    state = {
        isDisabled: true,
        name: "",
        email: "",
        password: "",
        country: "",
        startDate: "",
        show1: false,
        show2: false,
        docType: ""
    };

    handleChange = event => {
        this.setState({
          [event.target.name]: event.target.value
        });
    };

    selectCountry (val) {
        this.setState({ country: val });
    }

    handleDateChange = date => {
        this.setState({startDate: date});
    }

    show1 = () => {
        this.setState({show1: !this.state.show1});
    }

    show2 = () => {
        this.setState({show2: !this.state.show2});
    }

    handlePasswordChange = event => {
        var reg = /(?=^.{6,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/;
        if(reg.test(event.target.value)){
            this.setState({
              password: event.target.value
            });
        }
        this.handleError();
    };
    
    handleEmailChange = e => {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{1,4})$/;
        if (reg.test(e.target.value)) {
          this.setState({ email: e.target.value });
        }
        this.handleError();
    };

    handleError = () => {
        if (this.state.password && this.state.password.length >= 5 && this.state.email)
        {
          this.setState({ isDisabled: false });
        } else {
          this.setState({ isDisabled: true });
        }
    };

    changeTab = (value,event) =>{
    console.log(value,"current tab");
    
    }

    createAccount = () => {
        const { history } = this.props;

        const add = this.state.streetNumber + "," + this.state.streetName + "," + this.state.city + "," + this.state.state + "," + this.state.postCode;
        // const phone = 


        axios.post( SIGN_UP_APP, {
            "company_name" : this.state.companyName,
            "password_hash" : this.state.password,
            "companys_country" : this.state.country,
            "address" : add,
            "company_number" : this.state.companyNumber,
            "company_adddata" : this.state.startDate,
            "first_name" : this.state.fName,
            "last_name" : this.state.lName,
            "middle_name": this.state.mName,
            "id_document_type" : this.state.docType,
            "id_document_num" : this.state.docNumber,
            "id_document_country": this.state.docCountry,
            "phone_number" : this.state.phoneNumber,
            "phone_country": this.state.pCode,
            "email" : this.state.email,
            "citizenship" : this.state.citizenship,
            "country_of_residence" : this.state.residence,
            "type_of_business" : this.state.typeBusiness
          })
          .then(function (response) {
            if (response.data.result.id !== null) {
                setTimeout(function() {
                    history.push('/login');
                }, 1500);
            } else {
                toast.error("Invalid email or password");
            }
          })
          .catch(function (error) {
              toast.error("Invalid email or password");
          })
    };

    renderSignUpForm =  () => {
        return <Fragment>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput1">Company Information</label>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Company Name</label>
                <input required className="form-control" id="exampleFormControlInput1" type="text" name="companyName" onChange={this.handleChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Password</label>
                <input required className="form-control" id="exampleFormControlInput1" type="password" name="name" onChange={this.handlePasswordChange}/>
            </div>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput1">Company Address</label>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput2">Street Number&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                <input required className="form-control" id="exampleFormControlInput2" type="text" name="streetNumber" onChange={this.handleChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput3">Street Name&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                <input required className="form-control" id="exampleFormControlInput3" type="text" name="streetName" onChange={this.handleChange}/>
            </div>
            <div className="form-row">
                <div className="form-group col-md-6">
                    <label  htmlFor="exampleFormControlInput4">City&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                    <input className="form-control" id="exampleFormControlInput4" type="text" name="city" onChange={this.handleChange}/>
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="exampleFormControlInput5">State&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                    <input className="form-control" id="exampleFormControlInput5" type="text" name="state" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md-6">
                    <label htmlFor="exampleFormControlInput7">Country&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                    <CountryDropdown className="form-control" name="country" value={this.state.country} onChange={(val) => this.selectCountry(val)} />
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="exampleFormControlInput8">Post Code&nbsp;<span style={{ "color" : "#e4566e"}}>*</span></label>
                    <input className="form-control" id="exampleFormControlInput8" type="text" name="postCode" onChange={this.handleChange}/>
                </div>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput9">Company Name Registrar</label>
                <input className="form-control" id="exampleFormControlInput9" type="text" name="countryResidence" onChange={this.handleChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput10">Company Registration Number</label>
                <input className="form-control" id="exampleFormControlInput10" type="text" name="companyNumber" onChange={this.handleChange}/>
            </div>
            <div className="form-group">
                <label htmlFor="exampleFormControlInput11">Company Registration Date</label>
                <DatePicker className="form-control" selected={this.state.startDate} onChange={this.handleDateChange}/>
            </div>
            <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                Beneficiary information
                <i className="fa fa-angle-double-right btn" onClick={this.show1}></i>
            </label>
            { this.state.show1 ?
            <Fragment>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <label htmlFor="exampleFormControlInput13">First Name</label>
                        <input className="form-control" id="exampleFormControlInput13" type="text" name="fName" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-4">
                        <label htmlFor="exampleFormControlInput12">Middle Name</label>
                        <input className="form-control" id="exampleFormControlInput12" type="text" name="mName" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-4">
                        <label htmlFor="exampleFormControlInput14">Last Name</label>
                        <input className="form-control" id="exampleFormControlInput14" type="text" name="lName" onChange={this.handleChange}/>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput15">Document type</label>
                    <select className="selectpicker show-tick form-control"  id="id3" name="docType" onChange={this.handleChange}>
                        <option disabled selected value> -- select an option -- </option>
                        <option value="passport">Passport</option>
                        <option value="licence">Licence</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                { this.state.docType === "passport" ? 
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="exampleFormControlInput16">Passport Country</label>
                        <input className="form-control" id="exampleFormControlInput16" type="text" name="docCountry" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlInput17">Passport Number</label>
                        <input className="form-control" id="exampleFormControlInput17" type="text" name="docNumber" onChange={this.handleChange}/>
                    </div>
                </div> : null }
                { this.state.docType === "licence" ? 
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="exampleFormControlInput18">Licence Country</label>
                        <input className="form-control" id="exampleFormControlInput18" type="text" name="docCountry" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="exampleFormControlInput17">Licence Number</label>
                        <input className="form-control" id="exampleFormControlInput17" type="text" name="docNumber" onChange={this.handleChange}/>
                    </div>
                </div>: null }
                { this.state.docType === "other" ?
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="exampleFormControlInput18">ID Type</label>
                        <input className="form-control" id="exampleFormControlInput18" type="text" name="docCountry" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="exampleFormControlInput17">ID Number</label>
                        <input className="form-control" id="exampleFormControlInput17" type="text" name="docNumber" onChange={this.handleChange}/>
                    </div>
                </div> : null }
                <div className="form-row">
                    <div className="form-group col-md-3">
                        <label htmlFor="exampleFormControlInput18">Country Code</label>
                        <input className="form-control" id="exampleFormControlInput18" type="text" name="pCode" onChange={this.handleChange}/>
                    </div>
                    <div className="form-group col-md-9">
                        <label htmlFor="exampleFormControlInput17">Phone Number</label>
                        <input className="form-control" id="exampleFormControlInput17" type="text" name="phoneNumber" onChange={this.handleChange}/>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput18">E-mail </label>
                    <input className="form-control" id="exampleFormControlInput18" type="email" name="email" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput17">Citizenship</label>
                    <input className="form-control" id="exampleFormControlInput17" type="text" name="citizenship" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput18">Dual Citizenship</label>
                    <input className="form-control" id="exampleFormControlInput18" type="text" name="dualCitizenship" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput17">Residence address</label>
                    <input className="form-control" id="exampleFormControlInput17" type="text" name="residence" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput18">Title in company</label>
                    <select className="selectpicker show-tick form-control"  id="id3" name="type" onChange={this.handleChange}>
                        <option disabled selected value> -- select an option -- </option>
                        <option value="Shareholder">Shareholder</option>
                        <option value="Director">Director</option>
                        <option value="Beneficiary">Beneficiary</option>
                    </select>
                </div>
            </Fragment>
             : null }
            <label className="font-weight-bold" htmlFor="exampleFormControlInput12">
                Description of commercial activities
                <i className="fa fa-angle-double-right btn" onClick={this.show2}></i>
            </label>
            { this.state.show2 ?
            <Fragment>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput18">Type of business</label>
                    <input className="form-control" id="exampleFormControlInput18" type="text" name="typeBusiness" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput17">License requirement</label>
                    <select className="selectpicker show-tick form-control"  id="id3" name="licenseRequirement" onChange={this.handleChange}>
                        <option disabled selected value> -- select an option -- </option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleFormControlInput18">Website</label>
                    <input className="form-control" id="exampleFormControlInput18" type="text" name="website" onChange={this.handleChange}/>
                </div>
            </Fragment> : null }
        </Fragment>
      }

    render() {
        return(
            <div>
            <div className="page-wrapper">
                <div className="container-fluid p-0">
                    {/* <!-- login page start--> */}
                    <div className="authentication-main">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="auth-innerright">
                                    <div className="authentication-box">
                                        <div className="card mt-4">
                                            <div className="card-body">
                                                <div className="text-center">
                                                    <img className="mb-3" src={logo} alt="" />
                                                    <h4 className="mb-1">SIGN UP</h4>
                                                    <button className="btn btn-primary mr-3" type="button" value="Individual" onClick={this.changeTab} >Individual</button>
                                                    <button className="btn btn-primary ml-2" type="button" value="Corporate" onClick={this.changeTab} >Corporate</button>
                                                </div>
                                                <form className="theme-form" >
                                                    {this.renderSignUpForm()}
                                                    <br/>
                                                    <div className="form-group form-row mb-0">
                                                        <button className="btn btn-primary btn-block" type="button" onClick={this.createAccount} >Create Account</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ToastContainer />
                    {/* <!-- login page end--> */}
                </div>
            </div>
        </div>
        );
    }
}
    // createAccount = () =>{
    //     axios.post( SIGN_UP_APP, {
    //         "company_type":,
    //         "company_name" : this.state.companyName,
    //         "password_hash" : this.state.password,
    //         "street_no":,
    //          "city":,
    //          "state":,
    //          "post_code":,
    //          "company_reg_name":,
    //          "company_reg_no":,
    //          "company_reg_date":, 
    //          "first_name":,
    //          "middle_name":,
    //          "last_name":,
    //          "id_document_type":,
    //          "id_document_num":,
    //           "doc_country":,
    //           "country_code":,
    //           "phone_number":,
    //           "email":,
    //           "citizenship":,
    //           "residential_address":,
    //           "company_title":,
    //           "type_of_business":,
    //           "licence_required":,
    //           "website":,
    //           "resident_business_company_name":,
    //           "resident_business_company_address":,
    //           "resident_business_type":,
    //           "resident_business_company_employee_count":,
    //           "resident_business_company_director_full_name":,
    //           "resident_business_company_shareholder_full_name":,
    //           "estimate_credit_euro_monthly":,
    //           "estimate_debit_euro_monthly":,
    //           "estimate_net_balance_euro":,
    //           "avg_amount_per_month_transaction_euro":,
    //           "monthly_volume_transaction_euro":,
    //           "estimate_credit_usd_monthly":,
    //           "estimate_debit_usd_monthly":,
    //           "estimate_net_balance_usd":,
    //           "avg_amount_per_month_transaction_usd":,
    //           "monthly_volume_transaction_usd":,
    //           "estimate_credit_gbp_monthly":,
    //           "estimate_debit_gbp_monthly":,
    //           "estimate_net_balance_gbp":,
    //           "avg_amount_per_month_transaction_gbp":,
    //           "monthly_volume_transaction_gbp":,
    //           "estimate_credit_rub_monthly":,
    //           "estimate_debit_rub_monthly":,
    //           "estimate_net_balance_rub":,
    //           "avg_amount_per_month_transaction_rub":,
    //           "monthly_volume_transaction_rub":,
    //           "partner_name":,
    //           "partner_country":,
    //           "partner_turnover_monthly":,
    //           "partner_currency":,
    //           "parter_payment_purpose":,
    //           "status":,
    //       })
    //       .then(function (response) {
    //         if (response.data.result.id !== null) {
    //             setTimeout(function() {
    //                 history.push('/login');
    //             }, 1500);
    //         } else {
    //             toast.error("Invalid email or password");
    //         }
    //       })
    //       .catch(function (error) {
    //           toast.error("Invalid email or password");
    //       })
    // };
export default SignUpOld;