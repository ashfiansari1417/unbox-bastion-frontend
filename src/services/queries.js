import { gql } from "apollo-boost";

const AdminLogin = gql`
  query($Email: Email, $Password: String!) {
    adminLogin(Email: $Email, Password: $Password) {
      ID
      Name
      Email
      Description
      Password
      Avatar
    }
  }
`;

const isResetURLValid = gql`
  query($UniqueLinkKey: String!) {
    isResetURLValid(UniqueLinkKey: $UniqueLinkKey) {
      Message
    }
  }
`;

const getCommunity = gql`
  query {
    getCommunity {
      CommunityID,
      Name,
    	HashTag,
      Description,
      GroupsCount,
      Status
    }
  }
`;

const getCommunityNames = gql`
  query {
    getCommunity {
      CommunityID,
      Name
    }
  }
`;

const getCommunityByID = gql`
  query($CommunityID: Int!) {
    getCommunityByID(CommunityID: $CommunityID) {
      CommunityID,
      Name,
    	HashTag,
      Slug,
      Description,
      GroupsCount,
      Status,
      FeaturedImage,
      Images
    }
  }
`;

const getGroups = gql`
  query {
    getGroups {
      GroupID,
      Name,
    	Hashtag,
      Description,
      Status
    }
  }
`;

const getGroupByID = gql`
  query($GroupID: Int!) {
    getGroupByID(GroupID: $GroupID) {
      GroupID,
      CommunityID,
      Name,
    	Hashtag,
      Slug,
      Description,
      Status,
      FeaturedImage,
      Images
    }
  }
`;

const getUsers = gql`
  query {
    getUsers {
      ID,
      Name,
    	Email,
      Age,
      MobileNo,
      Status
    }
  }
`;

const getUserByID = gql`
  query($ID: Int!) {
    getUserByID(ID: $ID) {
      ID,
      Name,
      Description,
      Email,
      DateOfBirth,
      MobileNo,
      Gender,
      Age,
      Avatar,
      Bio,
      Status,
      isVerified,
      groupsFollows {
        Name
      }
    }
  }
`;

export { 
AdminLogin, isResetURLValid, getCommunity, getCommunityNames, getCommunityByID, getGroups, getGroupByID,
getUsers, getUserByID };