import { gql } from "apollo-boost";

const forgotPassword = gql`
  mutation($Email: Email!) {
    forgotPassword(Email: $Email) {
      Message
    }
  }
`;

const resetPassword = gql`
  mutation($UniqueLinkKey: String, $Password: String!) {
    resetPassword(UniqueLinkKey: $UniqueLinkKey, Password: $Password) {
      ID
    }
  }
`;

const setNewPassword = gql`
  mutation($Password: String!, $NewPassword: String!, $ConfirmPassword: String!, $UserID: Int!) {
    setNewPassword(Password: $Password, NewPassword: $NewPassword, ConfirmPassword: $ConfirmPassword) {
      CustomMessage
    }
  }
`;

const addCommunity = gql`
  mutation(
    $CommunityID: Int
    $Name: String!
    $Description: String
    $HashTag: [String]
    $FeaturedImage: String
    $Images: [String]
  ) {
    addCommunity(
      CommunityID: $CommunityID
      Name: $Name
      Description: $Description
      HashTag: $HashTag
      FeaturedImage: $FeaturedImage
      Images: $Images
    ) {
      CommunityID,
      FeaturedImage,
      Name,
      Description,
      HashTag,
      Images
    }
  }
`;

const deleteCommunity = gql`
  mutation($CommunityID: Int!) {
    deleteCommunity(CommunityID: $CommunityID) {
      CommunityID
    }
  }
`;

const addGroups = gql`
  mutation(
    $GroupID: Int
    $CommunityID: Int!
    $Name: String!
    $Description: String
    $Hashtag: [String]
    $FeaturedImage: String
    $Images: [String]
    $isScope: Int
    $GroupQuestions: [GroupQuestionsInputs]
  ) {
    addGroups(
      GroupID: $GroupID
      CommunityID: $CommunityID
      Name: $Name
      Description: $Description
      Hashtag: $Hashtag
      FeaturedImage: $FeaturedImage
      Images: $Images
      isScope: $isScope
      GroupQuestions: $GroupQuestions
    ) {
      GroupID,
      CommunityID,
      FeaturedImage,
      Name,
      Description,
      Hashtag,
      Images,
      isScope
    }
  }
`;

const deleteGroup = gql`
  mutation($GroupID: Int!) {
    deleteGroup(GroupID: $GroupID) {
      GroupID
    }
  }
`;

const deleteUser = gql`
  mutation($ID: Int!) {
    deleteUser(ID: $ID) {
      ID
    }
  }
`;

export {
  forgotPassword, resetPassword, setNewPassword, addCommunity, deleteCommunity , addGroups, deleteGroup, deleteUser};