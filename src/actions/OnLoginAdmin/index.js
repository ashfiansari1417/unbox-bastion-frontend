import axios from 'axios';
import { BASE_URL } from '../../constant';
import { ON_ADMIN_LOGIN } from '../../constant/actionTypes';

export default (state, history) => (dispatch) => {
  const url = `${BASE_URL}/login/token/`;
  const config = {
    headers: {},
  };

  const { username, password } = state || {};

  const body = { username, password };
  // console.log(body);

  axios
    .post(url, body, config)
    .then((res) => {
      console.log(res);

      const { refresh, access, is_superuser, first_name, is_staff } =
        res.data || {};
      localStorage.setItem('refresh', refresh);
      localStorage.setItem('access', access);
      localStorage.setItem('isSuperUser', is_superuser);
      localStorage.setItem('isStaff', is_staff);
      localStorage.setItem('AdminFirstName', first_name);
      if (is_superuser) {
        history.push('/admin/home');
      } else history.push('/admin/swift-outgoing');
      dispatch({
        type: ON_ADMIN_LOGIN,
        payload: { ...res.data, error: false },
      });
    })
    .catch((err) => {
      dispatch({
        type: ON_ADMIN_LOGIN,
        payload: { ...err.response.data, error: true },
      });
    });
};
