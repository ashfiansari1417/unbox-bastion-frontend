import axios from 'axios';
import { BASE_URL } from '../../constant';
import {
  ON_GET_TRANSACTIONS,
  EXCHANGE_CURRENCY,
  TRANSACTION_OUTGOING,
  GET_EXCHANGE_RATE,
  ON_EXCHANGE_CURRENCY,
  TRANSACTION_INTERNAL,
} from '../../constant/actionTypes';
import {
  startTransferLoading,
  stopTransferLoading,
} from '../LoadingActions/LoadingActions';
import { OnGetAccounts } from '../../actions';
import jwt_decode from 'jwt-decode';

const url = `${BASE_URL}/transactions/`;

const token = localStorage.getItem('access');

const config = {
  headers: {
    'Content-Type': 'application/json',
    Authorization: token !== null ? 'Bearer ' + token : '',
  },
};

export const getTransactions = (acc_id, accessToken) => (dispatch) => {
  return axios
    .get(url + `get_transactions/${acc_id}/`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      dispatch({
        type: ON_GET_TRANSACTIONS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({ type: ON_GET_TRANSACTIONS, payload: { error: true } });
    });
};

// export const exchangeCurrency = (data) => (dispatch) => {
//   return axios
//     .post(url + 'convert_currency/', data, config)
//     .then((res) => {
//       dispatch({
//         type: EXCHANGE_CURRENCY,
//         payload: res.data,
//       });
//     })
//     .catch((err) => {
//       dispatch({ type: EXCHANGE_CURRENCY, payload: { error: true } });
//     });
// };

export const externalOutgoingTransaction = (body, accessToken) => (
  dispatch
) => {
  for (var pair of body.entries()) {
    console.log(pair[0] + ', ' + pair[1]);
  }
  dispatch(startTransferLoading());
  return axios
    .post(url + `outgoing_external/`, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      // console.log(res);
      dispatch({
        type: TRANSACTION_OUTGOING,
        payload: res.data,
      });
      dispatch(stopTransferLoading());
      if (res.data.error) {
        alert(res.data.error);
      } else if (res.data.status === 200) {
        alert(`Amount transferred`);
        dispatch(OnGetAccounts(accessToken));
      } else if (res.data.Error) {
        alert(res.data.Error);
      }
    })
    .catch((err) => {
      console.log(err.response);
      dispatch(stopTransferLoading());
      alert('Server Error,please try again');
    });
};

export const internalTransaction = (body, accessToken) => (dispatch) => {
  // for (var pair of body.entries()) {
  //   console.log(pair[0] + ', ' + pair[1]);
  // }
  console.log(accessToken);
  dispatch(startTransferLoading());
  return axios
    .post(url + `outgoing_internal/`, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      dispatch({
        type: TRANSACTION_INTERNAL,
        payload: res.data,
      });
      dispatch(stopTransferLoading());
      if (res.data.success && res.data.txt_id) {
        alert(`Amount transferred`);
        dispatch(OnGetAccounts(accessToken));
      } else if (res.data.error) {
        alert(res.data.error);
      }
    })
    .catch((err) => {
      console.log(err);
      dispatch(stopTransferLoading());
      alert('Transfer failed, Internaal server error');
    });
};
export const getExchangeRates = (accessToken) => (dispatch) => {
  return axios
    .get(
      'https://api.sterlingpaymentservice.com/bank/exchange-rate-conversion/',
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    )
    .then((res) => {
      // console.log('exchange', res);
      dispatch({
        type: GET_EXCHANGE_RATE,
        payload: res.data,
        error: false,
      });
    })
    .catch((err) => {
      dispatch({ type: GET_EXCHANGE_RATE, payload: { error: true } });
      // console.log(err);
    });
};
export const onExchangeCurrency = (body, accessToken) => (dispatch) => {
  // console.log(body);
  return axios
    .post(
      'https://api.sterlingpaymentservice.com/bank/currency-conversion/',
      body,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    )
    .then((res) => {
      console.log('response', res.data);
      dispatch({
        type: ON_EXCHANGE_CURRENCY,
        payload: res.data,
        error: false,
      });
      if (res.data.status === 200 && res.data.txtid) {
        alert('Exchange done');
      }
      if (res.data.status === 'insufficient funds') {
        alert(res.data.status);
      }
      dispatch(OnGetAccounts(accessToken));
    })
    .catch((err) => {
      dispatch({ type: ON_EXCHANGE_CURRENCY, payload: { error: true } });

      alert('Server Error,please try again');
    });
};
