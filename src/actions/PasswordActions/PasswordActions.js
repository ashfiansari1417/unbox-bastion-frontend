import axios from 'axios';
import { BASE_URL } from '../../constant';
import { RESET_PASSWORD } from '../../constant/actionTypes';
import { startLoading, stopLoading } from '../LoadingActions/LoadingActions';

export const resetPassword = (body, accessToken, history) => (dispatch) => {
  dispatch(startLoading());
  return axios
    .post(BASE_URL + `/resetpassword/`, body, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      console.log(res);
      dispatch({
        type: RESET_PASSWORD,
        payload: res.data,
      });

      if (res.data.Error) {
        alert(res.data.Error);
        dispatch(stopLoading());
      } else if (res.data.status === 200) {
        alert(`Password Changed Successfully`);
        dispatch(stopLoading());
        history.push('/home');
        //   dispatch(OnGetAccounts(accessToken));
      }
    })
    .catch((err) => {
      console.log(err);

      alert('Server Error,please try again');
      dispatch(stopLoading());
    });
};
export const forgotPassword = (body, history) => (dispatch) => {
  console.log(history);
  dispatch(startLoading());
  return axios
    .post(BASE_URL + `/forgot-password/`, body)
    .then((res) => {
      console.log(res.data);

      dispatch(stopLoading());

      if (res.data.msg === 'new password set') {
        alert(res.data.msg);
        history.push('/login');
      } else if (res.data.error) {
        alert(res.data.error);
      }
    })
    .catch((err) => {
      console.log(err);

      alert('Server Error,please try again');
      dispatch(stopLoading());
    });
};
