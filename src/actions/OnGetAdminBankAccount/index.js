import axios from 'axios';
import { BASE_URL } from '../../constant';
import { ON_GET_ADMIN_BANK_ACCOUNTS } from '../../constant/actionTypes';
import { startLoading, stopLoading } from '../LoadingActions/LoadingActions';

export default (accessToken) => (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: accessToken !== null ? 'Bearer ' + accessToken : '',
    },
  };
  dispatch(startLoading());

  axios
    .get(`${BASE_URL}/bank/bankbalance/`, config)
    .then((res) => {
      dispatch({
        type: ON_GET_ADMIN_BANK_ACCOUNTS,
        payload: { ...res.data },
      });
      dispatch(stopLoading());
    })
    .catch((err) => {
      console.log(err);
      dispatch(stopLoading());
    });
};
