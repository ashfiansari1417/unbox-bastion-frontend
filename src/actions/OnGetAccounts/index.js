import axios from 'axios';
import { BASE_URL } from '../../constant';
import { ON_GET_ACCOUNTS } from '../../constant/actionTypes';
import { startLoading, stopLoading } from '../LoadingActions/LoadingActions';
import jwt_decode from 'jwt-decode';

export default (accessToken) => (dispatch) => {
  const token = localStorage.getItem('access');
  const decode = jwt_decode(token);
  const url = `${BASE_URL}/transactions/get_accounts/${decode.user_id}`;

  const config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: accessToken !== null ? 'Bearer ' + accessToken : '',
    },
  };
  dispatch(startLoading());

  axios
    .get(url, config)
    .then((res) => {
      dispatch({
        type: ON_GET_ACCOUNTS,
        payload: { ...res.data },
      });
      dispatch(stopLoading());
    })
    .catch((err) => {
      dispatch({ type: ON_GET_ACCOUNTS, payload: { error: true } });
      dispatch(stopLoading());
    });
};
