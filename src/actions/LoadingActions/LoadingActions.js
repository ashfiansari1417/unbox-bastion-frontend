import {
  LOADING_START,
  LOADING_STOP,
  TRANSFER_LOADING_START,
  TRANSFER_LOADING_STOP,
} from '../../constant/actionTypes';

export const startLoading = () => {
  return {
    type: LOADING_START,
  };
};

export const stopLoading = () => {
  return {
    type: LOADING_STOP,
  };
};
export const startTransferLoading = () => {
  return {
    type: TRANSFER_LOADING_START,
  };
};

export const stopTransferLoading = () => {
  return {
    type: TRANSFER_LOADING_STOP,
  };
};
