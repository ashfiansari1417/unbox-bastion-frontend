import {
  GET_ACTIVE_CLIENTS,
  GET_CLIENT_TRANSACTION,
} from '../../constant/actionTypes';
import { BASE_URL } from '../../constant/index';
import axios from 'axios';

export const getActiveClients = (accessToken) => (dispatch) => {
  return axios
    .get(BASE_URL + `/get_all_users/`, {
      headers: { Authorization: `Bearer ${accessToken}` },
    })
    .then((res) => {
      dispatch({
        type: GET_ACTIVE_CLIENTS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const getClientTransaction = (accessToken, clientId) => (dispatch) => {
  console.log(clientId);
  return axios
    .get(BASE_URL + `/bank/client_transaction/${clientId}/`, {
      headers: { Authorization: `Bearer ${accessToken}` },
    })
    .then((res) => {
      console.log('clients', res.data);
      dispatch({
        type: GET_CLIENT_TRANSACTION,
        payload: res.data.txt,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
