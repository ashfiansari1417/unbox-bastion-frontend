import {
  GET_SWIFT_OUTGOING_PENDING,
  SWIFT_ADMIN_APPROVAL,
} from '../../constant/actionTypes';
import { BASE_URL } from '../../constant/index';
import axios from 'axios';

const url = `${BASE_URL}/`;

export const getPendingOutgoingTransactions = (accessToken) => (dispatch) => {
  return axios
    .get(url + `bank/swift/all-transactions/`, {
      headers: { Authorization: `Bearer ${accessToken}` },
    })
    .then((res) => {
      // console.log(res.data);
      dispatch({
        type: GET_SWIFT_OUTGOING_PENDING,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const swiftAdminApproval = (body, accessToken) => (dispatch) => {
  console.log(body);
  console.log(accessToken);
  return axios
    .post(url + `transactions/outgoing_admin/`, body, {
      headers: { Authorization: `Bearer ${accessToken}` },
    })
    .then((res) => {
      dispatch({
        type: SWIFT_ADMIN_APPROVAL,
        payload: res.data,
      });
      if (res.data.status === 200 && res.data.success === true) {
        alert('Action Performed successfully');
        dispatch(getPendingOutgoingTransactions(accessToken));
      } else if (res.data.error) alert(res.data.error);
    })
    .catch((err) => {
      console.log(err);
      alert('Server Error,please try again');
      dispatch(getPendingOutgoingTransactions(accessToken));
    });
};
