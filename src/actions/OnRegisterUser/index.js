import axios from 'axios';
import { BASE_URL } from '../../constant';
import { startLoading, stopLoading } from '../LoadingActions/LoadingActions';

export default (body, history) => (dispatch) => {
  for (var pair of body.entries()) {
    console.log(pair[0] + ', ' + pair[1]);
  }
  const url = `${BASE_URL}/register/`;
  //   const url = `${BASE_URL}/login/token/`;
  dispatch(startLoading());
  axios
    .post(url, body, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
    .then((res) => {
      console.log(res);
      if (res.data.error) {
        alert(res.data.error);
        dispatch(stopLoading());
      } else if (res.data.user === 'created') {
        alert('User Created successully');
        dispatch(stopLoading());
        history.push('/login');
      }
    })
    .catch((err) => {
      // dispatch({ type: ON_LOGIN, payload: err.response.data.detail });
      console.log(err);
      alert('Internal server error ! Cannot register user');
      dispatch(stopLoading());
    });
};
