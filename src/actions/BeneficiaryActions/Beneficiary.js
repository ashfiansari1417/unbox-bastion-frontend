import axios from 'axios';
import { BASE_URL } from '../../constant';
import {
  GET_BENEFICIARY,
  ADD_BENEFICIARY,
  DELETE_BENEFICIARY,
} from '../../constant/actionTypes';

const url = `${BASE_URL}/`;

const token = localStorage.getItem('access');

const config = {
  headers: {
    'Content-Type': 'application/json',
    Authorization: token !== null ? 'Bearer ' + token : '',
  },
};

export const addBeneficiary = (
  body,
  accessToken,
  history
  // currenctCurrency
) => (dispatch) => {
  // console.log(body);
  return axios
    .post(url + `beneficiaries/`, body, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      dispatch({
        type: ADD_BENEFICIARY,
        payload: res.data,
      });
      if (res.data.error) {
        alert(res.data.error);
      }
      if (res.data.Benificiary) {
        alert('Beneficiary added successfully');
        //   history.push('/', {
        //     tab: 'transfer',
        //     currency: currenctCurrency,
        //   });
        history.push('/home');
      }
    })
    .catch((err) => {
      alert('Server Error,please try again');
    });
};

export const getBeneficiaries = (acc_id, accessToken) => (dispatch) => {
  axios
    .get(url + 'get_beneficiaries/' + acc_id, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      dispatch({
        type: GET_BENEFICIARY,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({ type: GET_BENEFICIARY, payload: { error: true } });
    });
};

export const deleteRecipient = (body, accessToken) => (dispatch) => {
  console.log(accessToken);
  return axios
    .post('https://api.sterlingpaymentservice.com/del_beneficiaries/', body, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
    .then((res) => {
      console.log(res.data);
      alert('Recipient Deleted Successfully');
      dispatch(getBeneficiaries(body.acc_id, accessToken));
      // dispatch({
      //   type: DELETE_BENEFICIARY,
      //     payload: res.data,
      // })
    })
    .catch((err) => {
      console.log(err);
    });
};
