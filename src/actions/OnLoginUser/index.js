import axios from 'axios';
import { BASE_URL } from '../../constant';
import { ON_LOGIN } from '../../constant/actionTypes';

export default (state, history) => (dispatch) => {
  const url = `${BASE_URL}/login/token/`;
  const config = {
    headers: {},
  };

  const { username, password, pin } = state || {};

  const body = { username, password, pin };

  axios
    .post(url, body, config)
    .then((res) => {
      const {
        refresh,
        access,
        QR,
        token,
        first_name,
        last_name,
        company_name,
      } = res.data || {};
      if (!token && !refresh) {
        localStorage.setItem('refresh', refresh);
        localStorage.setItem('access', access);
        localStorage.setItem(
          'QR',
          `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=${QR}`
        );
        window.open(
          `https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=${QR}`,
          '_blank'
        );
      } else if (token) {
        alert(token);
      } else if (access && refresh) {
        localStorage.setItem('refresh', refresh);
        localStorage.setItem('access', access);
        localStorage.setItem('firstName', first_name);
        localStorage.setItem('lastName', last_name);
        localStorage.setItem('CompanyName', company_name);
        history.push('/home');
      }
      dispatch({ type: ON_LOGIN, payload: { ...res.data, error: false } });
    })
    .catch((err) => {
      // dispatch({ type: ON_LOGIN, payload: err.response.data.detail });
      dispatch({
        type: ON_LOGIN,
        payload: { error: true, message: err.response.data.detail },
      });
    });
};
