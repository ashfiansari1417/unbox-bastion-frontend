import React, { Component } from "react";

export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
    };
  }

  onChange = (e) => {
    this.setState({ otp: e.target.value });
  };

  onVerifyOtp = () => {};

  render() {
    return (
      <div
        style={{
          display: "flex",
          height: "100vh",
          width: "100vw",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            padding: "20px 50px",
            backgroundColor: "#eee",
            borderRadius: 3,
          }}
        >
          <h4 style={{ textAlign: "center" }}>Verify Otp</h4>
          <input
            style={{
              margin: "20px 0",
              padding: "5px 0px",
              fontSize: 16,
              border: "none",
              borderBottom: "1px solid #ccc",
              backgroundColor: "transparent",
            }}
            name="otp"
            value={this.state.otp}
            onChange={this.onChange}
            type="text"
          />
          <button
            style={{
              padding: "5px 0px",
              fontSize: 16,
              border: "none",
              backgroundColor: "salmon",
              color: "white",
              cursor: "pointer",
            }}
          >
            Verify OTP
          </button>
        </div>
      </div>
    );
  }
}
