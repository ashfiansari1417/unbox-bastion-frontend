import React from 'react';

const Footer = (props) => {
  return (
    <footer
      className="page-footer font-small blue"
      style={{
        marginLeft: '0',
        padding: '5px',
        backgroundColor: '#300060',
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
      }}
    >
      <div
        className="footer-copyright text-center py-3"
        className="whiteText text-center"
      >
        © 2020 Copyright-Sterling. All Rights Reserved. Internet Banking
        Software provided by Sterling Payment Services (HK)
        <a href="/"> </a>
      </div>
    </footer>
  );
};

export default Footer;
