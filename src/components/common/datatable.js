import React, { Component, Fragment } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { APPROVE_TRANSFER_REQUEST  } from '../../constant/actionTypes';
import RequestData from '../dashboard/admin/fin-outgoing/requestData';

export class Datatable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checkedValues: [],
            myData: this.props.myData
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.myData !== state.myData) {
          return {
            myData: props.myData,
          };
      }
      return null;
    }

    componentDidMount() {
        this.setState({myData: this.props.myData})
    }

    rejectRequest = (id) => {
        
        axios.post( APPROVE_TRANSFER_REQUEST, {
            "id": id,
            "account_status":2
          })
          .then(function (response) {
                toast.success(response.data.message)
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }

    approveRequest = (id) => {
        
        axios.post( APPROVE_TRANSFER_REQUEST, {
            "id": id,
            "account_status":1
          })
          .then(function (response) {
                toast.success(response.data.message)
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }

    render() {
        const { pageSize, myClass, pagination } = this.props;
        const { myData } = this.state        

        const columns = [
            {
                Header: 'From Account',  
                accessor: 'from_account',
                style: {
                    textAlign: 'center'
                } 
            },
            {
                Header: 'Amount',  
                accessor: 'amount',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Beneficiary Name',  
                accessor: 'benbank_name',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Currency',  
                accessor: 'currency',
                style: {
                    textAlign: 'center'
                }  
            },
            {
                Header: 'Types',  
                accessor: 'client_type',
                width: 100,
                style: {
                    textAlign: 'center'
                }   
            }
        ] 

        columns.push(
            {
                Header: <b>Action</b>,
                id: 'delete',
                accessor: str => "delete",
                width: 150,
                Cell: (row) => (
                    <div>
                        <span 
                            title="Approve Request"
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to approve request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.approveRequest(row.original.id);
                                        let data = myData;
                                        data.splice(row.index, 1);
                                        this.setState({ myData: data })
                                    } else {
                                        let data = myData;
                                        this.setState({ myData: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-check" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}></i></span>
                        <span 
                            title="Reject Request" 
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to reject request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.rejectRequest(row.original.id);
                                        let data = myData;
                                        data.splice(row.index, 1);
                                        this.setState({ myData: data })
                                    } else {
                                        let data = myData;
                                        this.setState({ myData: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-times" style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}></i></span>
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
        );

        return (
            <Fragment>
                <ReactTable
                    data={myData}
                    columns={columns}
                    defaultPageSize={pageSize}
                    className={myClass}
                    showPagination={pagination}
                    SubComponent={(v) => <div style={{ padding: '10px' }}>
                                        <RequestData id={v.row}/> </div>}
                />
                <ToastContainer />
            </Fragment>
        )
    }
}

export default Datatable;