import React, { Fragment, useState, useEffect } from 'react';
import '../../assets/scss/loader.scss';
import Logo from '../../assets/images/headerLogoRed.png';

const Loader = () => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setShow(false);
    }, 1000);
  }, [show]);
  return (
    <Fragment>
      {/* <div className={`loader-wrapper ${show ? '' : 'loderhide'}`} >
                <div className="loader bg-white">
                    <div className="whirly-loader"> </div>
                </div>
            </div> */}
      <div className="row">
        <div className="col-md-12 mx-auto loader--wrapper">
          <div className={` text-center ${show ? '' : 'loderhide'}`}>
            <div className="row mt-3">
              <div className="col-md-4 mx-auto">
                <img src={Logo} height="220" width="220" />
              </div>
            </div>
            <div className="row mt-5">
              <div className="col-md-4 mx-auto">
                <div class="lds-spinner">
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  {/* <div></div>
                  <div></div> */}
                </div>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-4 mx-auto">
                <h4>Opening your banking portal</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Loader;
