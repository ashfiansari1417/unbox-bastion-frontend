import React, { Fragment } from 'react';
import man from '../../../assets/images/user/user.png'

const UserPanel = () => {

    return (
        <Fragment>
            <div className="sidebar-user text-center">
                <div>
                    <img className="img-60 rounded-circle lazyloaded blur-up" src={man} alt="#" />
                </div>
                <h6 className="mt-3 f-14">User</h6>
                <p>general manager</p>
            </div>
        </Fragment>
    );
};

export default UserPanel;