import React, { Component, Fragment } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import axios from 'axios'; 
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { ApproveTransferAccountStatusFinIncoming  } from '../../constant/actionTypes';
import InRequestData from '../dashboard/admin/fin-imcoming/inRequestData';
import Moment from 'react-moment';
// import 'moment-timezone';

export class Datatable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checkedValues: [],
            myData: this.props.myData
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.myData !== state.myData) {
          return {
            myData: props.myData,
          };
      }
      return null;
    }

    componentDidMount() {
        this.setState({myData: this.props.myData})
    }

    rejectRequest = (id) => { 
        
        axios.post(ApproveTransferAccountStatusFinIncoming, {
            "id": id,
            "status": 'rejected'
          })
          .then(function (response) {
              console.log(response.data.message)
              if(response.data.message)
                toast.success('Updated successfully!')
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }
    approveRequest = (id) => {        
        axios.post( ApproveTransferAccountStatusFinIncoming, {
            "id": id,
            "status": 'approved'
          })
          .then(function (response) {
            console.log(response.data)
            console.log(response.data.message)
            if(response.data.message)
                toast.success('Updated successfully!')
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }

    render() {
        const { pageSize, myClass, pagination } = this.props;
        const { myData } = this.state        
        console.log(myData);

        const columns = [
            {
                Header: 'Date',  
                id: 'date',
                accessor: (d) => d,
                Cell: (props) => <span>{props.value.group4_32A_Date? <Moment format="DD/MM/YYYY">{(props.value.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span>
            },
            {
                Header: 'Client Account',  
                accessor: 'Account_No',
                style: {
                    textAlign: 'center'
                }   
            }
            ,
            {
                Header: 'Amount',  
                accessor: 'group4_33B_Amount',
                style: {
                    textAlign: 'center'
                }  
            },
            {
                Header: 'Currency',  
                accessor: 'group4_32A_CurrencyCode',
                style: {
                    textAlign: 'center'
                }  
            }
        ] 
        columns.push(
            {
                Header: <b>Action</b>,
                id: 'delete',
                accessor: str => "delete",
                width: 150,
                Cell: (row) => (
                    <div>
                        <span 
                            title="Approve Request"
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to approve request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.approveRequest(row.original.id);
                                        let data = myData;
                                        data.splice(row.index, 1);
                                        this.setState({ myData: data })
                                    } else {
                                        let data = myData;
                                        this.setState({ myData: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-check" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}></i></span>
                        <span 
                            title="Reject Request" 
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to reject request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.rejectRequest(row.original.id);
                                        let data = myData;
                                        data.splice(row.index, 1);
                                        this.setState({ myData: data })
                                    } else {
                                        let data = myData;
                                        this.setState({ myData: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-times" style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}></i></span>
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
        );

        return (
            <Fragment>
                <ReactTable
                    data={myData}
                    columns={columns}
                    defaultPageSize={pageSize}
                    className={myClass}
                    showPagination={pagination}
                                                        
                    defaultSorted={[{
                        id: "date",desc: true}]
                      }
                    SubComponent={
                                    (v) => 
                                        <div style={{ padding: '10px'}}>
                                            <InRequestData id={v.row} /> 
                                        </div>
                                    }
                />
                <ToastContainer />
            </Fragment>
        )
    }
}

export default Datatable;