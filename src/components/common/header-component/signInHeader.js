import React, { useState, useEffect, Fragment } from 'react';
import logo from '../../../assets/images/sterling-logo.46d43575.png';
import Language from './language';
import UserMenu from './userMenu';
import Notification from './notification';
import SearchHeader from './searchHeader';
import { Link } from 'react-router-dom';
import {
  AlignLeft,
  Maximize,
  Bell,
  MessageCircle,
  MoreHorizontal,
} from 'react-feather';
import { Button, Navbar, Nav, Form, FormControl } from 'react-bootstrap';
import { MENUITEMS } from '../../../constant/menu';
import '../../../assets/scss/auth/signinheader.scss';
import signInHeaderLogo from '../../../assets/images/headerLogoRed.png';
import Footer from '../../common/footer';

const SignInheader = (props) => {
  const [sidebar, setSidebar] = useState(false);
  const [rightSidebar, setRightSidebar] = useState(true);
  const [headerbar, setHeaderbar] = useState(true);

  useEffect(() => {
    console.log();
  }, []);
  const openCloseSidebar = () => {
    if (sidebar) {
      setSidebar(!sidebar);
      document.querySelector('.page-main-header').classList.remove('open');
      document.querySelector('.page-sidebar').classList.remove('open');
    } else {
      setSidebar(!sidebar);
      document.querySelector('.page-main-header').classList.add('open');
      document.querySelector('.page-sidebar').classList.add('open');
    }
  };

  function showRightSidebar() {
    if (rightSidebar) {
      setRightSidebar(!rightSidebar);
      document.querySelector('.right-sidebar').classList.add('show');
    } else {
      setRightSidebar(!rightSidebar);
      document.querySelector('.right-sidebar').classList.remove('show');
    }
  }

  //full screen function
  function goFull() {
    if (
      (document.fullScreenElement && document.fullScreenElement !== null) ||
      (!document.mozFullScreen && !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }
  }

  return (
    <Fragment>
      <div className="row signin-header">
        <div className="col-md-6 left-header">
          <div className="row">
            <div className="col-md-12 pt-2">
              <a href="http://sterlingsafepayment.com/">
                <img
                  src={signInHeaderLogo}
                  alt="image"
                  width="120"
                  height="110"
                  style={{ marginLeft: -4 }}
                />
              </a>
            </div>
          </div>
        </div>
        {/* {
                    window.location.href.includes('admin')!==true?
                        <div className="col-md-6 right-header">
                            <nav className="navbar navbar-expand-md">
                                <div className="navbar-collapse collapse pt-2 pt-md-0" id="navbar2">
                                    <ul>
                                        <li className="nav-item active">
                                            <Link className="nav-link" to={`/home`} className="linkStyle">Home</Link>
                                        </li>
                                        <li className="nav-item active">
                                            <Link className="nav-link" to="#" className="linkStyle">About</Link>
                                        </li>
                                        <li className="nav-item active">
                                            <Link className="nav-link" to="#" className="linkStyle">Help</Link>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    : undefined
                } */}
      </div>
    </Fragment>
  );
};
export default SignInheader;
