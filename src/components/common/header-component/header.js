import React, { useState, Fragment } from 'react';
import logo from '../../../assets/images/headerLogoRed.png';
import UserMenu from './userMenu';
import { Link, useLocation } from 'react-router-dom';
import './header.scss';
import '../../dashboard/defaultCompo/styles/header.css';

const Header = () => {
  // const [sidebar, setSidebar] = useState(false);
  // const [rightSidebar, setRightSidebar] = useState(true);
  // const [] = useState(true);
  const location = useLocation().pathname;

  //full screen function

  return (
    <Fragment>
      <nav
        className="navbar navbar-expand-md fixed-top-sm"
        style={{ display: 'flex', justifyContent: 'space-between' }}
      >
        {/* <Link
          to={`${
            localStorage.getItem('admin')
              ? '/admin/out-pending'
              : '/dashboard/my-account'
          }`}
          className="navbar-brand whiteText"
        > */}
        <Link to="/home">
          <img
            alt=""
            src={logo}
            width="120"
            height="110"
            className="d-inline-block align-top"
          />
        </Link>

        {/* </Link> */}
        {/* <ul className="navbar-nav flex-row mr-5">
          <li className="nav-item">
            <input
              type="search"
              style={{ width: '700px', margin: '0px 0px 0px 20px' }}
              placeholder="Search..."
            />
          </li>
        </ul> */}
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <ul>
            <Link
              to="/address-book"
              className={
                location.includes('address-book')
                  ? 'header-nav-links active-header-nav-link'
                  : 'header-nav-links'
              }
            >
              Address Book
            </Link>
          </ul>
          <ul>
            <Link
              to="/user-setting"
              className={
                location.includes('user-setting') ||
                location.includes('user-profile') ||
                location.includes('reset-password')
                  ? 'header-nav-links active-header-nav-link'
                  : 'header-nav-links'
              }
            >
              Settings
            </Link>
          </ul>
          <ul style={{ margin: '0px 30px' }}>
            <UserMenu />
          </ul>
        </div>

        {/* <button
          className="navbar-toggler ml-auto"
          type="button"
          data-toggle="collapse"
          data-target="#navbar2"
          onClick={() =>
            document.querySelector('#navbar2').classList.toggle('collapse')
          }
        >
          <span className="navbar-toggler-icon"></span>
        </button> */}
      </nav>
      {/* <nav className="navbar navbar-expand-md" style={{ backgroundColor:'#300060'}}>
    <div className="navbar-collapse collapse pt-2 pt-md-0" id="navbar2">
        <ul>
            <li className="nav-item active">
                <Link className="nav-link" to="#" className="linkStyle">Dashboard</Link>
            </li>
            <li className="nav-item active">
                <Link className="nav-link" to="#" className="linkStyle">View Accounts</Link>
            </li>
            <li className="nav-item active">
                <Link className="nav-link" to="#" className="linkStyle">Transaction History</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="#" className="linkStyle">Exchange Hub</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="#" className="linkStyle">Notifications</Link>
            </li>
        </ul>
    </div>
</nav> */}

      {/*<div className="page-main-header" style={{width:'100%', marginLeft:'0', backgroundColor:'#300060'}}>
        <div className="main-header-right row">
           <Navbar variant="dark">
              <Navbar.Brand>
                <Link to={`${localStorage.getItem('admin') ? "/admin/out-pending" :"/dashboard/my-account" }`}>

                <img
                  alt=""
                  src={logo}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                />{' '}</Link>
                Fincofex

              </Navbar.Brand>
            </Navbar>
            <SearchHeader />
            {/*<div className="main-header-left d-lg-none">
              <div className="logo-wrapper">
                <Link to={`${localStorage.getItem('admin') ? "/admin/out-pending" :"/dashboard/my-account" }`}>
                  <img className="img-fluid" src={logo} alt="" />
                </Link>
              </div>
            </div>*/}
      {/*<div className="mobile-sidebar d-lg-none">
              <div className="media-body text-right switch-sm">
                <label className="switch">
                  <a onClick={() => openCloseSidebar()}>
                    <AlignLeft />
                  </a>
                </label>
              </div>
            </div>
          <div className="nav-right header-right col p-0">
            <ul className={`nav-menus ${headerbar ? '' : 'open'}`}>
              {/* <li>
                <a onClick={goFull} className="text-dark" href="#!">
                  <Maximize />
                </a>
              </li>
              <UserMenu />
            </ul>
            <div className="d-lg-none mobile-toggle pull-right" onClick={() => setHeaderbar(!headerbar)}><MoreHorizontal/></div>
          </div>
        </div>
        <br/>
        <div>another Element</div>
      </div>*/}
    </Fragment>
  );
};
export default Header;
