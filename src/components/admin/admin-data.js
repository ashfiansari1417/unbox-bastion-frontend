import React, { Component, Fragment } from 'react';
import axios from 'axios';
// import { ToastContainer, toast } from 'react-toastify';

import { GET_TRANSFER_REQUEST } from '../../constant/actionTypes';

import Datatable from '../common/datatable';
import data from '../../data/dummyTableData';

class AdminData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        axios.get(GET_TRANSFER_REQUEST)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
            
        })
    }

    render() {
        const { requestDetails } = this.state;
        // const r = requestDetails ? requestDetails.map( (item) =>
        //     delete item[
        //         "interbank_address",
        //         "type_of_transfer",
        //         "benbank_address",
        //         "client_address2",
        //         "client_country",
        //         "message_to_operator",
        //      "interaccount_Number",
        //      "id",
        //     "foreign_bank_fees",
        //      "created_at",
        //      "interbank_country",
        //      "benbank_country",
        //      "account_status",
        //      "updated_at",
        //      "message_line1",
        //      "message_line2",
        //      "message_line3",
        //      "message_line4",
        //      "reference_message",
        //      "created_at"]
        // ) : null;

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Transfer Requests</h5>
                                </div>
                                <div className="card-body datatable-react">
                                    <Datatable
                                        history={this.props.history}
                                        multiSelectOption={false}
                                        myData={requestDetails}
                                        pageSize={10}
                                        pagination={true}
                                        class="-striped -highlight"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default AdminData;