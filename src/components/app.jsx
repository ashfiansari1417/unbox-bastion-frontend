import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from './common/footer';
import Header from './common/header-component/header';
import RightSidebar from './common/right-sidebar';
import Sidebar from './common/sidebar-component/sidebar';
// import Loader from './common/loader';

const AppLayout = ({ children }) => {
  return (
    <div>
      <div className="b">
        <div className="a">
          {window.location.pathname.includes('admin') !== true ? (
            <Header />
          ) : (
            <Sidebar />
          )}
          <RightSidebar />
          <div className="page-body page-content">{children}</div>
          <Footer />
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AppLayout;
