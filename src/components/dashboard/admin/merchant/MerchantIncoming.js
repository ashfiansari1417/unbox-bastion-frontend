import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { GetNewUserSignupDetails, NewUserRequest } from '../../../../constant/actionTypes';
import {Tab, Row, Col, Nav} from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify';
import {Link} from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import UaRequestData from '../swift/SwiftIncomingCollapsibleData';

class MerchantIncoming extends Component {
    constructor(props){
        super(props)
        this.state={
        	staticCompliance:[
        		{id:1,date:'21/09/2019' , sender:'Abhishek', receiver:'ABC', paymentDesc:'some Description', amount:'$40000'},
        		{id:2,date:'24/09/2019' , sender:'Abhishek', receiver:'XYZ', paymentDesc:'some Description', amount:'$2000'},
        		{id:3,date:'26/09/2019' , sender:'Abhishek', receiver:'ABC', paymentDesc:'some Description', amount:'$7000'},
        		{id:4,date:'29/09/2019' , sender:'Abhishek', receiver:'XYZ', paymentDesc:'some Description', amount:'$60000'},
        		{id:5,date:'31/10/2019' , sender:'Abhishek', receiver:'ABC', paymentDesc:'some Description', amount:'$30000'},
        		{id:6,date:'20/11/2019' , sender:'Abhishek', receiver:'XYZ', paymentDesc:'some Description', amount:'$10000'}
        	],
            requestDetails: [],
            isAdmin:false,
            optError: false
        }
    }

    rejectRequest = (id) => { 
        
        axios.post(NewUserRequest, {
            "id": id,
            "status": 'rejected'
          })
          .then(function (response) {
                console.log(response)
                toast.success('Updated successfully!')
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }
    approveRequest = (id) => {        
        axios.post( NewUserRequest, {
            "id": id,
            "status": 'approved'
          })
          .then(function (response) {
            console.log(response)
            // if(response.data.value)
                toast.success('Updated successfully!')
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }



    componentDidMount () {
        this.getTransferRequest();
        
    }
    getTransferRequest = () => {
        axios.post(GetNewUserSignupDetails,{
            'type': 0,
        })
        .then( res => {
            console.log(res)
            this.setState({
                requestDetails: res.data.value
            })
        })
    }
    handleHeaderClick = () => {
        if(this.state.isAdmin){
            let checkboxes = (document.querySelectorAll('.rowCheckbox'))
            checkboxes.forEach(c=>c.checked=!c.checked)
        }
    }
    changeColor=(value,e)=>{
        if(e.target.style.color==='rgb(0, 0, 0)'){
            alert('You have already preapprove/prereject the user')
        }
        else{
        var ans = window.confirm(`Are you sure you want to ${value} request?`);

            if(ans){
                if(value==='preapprove'){
                	if(document.querySelector(e.target.attributes['data-target'].value).style.color==='rgb(0, 0, 0)'){
                		alert('Request already prerejected')
                	}
                	else{
	                    e.target.style.color='#000'
	                    document.querySelector(e.target.attributes['data-target'].value).classList.add('disabled');
                	}
                }
                else if(value==='prereject'){
                	if(document.querySelector(e.target.attributes['data-target'].value).style.color==='rgb(0, 0, 0)'){
                		alert('Request already preapproved')
                	}
                	else{
	                    let reason = prompt('Please tell us why are you pre-rejecting this user?')
	                    if(reason!=='' && reason!==null){
	                    	var div = document.createElement('p');
		                    div.style.color='#ff0000'
		                    div.style.height='3em'
		                    div.innerHTML=reason
		                    e.currentTarget.appendChild(div)
		                    document.querySelector(e.target.attributes['data-target'].value).classList.add('disabled')
		                    e.target.style.color='#000'
	                    }
	                }
                }
            }
        }
    }
    render() {
        const { requestDetails } = this.state;
        const reusableColumns = [
            {
                Header: <input type="checkbox" disabled={!this.state.isAdmin} onChange={()=>{this.handleHeaderClick()}}/>,
                accessor: '',
                width: 50,
                Cell: (row) => (
                    <div>
                    <input type="checkbox"  className="rowCheckbox" disabled={!this.state.isAdmin}/>
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            },
            {
                Header: <b>Date <i className='fa fa-sort'></i></b>,  
                accessor: 'date',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: <b>Sender <i className='fa fa-sort'></i></b>,  
                accessor: 'sender',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: <b>Receiver <i className='fa fa-sort'></i></b>,  
                accessor: 'receiver',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: <b>Payment Description <i className='fa fa-sort'></i></b>,  
                accessor: 'paymentDesc',
                width: 350,
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: <b>Amount <i className='fa fa-sort'></i></b>,  
                accessor: 'amount',
                style: {
                    textAlign: 'center'
                }   
            }
        ] 
        const undercomplianceColumns = reusableColumns
        let undercomplianceActions =
            {
                Header: <b>Action</b>,
                id: 'delete',
                accessor: str => "delete",
                width: 150,
                Cell: (row) => (
                    <div>
                        <span title="Preapprove Request" style={{ cursor: 'pointer' }} onClick={this.changeColor.bind(this, 'preapprove')}><i id={`preapprove${row.original.id}`} className="fa fa-check-circle" data-target={`#prereject${row.original.id}`} style={{ width: 35, fontSize: 18, padding: 11, color: '#ffff00' }}></i></span>

                        <span 
                            title="Approve Request" className={this.state.isAdmin?"":"disabled"}
                            style={{ cursor: 'pointer' }}
                            onClick={() => { 
                                if(this.state.isAdmin){                  
                                    var d = window.confirm('Are you sure you want to approve request?');
                                    if(row.original.id) {
                                        if (d == true) {
                                            this.approveRequest(row.original.id);
                                            let data = requestDetails;
                                            data.splice(row.index, 1);
                                            this.setState({ requestDetails: data })
                                        } else {
                                            let data = requestDetails;
                                            this.setState({ requestDetails: data });
                                        }
                                    }
                                }
                                else{
                                    alert('You do not have access rights to approve/reject the request')
                                }
                            }}
                        ><i className="fa fa-check" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}></i></span>
                        <span 
                            title="Reject Request" 
                            style={{ cursor: 'pointer' }} className={this.state.isAdmin?"":"disabled"}
                            onClick={() => {  
                                if(this.state.isAdmin){                
                                    var d = window.confirm('Are you sure you want to reject request?');
                                    if(row.original.id) {
                                        if (d == true) {
                                            this.rejectRequest(row.original.id);
                                            let data = requestDetails;
                                            data.splice(row.index, 1);
                                            this.setState({ requestDetails: data })
                                        } else {
                                            let data = requestDetails;
                                            this.setState({ requestDetails: data });
                                        }
                                    }
                                }
                                else{
                                    alert('You do not access rights to approve/reject the request')
                                }
                            }}
                        ><i className="fa fa-undo" style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}></i></span>
                        <span title="Prereject Request" style={{ cursor: 'pointer' }} onClick={this.changeColor.bind(this, 'prereject')}><i id={`prereject${row.original.id}`} data-target={`#preapprove${row.original.id}`} className="fa fa-undo" style={{ width: 35, fontSize: 18, padding: 11, color: '#ffff00' }}></i></span>

                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
        
        	let approvedActions =
            {
                Header: <b>Action</b>,
                id: 'delete',
                accessor: str => "delete",
                width: 150,
                Cell: (row) => (
                    <div>
                        <span 
                            title="Approve Request" className={this.state.isAdmin?"":"disabled"}
                            style={{ cursor: 'pointer' }}
                            onClick={() => { 
                                if(this.state.isAdmin){                  
                                    var d = window.confirm('Are you sure you want to approve request?');
                                    if(row.original.id) {
                                        if (d == true) {
                                            this.approveRequest(row.original.id);
                                            let data = requestDetails;
                                            data.splice(row.index, 1);
                                            this.setState({ requestDetails: data })
                                        } else {
                                            let data = requestDetails;
                                            this.setState({ requestDetails: data });
                                        }
                                    }
                                }
                                else{
                                    alert('You do not have access rights to approve/reject the request')
                                }
                            }}
                        ><i className="fa fa-check" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}></i></span>
                        <span 
                            title="Reject Request" 
                            style={{ cursor: 'pointer' }} className={this.state.isAdmin?"":"disabled"}
                            onClick={() => {  
                                if(this.state.isAdmin){                
                                    var d = window.confirm('Are you sure you want to reject request?');
                                    if(row.original.id) {
                                        if (d == true) {
                                            this.rejectRequest(row.original.id);
                                            let data = requestDetails;
                                            data.splice(row.index, 1);
                                            this.setState({ requestDetails: data })
                                        } else {
                                            let data = requestDetails;
                                            this.setState({ requestDetails: data });
                                        }
                                    }
                                }
                                else{
                                    alert('You do not access rights to approve/reject the request')
                                }
                            }}
                        ><i className="fa fa-times" style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}></i></span>
                        
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
            let rejectedActions =
            {
                Header: <b>Reason</b>,
                id:'',
                accessor:'',
                width: 150,
                Cell: (row) => (
                    <div>
                    	<p>Bad Invoice</p>    
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
            let executedActions =
            {
                Header: <b>Executed</b>,
                width: 150,
                Cell: (row) => (
                    <div>
                        <i style={{ width: 35, fontSize: 18, padding: 11}} className="fa fa-envelope-open"></i>
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
        return(
        <Fragment>
        	<div className="swift-outgoing-search">
        		<input type="search" placeholder="Search..."/>
        		<button>Circuit Break</button>
        	</div>
 
            <Tab.Container id="left-tabs-example" defaultActiveKey="undercompliance">
			  <Row>
			    <Col sm={6}>
			      <Nav variant="pills" style={{marginLeft:'2%'}}>
			        <Nav.Item>
			          <Nav.Link eventKey="undercompliance">Under Compliance</Nav.Link>
			        </Nav.Item>
			        <Nav.Item>
			          <Nav.Link eventKey="executed">Executed Payments</Nav.Link>
			        </Nav.Item>
			        <Nav.Item>
			          <Nav.Link eventKey="rejected">Rejected Requests</Nav.Link>
			        </Nav.Item>
			      </Nav>
			    </Col>
			    <Col sm={12}>
			      <Tab.Content>
			        <Tab.Pane eventKey="undercompliance">
			          	<div className="container-fluid">
			                <div className="row">
			                    <div className="col-sm-12">
			                        <div className="card">
			                            <div className="card-body datatable-react sudata-table">
			                            <ReactTable
			                                    data={this.state.staticCompliance}
			                                    columns={[...reusableColumns, undercomplianceActions]}
			                                    SubComponent={
		                                        (v) => 
		                                            <div>
		                                                <UaRequestData id={v.row} /> 
		                                            </div>
		                                        }
			                                />
			                            </div>
			                        </div>
			                    </div>
			                </div>
		            	</div>
			        </Tab.Pane>
			        <Tab.Pane eventKey="rejected">
			        	<div className="container-fluid">
			                <div className="row">
			                    <div className="col-sm-12">
			                        <div className="card">
			                            <div className="card-body datatable-react sudata-table">
			                            <ReactTable
			                                    data={this.state.staticCompliance}
			                                    columns={[...reusableColumns, rejectedActions]}
			                                    SubComponent={
		                                        (v) => 
		                                            <div>
		                                                <UaRequestData id={v.row} /> 
		                                            </div>
		                                        }
			                                />
			                            </div>
			                        </div>
			                    </div>
			                </div>
		            	</div>
			        </Tab.Pane>
			        <Tab.Pane eventKey="executed">
			        	<div className="container-fluid">
			                <div className="row">
			                    <div className="col-sm-12">
			                        <div className="card">
			                            <div className="card-body datatable-react sudata-table">
			                            <ReactTable
			                                    data={this.state.staticCompliance}
			                                    columns={[...reusableColumns, executedActions]}
			                                    SubComponent={
		                                        (v) => 
		                                            <div>
		                                                <UaRequestData id={v.row} /> 
		                                            </div>
		                                        }
			                                />
			                            </div>
			                        </div>
			                    </div>
			                </div>
		            	</div>
			        </Tab.Pane>
			      </Tab.Content>
			    </Col>
			  </Row>
			</Tab.Container>
            
        </Fragment>
        )
    }
}

export default MerchantIncoming;
