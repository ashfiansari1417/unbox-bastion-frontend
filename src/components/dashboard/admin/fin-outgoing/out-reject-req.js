import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Moment from 'react-moment';
// import { ToastContainer, toast } from 'react-toastify';

import { GET_REJECT_REQUEST } from '../../../../constant/actionTypes';
import RequestData from './requestData'

class OutRejectRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        axios.get(GET_REJECT_REQUEST)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
            
        })
    }

    render() {
        const { requestDetails } = this.state;

        const columns = [ 
            {
                Header: 'Beneficiary Name',  
                accessor: 'benbank_name',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Beneficiary SWIFT',  
                accessor: 'benbank_SWIFT',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'From Account',  
                accessor: 'account_number',
                style: {
                    textAlign: 'center'
                } 
            },
            {
                Header: 'Currency',  
                accessor: 'currency',
                width: 100,
                style: {
                    textAlign: 'center'
                }  
            },
            {
                Header: 'Amount',  
                accessor: 'amount',
                cell: row =>
                    <span >

                        {row.amount ? <span>{new Intl.NumberFormat('en-IN', { style: 'currency', currency: row.currency }).format(row.amount)}</span> : <span>-</span>}
                    </span>,
                width: 100,
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Types',  
                accessor: 'client_type',
                width: 100,
                style: {
                    textAlign: 'center'
                }   
            }
        ]

        columns.push( {
            Header: 'Created at',
            id: 'created_at',
            style: {
                textAlign: 'center'
            },
            accessor: (d) => d,
            Cell: (props) => <span>{props.value.created_at? <Moment format="DD/MM/YYYY">{props.value.created_at}</Moment> : '-'}</span>
        })
        // console.log(requestDetails)
        requestDetails.sort(function(a,b){     
            let aVal= a.updated_at == null? (new Date().getTime()) : new Date(a.updated_at);
            let bVal= b.updated_at == null? (new Date().getTime()) : new Date(b.updated_at);
            return aVal - bVal;
          })

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Disapproved Outgoing Requests</h5>
                                </div>
                                <div className="card-body datatable-react">
                                <ReactTable
                                    data={requestDetails}
                                    columns={columns}
                                    defaultPageSize={15}
                                    defaultSorted={[{
                                          id: "created_at",desc: true}]
                                        }
                                    className="-striped -highlight"
                                    showPagination={true}
                                    SubComponent={(v) => <div style={{ padding: '10px' }}>
                                        <RequestData id={v.row}/> </div>}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
export default OutRejectRequest;