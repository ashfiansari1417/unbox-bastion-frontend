import React, { Component, Fragment } from 'react';

class RequestData extends Component {
    constructor(props) {
        super(props); 
        this.state= {
            data : []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.id._original
        })
    }

    render() {
        console.log(this.state.data);
        const { data } = this.state;

        
        return (
            <Fragment>
                { this.state.data ? 
                    <Fragment>
                        <div className="card">
                            <div className="card-body">
                                <div ><strong>Other Details:</strong></div>
                                <span>ID: {data.id}</span>&nbsp;&nbsp;
                                <span>Type of Transfer : {data.type_of_transfer}</span>&nbsp;&nbsp;
                                <span>Message to Operator: {data.message_to_operator}</span>&nbsp;&nbsp;
                                <span>Reference Message: {data.reference_message}</span>
                            </div>
                        </div> 
            
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-6 d-flex">
                                    <div className="card flex-fill">
                                        <div className="card-body border">
                                            <div><strong>Debit Details:</strong></div>
                                            <div>Account Number: {data.from_account}</div>
                                            <div>Currency: {data.currency}</div>
                                            {/* <div>Available Balance: 200</div> */}
                                            <div>Total Amount: {data.amount}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 d-flex">    
                                    <div className="card flex-fill">
                                    <div className="card-body border">
                                            <div><strong>Beneficiary Bank Client:</strong></div>
                                            <div>Client Type: {data.client_type}</div>
                                            <div>Client Name: {data.client_name}</div>
                                            <div>Client Address: {data.client_address}</div>
                                            <div>Client Address 2: {data.client_address2}</div>
                                            <div>Client Country: {data.client_country}</div>
                                            <div>Client Account Number: {data.client_account_number}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <div className="container">
                            <div className="row">
                                <div className="col-sm-6 d-flex">
                                    <div className="card flex-fill">
                                        <div className="card-body border">
                                            <div><strong>Beneficiary Bank Details:</strong></div>
                                            <div>Beneficiary Bank SWIFT: {data.benbank_SWIFT}</div>
                                            <div>Beneficiary Bank Name: {data.benbank_name}</div>
                                            <div>Beneficiary Bank Address: {data.benbank_address}</div>
                                            <div>Beneficiary Bank Location: {data.benbank_location}</div>
                                            <div>Beneficiary Bank Country: {data.benbank_country}</div>
                                            <div>Beneficiary Bank NCS Number: {data.benbank_NCS_number}</div>
                                            <div>Beneficiary ABA RTA: {data.benbank_ABA}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 d-flex">
                                    <div className="card flex-fill">
                                        <div className="card-body border">
                                            <div><strong>Intermediary Bank:</strong></div>
                                            <div>Inter Bank SWIFT: {data.interbank_SWIFT}</div>
                                            <div>Inter Bank Name: {data.interbank_name}</div>
                                            <div>Inter Bank Address: {data.interbank_address}</div>
                                            <div>Inter Bank Location: {data.interbank_location}</div>
                                            <div>Inter Bank Country:{data.interbank_country}</div>
                                            <div>Inter Bank NCS Number: {data.interbank_NCS_number}</div>
                                            <div>Inter Bank ABA RTA: {data.interbank_ABA}</div>
                                            <div>Inter Bank Account Number: {data.interaccount_Number}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </Fragment>
                   : "loading...."
                }  
            </Fragment>
        )
    }
}

export default RequestData;
