import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import RequestData from './requestData'
import Moment from 'react-moment';
// import { ToastContainer, toast } from 'react-toastify';

import { GET_APPROVE_REQUEST } from '../../../../constant/actionTypes';

import Datatable from '../../../common/datatable';

class OutApproveRequest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        axios.get(GET_APPROVE_REQUEST)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
        })
    }

    render() {
        const { requestDetails } = this.state;
        const columns = [ 
            {
                Header: 'Beneficiary Name',  
                accessor: 'benbank_name',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Beneficiary SWIFT',  
                accessor: 'benbank_SWIFT',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'From Account',  
                accessor: 'account_number',
                style: {
                    textAlign: 'center'
                } 
            },
            {
                Header: 'Currency',  
                accessor: 'currency',
                width: 100,
                style: {
                    textAlign: 'center'
                }  
            },
            {
                Header: 'Amount',  
                accessor: 'credit_amount',
                width: 100,
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Types',  
                accessor: 'client_type',
                width: 100,
                style: {
                    textAlign: 'center'
                }   
            }
        ]

        columns.push( {
            Header: 'Created at',
            id: 'created_at',
            sortable: true,
            style: {
                textAlign: 'center'
            },
            accessor: (d) => d,
            Cell: (props) => <span>{props.value.created_at? <Moment format="DD/MM/YYYY">{props.value.created_at}</Moment> : '-'}</span>
            // Cell: (props) => <span>{props.value.created_at? <Moment format="ddd,MMM DD,YYYY">{props.value.created_at}</Moment> : '-'}</span>
        })
        console.log(requestDetails)
        requestDetails.sort(function(a,b){            
            let aVal= a.updated_at == null? (new Date().getTime()) : new Date(a.updated_at);
            let bVal= b.updated_at == null? (new Date().getTime()) : new Date(b.updated_at);
            return aVal - bVal;
          })

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Approved Outgoing Requests</h5>
                                </div>
                                <div className="card-body datatable-react">
                                <ReactTable
                                    data={requestDetails}
                                    columns={columns}
                                    defaultPageSize={15}
                                    className="-striped -highlight"
                                    showPagination={true}
                                    defaultSorted={[{
                                          id: "created_at",desc: true}]
                                        }
                                    SubComponent={(v) => <div style={{ padding: '10px' }}>
                                        <RequestData id={v.row}/> </div>}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default OutApproveRequest;