import React, { Component } from 'react';
import printIcon from '../../../assets/images/payment-history/payment-history.png';
import eurIcon from '../../../assets/images/HomePage/eur.png';
import gbpIcon from '../../../assets/images/gbp.png';
import usaIcon from '../../../assets/images/usaIcon.png';
import russiaIcon from '../../../assets/images/russiaIcon.png';
import pageIcon from '../../../assets/images/HomePage/page.png';
import sepaIcon from '../../../assets/images/sepaIcon.png';
import swiftIcon from '../../../assets/images/payment-history/swifticon.png';
import logo from '../../../assets/images/headerLogoRed.png';
import { OnGetAdminBankBalance } from '../../../actions';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import './styles/AdminHome.css';
import '../defaultCompo/styles/Home.css';

export class AdminHome extends Component {
  state = {
    bankAccounts: [],
    mainAccount: {},
  };
  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });

      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        this.props.OnGetAdminBankBalance(localStorage.getItem('access'));
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.adminBankBalance !== this.props.adminBankBalance) {
      if (this.props.adminBankBalance.Currencies)
        this.setState({
          bankAccounts: this.props.adminBankBalance.Currencies,
        });
      if (this.props.adminBankBalance.CurrencyMain)
        this.setState({
          mainAccount: this.props.adminBankBalance.CurrencyMain,
        });
    }
  }
  accountCards = (accountCardsData) => {
    return accountCardsData.map((item, index) => {
      return (
        <React.Fragment>
          <div className="row">
            <div
              style={{ height: 130 }}
              // onClick={() => this.changeCurrency(index)}
              className="card"
            >
              <div
                className={`card-body text-center ${
                  item.active ? 'current-card' : ''
                }`}
                style={{
                  background: item.active ? '#f3e3ff' : '#fff',
                  cursor: 'pointer',
                }}
              >
                <div className="row">
                  <div className="col-md-6 ">
                    <div className="row ">Total Funds</div>
                    <div className="row pt-2">
                      <img
                        src={
                          item.currency === 'EUR'
                            ? eurIcon
                            : item.currency === 'USD'
                            ? usaIcon
                            : item.currency === 'RUB'
                            ? russiaIcon
                            : gbpIcon
                        }
                      />
                      <span className="pl-2">{item.currency}</span>
                    </div>
                  </div>
                  <div className="col-md-6 ">
                    <div className="row pull-right">
                      <img src={pageIcon} />
                    </div>
                    <div className="row pt-4 pull-right">
                      <img src={item.sepaIcon ? sepaIcon : swiftIcon} />
                      <span className="pl-1">
                        {item.sepaIcon ? '' : 'Swift'}
                      </span>
                    </div>
                  </div>
                </div>

                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span style={{ textAlign: 'left' }}>Total Balance</span>
                    <span>{item.grand_total_balance}</span>
                  </div>
                </div>
                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span style={{ textAlign: 'left' }}>Available Balance</span>
                    <span>{item.grand_available_balance}</span>
                  </div>
                </div>
                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span style={{ textAlign: 'left' }}>Blocked Balance</span>
                    <span>{item.grand_bloacked_balance}</span>
                  </div>
                </div>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}></div>
            </div>
          </div>
        </React.Fragment>
      );
    });
  };

  render() {
    const { mainAccount, bankAccounts } = this.state;
    return (
      <div className="admin-home-container">
        {/* <div className="carousel-home"> */}
        {this.props.isloading ? (
          <div style={{ height: '100vh' }}>
            <div className="customLoader-ring" id="spinner-id">
              <div></div>
              <div></div>
            </div>
          </div>
        ) : (
          <div>
            <div className="row header-body-content my-0">
              <div className="main-account-card">
                <h4 style={{ textAlign: 'left' }}>Main Account</h4>
                <div className="main-account-card-display">
                  <div className="main-account-car-header-inner">
                    <img
                      src={logo}
                      alt="company-logo"
                      className="companyLogo"
                    />
                    <h5>Sterlings Payment Services</h5>
                  </div>
                  {/* <h4 style={{ textAlign: 'left' }}>Main Account</h4> */}
                  <div>
                    <img src={swiftIcon} className="swift-logo-main" />
                    <span className="pl-1">Swift</span>
                  </div>
                </div>
                <div className="main-account-card-display balance-block-container">
                  <div className="balance-block">
                    <span>Total Balance</span>
                    <span className="balance-block-amount">
                      € {mainAccount && mainAccount.total_balance}
                    </span>
                  </div>
                  <div className="balance-block">
                    <span>Available Balance</span>
                    <span className="balance-block-amount">
                      € {mainAccount && mainAccount.available_balance}
                    </span>
                  </div>
                  <div className="balance-block">
                    <span>Blocked Balance</span>
                    <span className="balance-block-amount">
                      € {mainAccount && mainAccount.bloacked_balance}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="row header-body-content my-0 admin-currency-card-container ">
              {this.accountCards(this.state.bankAccounts)}
            </div>
          </div>
        )}

        {/* </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  // const { getAccounts, transactions } = state;
  // return { getAccounts, transactions };
  adminBankBalance: state.adminReducer.adminBankBalance,
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToProps, {
  OnGetAdminBankBalance,
})(AdminHome);
