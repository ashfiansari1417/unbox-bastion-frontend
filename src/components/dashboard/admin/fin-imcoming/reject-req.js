import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
// import RequestData from './requestData'
// import { ToastContainer, toast } from 'react-toastify';
import InRequestData from './inRequestData';
import Moment from 'react-moment';
import { GET_FinIncomingRejectRequest } from '../../../../constant/actionTypes';

class InDisapproved extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        console.log('call');
        axios.get(GET_FinIncomingRejectRequest)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
        })
    }

    render() {
        const { requestDetails } = this.state;
        console.log(requestDetails)
        const columns = [ 
            {
                Header: 'Date',  
                id: 'Date',
                accessor: (d) => d,
                Cell: (props) => <span>{props.value.Date? <Moment format="DD/MM/YYYY">{props.value.Date}</Moment> : '-'}</span>
            },
            {
                Header: 'Transfer Id',  
                accessor: 'id',
                style: {
                    textAlign: 'center'
                }   
            },
            // {
            //     Header: 'Client Type',  
            //     accessor: 'group1_transactionType',
            //     style: {
            //         textAlign: 'center'
            //     } 
            // },
            // {
            //     Header: 'Client Name',  
            //     accessor: 'group1_sequenceNo',
            //     style: {
            //         textAlign: 'center'
            //     }  
            // },
            {
                Header: 'Client Account',  
                accessor: 'Account_No',
                style: {
                    textAlign: 'center'
                }   
            },
            // {
            //     Header: 'Beneficiary Client Name',  
            //     accessor: 'group5_CHK',
            //     style: {
            //         textAlign: 'center'
            //     }   
            // },
            {
                Header: 'Beneficiary Bank Swift',  
                accessor: 'group4_52A',
               
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Amount',  
                accessor: 'group4_33B_Amount',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Currency',  
                accessor: 'group4_32A_CurrencyCode',
                style: {
                    textAlign: 'center'
                }   
            }
        ]
        requestDetails.sort(function(a,b){
            let aVal= a.Date == null? (new Date().getTime()) : new Date(a.Date);
            let bVal= b.Date == null? (new Date().getTime()) : new Date(b.Date);
            return aVal - bVal;
          })
        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Incoming Disapproved Requests</h5>
                                </div>
                                <div className="card-body datatable-react">
                                <ReactTable
                                    data={requestDetails}
                                    columns={columns}
                                    defaultPageSize={15}
                                    className="-striped -highlight"
                                    showPagination={true}
                                    
                                    defaultSorted={[{
                                        id: "Date",desc: true}]
                                      }
                                    SubComponent={
                                        (v) => 
                                            <div style={{ padding: '10px'}}>
                                                <InRequestData id={v.row} /> 
                                            </div>
                                        }
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default InDisapproved;
