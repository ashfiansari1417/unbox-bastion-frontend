import React, { Component, Fragment } from 'react';
import axios from 'axios';

import { GET_FinIncoming } from '../../../../constant/actionTypes';

import Datatable from '../../../common/inDatatable';


class InPending extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }
    
    UNSAFE_componentWillMount () {
        // this.getTransferRequest();
    }
    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        axios.get(GET_FinIncoming)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
        })
    }
    render() {
        const { requestDetails } = this.state;
        requestDetails.sort(function(a,b){
            let aVal= a.group4_32A_Date == null? (new Date()) : new Date((a.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'20$1-$2-$3'));
            let bVal= b.group4_32A_Date == null? (new Date()) : new Date((b.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'20$1-$2-$3'));
            return aVal - bVal;
          })
        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Incoming Pending Requests</h5>
                                </div>
                                <div className="card-body datatable-react">
                                    <Datatable
                                        history={this.props.history}
                                        multiSelectOption={false}
                                        myData={requestDetails}
                                        pageSize={10}
                                        pagination={true}
                                        class="-striped -highlight"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default InPending;