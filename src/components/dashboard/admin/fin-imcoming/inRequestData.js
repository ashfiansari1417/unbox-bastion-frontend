import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';

class InRequestData extends Component {
    constructor(props) {
        super(props);
        this.state= { 
            data : []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.id._original
        })
    }

    render() {
        const { data } = this.state;
        return (
            <Fragment>
                { this.state.data ? 
                    <Fragment>
                        <div className="container">
                            <div className="dtDpValWrapper row">
                                <div className="col-12 all-info">    
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>All Information:</strong></div>
                                            <div>id: <span className="dtDpVal">{data.id}</span></div>
                                            <div>group4_32A_Date: <span className="dtDpVal">{data.group4_32A_Date? <Moment format="DD/MM/YYYY">{(data.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span></div>
                                            <div>Account_No <span className="dtDpVal">{data.Account_No}</span></div>
                                            <div>Event ID: <span className="dtDpVal">{data['Event ID']}</span></div>
                                            <div>Event Name: <span className="dtDpVal">{data['Event Name']}</span></div>
                                            <div>group1_LTCode: <span className="dtDpVal">{data.group1_LTCode}</span></div>
                                            <div>group1_branchCode: <span className="dtDpVal">{data.group1_branchCode}</span></div>
                                            <div>group1_senderBIC: <span className="dtDpVal">{data.group1_senderBIC}</span></div>
                                            <div>group1_sequenceNo: <span className="dtDpVal">{data.group1_sequenceNo}</span></div>
                                            <div>group1_sessionNo: <span className="dtDpVal">{data.group1_sessionNo}</span></div>
                                            <div>group1_transactionType: <span className="dtDpVal">{data.group1_transactionType}</span></div>
                                            <div>group1_typeOfMessage: <span className="dtDpVal">{data.group1_typeOfMessage}</span></div>
                                            <div>group2: <span className="dtDpVal">{data.group2}</span></div>
                                            <div>group2_I/O":  <span className="dtDpVal">{data['group2_I/O']}</span></div>
                                            <div>group2_MT_type: <span className="dtDpVal">{data.group2_MT_type}</span></div>
                                            <div>group3_121: <span className="dtDpVal">{data.group3_121}</span></div>
                                            <div>group4_20: <span className="dtDpVal">{data.group4_20}</span></div>
                                            <div>group4_23B: <span className="dtDpVal">{data.group4_23B}</span></div>
                                            <div>group4_32A_Amount: <span className="dtDpVal">{data.group4_32A_Amount}</span></div>
                                            <div>group4_32A_CurrencyCode: <span className="dtDpVal">{data.group4_32A_CurrencyCode}</span></div>
                                            <div>group4_32A_comma: <span className="dtDpVal">{data.group4_32A_comma}</span></div>
                                            <div>group4_33B_Amount: <span className="dtDpVal">{data.group4_33B_Amount}</span></div>
                                            <div>group4_33B_CurrencyCode: <span className="dtDpVal">{data.group4_33B_CurrencyCode}</span></div>
                                            <div>group4_33B_comma: <span className="dtDpVal">{data.group4_33B_comma}</span></div>
                                            <div>group4_50F: <span className="dtDpVal">{data.group4_50F}</span></div>
                                            <div>group4_52A: <span className="dtDpVal">{data.group4_52A}</span></div>
                                            <div>group4_53B: <span className="dtDpVal">{data.group4_53B}</span></div>
                                            <div>group4_57A: <span className="dtDpVal">{data.group4_57A}</span></div>
                                            <div>group4_59: <span className="dtDpVal">{data.group4_59}</span></div>
                                            <div>group4_70: <span className="dtDpVal">{data.group4_70}</span></div>
                                            <div>group4_71A: <span className="dtDpVal">{data.group4_71A}</span></div>
                                            <div>group5_CHK: <span className="dtDpVal">{data.group5_CHK}</span></div>
                                            <div>group5_MAC: <span className="dtDpVal">{data.group5_MAC}</span></div>
                                        </div>
                                    </div>
                                </div> 
                                {/* <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Client Details:</strong></div>
                                            <div>Client Type:  <span className="dtDpVal">{data.client_type}</span></div>
                                            <div>Client Name:  <span className="dtDpVal">{data.client_name}</span></div>
                                            <div>Client Address: <span className="dtDpVal">{data.client_address}</span></div>
                                            <div>Client Address2: <span className="dtDpVal">{data.client_address2}</span></div>
                                            <div>Client Account: <span className="dtDpVal">{data.client_account_number}</span></div>
                                            <div>Client Country: <span className="dtDpVal">{data.client_country}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                    <div className="card-body border">
                                            <div><strong>Transfer Details:</strong></div>
                                            <div>Transfer ID: <span className="dtDpVal">{data.id}</span></div>
                                            <div>Currency: <span className="dtDpVal">{data.currency}</span></div>
                                            <div>Amount: <span className="dtDpVal">{data.amount}</span></div>
                                            <div>Date: <span className="dtDpVal">{data.group4_32A_Date? <Moment format="DD/MM/YYYY">{(data.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span></div>
                                            <div>Transfer Type: <span className="dtDpVal">{data.type_of_transfer}</span></div>
                                            <div>Status: <span className="dtDpVal">-</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Beneficiary Bank Details:</strong></div>
                                            <div>Beneficiary Bank SWIFT: <span className="dtDpVal">{data.benbank_SWIFT}</span></div>
                                            <div>Beneficiary Bank Name: <span className="dtDpVal">{data.benbank_name}</span></div>
                                            <div>Beneficiary Bank Address: <span className="dtDpVal">{data.benbank_address}</span></div>
                                            <div>Beneficiary Bank Location: <span className="dtDpVal">{data.benbank_location}</span></div>
                                            <div>Beneficiary Bank Country: <span className="dtDpVal">{data.benbank_country}</span></div>
                                            <div>Beneficiary Bank NCS Number: <span className="dtDpVal">{data.benbank_NCS_number}</span></div>
                                            <div>Beneficiary ABA RTA: <span className="dtDpVal">{data.benbank_ABA}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Intermediary Bank Details:</strong></div>
                                            <div>Inter Bank SWIFT: <span className="dtDpVal">{data.interbank_SWIFT}</span></div>
                                            <div>Inter Bank Name: <span className="dtDpVal">{data.interbank_name}</span></div>
                                            <div>Inter Bank Address: <span className="dtDpVal">{data.interbank_address}</span></div>
                                            <div>Inter Bank Location: <span className="dtDpVal">{data.interbank_location}</span></div>
                                            <div>Inter Bank Country: <span className="dtDpVal">{data.interbank_country}</span></div>
                                            <div>Inter Bank NCS Number: <span className="dtDpVal">{data.interbank_NCS_number}</span></div>
                                            <div>Inter Bank ABA RTA: <span className="dtDpVal">{data.interbank_ABA}</span></div>
                                            <div>Inter Bank Account Number: <span className="dtDpVal">{data.interaccount_Number}</span></div>
                                        </div>
                                    </div>
                                </div> */}
                                {/* <div className="col-sm-12">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Reference Message:</strong></div>
                                            <div>
                                                <span className="dtDpVal">
                                                    {data.reference_message}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>    
                    </Fragment>
                   : "loading...."
                }  
            </Fragment>
        )
    }
}

export default InRequestData;
