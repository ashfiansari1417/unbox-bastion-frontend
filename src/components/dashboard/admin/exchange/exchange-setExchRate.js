import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import { ToastContainer, toast } from 'react-toastify';
import { setExchangeRate,getExchangeRates } from '../../../../constant/actionTypes';

class ExcSetRate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: [],
            outExchRate: '',
            outExchTo: '',
            outExchFrom: '',
            step1Msg: '',
            email:''
        }
    }
    componentDidMount () {
        this.getTransferRequest();
        let localStg=JSON.parse(localStorage.getItem("admin"));
        // console.log(localStg.email)
        this.setState({email: localStg.email});
    }
    getTransferRequest = () => {
        axios.get(getExchangeRates)
            .then( res => {
                // console.log('call');
                // console.log(res);
                // console.log(res.data);
                // console.log(res.data.result.Items);
                let requestDetailsvals=res.data.result.Items;
                requestDetailsvals.forEach(function(obj,index){
                    let newval=obj.currency;
                    let newArr=newval.split('_');
                    obj.currency=newArr[0];
                    obj.currency1=newArr[1];
                  })
                this.setState({
                    requestDetails: requestDetailsvals
                })
            })
    }
    makeExchangeChnaged = (event) => {
        let name = event.target.name;
        let val = event.target.value;
        if(name == 'exchRate'){
            if (val != '0' && val != '0.' && val != '.' && val != '.0' && val != '0.0' && val != '0.00' && val != '0.000' && val != '' && !Number(val)) {
                alert("Input must be a number");
                event.target.value='';
            }
            this.setState({outExchRate: val,step1Msg: '',step2Msg: ''});
        }
        else if(name == 'exchAmt'){
            if (val != '0' && val != '0.' && val != '.' && val != '.0' && val != '0.0' && val != '0.00' && val != '0.000' && val != '' && !Number(val)) {
                alert("Input must be a number");
                event.target.value='';
            }
            this.setState({outExchAmt: val,step1Msg: '',step2Msg: ''});
        }
        else if(name == 'exchCurTo')
        this.setState({outExchTo: val,step1Msg: '',step2Msg: ''});
        else if(name == 'exchCurrFrm'){
            this.setState({outExchFrom: val,step1Msg: '',step2Msg: ''});
            let newVal=event.target.value;
            document.querySelectorAll('#exchCurTo option').forEach(function(userItem) {
                userItem.removeAttribute('disabled');
            })
            document.querySelectorAll('#exchCurTo option').forEach(function(userItem) {
                if(userItem.value == newVal){
                    userItem.setAttribute('disabled',true);
                }
            })
        }
    }

    makeChangeClick = (event) => {
        const {requestDetails,outExchTo,outExchFrom,outExchRate,email}=this.state;
        let $this=this,$target=event.target;
        
        if(outExchTo == outExchFrom && outExchTo != '' && outExchFrom != ''){
            this.setState({
                step1Msg: 'Select two different currencies.'
            })
        }
        else if(outExchTo !== '' && outExchFrom !== '' && outExchRate !== '')
        {
            event.target.classList.add('spin');
            this.setState({
                step1Msg: '',
              });
              axios.post(setExchangeRate, {
                "from_currency": outExchFrom,
                "to_currency": outExchTo,
                "rate": outExchRate,
                "update_by": email 
              })
              .then(function (response) {
                    console.log(response)
                    toast.success('Updated Successfully!');

                    $this.getTransferRequest();

                    $this.setState({
                        // requestDetails: newData,
                        outExchRate: '',
                        outExchTo: '',
                        outExchFrom: '',
                        step1Msg: '',
                      });
                    document.getElementById("saetExchangeRateForm").reset();
                    $target.classList.remove('spin');
              })
              .catch(function (error) {
                // console.log(error)
                  toast.error("Something went wrong");
                  $target.classList.remove('spin');
              })
        }
        else{
            this.setState({
                step1Msg: 'All  fields are mandatory.'
            })
        }
       }
    handleSubmit=(event)=>{
        event.preventDefault();
    }
    // handleSelectChange=(event)=>{
        
    // }
    render() {
        const { requestDetails } = this.state;
        console.log(requestDetails);
        
        const columns = [ 
            {
                Header: 'Currency From',  
                accessor: 'currency',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Currency To',
                accessor: 'currency1',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Rate ',  
                accessor: 'rate',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Updated By ',  
                accessor: 'update_by',
                style: {
                    textAlign: 'center'
                }   
            }
        ];

        return(
            <Fragment>
                <div className="container-fluid ">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Set Exchange Rate</h5>
                                </div>
                                <div className="card-body datatable-react sudata-table">
                                    <form onSubmit={this.handleSubmit} id="saetExchangeRateForm" className="custom-form theme-form ">
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="cs-label">Currency From </label>
                                                    <select name='exchCurrFrm' className="form-control" onChange={this.makeExchangeChnaged}>
                                                        <option value="">Currency From</option>
                                                        <option value="RUB">RUB</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="GBP">GBP</option>
                                                        <option value="USD">USD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="cs-label">Currency To </label>
                                                    <select id="exchCurTo" name='exchCurTo' className="form-control" onChange={this.makeExchangeChnaged}>
                                                        <option value="">Currency To</option>
                                                        <option value="RUB">RUB</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="GBP">GBP</option>
                                                        <option value="USD">USD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="cs-label" htmlFor="convertionRate">Rate </label>
                                                    <input type='text' name='exchRate' id="convertionRate" placeholder="Rate" className="form-control" onChange={this.makeExchangeChnaged}  />
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <button className="btn btn-primary" type="button" name="makeChange" onClick={this.makeChangeClick}><i className="fa fa-spinner fa-spin"></i> Set Exchange Rate</button>
                                                &nbsp;&nbsp;<i style={{color: 'rgb(228, 86, 110)'}}>{this.state.step1Msg}</i>
                                            </div>
                                        </div>
                                    </form>
                                    <br/><br/>
                                    <ReactTable
                                        data={requestDetails}
                                        columns={columns}
                                        defaultPageSize={5}
                                        className="-striped -highlight"
                                        showPagination={true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default ExcSetRate;
