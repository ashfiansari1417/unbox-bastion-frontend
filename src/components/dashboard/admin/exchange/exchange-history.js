import React, { Component, Fragment } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';



var columns;


class TransactionLog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetailsStatic: [
                {id: 101,date: "19/02/2020",currency : "USD",currency1: "EUR",rate : '10',},
                {id: 111,date: "21/02/2020",currency : "USD",currency1: "EUR",rate : '5',},
                {id: 121,date: "15/02/2020",currency : "USD",currency1: "EUR",rate : '15',}
            ]
        }
    }
    componentDidMount () {
       
    }

    gotoTransferForm = () => {
        this.props.history.push('./transfer-form');
    }

    render() {
        const { userDetailsStatic } = this.state;
        columns = [ 
            {
                Header: 'Date',  
                accessor: 'date',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Id',  
                accessor: 'id',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Currency From',  
                accessor: 'currency',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Currency To',
                accessor: 'currency1',
                style: {
                    textAlign: 'center'
                }
            },
            {
                Header: 'Rate ',  
                accessor: 'rate',
                style: {
                    textAlign: 'center'
                }   
            }
        ]
        return(
        <Fragment>
            <div className="container-fluid ">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header span">
                                <h5>Exchanged Transactions</h5>
                            </div>
                            <div className="card-body datatable-react sudata-table">
                                <ReactTable
                                    data={userDetailsStatic}
                                    columns={columns}
                                    defaultPageSize={10}
                                    className="-striped -highlight"
                                    defaultSorted={[{
                                        id: "date",desc: true}]
                                      }
                                    showPagination={true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}

export default TransactionLog;