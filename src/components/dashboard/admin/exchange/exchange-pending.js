import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { Get_PendingCurrencyExchange,ExchangeApproveDisapprove } from '../../../../constant/actionTypes';

import Moment from 'react-moment';

import ReactTable from 'react-table';
import 'react-table/react-table.css'; 
import ExchRequestData from './exchRequestData';



class ExcPending extends Component {
    constructor(props){
        super(props)
        this.state={
            requestDetails: [],
            show: false,
            show1: false,
            riskLevel_options: null,
            optError: false
        }
    }
    rejectRequest = (id) => {
        axios.post( ExchangeApproveDisapprove, {
            "id": id,
            "status": 'rejected'
          })
          .then(function (response) {
                // console.log(response.data.message)
                toast.success("Updated successfully!")
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }

    approveRequest = (id) => {
        axios.post( ExchangeApproveDisapprove, {
            "id": id,
            "status": 'approved'
          })
          .then(function (response) {
            // console.log(response.data.message)
            toast.success("Updated successfully!")
          })
          .catch(function (error) {
              toast.error("Something went wrong");
          })
    }

    componentDidMount () {
        this.getTransferRequest();
    }
    getTransferRequest = () => {
        axios.get(Get_PendingCurrencyExchange)
        .then( res => {
            this.setState({
                requestDetails: res.data.result.Items
            })
        })
    }
    render() {
        const { requestDetails } = this.state;
        // console.log(requestDetails);
        const column = [
            {
                Header: 'Date',  
                id: 'date',
                style: {
                    textAlign: 'center'
                }   ,
                accessor: (d) => d,
                Cell: (props) => <span>{props.value.date? <Moment format="DD/MM/YYYY">{props.value.date}</Moment> : '-'}</span>
            },
            {
                Header: 'Id',  
                accessor: 'id',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Added Amount',  
                accessor: 'adding_amount',
                style: {
                    textAlign: 'center'
                }   ,
                Cell: (props) =><span>{new Intl.NumberFormat('en-US',{ minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(props.value) }</span> 
            },
            {
                Header: 'Deducted Amount',  
                accessor: 'deducting_amount',
                style: {
                    textAlign: 'center'
                }   ,
                Cell: (props) =><span>{new Intl.NumberFormat('en-US',{ minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(props.value) }</span>
            },
            {
                Header: <b>Action</b>,
                id: 'delete',
                accessor: str => "delete",
                width: 150,
                Cell: (row) => (
                    <div>
                        <span 
                            title="Approve Request"
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to approve request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.approveRequest(row.original.id);
                                        let data = requestDetails;
                                        data.splice(row.index, 1);
                                        this.setState({ requestDetails: data })
                                    } else {
                                        let data = requestDetails;
                                        this.setState({ requestDetails: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-check" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}></i></span>
                        <span 
                            title="Reject Request" 
                            style={{ cursor: 'pointer' }}
                            onClick={() => {                   
                                var d = window.confirm('Are you sure you want to reject request?');
                                if(row.original.id) {
                                    if (d == true) {
                                        this.rejectRequest(row.original.id);
                                        let data = requestDetails;
                                        data.splice(row.index, 1);
                                        this.setState({ requestDetails: data })
                                    } else {
                                        let data = requestDetails;
                                        this.setState({ requestDetails: data });
                                    }
                                }
                            }}
                        ><i className="fa fa-times" style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}></i></span>
                    </div>
                ),
                style: {
                    textAlign: 'center'
                },
                sortable: false
            }
        ] 
        requestDetails.sort(function(a,b){
            let aVal= a.date == null? (new Date().getTime()) : new Date(a.date);
            let bVal= b.date == null? (new Date().getTime()) : new Date(b.date);
            return aVal - bVal;
          })
        return(
        <Fragment>
            <div className="container-fluid ">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header span">
                                <h5>Currency Exchange Pending Requests</h5>
                            </div>
                            <div className="card-body datatable-react sudata-table">
                            <ReactTable
                                    data={requestDetails}
                                    columns={column}
                                    defaultSorted={[{
                                          id: "date",desc: true}]
                                        }
                                    SubComponent={
                                        (v) => 
                                            <div style={{ padding: '10px'}}>
                                                <ExchRequestData id={v.row} /> 
                                            </div>
                                        }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}


export default ExcPending;
