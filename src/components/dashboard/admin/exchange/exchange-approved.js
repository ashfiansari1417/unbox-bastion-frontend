import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
// import RequestData from './requestData'
// import { ToastContainer, toast } from 'react-toastify';
import ExchRequestData from './exchRequestData';
import Moment from 'react-moment';
import { Get_ApprovedCurrencyExchange } from '../../../../constant/actionTypes';

class ExcApproved extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: []
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        console.log('call');
        axios.get(Get_ApprovedCurrencyExchange)
        .then( res => {
            // console.log(res.data.result.Items)
            let allRecords=res.data.result.Items;
            let newRecords=[];
            allRecords.forEach(function(elem,index){
                newRecords.push(elem.record)
            })
            newRecords.sort(function(a,b){
                let aVal= a.date == null? (new Date().getTime()) : new Date(a.date);
                let bVal= b.date == null? (new Date().getTime()) : new Date(b.date);
                return aVal - bVal;
              })
            this.setState({
                requestDetails: newRecords
            })
        })
    }

    render() {
        const { requestDetails } = this.state;
        // console.log('requestDetails')
        // console.log(requestDetails)
        const columns = [ 
            {
                Header: 'Date',  
                id: 'date',
                style: {
                    textAlign: 'center'
                }   ,
                accessor: (d) => d,
                Cell: (props) => <span>{props.value.date? <Moment format="DD/MM/YYYY">{props.value.date}</Moment> : '-'}</span>
                // Cell: (props) => <span>{props.value.group4_32A_Date? <Moment format="YYYY/MM/DD">{(props.value.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span>
                // Cell: (props) => <span>{props.value.group4_32A_Date? <Moment format="YYYY/MM/DD">{props.value.group4_32A_Date}</Moment> : '-'}</span>
            },
            {
                Header: '   Id',  
                accessor: 'id',
                style: {
                    textAlign: 'center'
                }   
            },
            {
                Header: 'Adding Amount',  
                accessor: 'adding_amount',
                style: {
                    textAlign: 'center'
                }    ,
                Cell: (props) =><span>{new Intl.NumberFormat('en-US',{ minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(props.value) }</span>
            },
            {
                Header: 'Deducting Amount',  
                accessor: 'deducting_amount',
               
                style: {
                    textAlign: 'center'
                }    ,
                Cell: (props) =><span>{new Intl.NumberFormat('en-US',{ minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(props.value) }</span>
            }
        ];
        console.log(typeof requestDetails)
        requestDetails.sort(function(a,b){
            let aVal= a.date == null? (new Date().getTime()) : a.date;
            let bVal= b.date == null? (new Date().getTime()) : b.date;
            return parseInt(aVal) - parseInt(bVal);
          })
          console.log(requestDetails)
        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Currency Exchange Approved Clients</h5>
                                </div>
                                <div className="card-body datatable-react">
                                <ReactTable
                                    data={requestDetails}
                                    columns={columns}
                                    defaultPageSize={15}
                                    className="-striped -highlight"
                                    showPagination={true}
                                    defaultSorted={[{
                                        id: "date",desc: true}]
                                      }
                                    SubComponent={
                                        (v) => 
                                            <div style={{ padding: '10px'}}>
                                                <ExchRequestData id={v.row} /> 
                                            </div>
                                        }
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
export default ExcApproved;
