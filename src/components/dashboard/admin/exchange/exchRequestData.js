import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import {Form, Button, Col} from 'react-bootstrap';
import { CustomInput, Input } from 'reactstrap';
import '../client-accounts/uarequestdata.scss'

class UaRequestData extends Component {
    constructor(props) {
        super(props);
        this.state= { 
            data : []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.id._original
        })
    }

    render() {
        const { data } = this.state;
        console.log(data)
        
        return (
            <Fragment>
                { this.state.data ? 
                    <Fragment>
                            <div className="dtDpValWrapper row" style={{borderRadius:'20px', backgroundColor:'#f9f9f9'}}>
                                <div className="col-12 all-info">    
                                    <div className="card">
                                        <h5>Transaction ID : XXXX XXXX XXXX</h5>
                                        <div className="card-inner">
                                            <div className="column-styling">
                                                <label><strong> -------Sender------- </strong></label>
                                                <div><strong>Company Name:</strong>  <span className="dtDpVal">{data.client_name || 'Sterling Payment'}</span></div>
                                                <div><strong>Company Country:</strong> <span className="dtDpVal">{data.client_country || 'Hong Kong'}</span></div>
                                            </div>
                                            <div className="column-styling">
                                                <br/>
                                                <div><strong>Account Number:</strong>  <span className="dtDpVal">{data.client_name || 'XYZXYZXYZ'}</span></div>
                                                <div><strong>Amount Deducted:</strong> <span className="dtDpVal">{data.client_country || '$ XXX'}</span></div>
                                            </div>
                                            <div className="column-styling">
                                                <label><strong> -------Receiving Amount------- </strong></label>
                                                <div><strong>Account Number :</strong> <span>XXXX XXXX XXXX</span></div>
                                                <div><strong>Amount Received:</strong> <span className="dtDpVal">{data.client_country || '$ XXX'}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </Fragment>
                   : "loading...."
                }  
            </Fragment>
        )
    }
}

export default UaRequestData;
