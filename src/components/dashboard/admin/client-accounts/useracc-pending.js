import React, { Component, Fragment } from 'react';
import axios from 'axios';
import {
  GetNewUserSignupDetails,
  NewUserRequest,
} from '../../../../constant/actionTypes';
import { Tab, Row, Col, Nav } from 'react-bootstrap';

import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import UaRequestData from './uaRequestData';

// var columns;
class UaPending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestDetails: [],
      isAdmin: false,
      optError: false,
    };
  }

  rejectRequest = (id) => {
    axios
      .post(NewUserRequest, {
        id: id,
        status: 'rejected',
      })
      .then(function (response) {
        console.log(response);
        toast.success('Updated successfully!');
      })
      .catch(function () {
        toast.error('Something went wrong');
      });
  };
  approveRequest = (id) => {
    axios
      .post(NewUserRequest, {
        id: id,
        status: 'approved',
      })
      .then(function (response) {
        console.log(response);
        // if(response.data.value)
        toast.success('Updated successfully!');
      })
      .catch(function () {
        toast.error('Something went wrong');
      });
  };

  componentDidMount() {
    this.getTransferRequest();
    // let selector =
    //   '#' +
    //   this.props.location.pathname.split('/')[2].split('-')[1].toLowerCase();
    // document.querySelector(selector).style['background-color'] = '#a9a9a9';
  }
  getTransferRequest = () => {
    axios
      .post(GetNewUserSignupDetails, {
        type: 0,
      })
      .then((res) => {
        console.log(res);
        this.setState({
          requestDetails: res.data.value,
        });
      });
  };
  handleHeaderClick = () => {
    if (this.state.isAdmin) {
      let checkboxes = document.querySelectorAll('.rowCheckbox');
      checkboxes.forEach((c) => (c.checked = !c.checked));
    }
  };
  changeColor = (value, e) => {
    if (e.target.style.color === 'rgb(0, 0, 0)') {
      alert('You have already preapprove/prereject the user');
    } else {
      var ans = window.confirm(`Are you sure you want to ${value} request?`);

      if (ans) {
        if (value === 'preapprove') {
          if (
            document.querySelector(e.target.attributes['data-target'].value)
              .style.color === 'rgb(0, 0, 0)'
          ) {
            alert('Request already prerejected');
          } else {
            e.target.style.color = '#000';
            document
              .querySelector(e.target.attributes['data-target'].value)
              .classList.add('disabled');
          }
        } else if (value === 'prereject') {
          if (
            document.querySelector(e.target.attributes['data-target'].value)
              .style.color === 'rgb(0, 0, 0)'
          ) {
            alert('Request already preapproved');
          } else {
            let reason = prompt(
              'Please tell us why are you pre-rejecting this user?'
            );
            if (reason !== '' && reason !== null) {
              var div = document.createElement('p');
              div.style.color = '#ff0000';
              div.style.height = '3em';
              div.innerHTML = reason;
              e.currentTarget.appendChild(div);
              document
                .querySelector(e.target.attributes['data-target'].value)
                .classList.add('disabled');
              e.target.style.color = '#000';
            }
          }
        }
      }
    }
  };
  render() {
    const { requestDetails } = this.state;
    const column = [
      {
        Header: (
          <input
            type="checkbox"
            disabled={!this.state.isAdmin}
            onChange={() => {
              this.handleHeaderClick();
            }}
          />
        ),
        accessor: '',
        width: 50,
        Cell: () => (
          <div>
            <input
              type="checkbox"
              className="rowCheckbox"
              disabled={!this.state.isAdmin}
            />
          </div>
        ),
        style: {
          textAlign: 'center',
        },
        sortable: false,
      },
      {
        Header: (
          <b>
            Company Name <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'company_name',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company type <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'id',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Country <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'companys_country',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Business Type <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'type_of_business',
        style: {
          textAlign: 'center',
        },
      },
    ];
    column.push({
      Header: <b>Risk Type</b>,
      id: 'risk',
      accessor: 'risk',
      width: 150,
      Cell: () => (
        <div>
          <select>
            <option>Zero</option>
            <option>Low</option>
            <option>Low Digi</option>
            <option>Medium</option>
            <option>Upper</option>
            <option>Upper Special</option>
            <option>High Digi</option>
          </select>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    });
    column.push({
      Header: <b>Action</b>,
      id: 'delete',
      accessor: () => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span
            title="Preapprove Request"
            style={{ cursor: 'pointer' }}
            onClick={this.changeColor.bind(this, 'preapprove')}
          >
            <i
              id={`preapprove${row.original.id}`}
              className="fa fa-check-circle"
              data-target={`#prereject${row.original.id}`}
              style={{ width: 35, fontSize: 18, padding: 11, color: '#ffff00' }}
            ></i>
          </span>

          <span
            title="Approve Request"
            className={this.state.isAdmin ? '' : 'disabled'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              if (this.state.isAdmin) {
                var d = window.confirm(
                  'Are you sure you want to approve request?'
                );
                if (row.original.id) {
                  if (d === true) {
                    this.approveRequest(row.original.id);
                    let data = requestDetails;
                    data.splice(row.index, 1);
                    this.setState({ requestDetails: data });
                  } else {
                    let data = requestDetails;
                    this.setState({ requestDetails: data });
                  }
                }
              } else {
                alert(
                  'You do not have access rights to approve/reject the request'
                );
              }
            }}
          >
            <i
              className="fa fa-check"
              style={{
                width: 35,
                fontSize: 16,
                padding: 11,
                color: 'rgb(40, 167, 69)',
              }}
            ></i>
          </span>
          <span
            title="Reject Request"
            style={{ cursor: 'pointer' }}
            className={this.state.isAdmin ? '' : 'disabled'}
            onClick={() => {
              if (this.state.isAdmin) {
                var d = window.confirm(
                  'Are you sure you want to reject request?'
                );
                if (row.original.id) {
                  if (d === true) {
                    this.rejectRequest(row.original.id);
                    let data = requestDetails;
                    data.splice(row.index, 1);
                    this.setState({ requestDetails: data });
                  } else {
                    let data = requestDetails;
                    this.setState({ requestDetails: data });
                  }
                }
              } else {
                alert('You do not access rights to approve/reject the request');
              }
            }}
          >
            <i
              className="fa fa-times"
              style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}
            ></i>
          </span>
          <span
            title="Prereject Request"
            style={{ cursor: 'pointer' }}
            onClick={this.changeColor.bind(this, 'prereject')}
          >
            <i
              id={`prereject${row.original.id}`}
              data-target={`#preapprove${row.original.id}`}
              className="fa fa-times-circle"
              style={{ width: 35, fontSize: 18, padding: 11, color: '#ffff00' }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    });
    return (
      <Fragment>
        {/* <div className="client-accounts-buttons">
                <Link to="clients-underCompliance"><button id="undercompliance">Under Compliance</button></Link>
                <Link to="clients-active"><button id="active">Active Clients</button></Link>
                <Link to="clients-closed"><button id="closed">Closed Clients</button></Link>
            </div> */}

        <Tab.Container
          id="usser-acc-tabs-example"
          defaultActiveKey="undercompliance"
        >
          <Row>
            <Col sm={9}>
              <Nav variant="pills" style={{ marginLeft: '2%' }}>
                <Nav.Item>
                  <Nav.Link
                    eventKey="undercompliance"
                    as={Link}
                    to="clients-underCompliance"
                  >
                    Under Compliance
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="active" as={Link} to="clients-active">
                    Active Clients
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="closed" as={Link} to="clients-closed">
                    Closed Clients
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
        </Tab.Container>
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-body datatable-react sudata-table">
                  <ReactTable
                    data={requestDetails}
                    columns={column}
                    SubComponent={(v) => (
                      <div>
                        <UaRequestData id={v.row} />
                      </div>
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UaPending;
