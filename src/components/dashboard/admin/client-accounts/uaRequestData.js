import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import {Form, Button, Col} from 'react-bootstrap';
import { CustomInput, Input } from 'reactstrap';
import './uarequestdata.scss'

class UaRequestData extends Component {
    constructor(props) {
        super(props);
        this.state= { 
            data : []
        }
    }

    componentDidMount() {
        this.setState({
            data: this.props.id._original
        })
    }

    render() {
        const { data } = this.state;
        console.log(data)
        
        return (
            <Fragment>
                { this.state.data ? 
                    <Fragment>
                        
                            <div className="dtDpValWrapper row">
                                <div className="col-12 all-info">    
                                    <div className="card">
                                        <div className="card-inner">
                                            <div className="column-styling column-side-border">
                                                <h5>Company information</h5>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Company</strong></Form.Label>
                                                        
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="5" controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Company name</Form.Label>
                                                              <Form.Control type="text" placeholder="Company name" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} md="4" controlId="">
                                                              <Form.Label className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="Country" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} md="3" controlId="formGridEntity">
                                                              <Form.Label className="column-form-label">Entity type</Form.Label>
                                                              <Form.Control as="select" value="Choose...">
                                                                <option>Choose...</option>
                                                                <option>...</option>
                                                              </Form.Control>
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="4" controlId="">
                                                              <Form.Label className="column-form-label">Registration Date</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                            <Form.Group as={Col} md="4" controlId="">
                                                              <Form.Label className="column-form-label">Registration number</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="5" controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Address</Form.Label>
                                                              <Form.Control type="text" placeholder="Company name" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} md="3" controlId="">
                                                              <Form.Label className="column-form-label">Business Type</Form.Label>
                                                              <Form.Control type="text" placeholder="Country" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="">
                                                                <Form.Label className="column-form-label">Registration Certificate</Form.Label>
                                                                <CustomInput type="file" id="exampleCustomFileBrowser" name="customFile" label="Upload!" />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Volume per month</strong></Form.Label>
                                                        
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Turnover (GBP)</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Turnover (USD)</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Turnover (EUR)</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Quantity of Payments(GBP)</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Quantity of Payments(USD)</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="formGridEntity">
                                                              <Form.Label className="column-form-label">Quantity of Payments(EUR)</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="4" controlId="">
                                                              <Form.Label className="column-form-label">Turnover(enter other)</Form.Label>
                                                              <Form.Control/>
                                                            
                                                            </Form.Group>
                                                            <Form.Group as={Col} md="6" controlId="">
                                                              <Form.Label className="column-form-label">Volume of Payments(other)</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Business Partners</strong></Form.Label>
                                                        
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Full Name</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Full Name</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="formGridEntity">
                                                              <Form.Label className="column-form-label">Full Name</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="formGridEntity">
                                                              <Form.Label className="column-form-label">Country</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Residence Business Company</strong></Form.Label>
                                                        
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Company Name</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Type of Business</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                            </div>
                                            <div className="column-styling">
                                                <h5>People information</h5>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Director</strong></Form.Label>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Full name</Form.Label>
                                                              <Form.Control type="text" placeholder="Company name" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="Country" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEntity">
                                                              <Form.Label  className="column-form-label">Date of Birth</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Date of Issue</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Passport number</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        
                                                            <Form.Group as={Col} controlId="">
                                                                    <Form.Label className="column-form-label">Passport Scan</Form.Label>
                                                              
                                                                 <CustomInput type="file" id="exampleCustomFileBrowser" name="customFile" label="Upload!" />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Shareholder</strong></Form.Label>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridCompanyName">
                                                              <Form.Label  className="column-form-label">Full name</Form.Label>
                                                              <Form.Control type="text" placeholder="Company name" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="Country" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEntity">
                                                              <Form.Label  className="column-form-label">Date of Birth</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Date of Issue</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Passport number</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        
                                                            <Form.Group as={Col} controlId="">
                                                                 <Form.Label className="column-form-label">Passport Scan</Form.Label>
                                                              
                                                                 <CustomInput type="file" id="exampleCustomFileBrowser" name="customFile" label="Upload!" />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Account Beneficiary</strong></Form.Label>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridCompanyName">
                                                              <Form.Label  className="column-form-label">Full name</Form.Label>
                                                              <Form.Control type="text" placeholder="Company name" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Country</Form.Label>
                                                              <Form.Control type="text" placeholder="Country" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEntity">
                                                              <Form.Label  className="column-form-label">Date of Birth</Form.Label>
                                                              <Input type="date" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Passport Number</Form.Label>
                                                                <Form.Control/>
                                                            </Form.Group>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Passport Issue</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        
                                                            <Form.Group as={Col} controlId="">
                                                                <Form.Label className="column-form-label">Address</Form.Label>
                                                               <Form.Control/>
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label  className="column-form-label">Email</Form.Label>
                                                              <Input type="email" name="date" id="exampleDate" placeholder="date placeholder" />
                                                            
                                                            </Form.Group>
                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Telephone</Form.Label>
                                                              <Form.Control/>
                                                            </Form.Group>
                                                        
                                                            <Form.Group as={Col} controlId="">
                                                                <Form.Label className="column-form-label">Passport Scan</Form.Label>
                                                                <CustomInput type="file" id="exampleCustomFileBrowser" name="customFile" label="Upload!" />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                                <div className="column-form">
                                                    <Form>
                                                        <Form.Label><strong>Residence Business People</strong></Form.Label>
                                                        
                                                        <Form.Row>
                                                            <Form.Group as={Col}  controlId="formGridCompanyName">
                                                              <Form.Label className="column-form-label">Quantity of Employees</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Director's Full Name</Form.Label>
                                                              <Form.Control type="text" placeholder="" />
                                                            </Form.Group>

                                                            <Form.Group as={Col}  controlId="">
                                                              <Form.Label className="column-form-label">Shareholder's Full Name</Form.Label>
                                                              <Form.Control />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </Form>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div> 
                                {/* <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Client Details:</strong></div>
                                            <div>Client Type:  <span className="dtDpVal">{data.client_type}</span></div>
                                            <div>Client Name:  <span className="dtDpVal">{data.client_name}</span></div>
                                            <div>Client Address: <span className="dtDpVal">{data.client_address}</span></div>
                                            <div>Client Address2: <span className="dtDpVal">{data.client_address2}</span></div>
                                            <div>Client Account: <span className="dtDpVal">{data.client_account_number}</span></div>
                                            <div>Client Country: <span className="dtDpVal">{data.client_country}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                    <div className="card-body border">
                                            <div><strong>Transfer Details:</strong></div>
                                            <div>Transfer ID: <span className="dtDpVal">{data.id}</span></div>
                                            <div>Currency: <span className="dtDpVal">{data.currency}</span></div>
                                            <div>Amount: <span className="dtDpVal">{data.amount}</span></div>
                                            <div>Date: <span className="dtDpVal">{data.group4_32A_Date? <Moment format="DD/MM/YYYY">{(data.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span></div>
                                            <div>Transfer Type: <span className="dtDpVal">{data.type_of_transfer}</span></div>
                                            <div>Status: <span className="dtDpVal">-</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Beneficiary Bank Details:</strong></div>
                                            <div>Beneficiary Bank SWIFT: <span className="dtDpVal">{data.benbank_SWIFT}</span></div>
                                            <div>Beneficiary Bank Name: <span className="dtDpVal">{data.benbank_name}</span></div>
                                            <div>Beneficiary Bank Address: <span className="dtDpVal">{data.benbank_address}</span></div>
                                            <div>Beneficiary Bank Location: <span className="dtDpVal">{data.benbank_location}</span></div>
                                            <div>Beneficiary Bank Country: <span className="dtDpVal">{data.benbank_country}</span></div>
                                            <div>Beneficiary Bank NCS Number: <span className="dtDpVal">{data.benbank_NCS_number}</span></div>
                                            <div>Beneficiary ABA RTA: <span className="dtDpVal">{data.benbank_ABA}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Intermediary Bank Details:</strong></div>
                                            <div>Inter Bank SWIFT: <span className="dtDpVal">{data.interbank_SWIFT}</span></div>
                                            <div>Inter Bank Name: <span className="dtDpVal">{data.interbank_name}</span></div>
                                            <div>Inter Bank Address: <span className="dtDpVal">{data.interbank_address}</span></div>
                                            <div>Inter Bank Location: <span className="dtDpVal">{data.interbank_location}</span></div>
                                            <div>Inter Bank Country: <span className="dtDpVal">{data.interbank_country}</span></div>
                                            <div>Inter Bank NCS Number: <span className="dtDpVal">{data.interbank_NCS_number}</span></div>
                                            <div>Inter Bank ABA RTA: <span className="dtDpVal">{data.interbank_ABA}</span></div>
                                            <div>Inter Bank Account Number: <span className="dtDpVal">{data.interaccount_Number}</span></div>
                                        </div>
                                    </div>
                                </div> */}
                                {/* <div className="col-sm-12">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Reference Message:</strong></div>
                                            <div>
                                                <span className="dtDpVal">
                                                    {data.reference_message}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                            
                    </Fragment>
                   : "loading...."
                }  
            </Fragment>
        )
    }
}

export default UaRequestData;
