import React, { Component, Fragment } from 'react';
import UaRequestData from './uaRequestData';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
import {
  Modal,
  Table,
  Card,
  CardDeck,
  Form,
  Button,
  Row,
  Col,
  Tab,
  Nav,
} from 'react-bootstrap';
import { CustomInput, Input } from 'reactstrap';
import eurIcon from '../../../../assets/images/HomePage/eur.png';
import gbpIcon from '../../../../assets/images/HomePage/gbp.png';
import pageIcon from '../../../../assets/images/HomePage/page.png';
import transactionIcon from '../../../../assets/images/HomePage/transaction.png';
import transferIcon from '../../../../assets/images/HomePage/transfer.png';
import exchangeIcon from '../../../../assets/images/HomePage/exchange.png';
import swiftIcon from '../../../../assets/images/payment-history/swifticon.png';
import DatePicker from 'react-datepicker';
import UserTransactionHistory from './UserTransactionHistory';
import { getActiveClients } from '../../../../actions/admin/ClientAccounts';
import { connect } from 'react-redux';

var columns;
function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      dialogClassName="my-modal modal right"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ padding: '0.5rem', borderBottom: 'none' }}>
        <Modal.Title
          id="contained-modal-title-vcenter"
          style={{ cursor: 'pointer' }}
          onClick={props.onHide}
        >
          X
        </Modal.Title>
        <div
          style={{
            backgroundColor: '#a9a9a9',
            padding: '0.5rem',
            borderRadius: '20px',
          }}
        >
          {/* <h5>Client profile</h5> */}
          <h5>Client Transaction history</h5>
        </div>
        <div>
          <img
            className="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAPFBMVEX///+xsbGsrKyrq6vr6+u0tLS/v7/29vbExMTv7+/i4uLQ0NDz8/PJycnBwcH39/e5ubnW1tbc3Nzl5eWVAQX4AAAHYklEQVR4nO1d2XarOgwNNoMZkgD5/389kA7n9pQGSVtGLtf7sasrzo5sSdbkyyUjIyMjIyMjIyMjIyMjIyPjf4mhnKdrqMeqcH6BK6qxDtdpLgfrbwZj6PpQebeg+Ib1r74KffdbaZbTzfktZv/y9O42ldbflolhvm1K7WeW7jb/Gkm2/bIr6eQ+Sfqqb62/+z6GScTuk+OUthy7Ws7ug2PdWbP4CU3POXYvOLq+seaygTagwvsPRR9SO41t7bXYvcHXKVFUp5cWxSZEoPekGJI4i73a0fsO11uzu3RFRH4Lw8LWaDR1VHpPirXhPp2j03tSnI3o3eOL751hfbfg1x1E70nR4CReI9mGbfjrwfSa8UD5rXDjobrmcTC9J8XHcfymQ7fnB/x0FL+rgfxWuIMO4lHWYYNhfQS/o9XLF4ZjdHr3yo7eiiqyzbfmF5vh3ZreiogM7eW3IqIMk+C3MIzFb7Rm9oFIutTO/v2LOPbQyn/ZQgyfZkqI38JQ3S99mPjXP8Mr3y2apOS3wuneD5NRoH+hqkpTUjAf0FQ0XWIH8A1eLRJ1T1B+K5yWz1ZbM/kJSvZeL37t3qH2eSoxbyUL4VwVpvlRlo9uuo5KKRsVW6GxQdcymC/f5f64qnBU2KQKEXpXbFYVzJXCR+OaFP4OL7KYGslFlB+cv3X1q9Ie2INAc8Cwhtlz+x/g56N6JqDr75YRNmgcJCD8WtRHo1SCgI68R6pNUBNBWxtkCJgKUICOWOYKRiMBEWICpDtSLabKxCIEBcg4/VjARyxCUIVyLjPYMRQq0gYSIM/Txzapl9lCzIlhxteh3SJ0ZyB+3KsaJkIn4QdeI7jLQRpbdKnAbAQ75IX9ngJLMWAqht3KguVWPb8bAcxFsNcD9yg/V4G5T4Itg/2i7KQo5sVIkj/lsd4MZgQlWg27W7NNIbZD+TrmgkZ/mHsU06GFk7RYgZcm3pJgNFsUKAE3Dc91ukGLWRAsbqzF0CY5A4IsfxRT2SZnkKfY0JIKiRZF6+BYthc8gqK81oDGmDmHEE4ZCPLncJaHcQhBK1iISiCu6JoMS4inzARqFC5lZPiHeEsg/xDCR5DjjsIpF8F9SaEWjh49VCh8ZVtChTXp/rZG3Q9Tj2r0snnqYvhxKNgi1KiWJi+JOmpvYMXTVWpxyP6TTuUPp1tMp12BrLq1invp2RedYjGyNwr7FO8gO4davyhVsSmYwSeolletG5h67NWqC2l7Rq+dlOpd6NUvU2So2M1NdfEVO3jc7q7RbFegujJ6K64tjS9DzvegWk1MJKhbwfzqIHaqK5GvvMol6K76wQCX2t2yVGdUvcbebUwTu8/6zcBmBNeS33EqPz2be7tOztNfxZBg8axIr8ZbCLe6KmKwK+gEE22T2AdVyVh/TzmIBBNp1eWDaugTbDWjgeqqqbfyPLtdqrG+LTpm0TK35zRcxQ6YD1Cdba3rUvGmOut+fgzfLr/34TH3daVKk3pd0rrwLsavf+yEuJtHP6pxpF54VRx8V4SOGLO4d0Glz4ccslAIOrlxZjX3qThu5KATHDZ0kvGgLXxzIocNwcCvC8I5vQNIkR5rRpxRNwIzs7ELFDl0j7gy6IgCRMHRky+AIYRHnpfytenJAmkCVGXKongmJCMBKozk7YfQaBDqGkYKW1aEoMVPypBTjidaQHGKjcjd59RySQqBVCctSfQ4pxBIoKyd6lRsQZ8Iy0DxnTXtET38n5hXH8f+ePVBWeywAq+9h3sI9aebspNqvIJY7o0pwqQzpgiZtVVMSxhjijLT2+A29zD1tD4/bniWa6V47miU2bSs0BC7MYTV2hNnzjdrj/IblTl7VFSDvgtWYIHvR7EsbQR6F9YhFPgZHD0aaeAnYxMJGiQ5Hr09QclNhnHGzQnKtNxvIij6fLoptCYoHBRAH/VgTVA46oEePbQmKA0Gkb0ZY4LyiTlUS2FMUB7toorQliAytYooQluCSLiSKEJTgtDYMaIiNSWIxdNp4wksCaIjRknujCFB/AE/yq9oKUF4Fcqlwo6gRrCEYCrsCGpktAh6pmrLCGj3CeoMvCdEuV0U7C+r9PTi2QeJn38U/OmH+Z//OYYkq5x1M3anfxLl9I/anP9ZorQUTZzHMs/+NFhCqjTaQ5mJ9PxEe57v/A8snv6JzBRkePZnXGPzu5z+oeHL+Z+KPv9j3+d/rl1zQgod+iWpr9AcrWrcqHz/28X10G3qDzt+f6E46GYXccoZ9yBuMmLT02iJEkHv6cWX/JTi1xI08YXo6qO1y1dovLD3il5hcvq+AJ+2+oIfnL/VQBMiWQwfbHfnX7R1BIq+Vm33AqFOMS16K9rg9d7X9ZJJA9HR9DpTN5zbfPQ1CXQ1Kkbna3vD8ArDVMk5Or8xgC09tL2I48KuT/HkbWJYB8MxSLr1LelfILsvKKeb8/ss3fJPtwmeEGGEoevDsl83pbn+1Veh736b5L5jKOfpGp6juPyC54CucJ3m8vdTy8jIyMjIyMjIyMjIyMjIyBDhD3/FdXNd3zfAAAAAAElFTkSuQmCC"
            alt="header-user"
          />
          <h6>Mr.Abhishek</h6>
        </div>
      </Modal.Header>
      <Modal.Body style={{ padding: '0 5%' }}>
        <Form.Row>
          <Form.Group as={Col} controlId="">
            <Form.Label className="column-form-label">
              Amount <input type="text" style={{ width: '70%' }} />
            </Form.Label>
            <Form.Control as="select" value="Choose...">
              <option>SWIFT</option>
              <option>...</option>
            </Form.Control>
          </Form.Group>

          <Form.Group as={Col} controlId="">
            <Form.Label className="column-form-label">EUR</Form.Label>
            <Form.Control as="select" value="Choose...">
              <option>099-79-0001 USD</option>
              <option>...</option>
            </Form.Control>
          </Form.Group>

          <Form.Group as={Col} controlId="">
            <Form.Label className="column-form-label">Message</Form.Label>
            <Form.Control />
          </Form.Group>

          <Form.Group as={Col} controlId="">
            <Button
              style={{
                height: '90%',
                borderRadius: '20px',
                backgroundColor: '#a9a9a9 !important',
              }}
            >
              Deduct Manual Commission
            </Button>
          </Form.Group>
        </Form.Row>

        <Form.Row style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Form.Group as={Col} md="6" controlId="">
            <Form.Row style={{ backgroundColor: '#a9a9a9', padding: '1%' }}>
              <Form.Group as={Col} md="4" controlId="">
                <Form.Control as="select" value="Choose...">
                  <option>SWIFT</option>
                  <option>...</option>
                </Form.Control>
              </Form.Group>

              <Form.Group as={Col} md="5" controlId="">
                <Form.Control as="select" value="Choose...">
                  <option>099-79-0001 USD</option>
                  <option>...</option>
                </Form.Control>
              </Form.Group>

              <Form.Group as={Col} md="3" controlId="">
                <Form.Label className="column-form-label">
                  ACC HISTORY
                </Form.Label>
              </Form.Group>
              <Card style={{ width: '100%', margin: 0 }}>
                <Card.Body>
                  <table className="table-responsive" responsive>
                    <thead>
                      <tr style={{ borderBottom: '1px solid black' }}>
                        <td></td>
                        <td>
                          <strong>Transaction history</strong>
                        </td>
                        <td>
                          <input type="search" style={{ width: '70%' }} />
                        </td>
                        <td>Account Number</td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style={{ borderBottom: '1px solid black' }}>
                        <td>
                          <i
                            className="fa fa-undo"
                            style={{ fontSize: 18, color: 'blue' }}
                          ></i>
                        </td>
                        <td>01.02.2020</td>
                        <td>
                          Tranfer to XXXX <br />
                          Bank of America
                        </td>
                        <td>
                          $40000
                          <br />
                          $600000
                        </td>
                        <td>
                          <p
                            style={{
                              backgroundColor: 'purple',
                              color: 'white',
                              padding: '2%',
                            }}
                          >
                            under compliance
                          </p>
                        </td>
                      </tr>
                      <tr style={{ borderBottom: '1px solid black' }}>
                        <td>
                          <i
                            className="fa fa-undo"
                            style={{ fontSize: 18, color: 'blue' }}
                          ></i>
                        </td>
                        <td>01.02.2020</td>
                        <td>
                          Tranfer to XXXX <br />
                          Bank of America
                        </td>
                        <td>
                          $40000
                          <br />
                          $600000
                        </td>
                        <td>
                          <p
                            style={{
                              backgroundColor: 'green',
                              color: 'white',
                              padding: '2%',
                            }}
                          >
                            Approved
                          </p>
                        </td>
                      </tr>
                      <tr style={{ borderBottom: '1px solid black' }}>
                        <td>
                          <i
                            className="fa fa-undo"
                            style={{ fontSize: 18, color: 'blue' }}
                          ></i>
                        </td>
                        <td>01.02.2020</td>
                        <td>
                          Tranfer to XXXX <br />
                          Bank of America
                        </td>
                        <td>
                          $40000
                          <br />
                          $600000
                        </td>
                        <td>
                          <p
                            style={{
                              backgroundColor: 'red',
                              color: 'white',
                              padding: '2%',
                            }}
                          >
                            Rejected
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div>
                    <center>
                      <button className="btn btn-primary">Next</button>
                    </center>
                  </div>
                </Card.Body>
              </Card>
            </Form.Row>
          </Form.Group>
          <Form.Group as={Col} md="5" controlId="">
            <Card>
              <Card.Body>
                <Table responsive>
                  <thead>
                    <tr>
                      <td>Documents</td>
                      <td></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <i className="fa fa-file" style={{ fontSize: 100 }}></i>
                        <br />
                        doc0201.pdf
                      </td>
                      <td>
                        <i className="fa fa-file" style={{ fontSize: 100 }}></i>
                        <br />
                        doc1098.pdf
                      </td>
                      <td>
                        <i className="fa fa-file" style={{ fontSize: 100 }}></i>
                        <br />
                        doc14566.pdf
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <CustomInput
                  type="file"
                  id="exampleCustomFileBrowser"
                  name="customFile"
                  label="Upload New document!"
                />
              </Card.Body>
            </Card>
          </Form.Group>
        </Form.Row>

        <Form.Row style={{ justifyContent: 'space-between' }}>
          <Form.Group as={Col} md="3" controlId="">
            <Button
              style={{
                height: '50%',
                borderRadius: '20px',
                backgroundColor: '#a9a9a9 !important',
              }}
            >
              Deduct Manual Commission
            </Button>
          </Form.Group>
          <Form.Group as={Col} md="2" controlId="">
            <Form.Label className="column-form-label">50 EUR </Form.Label>
            <Form.Control as="select" value="Choose...">
              <option>SWIFT</option>
              <option>...</option>
            </Form.Control>
          </Form.Group>
          <Form.Group as={Col} md="5">
            <Card style={{ margin: 0 }}>
              <Card.Body style={{ padding: '5%' }}>
                <Card.Text
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    lineHeight: 1,
                    letterSpacing: '0.5px',
                  }}
                >
                  <span>Total Funds</span>
                  <span>
                    Copy Details{' '}
                    <i className="fa fa-copy" style={{ fontSize: 18 }}></i>
                  </span>
                </Card.Text>
                <Card.Text
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span>
                    <img src={eurIcon} />
                    EUR
                  </span>
                  <span>
                    <img src={swiftIcon} height="20" width="20" /> Swift
                  </span>
                </Card.Text>
                <button className="btn btn-primary btn-block">$4000000</button>
              </Card.Body>
            </Card>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <CardDeck style={{ width: '100%' }}>
            <Card>
              <Card.Body style={{ padding: '5%' }}>
                <Card.Text
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    lineHeight: 1,
                    letterSpacing: '0.5px',
                  }}
                >
                  <span>Total Funds</span>
                  <span>
                    Copy Details{' '}
                    <i className="fa fa-copy" style={{ fontSize: 18 }}></i>
                  </span>
                </Card.Text>
                <Card.Text
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span>
                    <img src={eurIcon} />
                    EUR
                  </span>
                  <span>
                    <img src={swiftIcon} height="20" width="20" /> Swift
                  </span>
                </Card.Text>
                <button className="btn btn-primary btn-block">$4000000</button>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body style={{ padding: '5%' }}>
                <Card.Text
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    lineHeight: 1,
                    letterSpacing: '0.5px',
                  }}
                >
                  <span>Total Funds</span>
                  <span>
                    Copy Details{' '}
                    <i className="fa fa-copy" style={{ fontSize: 18 }}></i>
                  </span>
                </Card.Text>
                <Card.Text
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span>
                    <img src={gbpIcon} />
                    GBP
                  </span>
                  <span>
                    <img src={swiftIcon} height="20" width="20" /> Swift
                  </span>
                </Card.Text>
                <button className="btn btn-primary btn-block">$4000000</button>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body style={{ padding: '5%' }}>
                <Card.Text
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    lineHeight: 1,
                    letterSpacing: '0.5px',
                  }}
                >
                  <span>Total Funds</span>
                  <span>
                    Copy Details{' '}
                    <i className="fa fa-copy" style={{ fontSize: 18 }}></i>
                  </span>
                </Card.Text>
                <Card.Text
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span>
                    <img src={exchangeIcon} />
                    USD
                  </span>
                  <span>
                    <img src={swiftIcon} height="20" width="20" /> Swift
                  </span>
                </Card.Text>
                <button className="btn btn-primary btn-block">$4000000</button>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body style={{ padding: '5%' }}>
                <Card.Text
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    lineHeight: 1,
                    letterSpacing: '0.5px',
                  }}
                >
                  <span>Total Funds</span>
                  <span>
                    Copy Details{' '}
                    <i className="fa fa-copy" style={{ fontSize: 18 }}></i>
                  </span>
                </Card.Text>
                <Card.Text
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span>
                    <img src={eurIcon} />
                    RUB
                  </span>
                  <span>
                    <img src={swiftIcon} height="20" width="20" /> Swift
                  </span>
                </Card.Text>
                <button className="btn btn-primary btn-block">$4000000</button>
              </Card.Body>
            </Card>
          </CardDeck>
        </Form.Row>
        <br />
        <br />
        <Form.Row>
          <Form.Group as={Col}>
            <h6>
              <i
                className="fa fa-phone"
                style={{
                  fontSize: 18,
                  padding: '2%',
                  borderRadius: '50%',
                  color: 'white',
                  backgroundColor: 'limegreen',
                }}
              ></i>{' '}
              Mobile Number : +6140319621
            </h6>
            <h6>
              <i
                className="fa fa-paper-plane"
                style={{
                  fontSize: 18,
                  padding: '2%',
                  borderRadius: '50%',
                  color: 'white',
                  backgroundColor: 'black',
                }}
              ></i>{' '}
              Email : abhishek@vastdreams.com{' '}
            </h6>
          </Form.Group>
          <Form.Group as={Col}>
            <div
              style={{
                borderRadius: '20px',
                backgroundColor: '#a9a9a9',
                padding: '2%',
              }}
            >
              Admin comments
              <div
                style={{
                  borderRadius: '20px',
                  backgroundColor: 'white',
                  padding: '2%',
                  height: '5em',
                }}
              >
                Some comments here
              </div>
            </div>
          </Form.Group>
        </Form.Row>
      </Modal.Body>
      <Modal.Footer style={{ justifyContent: 'center' }}>
        <center>
          <Button onClick={props.onHide}>Close Account</Button>
        </center>
      </Modal.Footer>
    </Modal>
  );
}
class UaApproved extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeClients: [],
      isAdmin: false,
      modalShow: false,
      currentUserId: '',
    };
  }
  componentDidMount() {
    this.props.getActiveClients(localStorage.getItem('access'));
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.activeClients !== this.props.activeClients) {
      let data = [];
      for (let i in this.props.activeClients) {
        data.push(this.props.activeClients[i]);
      }
      this.setState({ activeClients: data });
    }
  }
  handleHeaderClick = () => {
    if (this.state.isAdmin) {
      let checkboxes = document.querySelectorAll('.rowCheckbox');
      checkboxes.forEach((c) => (c.checked = !c.checked));
    }
  };

  render() {
    columns = [
      //   {
      //     Header: (
      //       <input
      //         type="checkbox"
      //         disabled={!this.state.isAdmin}
      //         onChange={() => this.handleHeaderClick()}
      //       />
      //     ),
      //     accessor: '',
      //     width: 50,
      //     Cell: (row) => (
      //       <div>
      //         <input
      //           type="checkbox"
      //           className="rowCheckbox"
      //           disabled={!this.state.isAdmin}
      //         />
      //       </div>
      //     ),
      //     style: {
      //       textAlign: 'center',
      //     },
      //     sortable: false,
      //   },
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'registeration_date',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Name <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'company_name',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Type <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacBenFicName',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Country <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'company_country',
        sortable: true,
        maxWidth: '25%',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Risk <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'risk',
        sortable: true,
        maxWidth: '25%',
        style: {
          textAlign: 'center',
        },
      },
    ];

    columns.push({
      Header: <b>Settings</b>,
      id: 'delete',
      accessor: (str) => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span style={{ cursor: 'pointer' }}>
            <i
              className="fa fa-cog"
              onClick={() =>
                this.setState({
                  modalShow: true,
                  currentUserId: row.original.id,
                })
              }
              style={{ width: 35, fontSize: 18, padding: 11, color: '#000' }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    });
    return (
      <Fragment>
        {/* <MyVerticallyCenteredModal
          show={this.state.modalShow}
          onHide={() => this.setState({ modalShow: false })}
        /> */}
        {this.state.modalShow && (
          <UserTransactionHistory
            show={this.state.modalShow}
            currentUserId={this.state.currentUserId}
            onHide={() => this.setState({ modalShow: false })}
          />
        )}
        {/* <div className="client-accounts-buttons">
                <Link to="clients-underCompliance"><button id="undercompliance">Under Compliance</button></Link>
                <Link to="clients-active"><button id="active">Active Clients</button></Link>
                <Link to="clients-closed"><button id="closed">Closed Clients</button></Link>
            </div> */}

        <Tab.Container id="usser-acc-tabs-example" defaultActiveKey="active">
          <Row>
            <Col sm={9}>
              <Nav variant="pills" style={{ marginLeft: '2%' }}>
                <Nav.Item>
                  <Nav.Link
                    eventKey="undercompliance"
                    as={Link}
                    to="clients-underCompliance"
                  >
                    Under Compliance
                  </Nav.Link>
                </Nav.Item>

                <Nav.Item>
                  <Nav.Link eventKey="active" as={Link} to="clients-active">
                    Active Clients
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="closed" as={Link} to="clients-closed">
                    Closed Clients
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
        </Tab.Container>

        <div className="container-fluid ">
          <div className="row">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-body datatable-react sudata-table">
                  <ReactTable
                    columns={columns}
                    data={this.state.activeClients}
                    // NoDataComponent={() => <div>fetchng rowss</div>}
                    // SubComponent={(v) => (
                    //   <div>
                    //     <UaRequestData id={v.row} />
                    //   </div>
                    // )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  activeClients: state.adminReducer.activeClients,
  //   swiftAdminApprovalMessage: state.AdminSepaReducer.swiftAdminApprovalMessage,
});

export default connect(mapStateToProps, { getActiveClients })(UaApproved);
