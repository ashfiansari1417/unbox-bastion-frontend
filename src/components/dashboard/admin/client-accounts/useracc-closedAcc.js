import React, { Component, Fragment } from 'react';
import DataTable from 'react-data-table-component';
import { Tab, Row, Col, Nav } from 'react-bootstrap';

import UaRequestData from './uaRequestData';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';
var columns;
class UaClosedacc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetailsStatic: [
        {
          id: 1,
          cacDate: '19/02/2020',
          cacCompName: 'Google',
          cacCompCntry: 'India',
          cacBenFicName: 'XYZ',
          cacBenFicCntry: 'India',
          cacAccNoEurSw: '123',
          cacAccNoUsdSw: '123',
          cacAccNoRubSw: '123',
          cacAccNoGbpSw: '123',
          cacAccNoEurSepa: '123',
          cacRiskType: 'High',
        },
        {
          id: 2,
          cacDate: '18/02/2020',
          cacCompName: 'Facebook',
          cacCompCntry: 'India',
          cacBenFicName: 'ABC',
          cacBenFicCntry: 'India',
          cacAccNoEurSw: '123',
          cacAccNoUsdSw: '123',
          cacAccNoRubSw: '231',
          cacAccNoGbpSw: '123',
          cacAccNoEurSepa: '123',
          cacRiskType: 'Low',
        },
        {
          id: 3,
          cacDate: '18/02/2020',
          cacCompName: 'Amazon',
          cacCompCntry: 'India',
          cacBenFicName: 'JKL',
          cacBenFicCntry: 'India',
          cacAccNoEurSw: '123',
          cacAccNoUsdSw: '123',
          cacAccNoRubSw: '123',
          cacAccNoGbpSw: '123',
          cacAccNoEurSepa: '123',
          cacRiskType: 'Zero',
        },
      ],
      isAdmin: false,
    };
  }
  componentDidMount() {
    // let selector =
    //   '#' +
    //   this.props.location.pathname.split('/')[2].split('-')[1].toLowerCase();
    // document.querySelector(selector).style['background-color'] = '#a9a9a9';
  }

  gotoTransferForm = () => {
    this.props.history.push('./transfer-form');
  };
  handleHeaderClick = () => {
    if (this.state.isAdmin) {
      let checkboxes = document.querySelectorAll('.rowCheckbox');
      checkboxes.forEach((c) => (c.checked = !c.checked));
    }
  };
  render() {
    columns = [
      //   {
      //     Header: (
      //       <input
      //         type="checkbox"
      //         disabled={!this.state.isAdmin}
      //         onChange={() => this.handleHeaderClick()}
      //       />
      //     ),
      //     accessor: '',
      //     width: 50,
      //     Cell: (row) => (
      //       <div>
      //         <input
      //           type="checkbox"
      //           className="rowCheckbox"
      //           disabled={!this.state.isAdmin}
      //         />
      //       </div>
      //     ),
      //     style: {
      //       textAlign: 'center',
      //     },
      //     sortable: false,
      //   },
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacDate',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Name <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacCompName',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Type <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacBenFicName',
        maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Company Country <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacCompCntry',
        sortable: true,
        maxWidth: '25%',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Risk <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'cacRiskType',
        sortable: true,
        maxWidth: '25%',
        style: {
          textAlign: 'center',
        },
      },
    ];
    // columns.push({
    //   Header: <b>Risk Type</b>,
    //   id: 'risk',
    //   accessor: 'risk',
    //   width: 150,
    //   Cell: (row) => (
    //     <div>
    //       <select>
    //         <option>Zero</option>
    //         <option>Low</option>
    //         <option>Low Digi</option>
    //         <option>Medium</option>
    //         <option>Upper</option>
    //         <option>Upper Special</option>
    //         <option>High Digi</option>
    //       </select>
    //     </div>
    //   ),
    //   style: {
    //     textAlign: 'center',
    //   },
    //   sortable: false,
    // });
    columns.push({
      Header: <b>Settings</b>,
      id: 'delete',
      accessor: (str) => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span style={{ cursor: 'pointer' }}>
            <i
              className="fa fa-cog"
              onClick={() => this.setState({ modalShow: true })}
              style={{ width: 35, fontSize: 18, padding: 11, color: '#000' }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    });
    return (
      <Fragment>
        {/* <div className="client-accounts-buttons">
                <Link to="clients-underCompliance"><button id="undercompliance">Under Compliance</button></Link>
                <Link to="clients-active"><button id="active">Active Clients</button></Link>
                <Link to="clients-closed"><button id="closed">Closed Clients</button></Link>
            </div> */}

        <Tab.Container id="usser-acc-tabs-example" defaultActiveKey="closed">
          <Row>
            <Col sm={9}>
              <Nav variant="pills" style={{ marginLeft: '2%' }}>
                <Nav.Item>
                  <Nav.Link
                    eventKey="undercompliance"
                    as={Link}
                    to="clients-underCompliance"
                  >
                    Under Compliance
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="active" as={Link} to="clients-active">
                    Active Clients
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="closed" as={Link} to="clients-closed">
                    Closed Clients
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
        </Tab.Container>
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-body datatable-react sudata-table">
                  <ReactTable
                    columns={columns}
                    data={this.state.userDetailsStatic}
                    SubComponent={(v) => (
                      <div>
                        <UaRequestData id={v.row} />
                      </div>
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UaClosedacc;
