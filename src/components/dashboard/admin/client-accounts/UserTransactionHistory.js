import React, { Component } from 'react';
import { Modal, Card, Form, Tab, Row, Col, Nav } from 'react-bootstrap';
import ReactTable from 'react-table';
import { connect } from 'react-redux';
import { getClientTransaction } from '../../../../actions/admin/ClientAccounts';
import 'react-table/react-table.css';

class UserTransactionHistory extends Component {
  state = {
    eurData: [],
    usdData: [],
    gbpData: [],
    rubData: [],
    currentCurrency: 'EUR',
    currentUser: '',
    currentAccountNum: '',
  };
  componentDidMount() {
    this.props.getClientTransaction(
      localStorage.getItem('access'),
      this.props.currentUserId
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { clientTransaction } = this.props;
    if (prevProps.clientTransaction !== this.props.clientTransaction) {
      let Edata = [];
      let Udata = [];
      let Rdata = [];
      let Gdata = [];
      for (let eur in clientTransaction['EUR']) {
        Edata.push(clientTransaction['EUR'][eur]);
      }
      for (let usd in clientTransaction['USD']) {
        Udata.push(clientTransaction['USD'][usd]);
      }
      for (let rub in clientTransaction['RUB']) {
        Rdata.push(clientTransaction['RUB'][rub]);
      }
      for (let gbp in clientTransaction['GBP']) {
        Gdata.push(clientTransaction['GBP'][gbp]);
      }
      this.setState({
        currentUser: clientTransaction['EUR'].first_name,

        eurData: Edata,
        usdData: Udata,
        gbpData: Gdata,
        rubData: Rdata,
      });
    }
    // if (prevState.currentCurrency !== this.state.currentCurrency) {
    // if (this.state.currentCurrency === 'EUR') {
    //   this.setState({
    //     currentAccountNum: clientTransaction['EUR'].acc_no,
    //   });
    // } else if (this.state.currentCurrency === 'USD') {
    //   this.setState({
    //     currentAccountNum: clientTransaction['USD'].acc_no,
    //   });
    // }
    // }
  }

  render() {
    const {
      eurData,
      usdData,
      gbpData,
      rubData,
      currentCurrency,
      currentUser,
      currentAccountNum,
    } = this.state;
    const { clientTransaction } = this.props;

    console.log(this.state.eurData);
    let transactionHistoryColumn = [
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'date',
        // maxWidth: '25%',
        sortable: true,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Counter Party Account Number <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'CounterPartyAccountNo',
        // maxWidth: '25%',

        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Amount <i className="fa fa-sort"></i>
          </b>
        ),
        // accessor: 'Amount',
        // maxWidth: '25%',
        Cell: (row) => (
          // <span>{Math.round(row.original.Amount).toFixed(2)}</span>
          <span>{row.original.Amount && row.original.Amount.toFixed(2)}</span>
        ),
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Payment Description <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'Description',
        // maxWidth: '25%',

        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Status <i className="fa fa-sort"></i>
          </b>
        ),
        //   accessor: 'statue',
        // maxWidth: '25%',

        Cell: (row) => (
          <div>
            {row.original.Status === 'PE' ? (
              <p
                style={{
                  backgroundColor: 'purple',
                  color: 'white',
                  padding: '1%',
                }}
              >
                PENDING
              </p>
            ) : row.original.Status === 'R' ? (
              <p
                style={{
                  backgroundColor: 'red',
                  color: 'white',
                  padding: '1%',
                }}
              >
                REJECTED
              </p>
            ) : row.original.Status === 'AP' ? (
              <p
                style={{
                  backgroundColor: 'green',
                  color: 'white',
                  padding: '1%',
                }}
              >
                APPROVED
              </p>
            ) : (
              ''
            )}
          </div>
        ),
        style: {
          textAlign: 'center',
        },
      },
    ];
    return (
      <Modal
        {...this.props}
        size="lg"
        dialogClassName="my-modal modal right"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header style={{ padding: '0.5rem', borderBottom: 'none' }}>
          <Modal.Title
            id="contained-modal-title-vcenter"
            style={{ cursor: 'pointer' }}
            onClick={this.props.onHide}
          >
            X
          </Modal.Title>
          <div
            style={{
              backgroundColor: '#a9a9a9',
              padding: '0.5rem',
              borderRadius: '20px',
            }}
          >
            {/* <h5>Client profile</h5> */}
            <h5>Client Transaction history</h5>
          </div>
          <div>
            <img
              className="align-self-center pull-right img-50 rounded-circle blur-up lazyloaded"
              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAPFBMVEX///+xsbGsrKyrq6vr6+u0tLS/v7/29vbExMTv7+/i4uLQ0NDz8/PJycnBwcH39/e5ubnW1tbc3Nzl5eWVAQX4AAAHYklEQVR4nO1d2XarOgwNNoMZkgD5/389kA7n9pQGSVtGLtf7sasrzo5sSdbkyyUjIyMjIyMjIyMjIyMjIyPjf4mhnKdrqMeqcH6BK6qxDtdpLgfrbwZj6PpQebeg+Ib1r74KffdbaZbTzfktZv/y9O42ldbflolhvm1K7WeW7jb/Gkm2/bIr6eQ+Sfqqb62/+z6GScTuk+OUthy7Ws7ug2PdWbP4CU3POXYvOLq+seaygTagwvsPRR9SO41t7bXYvcHXKVFUp5cWxSZEoPekGJI4i73a0fsO11uzu3RFRH4Lw8LWaDR1VHpPirXhPp2j03tSnI3o3eOL751hfbfg1x1E70nR4CReI9mGbfjrwfSa8UD5rXDjobrmcTC9J8XHcfymQ7fnB/x0FL+rgfxWuIMO4lHWYYNhfQS/o9XLF4ZjdHr3yo7eiiqyzbfmF5vh3ZreiogM7eW3IqIMk+C3MIzFb7Rm9oFIutTO/v2LOPbQyn/ZQgyfZkqI38JQ3S99mPjXP8Mr3y2apOS3wuneD5NRoH+hqkpTUjAf0FQ0XWIH8A1eLRJ1T1B+K5yWz1ZbM/kJSvZeL37t3qH2eSoxbyUL4VwVpvlRlo9uuo5KKRsVW6GxQdcymC/f5f64qnBU2KQKEXpXbFYVzJXCR+OaFP4OL7KYGslFlB+cv3X1q9Ie2INAc8Cwhtlz+x/g56N6JqDr75YRNmgcJCD8WtRHo1SCgI68R6pNUBNBWxtkCJgKUICOWOYKRiMBEWICpDtSLabKxCIEBcg4/VjARyxCUIVyLjPYMRQq0gYSIM/Txzapl9lCzIlhxteh3SJ0ZyB+3KsaJkIn4QdeI7jLQRpbdKnAbAQ75IX9ngJLMWAqht3KguVWPb8bAcxFsNcD9yg/V4G5T4Itg/2i7KQo5sVIkj/lsd4MZgQlWg27W7NNIbZD+TrmgkZ/mHsU06GFk7RYgZcm3pJgNFsUKAE3Dc91ukGLWRAsbqzF0CY5A4IsfxRT2SZnkKfY0JIKiRZF6+BYthc8gqK81oDGmDmHEE4ZCPLncJaHcQhBK1iISiCu6JoMS4inzARqFC5lZPiHeEsg/xDCR5DjjsIpF8F9SaEWjh49VCh8ZVtChTXp/rZG3Q9Tj2r0snnqYvhxKNgi1KiWJi+JOmpvYMXTVWpxyP6TTuUPp1tMp12BrLq1invp2RedYjGyNwr7FO8gO4davyhVsSmYwSeolletG5h67NWqC2l7Rq+dlOpd6NUvU2So2M1NdfEVO3jc7q7RbFegujJ6K64tjS9DzvegWk1MJKhbwfzqIHaqK5GvvMol6K76wQCX2t2yVGdUvcbebUwTu8/6zcBmBNeS33EqPz2be7tOztNfxZBg8axIr8ZbCLe6KmKwK+gEE22T2AdVyVh/TzmIBBNp1eWDaugTbDWjgeqqqbfyPLtdqrG+LTpm0TK35zRcxQ6YD1Cdba3rUvGmOut+fgzfLr/34TH3daVKk3pd0rrwLsavf+yEuJtHP6pxpF54VRx8V4SOGLO4d0Glz4ccslAIOrlxZjX3qThu5KATHDZ0kvGgLXxzIocNwcCvC8I5vQNIkR5rRpxRNwIzs7ELFDl0j7gy6IgCRMHRky+AIYRHnpfytenJAmkCVGXKongmJCMBKozk7YfQaBDqGkYKW1aEoMVPypBTjidaQHGKjcjd59RySQqBVCctSfQ4pxBIoKyd6lRsQZ8Iy0DxnTXtET38n5hXH8f+ePVBWeywAq+9h3sI9aebspNqvIJY7o0pwqQzpgiZtVVMSxhjijLT2+A29zD1tD4/bniWa6V47miU2bSs0BC7MYTV2hNnzjdrj/IblTl7VFSDvgtWYIHvR7EsbQR6F9YhFPgZHD0aaeAnYxMJGiQ5Hr09QclNhnHGzQnKtNxvIij6fLoptCYoHBRAH/VgTVA46oEePbQmKA0Gkb0ZY4LyiTlUS2FMUB7toorQliAytYooQluCSLiSKEJTgtDYMaIiNSWIxdNp4wksCaIjRknujCFB/AE/yq9oKUF4Fcqlwo6gRrCEYCrsCGpktAh6pmrLCGj3CeoMvCdEuV0U7C+r9PTi2QeJn38U/OmH+Z//OYYkq5x1M3anfxLl9I/anP9ZorQUTZzHMs/+NFhCqjTaQ5mJ9PxEe57v/A8snv6JzBRkePZnXGPzu5z+oeHL+Z+KPv9j3+d/rl1zQgod+iWpr9AcrWrcqHz/28X10G3qDzt+f6E46GYXccoZ9yBuMmLT02iJEkHv6cWX/JTi1xI08YXo6qO1y1dovLD3il5hcvq+AJ+2+oIfnL/VQBMiWQwfbHfnX7R1BIq+Vm33AqFOMS16K9rg9d7X9ZJJA9HR9DpTN5zbfPQ1CXQ1Kkbna3vD8ArDVMk5Or8xgC09tL2I48KuT/HkbWJYB8MxSLr1LelfILsvKKeb8/ss3fJPtwmeEGGEoevDsl83pbn+1Veh736b5L5jKOfpGp6juPyC54CucJ3m8vdTy8jIyMjIyMjIyMjIyMjIyBDhD3/FdXNd3zfAAAAAAElFTkSuQmCC"
              alt="header-user"
            />
            <h6>Mr.{currentUser}</h6>
          </div>
        </Modal.Header>
        <Modal.Body>
          <Form.Row
            style={{ display: 'flex', justifyContent: 'space-between' }}
          >
            <Form.Group as={Col} md="12">
              <Form.Row style={{ backgroundColor: '#a9a9a9', padding: '1%' }}>
                {/* <Form.Group
                  as={Col}
                  md="6"
                  style={{ display: 'flex', alignItems: 'center' }}
                >
                  <h5 className="column-form-label">
                    USER TRANSACTION HISTORY
                  </h5>
                </Form.Group> */}
                {/* <Form.Group as={Col} md="4">
                  <Form.Control
                    value={
                      
                    }
                    id="user-id"
                  />
                </Form.Group> */}
                {/* <Form.Group as={Col} md="4" >
                <Form.Control value="USD" id="currency" />
              </Form.Group> */}

                <Card style={{ width: '100%', margin: 0 }}>
                  <Card.Body>
                    <Tab.Container
                      id="left-tabs-example"
                      defaultActiveKey="EUR"
                    >
                      <Row>
                        <Col>
                          <Nav
                            variant="pills"
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                              marginBottom: 10,
                              // marginTop: -50,
                            }}
                          >
                            <Nav.Item
                              onClick={() => {
                                this.setState({ currentCurrency: 'EUR' });
                              }}
                            >
                              <Nav.Link eventKey="EUR">EUR</Nav.Link>
                            </Nav.Item>
                            <Nav.Item
                              onClick={() => {
                                this.setState({ currentCurrency: 'USD' });
                              }}
                            >
                              <Nav.Link eventKey="USD">USD</Nav.Link>
                            </Nav.Item>
                            <Nav.Item
                              onClick={() => {
                                this.setState({ currentCurrency: 'GBP' });
                              }}
                            >
                              <Nav.Link eventKey="GBP">GBP</Nav.Link>
                            </Nav.Item>
                            <Nav.Item
                              onClick={() => {
                                this.setState({ currentCurrency: 'RUB' });
                              }}
                            >
                              <Nav.Link eventKey="RUB">RUB</Nav.Link>
                            </Nav.Item>
                          </Nav>
                        </Col>
                        <Col sm={12}>
                          <Tab.Content>
                            <Tab.Pane eventKey="EUR">
                              <div className="card-body datatable-react pt-0 ">
                                <ReactTable
                                  columns={transactionHistoryColumn}
                                  data={eurData}
                                  defaultPageSize={10}
                                />
                              </div>
                            </Tab.Pane>
                            <Tab.Pane eventKey="USD">
                              <div className="card-body datatable-react pt-0 ">
                                <ReactTable
                                  columns={transactionHistoryColumn}
                                  data={usdData}
                                  defaultPageSize={10}
                                />
                              </div>
                            </Tab.Pane>
                            <Tab.Pane eventKey="GBP">
                              <div className="card-body datatable-react pt-0 ">
                                <ReactTable
                                  columns={transactionHistoryColumn}
                                  data={gbpData}
                                  defaultPageSize={10}
                                />
                              </div>
                            </Tab.Pane>
                            <Tab.Pane eventKey="RUB">
                              <div className="card-body datatable-react pt-0 ">
                                <ReactTable
                                  columns={transactionHistoryColumn}
                                  data={rubData}
                                  defaultPageSize={10}
                                />
                              </div>
                            </Tab.Pane>
                          </Tab.Content>
                        </Col>
                      </Row>
                    </Tab.Container>
                    {/* <div className="card-body datatable-react sudata-table">
                      <ReactTable
                        columns={transactionHistoryColumn}
                        data={data}
                        defaultPageSize={10}
                      />
                    </div> */}
                  </Card.Body>
                </Card>
              </Form.Row>
            </Form.Group>
          </Form.Row>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  clientTransaction: state.adminReducer.clientTransaction,
});
export default connect(mapStateToProps, { getClientTransaction })(
  UserTransactionHistory
);
