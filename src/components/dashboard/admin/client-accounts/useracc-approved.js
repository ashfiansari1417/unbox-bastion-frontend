import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
// import RequestData from './requestData'
// import { ToastContainer, toast } from 'react-toastify';
import UaRequestData from './uaRequestData';
import { GetNewUserSignupDetails } from '../../../../constant/actionTypes';

class UaApproved extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestDetails: [],
    };
  }
  componentDidMount() {
    this.getTransferRequest();
  }

  getTransferRequest = () => {
    console.log('call');
    axios
      .post(GetNewUserSignupDetails, {
        type: 1,
      })
      .then((res) => {
        console.log(res);
        this.setState({
          requestDetails: res.data.value,
        });
      });
  };

  render() {
    const { requestDetails } = this.state;
    console.log(requestDetails);
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: 'First Name',
        accessor: 'first_name',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: 'Last Name',
        accessor: 'last_name',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: 'Email',
        accessor: 'email',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: 'Status',
        accessor: 'status',

        style: {
          textAlign: 'center',
        },
      },
    ];
    return (
      <Fragment>
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
              <div className="card">
                <div className="card-header span">
                  <h5>Client Accounts Approved</h5>
                </div>
                <div className="card-body datatable-react">
                  <ReactTable
                    data={requestDetails}
                    columns={columns}
                    defaultPageSize={15}
                    className="-striped -highlight"
                    showPagination={true}
                    SubComponent={(v) => (
                      <div style={{ padding: '10px' }}>
                        <UaRequestData id={v.row} />
                      </div>
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UaApproved;
