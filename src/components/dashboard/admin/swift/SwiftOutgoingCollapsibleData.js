import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import { Form, Button, Col } from 'react-bootstrap';
import { CustomInput, Input } from 'reactstrap';
import '../client-accounts/uarequestdata.scss';

class UaRequestData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.setState({
      data: this.props.id._original,
    });
  }

  render() {
    const { data } = this.props;

    return (
      <Fragment>
        {data ? (
          <Fragment>
            <div className="dtDpValWrapper row">
              <div className="col-12 all-info">
                <div className="card">
                  <center>
                    <h4>Transaction ID : {data[''].id}</h4>
                  </center>
                  <div className="card-inner">
                    <div className="column-styling column-side-border">
                      <div className="column-form">
                        <Form>
                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Transfer type
                              </Form.Label>

                              <Form.Control
                                type="text"
                                // placeholder="swift code"
                                value="External"
                                id="transfer-type"
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Sender Account
                              </Form.Label>
                              <Form.Control
                                value={data[''].senderAccountNumber}
                                id="sender-account"
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>

                          <Form.Row>
                            {/* <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Sender Currency
                              </Form.Label>
                              <Form.Control
                                id="sender-currency"
                                value={data[''].senderCurrency}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group> */}

                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Sender Country
                              </Form.Label>
                              <Form.Control
                                id="sender-company-country"
                                value={data[''].senderCompanyCountry}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>
                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Sender Address
                              </Form.Label>
                              <Form.Control
                                id="sender-address"
                                value={data[''].senderAddress}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>
                          <br />

                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                <strong>Commission :</strong> $35
                              </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                <strong>Message to operator :</strong>
                                {data[''].messageToOperator}
                              </Form.Label>
                            </Form.Group>
                          </Form.Row>
                        </Form>
                      </div>
                    </div>
                    <div className="column-styling column-side-border">
                      <div className="column-form">
                        <Form>
                          <Form.Label
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                            }}
                          >
                            <strong style={{ textAlign: 'center' }}>
                              Recipient Bank
                            </strong>
                          </Form.Label>

                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                SWIFT Code :
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="swift code"
                                id="recipient-swift"
                                value={data[''].swiftCode}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>
                          <br />

                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Account Number(IBAN) :
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="IBAN"
                                id="recipient-accno"
                                value={data[''].recipientAccountNo}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>
                          {/* <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Name of Bank :
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Name of Bank"
                              />
                            </Form.Group>
                          </Form.Row> */}
                        </Form>
                      </div>
                    </div>
                    <div className="column-styling">
                      <div className="column-form">
                        <Form>
                          <Form.Label
                            style={{
                              display: 'flex',
                              justifyContent: 'center',
                            }}
                          >
                            <strong style={{ textAlign: 'center' }}>
                              Reciever Information
                            </strong>
                          </Form.Label>
                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Reciever Name :
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="swift code"
                                style={{ fontSize: 14 }}
                                value={data[''].receiver}
                              />
                            </Form.Group>
                          </Form.Row>

                          <br />
                          <Form.Row>
                            <Form.Group as={Col}>
                              <Form.Label className="column-form-label">
                                Address :
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="address"
                                id="reciever-address"
                                value={data[''].recieverAddress}
                                style={{ fontSize: 14 }}
                              />
                            </Form.Group>
                          </Form.Row>
                        </Form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Client Details:</strong></div>
                                            <div>Client Type:  <span className="dtDpVal">{data.client_type}</span></div>
                                            <div>Client Name:  <span className="dtDpVal">{data.client_name}</span></div>
                                            <div>Client Address: <span className="dtDpVal">{data.client_address}</span></div>
                                            <div>Client Address2: <span className="dtDpVal">{data.client_address2}</span></div>
                                            <div>Client Account: <span className="dtDpVal">{data.client_account_number}</span></div>
                                            <div>Client Country: <span className="dtDpVal">{data.client_country}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                    <div className="card-body border">
                                            <div><strong>Transfer Details:</strong></div>
                                            <div>Transfer ID: <span className="dtDpVal">{data.id}</span></div>
                                            <div>Currency: <span className="dtDpVal">{data.currency}</span></div>
                                            <div>Amount: <span className="dtDpVal">{data.amount}</span></div>
                                            <div>Date: <span className="dtDpVal">{data.group4_32A_Date? <Moment format="DD/MM/YYYY">{(data.group4_32A_Date).replace(/(\d\d)(\d\d)(\d\d)/g,'$1$1-$2-$3')}</Moment> : '-'}</span></div>
                                            <div>Transfer Type: <span className="dtDpVal">{data.type_of_transfer}</span></div>
                                            <div>Status: <span className="dtDpVal">-</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Beneficiary Bank Details:</strong></div>
                                            <div>Beneficiary Bank SWIFT: <span className="dtDpVal">{data.benbank_SWIFT}</span></div>
                                            <div>Beneficiary Bank Name: <span className="dtDpVal">{data.benbank_name}</span></div>
                                            <div>Beneficiary Bank Address: <span className="dtDpVal">{data.benbank_address}</span></div>
                                            <div>Beneficiary Bank Location: <span className="dtDpVal">{data.benbank_location}</span></div>
                                            <div>Beneficiary Bank Country: <span className="dtDpVal">{data.benbank_country}</span></div>
                                            <div>Beneficiary Bank NCS Number: <span className="dtDpVal">{data.benbank_NCS_number}</span></div>
                                            <div>Beneficiary ABA RTA: <span className="dtDpVal">{data.benbank_ABA}</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col">    
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Intermediary Bank Details:</strong></div>
                                            <div>Inter Bank SWIFT: <span className="dtDpVal">{data.interbank_SWIFT}</span></div>
                                            <div>Inter Bank Name: <span className="dtDpVal">{data.interbank_name}</span></div>
                                            <div>Inter Bank Address: <span className="dtDpVal">{data.interbank_address}</span></div>
                                            <div>Inter Bank Location: <span className="dtDpVal">{data.interbank_location}</span></div>
                                            <div>Inter Bank Country: <span className="dtDpVal">{data.interbank_country}</span></div>
                                            <div>Inter Bank NCS Number: <span className="dtDpVal">{data.interbank_NCS_number}</span></div>
                                            <div>Inter Bank ABA RTA: <span className="dtDpVal">{data.interbank_ABA}</span></div>
                                            <div>Inter Bank Account Number: <span className="dtDpVal">{data.interaccount_Number}</span></div>
                                        </div>
                                    </div>
                                </div> */}
              {/* <div className="col-sm-12">
                                    <div className="card">
                                        <div className="card-body border">
                                            <div><strong>Reference Message:</strong></div>
                                            <div>
                                                <span className="dtDpVal">
                                                    {data.reference_message}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
            </div>
          </Fragment>
        ) : (
          <span style={{ textAlign: 'center', padding: 20 }}>
            No data available
          </span>
        )}
      </Fragment>
    );
  }
}

export default UaRequestData;
