import React, { Component, Fragment } from 'react';
import { Tab, Row, Col, Nav } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import ReactTable from 'react-table';
import {
  getPendingOutgoingTransactions,
  swiftAdminApproval,
} from '../../../../actions/admin/swiftTransaction';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import { Link } from 'react-router-dom';
import 'react-table/react-table.css';
import UaRequestData from './SwiftInternalCollapsibleData';

export class SwiftInternal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      underCompliance: [],
      approvedRequests: [],
      rejectedRequests: [],
      isAdmin: true,
      optError: false,
      status: {
        PE: 'Pending',
        PA: 'Pre-Approved',
        AP: 'Approved',
        PR: 'Pre-Rejected',
        R: 'Rejected',
      },
      currentTab: 'undercompliance',
    };
  }

  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });

      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        this.props.getPendingOutgoingTransactions(
          localStorage.getItem('access')
        );
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.swiftOutgoingPendingTransactions !==
      this.props.swiftOutgoingPendingTransactions
    ) {
      const { swiftOutgoingPendingTransactions } = this.props;
      const pendingData = [];
      const approvedData = [];
      const rejectedData = [];

      for (let d in swiftOutgoingPendingTransactions.outgoing_internal) {
        // pending and preapproved
        if (
          swiftOutgoingPendingTransactions.outgoing_internal[d].status ===
            'P' ||
          swiftOutgoingPendingTransactions.outgoing_internal[d].status ===
            'Pre-Approved'
        ) {
          pendingData.push({
            id: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reference_number
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reference_number
              : '',
            // *** INVOICE ****
            invoice: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .url_invoice
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .url_invoice
              : '',
            // *** *****
            date: swiftOutgoingPendingTransactions.outgoing_internal[d].date
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].date
              : '-',
            sender: swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              : '',
            receiver: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reciver
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].reciver
              : '',
            paymentDesc: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .remark
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].remark
              : '',
            amount: swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              : '',
            status:
              swiftOutgoingPendingTransactions.outgoing_internal[d].status ===
              'Pre-Approved'
                ? 'PA'
                : 'P',
            senderAccountNumber: swiftOutgoingPendingTransactions
              .outgoing_internal[d].sender_acc_no
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_acc_no
              : '',
            senderCompany: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .sender_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_company
              : '',
            senderCompanyCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].sender_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_country
              : '',
            recipientCompanyName: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_company
              : '',
            recipientCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_country
              : '',
            recipientAccountNo: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_acc_no
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_acc_no
              : '',
          });
        }
        // approved
        else if (
          swiftOutgoingPendingTransactions.outgoing_internal[d].status ===
          'Approved'
        ) {
          approvedData.push({
            id: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reference_number
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reference_number
              : '',
            // *** INVOICE ****
            invoice: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .url_invoice
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .url_invoice
              : '',
            // *** *****
            date: swiftOutgoingPendingTransactions.outgoing_internal[d].date
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].date
              : '-',
            sender: swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              : '',
            receiver: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reciver
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].reciver
              : '',
            paymentDesc: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .remark
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].remark
              : '',
            amount: swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              : '',
            status:
              swiftOutgoingPendingTransactions.outgoing_internal[d].status,
            senderAccountNumber: swiftOutgoingPendingTransactions
              .outgoing_internal[d].sender_acc_no
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_acc_no
              : '',
            senderCompany: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .sender_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_company
              : '',
            senderCompanyCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].sender_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_country
              : '',
            recipientCompanyName: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_company
              : '',
            recipientCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_country
              : '',
            recipientAccountNo: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_acc_no
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_acc_no
              : '',
          });
        } else if (
          swiftOutgoingPendingTransactions.outgoing_internal[d].status ===
          'Rejected'
        ) {
          rejectedData.push({
            id: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reference_number
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reference_number
              : '',
            // *** INVOICE ****
            invoice: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .url_invoice
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .url_invoice
              : '',
            // *** *****
            date: swiftOutgoingPendingTransactions.outgoing_internal[d].date
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].date
              : '-',
            sender: swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].sender
              : '',
            receiver: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .reciver
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].reciver
              : '',
            paymentDesc: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .remark
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].remark
              : '',
            amount: swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              ? swiftOutgoingPendingTransactions.outgoing_internal[d].amt
              : '',
            status:
              swiftOutgoingPendingTransactions.outgoing_internal[d].status,
            senderCompany: swiftOutgoingPendingTransactions.outgoing_internal[d]
              .sender_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_company
              : '',
            senderCompanyCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].sender_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .sender_country
              : '',
            recipientCompanyName: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_company
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_company
              : '',
            recipientCountry: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_country
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_country
              : '',
            recipientAccountNo: swiftOutgoingPendingTransactions
              .outgoing_internal[d].reciver_acc_no
              ? swiftOutgoingPendingTransactions.outgoing_internal[d]
                  .reciver_acc_no
              : '',
          });
        }
      }

      this.setState({
        underCompliance: pendingData,
        approvedRequests: approvedData,
        rejectedRequests: rejectedData,
      });
    }
  }
  changeColor = (value, txnId, e) => {
    if (e.target.style.color === 'rgb(0, 0, 0)') {
      alert('You have already preapprove/prereject the user');
    } else {
      var ans = window.confirm(`Are you sure you want to ${value} request?`);

      if (ans) {
        if (value === 'preapprove') {
          if (
            document.querySelector(e.target.attributes['data-target'].value)
              .style.color === 'rgb(0, 0, 0)'
          ) {
            alert('Request already preapproved');
          } else {
            //  preapprove api action call here
            this.handleActionClick(txnId, 'PA');
            e.target.style.color = '#000';
            document
              .querySelector(e.target.attributes['data-target'].value)
              .classList.add('disabled');
          }
        } else if (value === 'prereject') {
          if (
            document.querySelector(e.target.attributes['data-target'].value)
              .style.color === 'rgb(0, 0, 0)'
          ) {
            alert('Request already prerejected');
          } else {
            let reason = prompt(
              'Please tell us why are you pre-rejecting this user?'
            );
            if (reason !== '' && reason !== null) {
              //  prereject api action call here
              var div = document.createElement('p');
              div.style.color = '#ff0000';
              div.style.height = '3em';
              div.innerHTML = reason;
              e.currentTarget.appendChild(div);
              document
                .querySelector(e.target.attributes['data-target'].value)
                .classList.add('disabled');
              e.target.style.color = '#000';
            }
          }
        }
      }
    }
  };

  handleActionClick = (txn_id, status) => {
    let data = { txn_id, status };
    this.props.swiftAdminApproval(data, localStorage.getItem('access'));
  };

  // handleHeaderClick = () => {
  //   if (this.state.isAdmin) {
  //     let checkboxes = document.querySelectorAll('.rowCheckbox');
  //     checkboxes.forEach((c) => (c.checked = !c.checked));
  //   }
  // };

  render() {
    const { currentTab } = this.state;

    const reusableColumns = [
      {
        Header: '',
        accessor: '',
        width: 50,
        Cell: (row) => (
          <div>
            <a
              href={row.value.invoice}
              download
              style={{ color: 'black' }}
              title="Download Invoice"
            >
              <i
                style={{ width: 35, fontSize: 18, padding: 11 }}
                className="fa fa-envelope-open"
              ></i>
            </a>
          </div>
        ),
        style: {
          textAlign: 'center',
        },
      },
      //   {
      //     Header: (
      //       <input
      //         type="checkbox"
      //         style={{ width: '100%' }}
      //         disabled={!this.state.isAdmin}
      //         onChange={() => {
      //           this.handleHeaderClick();
      //         }}
      //       />
      //     ),
      //     accessor: '',
      //     width: 50,
      //     Cell: (row) => (
      //       <div>
      //         <input
      //           type="checkbox"
      //           style={{ width: '100%' }}
      //           className="rowCheckbox"
      //           disabled={!this.state.isAdmin}
      //         />
      //       </div>
      //     ),
      //     style: {
      //       textAlign: 'center',
      //     },
      //     sortable: false,
      //   },
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        width: 100,
        accessor: 'date',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Sender <i className="fa fa-sort"></i>
          </b>
        ),
        width: 120,
        accessor: 'sender',
        style: {
          textAlign: 'left',
        },
      },
      {
        Header: (
          <b>
            Receiver <i className="fa fa-sort"></i>
          </b>
        ),
        width: 120,
        accessor: 'receiver',
        style: {
          textAlign: 'left',
        },
      },
      {
        Header: (
          <b>
            Payment Description <i className="fa fa-sort"></i>
          </b>
        ),
        accessor: 'paymentDesc',
        width: 200,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Amount <i className="fa fa-sort"></i>
          </b>
        ),
        // accessor: 'amount',
        Cell: (row) => (
          <span>{row.original.amount && row.original.amount.toFixed(2)}</span>
        ),
        style: {
          textAlign: 'center',
        },
      },
    ];
    let undercomplianceActions = {
      Header: <b>Action</b>,
      id: 'delete',
      accessor: (str) => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span
            title="Preapprove Request"
            style={{ cursor: 'pointer' }}
            onClick={this.changeColor.bind(this, 'preapprove', row.original.id)}
          >
            <i
              id={`preapprove${row.original.id}`}
              className="fa fa-check-circle"
              data-target={`#prereject${row.original.id}`}
              style={
                row.original.status === 'PA'
                  ? { width: 35, fontSize: 18, padding: 11, color: '#000' }
                  : { width: 35, fontSize: 18, padding: 11, color: '#ffff00' }
              }
            ></i>
          </span>

          <span
            title="Approve Request"
            className={this.state.isAdmin ? '' : 'disabled'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              // alert(this.state.status.AP);
              var d = window.confirm(
                'Are you sure you want to approve request?'
              );
              if (d) {
                this.handleActionClick(row.original.id, 'AP');
              }
            }}
          >
            <i
              className="fa fa-check"
              style={{
                width: 35,
                fontSize: 16,
                padding: 11,
                color: 'rgb(40, 167, 69)',
              }}
            ></i>
          </span>

          <span
            title="Prereject Request"
            style={{ cursor: 'pointer' }}
            onClick={this.changeColor.bind(this, 'prereject', row.original.id)}
          >
            <i
              id={`prereject${row.original.id}`}
              data-target={`#preapprove${row.original.id}`}
              className="fa fa-undo"
              style={{ width: 35, fontSize: 18, padding: 11, color: '#ffff00' }}
            ></i>
          </span>
          <span
            title="Reject Request"
            style={{ cursor: 'pointer' }}
            className={this.state.isAdmin ? '' : 'disabled'}
            onClick={() => {
              var d = window.confirm(
                'Are you sure you want to reject request?'
              );
              if (d) {
                this.handleActionClick(row.original.id, 'R');
              }
            }}
          >
            <i
              className="fa fa-undo"
              style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    };

    let approvedActions = {
      Header: <b>Action</b>,
      id: 'delete',
      accessor: (str) => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span
            title="Approve Request"
            className={this.state.isAdmin ? '' : 'disabled'}
            style={{ cursor: 'pointer' }}
            onClick={() => {
              // alert(this.state.status.AP);
              var d = window.confirm(
                'Are you sure you want to approve request?'
              );
              if (d) {
                this.handleActionClick(row.original.id, 'AP');
              }
            }}
          >
            <i
              className="fa fa-check"
              id={`approve${row.original.id}`}
              style={{
                width: 35,
                fontSize: 16,
                padding: 11,
                color: 'rgb(40, 167, 69)',
              }}
            ></i>
          </span>
          <span
            title="Reject Request"
            style={{ cursor: 'pointer' }}
            className={this.state.isAdmin ? '' : 'disabled'}
            onClick={() => {
              var d = window.confirm(
                'Are you sure you want to reject request?'
              );
              if (d) {
                this.handleActionClick(row.original.id, 'R');
              }
            }}
          >
            <i
              className="fa fa-times"
              id={`reject${row.original.id}`}
              style={{ width: 35, fontSize: 18, padding: 11, color: '#e4566e' }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    };
    let rejectedActions = {
      Header: <b>Reason</b>,
      id: '',
      accessor: '',
      width: 150,
      Cell: (row) => (
        <div>
          <p>Bad Invoice</p>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    };
    let executedActions = {
      Header: <b>Executed</b>,
      width: 150,
      Cell: (row) => (
        <div>
          <i
            style={{ width: 35, fontSize: 18, padding: 11 }}
            className="fa fa-envelope-open"
          ></i>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    };
    return (
      <Fragment>
        {/* <div className="swift-outgoing-search">
          <input type="search" placeholder="Search..." />
          <button>Go</button>

          <div className="logout-admin">
            <Link onClick={() => localStorage.clear()} to="/admin-login">
              Logout
            </Link>
          </div>
        </div> */}

        <Tab.Container
          id="left-tabs-example"
          defaultActiveKey="undercompliance"
        >
          <Row>
            <Col sm={9}>
              <Nav variant="pills" style={{ marginLeft: '6%' }}>
                <Nav.Item>
                  <Nav.Link eventKey="undercompliance">
                    Under Compliance
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="approved">Approved Requests</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="rejected">Rejected Requests</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="executed">Executed Payments</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={12}>
              <Tab.Content>
                <Tab.Pane eventKey="undercompliance">
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="card">
                          <div className="card-body datatable-react sudata-table">
                            <ReactTable
                              data={this.state.underCompliance}
                              columns={[
                                ...reusableColumns,
                                undercomplianceActions,
                              ]}
                              defaultPageSize={5}
                              SubComponent={(data) => (
                                <div>
                                  <UaRequestData
                                    id={data.row[''].id}
                                    data={data.row['']}
                                  />
                                </div>
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="approved">
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="card">
                          <div className="card-body datatable-react sudata-table">
                            <ReactTable
                              data={this.state.approvedRequests}
                              columns={[...reusableColumns, approvedActions]}
                              defaultPageSize={5}
                              SubComponent={(data) => (
                                <div>
                                  <UaRequestData
                                    id={data.row[''].id}
                                    data={data.row}
                                  />
                                </div>
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="rejected">
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="card">
                          <div className="card-body datatable-react sudata-table">
                            <ReactTable
                              data={this.state.rejectedRequests}
                              columns={[...reusableColumns, rejectedActions]}
                              defaultPageSize={5}
                              // SubComponent={(v) => (
                              //   <div>
                              //     <UaRequestData id={v.row} />
                              //   </div>
                              // )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="executed">
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="card">
                          <div className="card-body datatable-react sudata-table">
                            <ReactTable
                              data={this.state.executedRequests}
                              columns={[...reusableColumns, executedActions]}
                              // SubComponent={(v) => (
                              //   <div>
                              //     <UaRequestData id={v.row} />
                              //   </div>
                              // )}
                              defaultPageSize={5}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  swiftOutgoingPendingTransactions:
    state.adminReducer.swiftOutgoingPendingTransactions,
  //   swiftAdminApprovalMessage: state.AdminSepaReducer.swiftAdminApprovalMessage,
});
export default connect(mapStateToProps, {
  getPendingOutgoingTransactions,
  swiftAdminApproval,
})(SwiftInternal);

// SwiftInternal
