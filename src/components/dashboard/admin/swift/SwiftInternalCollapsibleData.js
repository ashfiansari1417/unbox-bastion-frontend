import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import { Form, Button, Col } from 'react-bootstrap';
import { CustomInput, Input } from 'reactstrap';
import '../client-accounts/uarequestdata.scss';

class UaRequestData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.setState({
      data: this.props.id._original,
    });
  }

  render() {
    const { data } = this.props;
    console.log(data);

    return (
      <Fragment>
        {this.props.data ? (
          <Fragment>
            <div
              className="dtDpValWrapper row"
              style={{ borderRadius: '20px', backgroundColor: '#f9f9f9' }}
            >
              <div className="col-12 all-info">
                <div className="card">
                  <h5>Transaction ID : {data.id}</h5>
                  <div className="card-inner">
                    <div className="column-styling">
                      <label>
                        <strong> -------Sender------- </strong>
                      </label>
                      <div>
                        <strong>Company Name:</strong>
                        <span className="dtDpVal">
                          {data.senderCompany || 'Sterling'}
                        </span>
                      </div>
                      <div>
                        <strong>Company Country:</strong>
                        <span className="dtDpVal">
                          {data.senderCompanyCountry}
                        </span>
                      </div>
                      <div>
                        <strong>Account Number :</strong>
                        <span>{data.senderAccountNumber}</span>
                      </div>
                    </div>

                    <div className="column-styling">
                      <label>
                        <strong> -------Receiver------- </strong>
                      </label>
                      <div>
                        <strong>Company Name:</strong>
                        <span className="dtDpVal">
                          {data.recipientCompanyName || 'Sterling Payment'}
                        </span>
                      </div>
                      <div>
                        <strong>Company Country:</strong>
                        <span className="dtDpVal">
                          {data.recipientCountry || 'Hong Kong'}
                        </span>
                      </div>
                      <div>
                        <strong>Account Number :</strong>
                        <span>{data.recipientAccountNo}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Fragment>
        ) : (
          'loading....'
        )}
      </Fragment>
    );
  }
}

export default UaRequestData;
