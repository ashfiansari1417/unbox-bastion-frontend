import React, { Component, Fragment } from 'react';
import axios from 'axios';
// import { ToastContainer, toast } from 'react-toastify';

import { GET_TOTAL_BALANCE } from '../../../constant/actionTypes';

// import Datatable from '../../common/datatable';

class TotalBalance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestDetails: [],
            RUB:0,
            EUR:0,
            GBP:0,
            USD:0,
            loder:true
        }
    }

    componentDidMount () {
        this.getTransferRequest();
    }

    getTransferRequest = () => {
        axios.get(GET_TOTAL_BALANCE)
        .then( res => {
            // console.log(res)
            this.setState({
                RUB: res.data.result.Items[0]['RUB'],
                EUR: res.data.result.Items[1]['EUR'],
                GBP: res.data.result.Items[2]['GBP'],
                USD: res.data.result.Items[3]['USD'],
                loder:false
            })
            // console.log(this.state)
            
        })
    }

    render() {
        const { RUB,EUR,GBP,USD} = this.state;

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Total Balance</h5>
                                </div>
                                <div className="card-body currenyBalTxt">
                                   <p><span>Total Balance : </span> RUB {new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2, currency: 'RUB' }).format(RUB)} <i hidden={this.state.loder === false} className="fa fa-spinner fa-spin" /></p>
                                   <p><span>Total Balance : </span> EUR {new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2, currency: 'EUR' }).format(EUR)} <i hidden={this.state.loder === false} className="fa fa-spinner fa-spin" /></p>
                                   <p><span>Total Balance : </span> GBP {new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2, currency: 'GBP' }).format(GBP)} <i hidden={this.state.loder === false} className="fa fa-spinner fa-spin" /></p>
                                   <p><span>Total Balance : </span> USD {new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2, currency: 'USD' }).format(USD)} <i hidden={this.state.loder === false} className="fa fa-spinner fa-spin" /></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default TotalBalance;