import React, { Component } from 'react';
// import { ReCaptcha } from 'react-recaptcha-google'
import ReCAPTCHA from "react-google-recaptcha";

export default class CaptchaComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.expiredCallback = this.expiredCallback.bind(this);
    this.isReady = this.isReady.bind(this)
  }
  componentDidMount() {
    console.log(this.captchaDemo)
    if (this.captchaDemo) {
        console.log("started, just a second...")
        this.captchaDemo.reset();
    }
  }
  onLoadRecaptcha() {
      if (this.captchaDemo) {
          this.captchaDemo.reset();
      }
  }
  isReady = function isReady() {
    return typeof window !== 'undefined' && typeof window.grecaptcha !== 'undefined';
};
  verifyCallback(recaptchaToken) {
    // Here you will get the final recaptchaToken!!!  
    console.log(recaptchaToken, "<= your recaptcha token")
    this.props.isVerified(true);
  }
  expiredCallback(){
    this.props.isVerified(false);
  }
  render() {
    return (
      <div >
        {/* You can replace captchaDemo with any ref word */}
        {<ReCAPTCHA
            ref={(el) => {this.captchaDemo = el;}}
            size="normal"
            theme="light"            
            render="explicit"
            sitekey="6Ld5hOQUAAAAAEnutmeaK9o3X9la4gyeSSBRScSI"
            // onloadCallback={this.onLoadRecaptcha}
            onChange={this.verifyCallback}
            onExpired={this.expiredCallback}
        />}
        {/* <code>
          1. Add <strong>your site key</strong> in the ReCaptcha component. <br/>
          2. Check <strong>console</strong> to see the token.
        </code> */}
      </div>
    );
  };
};

