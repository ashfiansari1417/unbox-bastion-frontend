import React, { Fragment, Component } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { CountryDropdown } from 'react-country-region-selector';
import { TRANSFER_REQUEST, GET_USER_ACCOUNT_DETAILS } from '../../../constant/actionTypes';
import CaptchaComponent from './Captcha';
class transferForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            account: "",
            userID: "",
            userName:"",
            userAddress:"",
            accountNo: [],
            isDisabled: true,
            loginProcessing:false,
            available_balance: 0.00,
            transfrTypeVal: 'External',
            userFullname: '',
            userCompany: '',
            allItems: [],
            captchaResponse: false,
            swiftError: false
        }

    }

    UNSAFE_componentWillMount() {
        const getID = localStorage.getItem('user');
        
        const userID = JSON.parse(getID)
        const userName=(userID.company_name=='')?userID.first_name+' '+userID.last_name:userID.company_name;
        const userAddress=userID.address;
        const usern1=userID.first_name;
        const usern2=userID.middle_name;
        const usern3=userID.last_name;
        // console.log(usern1)
        // console.log(usern2)
        // console.log(usern3)
        const userFullname= (usern1 == null ? '' : usern1) +' '+ (usern2 == null ? '' : usern2) +' '+ (usern3 == null ? '' : usern3);
        const userCompany=userID.company_name;

        this.setState({
            userID: userID.id,
            userName:userName,
            userAddress:userAddress,
            userFullname: userFullname,
            userCompany: userCompany
        })
    }

    componentDidMount() {
        this.getAccountNumber();
    }



    handleChange = event => {
        if(event.target.name == 'swift1') {
            var reg = /^[A-Z0-9]{8}$/;
            console.log(this.state.swift1);
            this.setState({ [event.target.name]: event.target.value });
            if(reg.test(event.target.value)){
                this.setState({swiftError : false})
            }
            else {
                this.setState({swiftError : true})
            }
        }
        else {
            this.setState({ [event.target.name]: event.target.value });
        }
        setTimeout(() => {
            this.handleError();
            console.log(this.state)
        }, 500)

    };
    selectCountry(val) {
        this.setState({ country: val });
    }
    handleAccountChange = event => {
        
        this.setState({ [event.target.name]: event.target.value });
        // console.log(event.target.value)
        let cur = event.target.value.split('_');
        // console.log(cur)
        this.state.accArrays.forEach(item => {
            if (item.account_number == cur[0]) {
                    if(typeof item.available_balance_ch!='undefined'){
                    var avalibalblebalance=parseFloat(item.available_balance_ch);
                    }else{
                        var avalibalblebalance=0;
                    }
                // console.log(avalibalblebalance);
                this.setState({ available_balance: (cur[1] +' '+new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(avalibalblebalance) )})
            }
        })
        this.setState({ currency: cur.pop() });
        setTimeout(() => {
            this.handleError();
        }, 500)
    }
    handleTransferTypeVal= event =>{
        const { account, amount, swift1, name1,country,ncs,aba,type,name2,address2, address3,country2,
            iban,transfrTypeVal,refmsg,swift2,name3,country3,ncsno,abartn,aniban, captchaResponse
        } = this.state;
        // console.log(event.target.value)
        if(event.target.value == 'Internal'){
            this.setState({transfrTypeVal: event.target.value});
            if (account && amount && type && iban && name2 && refmsg && captchaResponse) 
            {
                this.setState({ isDisabled: false });
            } else {
                this.setState({ isDisabled: true });
            }
        }
        else{
            this.setState({transfrTypeVal: event.target.value});
            if (account && amount && swift1 && name1 && country &&  
                type && name2 && address2 && country2 && iban && refmsg && captchaResponse
            ) 
            {
                this.setState({ isDisabled: false });
            } else {
                this.setState({ isDisabled: true });
            }
        }
        
    }
    goBack = () => {
        this.props.history.push('/dashboard/my-account');
        this.handleError();
    }

    handleError = () => {
        var swiftReg = /^[A-Z0-9]{8}$/;
        const { account, amount, swift1, name1,country,ncs,aba,
            type,name2,address2,address3,country2,iban,
            transfrTypeVal,refmsg,swift2,name3,
            country3,ncsno,abartn,aniban,captchaResponse
        } = this.state;
        if(transfrTypeVal === 'External'){
            
            if (account && amount && swiftReg.test(swift1) && name1 && country &&  
                type &&
                name2 &&
                address2 &&
                country2 &&
                iban &&
                refmsg &&
                captchaResponse
                
            ) 
            {
                this.setState({ isDisabled: false });
            } else {
                this.setState({ isDisabled: true });
            }
        }
        else{
            if (account && amount && type && iban && name2 && refmsg && captchaResponse) 
            {
                console.log()
                this.setState({ isDisabled: false });
            } else {
                this.setState({ isDisabled: true });
            }
        }
    }

    getAccountNumber = () => {
        axios.post(GET_USER_ACCOUNT_DETAILS, {
            "userID": this.state.userID
        })
        .then(res => {
            console.log(res);
            // console.log(res.data.result.Items);
            let accArrays = [];
            res.data.result.Items.forEach(item => {
                accArrays.push(item);
            });

            this.setState({ accArrays: accArrays,allItems: res.data.result.Items });
            let accountArr = res.data.result.Items.map((item) => [item.account_number, "_", item.currency])
            this.setState({
                accountNo: accountArr
            })
        })
    }
   
    createTransferRequest = () => {
        const { history } = this.props;
        this.setState({
            loginProcessing:true,
            isDisabled:true
        });
        
        axios.post(TRANSFER_REQUEST, {
            "from_account": this.state.account.split('_')[0],
            "currency": this.state.currency,
            "amount": parseFloat(this.state.amount),
            "benbank_SWIFT": this.state.swift1,
            "benbank_name": this.state.name1,
            // "benbank_address": this.state.address1,
            // "benbank_location": this.state.location1,
            "benbank_country": this.state.country,
            "benbank_NCS_number": this.state.ncs,
            "benbank_ABA": this.state.aba,
            "client_type": this.state.type,
            "client_name": this.state.name2,
            "client_address": this.state.address2,
            "client_address2": this.state.address3,
            "client_country": this.state.country2,
            "client_account_number": this.state.iban,
            "reference_message": this.state.refmsg,
            "message_line1": "",
            "message_line2": this.state.ml2,
            "message_line3": this.state.ml3,
            "message_line4": this.state.ml4,
            "interbank_SWIFT": this.state.swift2,
            "interbank_name": this.state.name3,
            // "interbank_address": this.state.address4,
            // "interbank_location": this.state.location2,
            "interbank_country": this.state.country3,
            "interbank_NCS_number": this.state.ncsno,
            "interbank_ABA": this.state.abartn,
            "interaccount_Number": this.state.aniban,
            // "type_of_transfer": this.state.tot,
            // "foreign_bank_fees": parseInt(this.state.fbfees),
            'user_id': JSON.parse(localStorage.getItem('user')).id,
            'company_name': JSON.parse(localStorage.getItem('user')).company_name,
            'user_name': JSON.parse(localStorage.getItem('user')).first_name+' '+JSON.parse(localStorage.getItem('user')).last_name,
            'userAddress': JSON.parse(localStorage.getItem('user')).address,
            'transfer_type' : this.state.transfrTypeVal

        })
        .then((response)=>{
            this.setState({
                loginProcessing:false,
                isDisabled:false
            });
            if (response.data.result.length === parseInt(0)) {
                toast.error(response.data.message)
            } else {
                toast.success(response.data.message)
                history.push('/dashboard/my-account')
            }
            console.log(response);

        })
        .catch((error) =>{
            this.setState({
                loginProcessing:false,
                isDisabled:false
            });
            toast.error("Something went wrong");
        })
    }

    renderAccountInfo = () => {

        const { account } = this.state;
        // console.log(accArrays)
    let optionItems = this.state.allItems.map((acc, i) => <option key={i} disabled={parseFloat(acc.available_balance_ch) > 0 ? false : true} value={acc.account_number+'_'+acc.currency}>{acc.account_number+'_'+acc.currency}</option>);
        

        const renderAccInfo ={
            display: 'flex',


            justifyContent: 'space-around'
        }
        const preExper = {
            width:'192px'
        }


        const formGroupStyle = {
            display:'flex',
            alignItems:'center',
            justifyContent:'flex-end',


        }

        const labStyle={
            marginRight:'20px',
        }

        const inpStyle = {
            // marginLeft:'10px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }

        const inpStyleUn = {
            // width:'100%',
            // display:'table-cell',
            // marginLeft:'25px',
            // borderRadius: '10px',
            border:'none',
        }

        const fieldStyle={
            paddingTop:'10px',
        }

        const exper = {
            // marginLeft:'10px',
            // maxWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }

        const gridLike={
            display:'flex',
            flexDirection:'column',
            justifyContent:'space-around',
            width:'40%',
        }

        const gridLike2={
            display:'flex',
            flexDirection:'column',
            justifyContent:'space-around',
            paddingLeft:'10px',
        }

        return <Fragment>
            
                <div style={renderAccInfo} >
                    <div style={gridLike}>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>From Account&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                </div>
                                <div style={preExper}>
                                    <select style={exper} className="selectpicker show-tick form-control" name="account" onChange={this.handleAccountChange} defaultValue=''>
                                            <option disabled value=''> Please Select Account </option>
                                            {optionItems}
                                    </select>

                                </div>
                            </div>
                        </div>

                    
                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                
                                <div>
                                    <label style={labStyle}>Transfer Type &nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                </div>
                                <div style={preExper}>
                                    <select style={exper} className="form-control" name="transferType" onChange={this.handleTransferTypeVal} defaultValue={this.state.transfrTypeVal}>
                                            <option value='' disabled> Please Select Transfer Type </option>
                                            <option value='External'> External </option>
                                            <option value='Internal'> Internal </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style={gridLike2}>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle}>Amount&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                </div>
                                <div>
                                <input style={inpStyle} className="form-control" id="exampleFormControlInput3" type="number" name="amount" onChange={this.handleChange} />

                                </div>
                            </div>   
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>Available Balance&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                </div>
                                <div>
                                    <input style={inpStyleUn} className="form-control" id="exampleFormControlInput3" disabled type="text" value={this.state.available_balance} />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>  
            
            {/* <div className="row" >
                <div className="col"> 
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput1">From Account&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <select className="selectpicker show-tick form-control" name="account" onChange={this.handleAccountChange} defaultValue=''>
                                <option disabled value=''> Please Select Account </option>
                                {optionItems}
                            </select>
                    </div>
                </div>
                <div className="col"> 
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput13">Available Balance&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput3" disabled type="text" value={this.state.available_balance} />
                    </div>
                </div>
                <div className="col"> 
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput13">Amount&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput3" type="text" name="amount" onChange={this.handleChange} />
                    </div>
                </div>
                <div className="col"> 
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput1">Transfer Type &nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <select className="form-control" name="transferType" onChange={this.handleTransferTypeVal} defaultValue={this.state.transfrTypeVal}>
                                <option value='' disabled> Please Select Transfer Type </option>
                                <option value='External'> External </option>
                                <option value='Internal'> Internal </option>
                            </select>
                    </div>
                </div>
            </div> */}
        
        </Fragment>
    }

    renderBeneficiaryBank = () => {
        const dimSetter = {
            width:'50%'
        }
        const renderAccInfo ={

            display:'flex',
            flexDirection:'column',
            paddingRight:'30px',
        }

        const formGroupStyle = {

            display:'flex',
            alignItems:'center',
            justifyContent:'flex-end',

        }

        const labStyle={
            marginRight:'20px',
  
        }

        
        const inpStyle = {

            // marginLeft:'10px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const fieldStyle={
            paddingTop:'20px',
        }
        const sectTitle = {
            textAlign:'left',
            color: '#2778E2',
        }

        const preExper = {
            width:'192px'
        }

        const exper = {
            // marginLeft:'10px',
            // maxWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',

        }
        const errorStyle  = {
            color : "red"
        }
        if(this.state.transfrTypeVal == "External"){
            return <Fragment>
                <div style={dimSetter} >
                    <div className="col-12" style={sectTitle}> 
                        <h4 className="subLabelSeprator" style={{fontSize:'16px',textAlign:'center', paddingRight:'97px'}}>BENEFICIARY BANK</h4>
                    </div>

                    <div style={renderAccInfo}>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>SWIFT Code</label>
                                </div>
                                <div>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput4" type="text" name="swift1" onChange={this.handleChange}/>
                                    {this.state.swiftError && <div style={errorStyle}>Not Valid Swift code</div>}
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                <label style={labStyle}>Name of Bank</label>
                                </div>
                                <div>
                                <input style={inpStyle} className="form-control" id="exampleFormControlInput5" type="text" name="name1" onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>Country of Bank</label>
                                </div>
                                <div style={preExper}>
                                    <CountryDropdown style={exper} blacklist={['AX']} className="form-control" name="country" value={this.state.country} onChange={(val) => {this.selectCountry(val); setTimeout(() => { this.handleError();   }, 500)}} />
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>NCS Number</label>
                                </div>
                                <div>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput9" type="text" name="ncs" onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>ABA (RTN)</label>
                                </div>
                                <div>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput10" type="text" name="aba" onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                    {/* <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput4">SWIFT Code&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput4" type="text" name="swift1" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col"> 
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput5">Name of Bank&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput5" type="text" name="name1" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col">  */}

                        {
                        // DO NOT COPY FROM HERE
                        /*
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput6">Address</label>
                            
                                <input className="form-control" id="exampleFormControlInput6" type="text" name="address1" onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput7">Location</label>
                            
                                <input className="form-control" id="exampleFormControlInput7" type="text" name="location1" onChange={this.handleChange} />
                            </div>
                        </div> 
                        // DO NOT COPY FROM HERE
                        */}
                        {/* <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput8">Country of Bank&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            
                                <CountryDropdown blacklist={['AX']} className="form-control" name="country" value={this.state.country} onChange={(val) => (this.selectCountry(val), this.handleError)} />
                            
                        </div>
                    </div>
                    <div className="col"> 
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput9">NCS Number</label>
                            
                                <input className="form-control" id="exampleFormControlInput9" type="text" name="ncs" onChange={this.handleChange} />
                          
                        </div>
                    </div>
                    <div className="col"> 
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput10">ABA (RTN)</label>
                            
                                <input className="form-control" id="exampleFormControlInput10" type="text" name="aba" onChange={this.handleChange} />
                            
                        </div>
                    </div>
                </div> */}

                {/* 
                    

                                    */}
            </Fragment>
        }
    }

    renderBeneficiaryClient = () => {
        const dimSetter = {
            // width:'40%'
            flex:'1 1 0',
            marginRight:'50px',

        }

        const preExper = {
            width:'192px'
        }
        const renderAccInfo ={

            display:'flex',
            flexDirection:'column',
            paddingRight:'30px',
        }

        const formGroupStyle = {

            display:'flex',
            alignItems:'center',
            justifyContent:'flex-end',

        }
        const IBAN = {
            minWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const labStyle={
            marginRight:'20px',
            textAlign:'right',
        }
        const inpStyle = {

            // marginLeft:'10px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const fieldStyle={
            paddingTop:'20px',
        }
        const sectTitle = {
            textAlign:'left',
            color: '#2778E2',
        }


        const exper = {
            // marginLeft:'10px',
            // maxWidth:'192px',
            // minWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',

        }
        if(this.state.transfrTypeVal == "External"){
            return <Fragment>
                <div style={dimSetter}>
                    <div className="col-12" style={sectTitle}> 
                        <h4 className="subLabelSeprator" style={{fontSize:'16px', paddingLeft:'71px'}}>BENEFICIARY CLIENT</h4>
                    </div>
                    <div style={renderAccInfo}>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                <label style={labStyle}>Type</label>
                                </div>
                                <div style={preExper}>
                                <select style={exper} className="selectpicker show-tick form-control" id="id3" name="type" onChange={this.handleChange} defaultValue=''>
                                    <option disabled value=''> -- select an option -- </option>
                                    <option value="company">Company</option>
                                    <option value="personal">Personal</option>
                                </select>
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                <label style={labStyle}>Account Number (IBAN)</label>
                                </div>
                                <div>
                                <input style={IBAN} className="form-control" id="exampleFormControlInput16" type="text" name="iban" onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle}>Beneficiary Name</label>
                                </div>
                                <div>

                                <input style={inpStyle} className="form-control" id="exampleFormControlInput12" type="text" name="name2" onChange={this.handleChange}/>
                                </div>

                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle}>Address Line</label>
                                </div>
                                <div>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput13" type="text" name="address2" onChange={this.handleChange} />
                                </div>

                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle} >Address Line 2</label>
                                </div>
                                <div>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput14" type="text" name="address3" onChange={this.handleChange}/>
                                </div>

                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>
                                    <label style={labStyle} >Country </label>
                                </div>
                                <div style={preExper}>
                                    <CountryDropdown style={exper} blacklist={['AX']} className="form-control" name="country" value={this.state.country2} onChange={(val) => {this.setState({ country2: val }); setTimeout(() => { this.handleError();   }, 500)}} />
                                </div>

                            </div>
                        </div>
                    </div>
                    </div>       
            </Fragment>
        }
        else{
            return <Fragment>
                <div >
                    <div className="col-12" style={sectTitle}> 
                        <h4 className="subLabelSeprator" style={{fontSize:'16px'}}>BENEFICIARY CLIENT</h4>
                    </div>
                    <div style={fieldStyle} className="col">
                        <div style={formGroupStyle} className=" form-group">
                            {/* <label className="cs-label" htmlFor="exampleFormControlInput11">Type&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label> */}
                            <div>

                            <label style={labStyle}>Type&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            </div>
                            <div style={preExper}>
                                <select style={exper} className="selectpicker show-tick form-control" id="id3" name="type" onChange={this.handleChange} defaultValue=''>
                                    <option disabled value=''> -- select an option -- </option>
                                    <option value="company">Company</option>
                                    <option value="personal">Personal</option>
                                </select>
                            </div>
                            
                        </div>
                    </div>
                    <div style={fieldStyle} className="col">
                        <div style={formGroupStyle} className=" form-group">
                            <div>

                            <label style={labStyle}>Account Number (IBAN)&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            </div>
                            {/* <label className="cs-label" htmlFor="exampleFormControlInput16">Account Number (IBAN)&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label> */}
                            <div style={preExper}>

                                <input style={IBAN} className="form-control" id="exampleFormControlInput16" type="text" name="iban" onChange={this.handleChange} />
                            </div>
                           
                        </div>
                    </div>
                    <div style={fieldStyle} className="col">
                        <div  style={formGroupStyle} className=" form-group">
                            {/* <label className="cs-label" htmlFor="exampleFormControlInput12">Beneficiary Name&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label> */}
                            <div>
                                <label style={labStyle}>Beneficiary Name&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            </div>
                            <div style={preExper}>
                                <input style={IBAN} className="form-control" id="exampleFormControlInput12" type="text" name="name2" onChange={this.handleChange} />
                            </div>
                            
                        </div>
                    </div>
                </div>
            </Fragment>
        }
    }

    renderInformation = () => {
        const dimSetter = {
            width:'50%'
        }
        const renderAccInfo ={

            display:'flex',
            flexDirection:'column',
            paddingRight:'30px',
        }

        const formGroupStyle = {

            display:'flex',
            alignItems:'center',
            justifyContent:'flex-end',

        }

        const labStyle={
            marginRight:'20px',
  
        }
        const inpPre = {
            paddingLeft:'10px'
        }
        const inpStyle = {

            // marginLeft:'10px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const fieldStyle={
            paddingTop:'20px',
        }
        const sectTitle = {
            textAlign:'left',
            color: '#2778E2',
        }


        
        if(this.state.transfrTypeVal == "External"){
            return <Fragment>
                <div style={dimSetter} >
                    <div className={"col-12"} style={sectTitle}> 
                        <h4 className="subLabelSeprator" style={{fontSize:'16px',textAlign:'center',paddingRight:'65px'}}>INFORMATION</h4>
                    </div>

                    <div style={renderAccInfo}>
                    
                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                    <div>
                                        <label style={labStyle}>Reference Message&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                    </div>
                                    <div style={inpPre}>
                                        <input style={inpStyle} className="form-control" id="exampleFormControlInput17" type="text" name="refmsg" onChange={this.handleChange} />
                                    </div>

                                </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                    <label style={labStyle}>Message Line 2</label>
                                </div>
                                <div style={inpPre}>
                                    <input style={inpStyle} className="form-control" id="exampleFormControlInput18" type="text" name="ml2" onChange={this.handleChange}/>

                                </div>


                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle}>Message Line 3</label>
                                </div>
                                <div style={inpPre}>

                                <input style={inpStyle} className="form-control" id="exampleFormControlInput19" type="text" name="ml3" onChange={this.handleChange} />
                                </div>

                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle} >Message Line 4</label>
                                </div>
                                <div style={inpPre}>

                                <input style={inpStyle} className="form-control" id="exampleFormControlInput20" type="text" name="ml4" onChange={this.handleChange}/>
                                </div>
                            </div>
                        </div>
                        

                    </div>

                </div>

                        

                
                    {/* <div className="col ">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput17">Reference Message&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput17" type="text" name="refmsg" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col ">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput18">Message Line 2</label>
                            
                                <input className="form-control" id="exampleFormControlInput18" type="text" name="ml2" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput19">Message Line 3</label>
                            
                                <input className="form-control" id="exampleFormControlInput19" type="text" name="ml3" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput20">Message Line 4</label>
                            
                                <input className="form-control" id="exampleFormControlInput20" type="text" name="ml4" onChange={this.handleChange} />
                        </div>
                    </div>
                </div>
            </Fragment>
        }else{
            return <Fragment>
                <div className="row">
                    <div className="col-12"> 
                        <h4 className="subLabelSeprator">Information:</h4>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput17">Reference Message&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                            <input className="form-control" id="exampleFormControlInput17" type="text" name="refmsg" onChange={this.handleChange} />
                        </div>
                    </div>
                </div> */}
            </Fragment>
        }
        else{
            return(
                <Fragment>
                   <div>
                        <div className={"col-12"} style={sectTitle}> 
                            <h4 className="subLabelSeprator" style={{fontSize:'16px',textAlign:'center',paddingRight:'65px'}}>INFORMATION</h4>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                    <div>
                                        <label style={labStyle}>Reference Message&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                    </div>
                                    <div style={inpPre}>
                                        <input style={inpStyle} className="form-control" id="exampleFormControlInput17" type="text" name="refmsg" onChange={this.handleChange} />
                                    </div>

                                </div>
                        </div>
                   </div>
                </Fragment>
            )
        }
    }

    renderIntermediaryBank = () => {
        const dimSetter = {
            // width:'40%'
            flex:'1 1 0',
            marginRight:'50px'
        }
        const renderAccInfo ={

            display:'flex',
            flexDirection:'column',
            paddingRight:'30px',
        }

        const formGroupStyle = {

            display:'flex',
            alignItems:'center',
            justifyContent:'flex-end',

        }

        const labStyle={
            marginRight:'20px',
            textAlign:'right',
        }
        const inpStyle = {

            // marginLeft:'10px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const fieldStyle={
            paddingTop:'20px',
        }
        const IBAN = {
            minWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',
        }
        const sectTitle = {
            textAlign:'left',
            color: '#2778E2',
        }
        const preExper = {
            width:'192px'
        }

        const exper = {
            // marginLeft:'10px',
            // maxWidth:'192px',
            // minWidth:'192px',
            border: '1px solid #808D98',
            borderRadius: '5px',
            background:'#f6f8f9',

        }
        if(this.state.transfrTypeVal == "External"){
            return <Fragment>
                <div style={dimSetter}>
                    <div className="col-12" style={sectTitle}> 
                        <h4 className="subLabelSeprator" style={{fontSize:'16px',paddingLeft:'71px'}}>INTERMEDIARY BANK</h4>
                    </div>

                    <div style={renderAccInfo} >

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle}>SWIFT Code</label>
                                </div>
                                <div>
                                <input style={inpStyle} className="form-control" id="exampleFormControlInput21" type="text" name="swift2" onChange={this.handleChange} />

                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                            <div>

                                <label style={labStyle}>Name of Bank</label>
                            </div>
                            <div>
                                <input style={inpStyle} className="form-control" id="exampleFormControlInput22" type="text" name="name3" onChange={this.handleChange} />
                                
                            </div>
                            </div>
                        </div>


                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                                <div>

                                <label style={labStyle}>Country of Bank</label>
                                </div>
                                <div style={preExper}>
                                    
                                <CountryDropdown style={exper} blacklist={['AX']} className="form-control" name="country" value={this.state.country3} onChange={(val) => {this.setState({ country3: val }); setTimeout(() => { this.handleError();   }, 500) }} />
                                </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                            <div>

                                <label style={labStyle}>NCS Number</label>
                            </div>
                            <div>
                                
                                <input style={inpStyle} className="form-control" id="exampleFormControlInput26" type="text" name="ncsno" onChange={this.handleChange}/>
                            </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                            <div>

                                <label style={labStyle}>ABA (RTN)</label>
                            </div>
                            <div>
                                
                                <input style={inpStyle}className="form-control" id="exampleFormControlInput27" type="text" name="abartn" onChange={this.handleChange}/>
                            </div>
                            </div>
                        </div>

                        <div style={fieldStyle}>
                            <div style={formGroupStyle}>
                            <div>
                                <label style={labStyle}>Account Number (IBAN)</label>

                            </div>
                            <div>
                                <input style={IBAN} className="form-control" id="exampleFormControlInput28" type="text" name="aniban" onChange={this.handleChange} />
                                
                            </div>
                            </div>
                        </div>


                    </div>
                </div>




                    {/* <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput21">SWIFT Code</label>
                            
                                <input className="form-control" id="exampleFormControlInput21" type="text" name="swift2" onChange={this.handleChange} />

                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput22">Name of Bank</label>
                            
                                <input className="form-control" id="exampleFormControlInput22" type="text" name="name3" onChange={this.handleChange} />

                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput25">Country of Bank</label>
                            
                                <CountryDropdown blacklist={['AX']} className="form-control" name="country" value={this.state.country3} onChange={(val) => this.setState({ country3: val })} />

                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput26">NCS Number</label>
                            
                                <input className="form-control" id="exampleFormControlInput26" type="text" name="ncsno" onChange={this.handleChange} />

                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput27">ABA (RTN)</label>
                            
                                <input className="form-control" id="exampleFormControlInput27" type="text" name="abartn" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col">
                        <div className=" form-group">
                            <label className="cs-label" htmlFor="exampleFormControlInput28">Account Number (IBAN)</label>
                            
                                <input className="form-control" id="exampleFormControlInput28" type="text" name="aniban" onChange={this.handleChange} />
                        </div>
                    </div>
                </div> */}
            </Fragment>
        }
    }

    renderOptionalInfo = () => {
        return <Fragment>
            <div className="row">
            <div className="col-12" > 
                    <h4 className="subLabelSeprator">Optional</h4>
                </div>
                <div className="col">
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput29">Type of transfer</label>
                        
                            <select className="selectpicker show-tick form-control" id="id1" name="tot" onChange={this.handleChange} defaultValue=''>
                                <option disabled value=""> -- select an option -- </option>
                                <option value="normal">Normal</option>
                            </select>
                    </div>
                </div>
                <div className="col">
                    <div className=" form-group">
                        <label className="cs-label" htmlFor="exampleFormControlInput30">Foreign bank fees covered by</label>
                        
                            <select className="selectpicker show-tick form-control" id="id2" name="fbfees" onChange={this.handleChange} defaultValue=''>
                                <option disabled value=""> -- select an option -- </option>
                                <option value="our">OUR</option>
                                <option value="sha">SHA</option>
                            </select>
                    </div>
                </div>
            </div>
        </Fragment>
    }

    render() {

        const cardLike = {
            // display:'flex',
            // flexWrap:'wrap',
            // textAlign:'right',
            // justifyContent:'space-around',
            display:'flex',
            justifyContent:'space-around',
        }
        const captchSheet = {
            padding:'20px',
        }
        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header">
                                    <h5>{this.state.userFullname} {this.state.userCompany == undefined ? '' : ('('+this.state.userCompany+')') }</h5>
                                </div>
                                <form className="form-horizontal theme-form custom-form trfrform-wrapper">
                                    {/* <div className="card-body">                                       
                                        {this.renderAccountInfo()}
                                        {this.renderBeneficiaryBank()}
                                        {this.renderBeneficiaryClient()}
                                        {this.renderInformation()}
                                        {this.renderIntermediaryBank()}  
                                    </div> */}
                                    <div >                                       
                                        {this.renderAccountInfo()}
                                        <div style={cardLike}>
                                            {this.renderBeneficiaryBank()}
                                            {this.renderBeneficiaryClient()}
                                        </div> 
                                        <div style={cardLike}>
                                            {this.renderInformation()}
                                            {this.renderIntermediaryBank()} 

                                        </div>
                                    </div>
                                    <div style={captchSheet}>
                                       <CaptchaComponent isVerified={(val)=> {this.setState({captchaResponse: val}); this.handleError();}}></CaptchaComponent>
                                    </div>
                                    <div className="card-footer">
                                        {this.state.isDisabled}
                                        <button disabled={this.state.isDisabled} onClick={this.createTransferRequest} className="btn btn-warning mr-3" type="button"><i hidden={this.state.loginProcessing == false} className="fa fa-spinner fa-spin" />Continue</button>
                                        <button onClick={this.goBack} className="btn btn-primary mr-1" type="button">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </Fragment>
        );
    }
}

export default transferForm;