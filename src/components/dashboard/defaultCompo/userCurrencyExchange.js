import React, { Component, Fragment } from 'react';
import axios from 'axios';

import { ToastContainer, toast } from 'react-toastify';

import { SendChangeingAmount,SaveExchangedAmount,SendGetRateOtehrInfo } from '../../../constant/actionTypes';

class userCurrencyExch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            outExchAmt: '',
            outExchRate: '',
            exchangedAmt: '',
            outExchTo: '',
            outExchFrom: '',
            step1: true,
            step2: false,
            step1Msg: '',
            step2Msg: '',
            frmBalance: '',
            userId: '',
            frmAcc: '',
            frmBal: '',
            toAcc: '',
            toBal: '',
            maxminamt:'',
            inRequest: false
        }
    }
    componentDidMount() {
        let localStg=JSON.parse(localStorage.getItem("user"));
        this.setState({userId: localStg.id});
    }

    makeExchangeChnaged = (event) => {
        let name = event.target.name;
        let val = event.target.value;
        console.log(val)
        if(name == 'exchAmt'){
            const re = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
            this.setState({maxminamt: ""});
            if (val === '' || re.test(val)) {
                //if valid value
                this.setState({
                    outExchAmt: val
                },()=>{console.log(this.state)})               
                if(parseInt(val) > parseInt(this.state.frmBalance)){
                    this.setState({maxminamt: "Input must be equal or less, from available balance."});
                }
            }
            else{
                this.setState({
                    outExchAmt: ''
                },()=>{console.log(this.state)})
            }   
            this.setState({step1Msg: '',step2Msg: '',exchangedAmt : val * this.state.outExchRate});
            // console.log(this.state)
        }
        else if(name == 'exchCurTo')
        this.setState({outExchTo: val,step1Msg: '',step2Msg: ''});
        else if(name == 'exchCurrFrm'){
            this.setState({outExchFrom: val,step1Msg: '',step2Msg: ''});
            let newVal=event.target.value;
            document.querySelectorAll('#exchCurTo option').forEach(function(userItem) {
                userItem.removeAttribute('disabled');
            })
            document.querySelectorAll('#exchCurTo option').forEach(function(userItem) {
                if(userItem.value == newVal){
                    userItem.setAttribute('disabled',true);
                }
            })
        }
        
    }

    makeChangeClick = (event) => {
        const {outExchTo,outExchFrom,outExchRate,userId}=this.state;
        var $this=this,$target=event.target;
        if(outExchTo == outExchFrom && outExchTo != '' && outExchFrom != ''){
            this.setState({
                step1Msg: 'Select two different currencies.'
            })
        }
        else if(outExchTo != '' && outExchFrom != '')
        {
            event.target.classList.add('spin');
            axios.post(SendChangeingAmount, {
                "from_currency": outExchFrom,
                "to_currency": outExchTo
              })
              .then(function (response) {
                    console.log(response)
                    if(response.data.result.Items[0])
                     {
                        let rateVal=response.data.result.Items[0].rate;
                        axios.post(SendGetRateOtehrInfo, {
                            "from_currency": outExchFrom,
                            "to_currency": outExchTo,
                            "id": userId
                          })
                          .then(function (response) {
                              console.log(response)
                              console.log(response.data.message.value.exchange_validity )
                              if(response.data.message.value.exchange_validity === 'valid'){
                                $this.setState({
                                    step1: false,
                                    step2: true,
                                    step1Msg: '',
                                    step2Msg: '',
                                    outExchRate: rateVal,
                                    frmAcc: response.data.message.value.payee_account.account_number,
                                    frmBal: response.data.message.value.payee_account.currency +' '+ new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(response.data.message.value.payee_account.available_balance),
                                    frmBalance: response.data.message.value.payee_account.available_balance,
                                    toAcc: response.data.message.value.receiver_account.account_number,
                                    toBal: response.data.message.value.receiver_account.currency +' '+ new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(response.data.message.value.receiver_account.available_balance),
                                });
                              }
                              else{
                                $this.setState({
                                    step1: true,
                                    step2: false,
                                    step1Msg: '',
                                    step2Msg: '',
                                    outExchRate: '',
                                    frmAcc: '',
                                    frmBal: '',
                                    frmBalance: '',
                                    toAcc: '',
                                    toBal: '',
                                });
                                toast.info('Exchange blocked with thease currencies,try another !');
                              }
                              $target.classList.remove('spin');
                          })
                          .catch(function (error) {
                            $target.classList.remove('spin');
                          })                          
                     } 
                    else
                    {
                        toast.success('No rate found!');
                        $target.classList.remove('spin');
                    }
              })
              .catch(function (error) {
                console.log(error)
                  toast.error("Something went wrong");
                  $target.classList.remove('spin');
              })
        }
        else{
            this.setState({
                step1Msg: 'All  fields are mandatory'
            })
        }
       }
    handleConfirmExchangeClick=(event)=>{
        event.preventDefault();
        this.setState({
            inRequest: true
        })
        if(this.state.outExchAmt != '' && this.state.outExchRate != '' && this.state.maxminamt === '')
        {
            if(parseInt(this.state.outExchAmt) > parseInt(this.state.frmBalance))
            {
                this.setState({maxminamt: "Input must be equal or less, from available balance."});
            }
            else{
                this.setState({maxminamt: ""});
                event.target.classList.add('spin');
                var $this=this,$target = event.target;
                const {exchangedAmt,frmAcc,toAcc,outExchFrom,outExchTo,outExchAmt}=this.state;
                axios.post(SaveExchangedAmount, {
                    // "amount" : exchangedAmt,
                    "from_account": frmAcc,
                    "to_account": toAcc,
                    "from_currency": outExchFrom,
                    "to_currency": outExchTo,
                    "deduct_amount": outExchAmt,
                })
                .then(function (response) {
                        console.log(response)
                        if(response.data.message['value'] == 'ok'){
                            toast.success('Updated Successfully!');    
                        }
                        else{
                            toast.error('Something went wrong!');    
                        }
                        document.getElementById("step2").reset();
                        document.getElementById("step1").reset();
                        $target.classList.remove('spin');
                        $this.setState({
                            outExchAmt: '',
                            outExchRate: '',
                            exchangedAmt: '',
                            outExchTo: '',
                            outExchFrom: '',
                            step1: true,
                            step2: false,
                            frmBalance: '',
                            step1Msg: '',
                            step2Msg: '',
                            frmAcc: '',
                            frmBal: '',
                            toAcc: '',
                            toBal: '',
                            maxminamt:'',
                            inRequest: false
                        });
                })
                .catch(function (error) {
                    toast.error("Something went wrong");
                    $target.classList.remove('spin');
                    this.setState({
                        inRequest: false
                    })
                })   
            }     
        }
        else{
            if(this.state.outExchAmt === '')
            this.setState({maxminamt: 'Please enter exchanging amount.'})
        }
    }
    handleGoBackClick =(event) => {
        this.setState({
           step1: true,
           step2: false,
           step1Msg: '',
           step2Msg: '',
           maxminamt:'',
           outExchAmt: ''
        });
    }
    handleSubmit=(event)=>{
        event.preventDefault();
    }
    render() {
        const {frmAcc,frmBal,toAcc,toBal,outExchTo}=this.state;
        return(
            <Fragment>
                <div className="container-fluid ">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Currency Exchange </h5>
                                </div>
                                <div className="card-body datatable-react sudata-table">
                                    <form id="step1" onSubmit={this.handleSubmit} className={this.state.step1 ? 'custom-form theme-form curencyExchFrm show' : 'custom-form theme-form curencyExchFrm'}>
                                        <div className="row">
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="cs-label">Currency From </label>
                                                    <select name='exchCurrFrm' className="form-control" onChange={this.makeExchangeChnaged}>
                                                        <option value="">Currency From</option>
                                                        <option value="RUB">RUB</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="GBP">GBP</option>
                                                        <option value="USD">USD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="form-group">
                                                    <label className="cs-label">Currency To </label>
                                                    <select id="exchCurTo" name='exchCurTo' className="form-control" onChange={this.makeExchangeChnaged}>
                                                        <option value="">Currency To</option>
                                                        <option value="RUB">RUB</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="GBP">GBP</option>
                                                        <option value="USD">USD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-12">
                                                <button className="btn btn-primary" type="button" name="makeChange" onClick={this.makeChangeClick}><i className="fa fa-spinner fa-spin"></i> Exchange</button>
                                                &nbsp;&nbsp;<i style={{color: 'rgb(228, 86, 110)'}}>{this.state.step1Msg}</i>
                                            </div>
                                        </div>
                                    </form>
                                    <form id="step2" onSubmit={this.handleSubmit} className={this.state.step2 ? 'custom-form theme-form curencyExchFrm show' : 'curencyExchFrm custom-form theme-form'}>
                                        <div className="row">
                                            <div className="col mx-351">
                                                <div className="form-group">
                                                    <p className="b-600">From </p>
                                                    <p className="form-control-static">
                                                        <span className="badge badge-light">Account No :</span> {frmAcc}
                                                        <br/>
                                                        <span className="badge badge-light">Available Balance :</span> {frmBal}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col mx-351">
                                                <div className="form-group">
                                                    <p className="b-600">To </p>
                                                    <p className="form-control-static">
                                                        <span className="badge badge-light">Account No :</span> {toAcc}
                                                        <br/>
                                                        <span className="badge badge-light">Available Balance :</span> {toBal}
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="col-auto"></div>
                                            <div className="col-12">
                                                <br/>
                                            </div>
                                            {/* <div className="col-12">
                                                <div className="form-group ">
                                                    <p className="form-control-static">
                                                        <span className="badge badge-light">Exchange Rate From {this.state.outExchFrom} to {this.state.outExchTo} :-</span> {this.state.outExchRate}
                                                    </p>
                                                </div>
                                            </div> */}
                                            <div className="col mx-351">
                                                <div className="form-group ">
                                                    <label className="cs-label" htmlFor="exchangingAmt">Amount to Exchange (in {this.state.outExchFrom})</label>
                                                    <input type='text' name='exchAmt' id="exchangingAmt"  placeholder="Exchanging amount" className="form-control" value={(this.state.outExchAmt)?this.state.outExchAmt:''} onChange={this.makeExchangeChnaged} />
                                                    <p className="form-control-static text-danger"><small>{this.state.maxminamt}</small></p>
                                                </div>
                                            </div>
                                            <div className="col mx-351">
                                                <div className="form-group setexchfrom">
                                                    <label className="cs-label">Exchanged Amount (in {outExchTo})</label>
                                                    <p className="form-control-static">{ outExchTo} : { new Intl.NumberFormat('en-US', { minimumFractionDigits: 2,maximumFractionDigits: 2 }).format(this.state.outExchAmt * this.state.outExchRate)}</p>
                                                </div>
                                            </div>
                                            <div className="col-auto"></div>
                                            <div className="col-12">
                                                <button className="btn btn-default" type="button" name="goBack" onClick={this.handleGoBackClick}>Go Back</button> &nbsp;&nbsp;
                                                <button className="btn btn-primary" type="button" name="confirmExchange" disabled={this.state.inRequest} onClick={this.handleConfirmExchangeClick}><i hidden={this.state.inRequest == false} className="fa fa-spinner fa-spin"></i>  Confirm Exchange</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default userCurrencyExch;
