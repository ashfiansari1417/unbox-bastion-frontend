import React, { Component } from 'react';
import Header from '../../common/header-component/header';
import './styles/EditProfile.css';

export default class EditUserProfile extends Component {
  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    return (
      <div>
        <Header />
        <h2
          className="forgot-password-title"
          style={{ textAlign: 'center', margin: '16px' }}
        >
          User Profile
        </h2>
        <div className="edit-input-container">
          <div className="edit-profile-input-div">
            <label>First Name</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-first-name"
              type="text"
              autoComplete="off"
              name="userFirstName"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Last Name</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-last-name"
              type="text"
              autoComplete="off"
              name="userLastName"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Email</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-email"
              type="text"
              autoComplete="off"
              name="userEmail"
            />
          </div>
          {/* <div className="edit-profile-input-div">
            <label>Phone</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-phone"
              type="text"
              autoComplete="off"
              name="userPhone"
            />
          </div> */}
          <div className="edit-profile-input-div">
            <label>Company Name</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-company-name"
              type="text"
              autoComplete="off"
              name="userCompanyName"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Company Country</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-company-country"
              type="text"
              autoComplete="off"
              name="userCompanyCountry"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Company Address</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-company-address"
              type="text"
              autoComplete="off"
              name="userCompanyAddress"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Company Registration Number</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-company-registration-number"
              type="text"
              autoComplete="off"
              name="userCompanyRegistrationNumber"
            />
          </div>
          <div className="edit-profile-input-div">
            <label>Company Registration Date</label>
            <input
              //   className="form-control"
              className="settings-input"
              id="user-company-registration-date"
              type="text"
              autoComplete="off"
              name="userCompanyRegistrationDate"
            />
          </div>
        </div>
        <div className="user-setting-button-div">
          <button>Update</button>
        </div>
      </div>
    );
  }
}
