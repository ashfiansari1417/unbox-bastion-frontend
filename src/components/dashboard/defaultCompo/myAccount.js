import React, { Component, Fragment } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import { GET_USER_ACCOUNT_DETAILS } from '../../../constant/actionTypes';
import logo from '../../../assets/images/united-states-of-america-flag-icon-16.png';
import Carousel from 'react-bootstrap/Carousel'
import card from '../../../assets/images/other-images/calender-bg.png'
// import Breadcrumb from '../../common/breadcrumb';
import Table from 'react-bootstrap/Table'
import Masonry from 'react-masonry-css'

import './myaccounts.scss'
var columns;
class MyAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "5",
            userDetails: [],
            isDisabled: true
        }        
    }

    UNSAFE_componentWillMount () {
        const getID = localStorage.getItem('user');
        const userID = JSON.parse(getID)
        this.setState({
            // id: userID.id
        });
        columns = [
            {
                name: 'Account Number',
                selector: "accNo",
                maxWidth: '25%',
                sortable: true,
                cell: row => 
                <span>
                    {row.account_number ? <span>{row.account_number}</span> :<span>-</span> }
                </span>
            },
            {
                name: 'Account Currency',
                selector: "currency",
                maxWidth: '25%',
                cell: row => 
                <span >
                    {row.currency ? <span>{row.currency}</span> :<span>-</span> }
                </span>
            },{
                name: 'Total Amount',
                selector: "avlBal",
                maxWidth: '25%',
                cell: row => 
                <span>
                    {row.available_balance_ch >= 0 ? <span>{Number(parseFloat(row.blocked_balance_ch) + parseFloat(row.available_balance_ch)).toFixed(2)}</span> :<span>0.00</span> }
                </span>
            },
            {
                name: 'Available Amount',
                selector: "blocBal",
                maxWidth: '25%',
                cell: row => 
                <span>
                    {row.available_balance_ch > 0 ? <span>{Number(row.available_balance_ch).toFixed(2)}</span> :<span>0.00</span> }
                </span>
            },
            {
                name: 'Blocked Amount',
                selector: "blocBal",
                maxWidth: '25%',
                cell: row => 
                <span>
                    {row.blocked_balance_ch > 0 ? <span>{Number(parseFloat(row.blocked_balance_ch)).toFixed(2)}</span> :<span>0.00</span> }
                </span>
            }

        ]
        
    }
    componentDidMount () {
        // this.getUserAccountDetails();
        this.getUserAccountDetails();
        document.querySelector('.page-body').style="margin-top:0 !important"
    }
    getUserAccountDetails = () => {
        axios.post(GET_USER_ACCOUNT_DETAILS, {
            "userID": this.state.id
        })
        .then( res => {
            console.log(res.data.result.Items)
            let allAcc=res.data.result.Items;
            allAcc.forEach(element => {
                if(parseFloat(element.available_balance_ch) > 0){
                    this.setState({isDisabled: false});
                }
            });
            this.setState({
                userDetails: res.data.result.Items
            })
            
        })
    }
    gotoTransferForm = () => {
        this.props.history.push('./transfer-form');
    }
    render() {
        const { userDetails } = this.state;
        const breakpointColumnsObj = {
              default: 3,
              1100: 2,
              700: 1,
              500: 1
            };
        return(
        <Fragment>

            <Masonry breakpointCols={breakpointColumnsObj} className="my-masonry-grid" columnClassName="my-masonry-grid_column" >
                <Card className="one">
                    <center><Card.Header as="h5" className="card-header-home">Business Accounts</Card.Header></center>
                    <Card.Body className="card-body-home">
                      <ul>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <img src={logo}/>
                                    <p className="swift-p">  SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <img src={logo}/>
                                    <p className="swift-p">  SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <img src={logo}/>
                                    <p className="swift-p">  SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                      </ul>
                      
                    </Card.Body>
                </Card>
                <Card className="two">
                    <center><Card.Header as="h5" className="card-header-home">E-Commerce Accounts</Card.Header></center>
                    <Card.Body className="card-body-home">
                      <ul>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p className="swift-p"> SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p className="swift-p"> SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p className="swift-p"> SWIFT <img src={logo}/></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Available funds</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>                        
                    </ul>
                    </Card.Body>
                </Card>
                <Card className="three">
                    <center><Card.Header as="h5" className="card-header-home">Credit Accounts</Card.Header></center>
                    <Card.Body className="card-body-home">
                      <ul>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p><strong>$4000000.00 Credit</strong></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Amount Borrowed</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p><strong>$4000000.00 Credit</strong></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Amount Borrowed</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="card-div">
                                <div className="card-div-upper">
                                    <p className="currency-p"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                    <p><strong>$4000000.00 Credit</strong></p>
                                </div>
                                <div className="card-div-lower">
                                    <div>
                                        <p className="shrunk-card-p">Amount Borrowed</p>
                                        <p>$40000</p>
                                    </div>
                                    <div className="flexShrink">
                                        <button className="button">Transaction</button>
                                        <button className="button">Exchange</button>
                                        <button className="button">Transfer</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                      </ul>
                    </Card.Body>
                </Card>
                <Card className="four">
                    <center><Card.Header as="h5" className="card-header-home">My Cards</Card.Header></center>
                    <Carousel>
                        <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src=
                              'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhMVFhUXGBUbGBgVFhUVFhsaFhgWFhcbGBUYHSggGBolHRUXITEiJSkrLi4uFx8zODMtNygtLisBCgoKDg0NGA8PGzclHyU3NzI3NzI3MC83MzM3Ljc1LTcxNDctNzc3NzcrLy43Nzc3Mi41KzUvNzArKy83Ny0tL//AABEIALABHgMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIGAwUHBAj/xABEEAABAwEFBAYHBgQFBAMAAAABAAIDEQQSITFBBRNRYQYHIoGRoRcjMjVxsdEUQlJjwdMzU3OSFXKj4fBik6LxJCVD/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAUGAwT/xAAoEQEAAQMBBwMFAAAAAAAAAAAAAQIDBBMFESEiMUFxFFGxEjKBkcH/2gAMAwEAAhEDEQA/AO1IQhAIQhAISqi8EDQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQleCaAQhCAQhCAQhCAScaJrDaj2SgovT/rFh2dRlDJM4VDGmlBiAXu+6KjgSuZy9c9vLjdiswGgLZXGn+YSCvgq11kWgv2nanHGj7tOTWtaPkq2Bpr936IOjembaGHYs1P8kv7qZ65No/y7Nnh2Jf3VztkZcaBpJJoWgEmvIZ1QMKg5jig6J6ZdoVpcs3/AG5c+H8VNvXHtE/cs1a49iX91c5BGWnHnxU66YVOZ5IOh+mTaGPYs1B+XL+6n6Yto4C5Zq69iXD/AFVzoPHcMhxKlyrjm4/og6H6YtoZ3LNy9XLj/qpnrh2h+Czc/Vy4f6q55fGfcAnfAw/u70HQ/S/tHHsWao/Ll/dTHW/tD8Nmofy5f3Vz4cNRlzCV4dx8ig6F6YNoZXLNX+nL+6n6X9ofhs1B+XL+6uf10148kfIZcyg6COt3aGHYs1f6cv7qB1vbQOTLNy9XL+6qABp4oBry4IL/AOl7aH4LNT+nL+6n6XNofhs1f6cv7qoFdfEKVPHRBfh1uW/8Nmp/Tl/dQetvaH4LNy9XL+6qCPI58ipDhr+iC++lu35XbNX+nLSv/dTb1tbQ/BZufq5f3VQa6aceafLXVBffS3b/AMNmp/kl/dQOtnaGV2zV19XL+6qFXXwTA08Sgvvpat/4LPy7Ev7ifpat+rbPz7En7ioVdfAJg0w01QdBs3W5bWntxQPAzDRIwn4OLnfJdW6HdK4rfFvI6gjBzHe008DxHA6+K+aPmMuYV66mrQ5u0C1p7D4X3hza5hb4Vd/cUH0GhRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73jaz+a6vwwW46mWu/xWG42NzrspZvSQAbhNRQElwFcOZOi0/T73la6ZiV3hgtJBKWEOY4tNatc0kOa4cCMQUH1XHBA+2F7GwQbTayr2m4/eRk0reFHOYSKB4uuFBeFOyeSW+NrulNLQyHGVhe0UfCHfZgaVeBeNQCSQO1XALmZtchk3pkeZQal9918njfrWvOqxvN4mpJvEmpxNTiak5oOjz2ewvsLrVAImie12OsLrt+B43u/jbXOE1a5vI0I7K2HWtZrQ0SGKFrbIJO250WzhT1gEW6MXrSw4e1jQ46rlAIz+9lTnxQGDLhmf0QXrpT0ifJs2yGlnvT/at7cs9na/1UrBHi1gMeHClVY+nlkMmz5Hui+ysjETo43M2e+E4taY7LaIKSkmpdU1qGmtFXmdFoHCDef/AB7whYaG9aHSyOjjLnXpDHuTWR7XsDcGEUwqsFl6FQSx32WghpjBY54Y07wki68CoaHAxEAuBpI72rhQWTbO37DAxsL2iWtkhAgZZLLcvvszS132sHeNIc4PqBUEYLPYbLN/hdkNhga+Qw9smPZzoid7IH33T+t3l2mWGXNVk7As/wBu3bIy6NsE8pYC97nEvnELQA9riWh0AIDmk7t1SDVO2dDoDMXRuuwVdTdgTVItJgc2MFxe+kcckpAvEVYO1UFwevZNnsUuzrDZpRHHLM+1GO0GgLZI5GCNkxz3UjXXanIhppTLcCzSXtpf4dDZ3yjaNKFllcBDclrdE3ZAv3cufNVmTonZ29mSUsxeAW3HmrTai4vN66WsZZ2OF1ovb7jiHauhULb1JnSObE53ZjpG4gAsLZHCgY6kl3MOuCjwXBqDy9PrOxs8dxsTXmGL7QILu6baADvmsDDdFAWVDcL17mq1XXwWy6RbNbZpzCxznNDWG88AO7QvCraAsNCOy4VFSMRQnW118EEgNPFOtc9ckmjTQZ8ynUHv8kBXXxUqaeCj8x5hSp4HLkgK6+KdNNdEgdfFOmngUE73/rmlTTxSrrqmBppqgK6+CYGmgzS+Zy5BMeQ8ygfzOXIJg+GqXzPkFKoHdpxQKmngrr1P+8gfypa/GrFSiNPBXXqf95A/lS18WIPoZmSkosyUkAhCEAhCEAsFr9krOsFr9koPlDp8P/sbXTPfOp5LQfI58it90+942s/muoOeC0Rw+GqAx7x5hMU7jlyKQGmoy5hMU7jnyKBg/wB2X+6Yp3DPmUgTlrlXlxTFO4ZcygA3kKnyCd0Z0wHmUU01OfIKTcfh934oHcAGIGOfJF0agVGWGfBFde4hSBphmdEDawZd55JXRnQVGeGfNMDTxKlz1/RAg3+3P/ZP5nyCVNdOHNSyx11+CB00HxHNAPDM58kAaeCAa8uKCXMaZc0/kfIqNdfAKR8tUAPMeYT+Ry5JDzGXMJ/I+RQOvimPIeZRy148kDyGSA+Z8gmPIZcylTTxUm/+kBlnrmgHjmEV101TBp8dEDpp581dOp/3kOO6lr4sVKp4DNXbqg95NOu6l8KsQfQrMlJRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73la+O9dTyWiaaYaDPmt70+95WumZld+i0OHcPMoHTTvaf0TB10yISxyOuXJMHXucEDFfZ8+SYOugwaP1SAOX3c68kwde5o/VBIDTvcf0Tca/A5cik3DuzPE8EyPA+RQOuuuoUgaYDHgVHHvHmFIcBkfLigYp3DzKYrXmfIJDyGXMp001OaB0Gen6p4948wo1/tyTp4jLmEEh5HLkUVrn3pDyPkVL46eaAr4nJSHlqo/M+QUvkPMoCmngnXXQ5pY5HuTrr4hBLl58kA6+CKaaceSAdfBAU08VLP9Ehh8BnzKKeB8igddfEJjh4FL5jPmExwGR/4UB8h5lXXqf95DjupfmxUqvgMldep/3kB+VLXxYg+hmZKSizJSQCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vK1gZmV3hgtDXXQZcyt90+942sfmuqeWC0Nde5oQOmh18imDrqM+YSpTPI58igHXUeYQSA/tz/ANkxXPU5cgkAM/u5058Ewa/E58ggeHcPMqRrmcjny4KF8dwyHNSpTHP8Q+KCWPeMuYUgOGRz5cVA4a/A/opNIORw1QSB18AnTTxKg2QHKlcgFIEZVyzQSrrpw5J008CoF9MSR8OSmBpocQUDB18UfHIZc0r2p70NI4g8EEqeJz5KVddNFHl4lDngajkEEqaH/hTrrqM0uROfzT+YzQOngn8z5BI0GOiQcDhUVOfwQW+ydB5DBLJI9l9sDJY4I5GumJkexse9jpVocHVFDXKtK0PosfV3aBKI7Q5ga5to7UUjSGywxmTdyOeAGnDE4igOOC1o6cWndlgbAHGJkRmEX/yS2ItMdZL2JbdGNMdakArPaOsO1ulbK77PeaJA4XHFrjM245z2OkIJpWgFG4nDEoHB0Kko8vkjoG2Z0U0T2vs7mz2gQEl5AJDe1UYEECuC1XSTYxsdpks+9ZKGOIL48sCQQ5tTcdhi2ppxK9dq6ZWhzXRgQRsdHExjI4yxjGwy79u7BcaOL8SXXqrXbZ2q61zOtD2xMc/F4iaWNLtXULibxriaoPFXXwV16n/eQH5UtfjVipVddTkrr1P+8Rw3UvjViD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6fe8bWPzXV8loQddTkFvun3vG18N66vktDj3nyCCwdAZYo9oWZ8zmCJr+2ZKXB2XYurhSpW527HanTWF9qnstojfNdjdZmxBnZfCJWuuRsr7TM6jE81VNkW1kLy90EVoYWlro5b4FDTtNcxwc12GDgcKle7b+3G2iOGGKzx2eOEyuY1j5ZKumuXyXSuJ/8Azbh8UFo6Z9E3wQ260yWe4TtD1bwRT7O/f0o1poG3jFpXJee0SWi3bKjaWtklZbHMbu4oo3buOyGQjsNbUABxxqcFVGbUIsjrNcbcdNHKXY3g5kckYAGVPWE9ysuzOnDIIhBFYIW0vuDt7aHG/JC6zvfRzyKljiLuXzQe3onJbIrPA8Wyw2ezPc4sZaGwbxzWyESEF0LnHtBwxdh8KKp9InxG12l8FN2Z5ywNFG7syOLLo0F2i2Fk6SRCz2ezz2GKfc7wRudNaI3UkkdIaiJ7Rm7yWitD2l7nMaGC84hgLnAAkm6HOJJAyqccEHs2PtV9lk3sVy9deGmRjZA28KEhrgReGhIPerN03tcpskMNsfGbYJHyvbGyJjo4nxMETJN01rb5Ic67iQHCqpd2uGh8itntnbDrTaX2p7GVLmOLMSwljWtoQTUtNzEV1KC59L5ppoJXWa02OSyMist6GIQ75mEDHFxEV7+MdH68MFrYXz2zZghDRI6O2WeKIMjia+6LNaTdvNaC72QauJOFarXWrpHGYZoobFBA60BgkfHJaHdlsjZaNZI8tYLzBgBgMF7dhdNWWSJrIrDCS1zX3zLaLzpmxvi3hbfoKtkf2R2e1lgKBl6MvtrLK2SK12KzQyPkufaRBee5oZfLS+F7iBeaMSPgtR0ymhfbrS6AsMLpSWFgozIVLRQYF1TzrVZNn7eiZZorNNY4rRuXSuY58s8ZBlul49U8A+w3OuS1FqlY57nMjEbHHBjXOcGcg55Lj3lBl2ZapIpWSRuayRpoHPaxzRe7Bc4Pa5tKOOJBpnorf0wfI7ZgkmnstpkFrDWyWVsXYabPISxzo42Yk0NKfd5KnWGZrJGukibKG1rG5z2NeKECroyHChIOB0W02nt1klnFngssdnj3olNySaQueGOjFTK91AA44DkgtvSvoq4DaVpNnpGI7G6B7SA0U3DZyGtP4Q6tRxovJ0VtErrJFDZLTZIbQ60SgsnEJkeHtgbCGCSJ/wB4PGmap9h2gYobRC1rbs7Yg46jdSCUXacSKY6La7H6RRwGJ5sNnfNEWuZLftEbqsN5rntZJde4EZkY0AQbDZmxLRadmFsEO8eLdJvKCMOAbZ4gO0aUAc52ANMVWLfYpYJHRTMLJWUvNdSuLQ4ZEjFrge9bOw7ejbA6CeyRWhhmdN23zRlr3sax1N08VFGDNeTbu0jarQ+YsawuEYDWkloEcbImgFxJOEYzQYtm20wSsmYGEtNQHtD2V/6mnNWrbW0pjs4/bZIzJO6GSBjYoWSCFm8vyO3TG3WvqwNDjU3SaUVM+Rz5FezaluNo3YeA3dQRQi7q2IOAJr943kHRNtbPps+SxiWymez2eBzoWlm+a+AyyWol12pqwx5nGh44+PortyG7YYG2rclpYyWL7BFMJXvncf47sW3mva2oypVaS2dLxI6aUWOFlotEckb5WyWgmkrLjyI3SFgJHLBaDZlqEM0coaHmJ7H3XEgOLHBwqWmuYCC2PtsllgtkkG7Y87TfFeMUMlIwyd4YBIxwAvNGQWq6ZG9NFIQ0OkstjkkutawOe+BjnOutAAJJ0CjZukDaTMms0c8c05tF1z5o7khDwaOie00o8ihXj23tT7TIJLjY2sZHGxjC4gNiYGMFXkudg3MlB4PmfIK69T/vEUyEUvzYqV8znyCuvU/7xbw3UtPFiD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6f+8bXXISu8cFoaeJz5Bb7p97xtZ/NdQc8Foaad5KBgjMaeYTA00ORSBriBiPMJinccuRQMHXucP1TA072lIHXUZ8wmBppm0/ogmMcRnl8EhTIZjLnxSB17nBTJA0wFLqBngMznyQDroMuZSppqcymDr4BBIDxOfIJg6+AU7MQHtvAOaHNLwSWgioq0uGLQRhUZLorOitnLmltjmLWuc3+NGRIIy+N1TvRu3X3NqRgd0KAXig5vTTXjzUq6+K3TIYxb2R7gGNz4Ru336esZHePYkLh7ReBfN3AEkBbOx7GZFaJxaI4d0ySAOEgtT3Bk4kljEYs5reMTTUvyIbzQVMDTUZfBA5ZnyWy+ysZBLO4EhznR2ZpJBJBDpJDQ4iNhDaHAvlbndcFYmbIgE0MEscO+MzA8QC2NaWCOR0jXvldRxLt3QxkfeQUz5DzKlTxOa9+2WNpZ3iFsJkgvOY3eXbwtFoiqBI5zhVsTNfmvBTTxQAOo7/gnTTwSB1GnyTp4HLkgddfFeyx2EuFXGg0pmfoF5oWXnAcSAfgdVvwOC43a5p4Qu7E2dRlV1V3ftp7e8sAsUf4fN31WewdHN84XXbtoIvOILgByFak96at+z4bkbW8gT8TiVPyMmu3TyzxloczAxNP6dOI8Ru+GeydCLGwULHSHUve/HuYQB4Ly7T6A2d49QXROGQJc9h+IdVw+IPcVYtmSVZTgad2YXrWYnPy7d2Z1J3+eH66IdeJajlmmHDrdYnwyOilF17fa4ciDqDxVu6n/AHk0/lS0HKrF7es6wgxxTatdcceLXBzhX4Fpp/mK8XU/7yafypafCrFscHK9TYpud+/lDv2tK5NL6GZkpKLMlJexxCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vG1n811PJaEDTTUrfdPveVr4711PJaLIYaZ96DYbH2d9ol3Qe1kjg7dA5PeB2IqkgNLzgCdaDVbax9FWvmliNpY1rHQRB4aXNdaZhhEKEdkPbIDJiKR1oahaDZ9pMMscrKFzHse29UjsODhWmlQthsfpBLZnSPjDHbwgkPaXBsjHXo5WCuD2Em6cRiag1QZ7J0dc4Qlz7rn/AGyou1u/ZI94Qe1jepTlzXs2n0VZDY2WgzEGSOJ7WuEIa4ybu8yOkpkLmCQONYwKA4iorr7B0hkiiEQjieWifdyPEhkaLQy5LduvDHEj8TXUJULVt6WWHcvDDHdhaBQ1YYG3Gvbjg8t7LjkRpgKBj2BYPtFphhL7m8e1jnXb1AfvXai98KraS9E3R2izwOkAMzpATd7MbY5Xxukz7bLkZkrhUeK02zLa+CZkzA0yRODm3gS004gEE+K2g6VzgwObcvwwOgiddN4McHNLjjQvDXFoNMtDmgz7N6LCR1pjmtDYDZ5Wxuc9hcHEma9ShwN2FxaPvEtbUVqpWno8yKOaWR85EdotFnaYoGvYDDuw0yPdI0Rl5koBj7JzWvte3pZDKX3CZXWd8jrpBL7O1zWkY0q68S7iThRZJtuuka4SwQSF808wLvtDSx1ouX7u7maKerbQOByQag5HgM+Z4K42jovaY5gx8s7Gutlns8cjmyNDhaN7elZV1DS7iATXeZiuNOIFKaDzK3I6QTb501I9460Q2ki667vIN5cHtVu+sdUVrliEG12Vsgy20QxS2yOW7K5zpI2w2m+GF4ArP7TwaVc5vta1Xog2fI2W1ubbbW7csgdI+zVlmfebiJbk931XaaTfcBQ5ZCts2mGEvhhihrHLGRGZyCJWljj62V5DgCaEU5gr1/4+8umMkMMpnEW9DxK0OdHjf9VI0h7nVc41oS4mmKDVzuqSA5zmVdcL86FxOIqQ0mpJAJFScTmvRJtSd90OnmNwhzb0sjrhGRZV3ZI4heV7gSSAGtcSQBeo2pyF4k0GWJJ4k5o+Pegy2m1SSOvyPfI6lAZHue6gy7TiTTE+KhTTQZqPzOXIKWXwGaCR89Pgl8j5FICmWf6Jnjoc0GWF91wP4SK8xqt8q98xlzC9ljt90AOxbodRy5hcbtE1cYXth7Roxqqrd3hTV39pbZXCwTB8bXDgK/EYFUgWtn4h5g+BXo2f0jELhRpkYSLza3fiQSM/n5idk49dynljjDRZmbi6f1akfid/w6bsyOjK8TXuyXrWgsnTGxPbXfBlNJGuaRyrS6e4lebanTmzRikVZn6AAtYObnuGXwBWXqwsq5dmNOd/j+9EKvKtTvqmqHi6zrY0RRQ1xc++R/0tDmjxLv8AxK8PU/7yFczFL4VYqntHaUlokdJK68ScvuigwAboAD/ypVt6n/eIrnupPmxbPAxfTWKbc9e/lCyLurcmp9CsyUlFmSkvY4hCEIBCEIBYbUOyVmUXCoQfKHWNDc2nagczJXuc1rh81WwfEZ8wvo/rC6t49oESNdupgKXwLzXNrUBzaitKmhBwrqufO6lrXXC0Q4a0f54IOZADIa+QTa7wyA4rpfoWtf8APhx5SfRMdTFrz38PLCT6IOaXdP7T+iYOvc4LpXoYtdKb+Hwkz8E/Q1a6138PPCTHyQc2A01GXMIFNMz5cV0n0M2v+fDywk+iZ6m7Xj66Gpzwk+iDm+HcPMqQHic+QXR/Q3a/50OGWEn0R6HLXT+PDjng/wCiDnLeOgy+qfLvBXRz1PWvCk0OHKTLwS9Dtrx9dDywkw8kHOa668OakMO7EldG9D1rz30Nfg/6I9D1rw9dDTXB+Pkg518ij45DzXRh1QWvWaHwk+iQ6n7XrNDhyf8ARBzz45nyTr4jP4LoZ6oLX/Ohx5P+ifohteHroa/B/wBEHOwNB3/BMHwyAXQ/RDa/50Pg/wCifoitf86Hlg/6IOe008FZobfs/dBr4GXrkbXPDZ7942U7x3t3bwtIZQ3aUJwoSt76IrXT+ND/AOf0R6I7X/Nh54P+iDwsteyd42rGXPXAUjtYc1hMG5vA3hLMGiYEgtaS72hgVCybT2a+4ZomEizwsutbaI6OYA2W+5gdV7qAsc2oHaqQSFsfRHa/50PLB/0TPVHa/wCbDjn7f0QaNtt2eWkGJoDdwKlk5c9oiBn3ZDqMnMt6jn0ZS7TCoPptNt2aKUijeS6MSbtlpYBFvZd4WCR9RJujEKk0vVpxWz9Elr/mw4ZYP+iB1SWv+dDz9v6IKt0itMLzCYTGWxxXCIo5omXjNPIS1stXUo9lSTmcMBhY+pqzOdby4CrWROq7hfc0NHfdd/aV6o+qO1Eis8TRxDXuw/ymlfFdQ6GdE4rBFu46kk1c53tOPPgOA/3KCzMyUkIQCEIQCEIQCEIQIhK4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4FJCEAhCEAhCEH/9k='  alt='first card'
                            />
                            <Carousel.Caption className="card-carousel-caption">
                              <p><strong>Global</strong></p>
                              <p>Available Balance - $40000</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src=
                              'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhMVFhUXGBUbGBgVFhUVFhsaFhgWFhcbGBUYHSggGBolHRUXITEiJSkrLi4uFx8zODMtNygtLisBCgoKDg0NGA8PGzclHyU3NzI3NzI3MC83MzM3Ljc1LTcxNDctNzc3NzcrLy43Nzc3Mi41KzUvNzArKy83Ny0tL//AABEIALABHgMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIGAwUHBAj/xABEEAABAwEFBAYHBgQFBAMAAAABAAIDEQQSITFBBRNRYQYHIoGRoRcjMjVxsdEUQlJjwdMzU3OSFXKj4fBik6LxJCVD/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAUGAwT/xAAoEQEAAQMBBwMFAAAAAAAAAAAAAQIDBBMFESEiMUFxFFGxEjKBkcH/2gAMAwEAAhEDEQA/AO1IQhAIQhAISqi8EDQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQleCaAQhCAQhCAQhCAScaJrDaj2SgovT/rFh2dRlDJM4VDGmlBiAXu+6KjgSuZy9c9vLjdiswGgLZXGn+YSCvgq11kWgv2nanHGj7tOTWtaPkq2Bpr936IOjembaGHYs1P8kv7qZ65No/y7Nnh2Jf3VztkZcaBpJJoWgEmvIZ1QMKg5jig6J6ZdoVpcs3/AG5c+H8VNvXHtE/cs1a49iX91c5BGWnHnxU66YVOZ5IOh+mTaGPYs1B+XL+6n6Yto4C5Zq69iXD/AFVzoPHcMhxKlyrjm4/og6H6YtoZ3LNy9XLj/qpnrh2h+Czc/Vy4f6q55fGfcAnfAw/u70HQ/S/tHHsWao/Ll/dTHW/tD8Nmofy5f3Vz4cNRlzCV4dx8ig6F6YNoZXLNX+nL+6n6X9ofhs1B+XL+6uf10148kfIZcyg6COt3aGHYs1f6cv7qB1vbQOTLNy9XL+6qABp4oBry4IL/AOl7aH4LNT+nL+6n6XNofhs1f6cv7qoFdfEKVPHRBfh1uW/8Nmp/Tl/dQetvaH4LNy9XL+6qCPI58ipDhr+iC++lu35XbNX+nLSv/dTb1tbQ/BZufq5f3VQa6aceafLXVBffS3b/AMNmp/kl/dQOtnaGV2zV19XL+6qFXXwTA08Sgvvpat/4LPy7Ev7ifpat+rbPz7En7ioVdfAJg0w01QdBs3W5bWntxQPAzDRIwn4OLnfJdW6HdK4rfFvI6gjBzHe008DxHA6+K+aPmMuYV66mrQ5u0C1p7D4X3hza5hb4Vd/cUH0GhRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73jaz+a6vwwW46mWu/xWG42NzrspZvSQAbhNRQElwFcOZOi0/T73la6ZiV3hgtJBKWEOY4tNatc0kOa4cCMQUH1XHBA+2F7GwQbTayr2m4/eRk0reFHOYSKB4uuFBeFOyeSW+NrulNLQyHGVhe0UfCHfZgaVeBeNQCSQO1XALmZtchk3pkeZQal9918njfrWvOqxvN4mpJvEmpxNTiak5oOjz2ewvsLrVAImie12OsLrt+B43u/jbXOE1a5vI0I7K2HWtZrQ0SGKFrbIJO250WzhT1gEW6MXrSw4e1jQ46rlAIz+9lTnxQGDLhmf0QXrpT0ifJs2yGlnvT/at7cs9na/1UrBHi1gMeHClVY+nlkMmz5Hui+ysjETo43M2e+E4taY7LaIKSkmpdU1qGmtFXmdFoHCDef/AB7whYaG9aHSyOjjLnXpDHuTWR7XsDcGEUwqsFl6FQSx32WghpjBY54Y07wki68CoaHAxEAuBpI72rhQWTbO37DAxsL2iWtkhAgZZLLcvvszS132sHeNIc4PqBUEYLPYbLN/hdkNhga+Qw9smPZzoid7IH33T+t3l2mWGXNVk7As/wBu3bIy6NsE8pYC97nEvnELQA9riWh0AIDmk7t1SDVO2dDoDMXRuuwVdTdgTVItJgc2MFxe+kcckpAvEVYO1UFwevZNnsUuzrDZpRHHLM+1GO0GgLZI5GCNkxz3UjXXanIhppTLcCzSXtpf4dDZ3yjaNKFllcBDclrdE3ZAv3cufNVmTonZ29mSUsxeAW3HmrTai4vN66WsZZ2OF1ovb7jiHauhULb1JnSObE53ZjpG4gAsLZHCgY6kl3MOuCjwXBqDy9PrOxs8dxsTXmGL7QILu6baADvmsDDdFAWVDcL17mq1XXwWy6RbNbZpzCxznNDWG88AO7QvCraAsNCOy4VFSMRQnW118EEgNPFOtc9ckmjTQZ8ynUHv8kBXXxUqaeCj8x5hSp4HLkgK6+KdNNdEgdfFOmngUE73/rmlTTxSrrqmBppqgK6+CYGmgzS+Zy5BMeQ8ygfzOXIJg+GqXzPkFKoHdpxQKmngrr1P+8gfypa/GrFSiNPBXXqf95A/lS18WIPoZmSkosyUkAhCEAhCEAsFr9krOsFr9koPlDp8P/sbXTPfOp5LQfI58it90+942s/muoOeC0Rw+GqAx7x5hMU7jlyKQGmoy5hMU7jnyKBg/wB2X+6Yp3DPmUgTlrlXlxTFO4ZcygA3kKnyCd0Z0wHmUU01OfIKTcfh934oHcAGIGOfJF0agVGWGfBFde4hSBphmdEDawZd55JXRnQVGeGfNMDTxKlz1/RAg3+3P/ZP5nyCVNdOHNSyx11+CB00HxHNAPDM58kAaeCAa8uKCXMaZc0/kfIqNdfAKR8tUAPMeYT+Ry5JDzGXMJ/I+RQOvimPIeZRy148kDyGSA+Z8gmPIZcylTTxUm/+kBlnrmgHjmEV101TBp8dEDpp581dOp/3kOO6lr4sVKp4DNXbqg95NOu6l8KsQfQrMlJRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73la+O9dTyWiaaYaDPmt70+95WumZld+i0OHcPMoHTTvaf0TB10yISxyOuXJMHXucEDFfZ8+SYOugwaP1SAOX3c68kwde5o/VBIDTvcf0Tca/A5cik3DuzPE8EyPA+RQOuuuoUgaYDHgVHHvHmFIcBkfLigYp3DzKYrXmfIJDyGXMp001OaB0Gen6p4948wo1/tyTp4jLmEEh5HLkUVrn3pDyPkVL46eaAr4nJSHlqo/M+QUvkPMoCmngnXXQ5pY5HuTrr4hBLl58kA6+CKaaceSAdfBAU08VLP9Ehh8BnzKKeB8igddfEJjh4FL5jPmExwGR/4UB8h5lXXqf95DjupfmxUqvgMldep/3kB+VLXxYg+hmZKSizJSQCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vK1gZmV3hgtDXXQZcyt90+942sfmuqeWC0Nde5oQOmh18imDrqM+YSpTPI58igHXUeYQSA/tz/ANkxXPU5cgkAM/u5058Ewa/E58ggeHcPMqRrmcjny4KF8dwyHNSpTHP8Q+KCWPeMuYUgOGRz5cVA4a/A/opNIORw1QSB18AnTTxKg2QHKlcgFIEZVyzQSrrpw5J008CoF9MSR8OSmBpocQUDB18UfHIZc0r2p70NI4g8EEqeJz5KVddNFHl4lDngajkEEqaH/hTrrqM0uROfzT+YzQOngn8z5BI0GOiQcDhUVOfwQW+ydB5DBLJI9l9sDJY4I5GumJkexse9jpVocHVFDXKtK0PosfV3aBKI7Q5ga5to7UUjSGywxmTdyOeAGnDE4igOOC1o6cWndlgbAHGJkRmEX/yS2ItMdZL2JbdGNMdakArPaOsO1ulbK77PeaJA4XHFrjM245z2OkIJpWgFG4nDEoHB0Kko8vkjoG2Z0U0T2vs7mz2gQEl5AJDe1UYEECuC1XSTYxsdpks+9ZKGOIL48sCQQ5tTcdhi2ppxK9dq6ZWhzXRgQRsdHExjI4yxjGwy79u7BcaOL8SXXqrXbZ2q61zOtD2xMc/F4iaWNLtXULibxriaoPFXXwV16n/eQH5UtfjVipVddTkrr1P+8Rw3UvjViD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6fe8bWPzXV8loQddTkFvun3vG18N66vktDj3nyCCwdAZYo9oWZ8zmCJr+2ZKXB2XYurhSpW527HanTWF9qnstojfNdjdZmxBnZfCJWuuRsr7TM6jE81VNkW1kLy90EVoYWlro5b4FDTtNcxwc12GDgcKle7b+3G2iOGGKzx2eOEyuY1j5ZKumuXyXSuJ/8Azbh8UFo6Z9E3wQ260yWe4TtD1bwRT7O/f0o1poG3jFpXJee0SWi3bKjaWtklZbHMbu4oo3buOyGQjsNbUABxxqcFVGbUIsjrNcbcdNHKXY3g5kckYAGVPWE9ysuzOnDIIhBFYIW0vuDt7aHG/JC6zvfRzyKljiLuXzQe3onJbIrPA8Wyw2ezPc4sZaGwbxzWyESEF0LnHtBwxdh8KKp9InxG12l8FN2Z5ywNFG7syOLLo0F2i2Fk6SRCz2ezz2GKfc7wRudNaI3UkkdIaiJ7Rm7yWitD2l7nMaGC84hgLnAAkm6HOJJAyqccEHs2PtV9lk3sVy9deGmRjZA28KEhrgReGhIPerN03tcpskMNsfGbYJHyvbGyJjo4nxMETJN01rb5Ic67iQHCqpd2uGh8itntnbDrTaX2p7GVLmOLMSwljWtoQTUtNzEV1KC59L5ppoJXWa02OSyMist6GIQ75mEDHFxEV7+MdH68MFrYXz2zZghDRI6O2WeKIMjia+6LNaTdvNaC72QauJOFarXWrpHGYZoobFBA60BgkfHJaHdlsjZaNZI8tYLzBgBgMF7dhdNWWSJrIrDCS1zX3zLaLzpmxvi3hbfoKtkf2R2e1lgKBl6MvtrLK2SK12KzQyPkufaRBee5oZfLS+F7iBeaMSPgtR0ymhfbrS6AsMLpSWFgozIVLRQYF1TzrVZNn7eiZZorNNY4rRuXSuY58s8ZBlul49U8A+w3OuS1FqlY57nMjEbHHBjXOcGcg55Lj3lBl2ZapIpWSRuayRpoHPaxzRe7Bc4Pa5tKOOJBpnorf0wfI7ZgkmnstpkFrDWyWVsXYabPISxzo42Yk0NKfd5KnWGZrJGukibKG1rG5z2NeKECroyHChIOB0W02nt1klnFngssdnj3olNySaQueGOjFTK91AA44DkgtvSvoq4DaVpNnpGI7G6B7SA0U3DZyGtP4Q6tRxovJ0VtErrJFDZLTZIbQ60SgsnEJkeHtgbCGCSJ/wB4PGmap9h2gYobRC1rbs7Yg46jdSCUXacSKY6La7H6RRwGJ5sNnfNEWuZLftEbqsN5rntZJde4EZkY0AQbDZmxLRadmFsEO8eLdJvKCMOAbZ4gO0aUAc52ANMVWLfYpYJHRTMLJWUvNdSuLQ4ZEjFrge9bOw7ejbA6CeyRWhhmdN23zRlr3sax1N08VFGDNeTbu0jarQ+YsawuEYDWkloEcbImgFxJOEYzQYtm20wSsmYGEtNQHtD2V/6mnNWrbW0pjs4/bZIzJO6GSBjYoWSCFm8vyO3TG3WvqwNDjU3SaUVM+Rz5FezaluNo3YeA3dQRQi7q2IOAJr943kHRNtbPps+SxiWymez2eBzoWlm+a+AyyWol12pqwx5nGh44+PortyG7YYG2rclpYyWL7BFMJXvncf47sW3mva2oypVaS2dLxI6aUWOFlotEckb5WyWgmkrLjyI3SFgJHLBaDZlqEM0coaHmJ7H3XEgOLHBwqWmuYCC2PtsllgtkkG7Y87TfFeMUMlIwyd4YBIxwAvNGQWq6ZG9NFIQ0OkstjkkutawOe+BjnOutAAJJ0CjZukDaTMms0c8c05tF1z5o7khDwaOie00o8ihXj23tT7TIJLjY2sZHGxjC4gNiYGMFXkudg3MlB4PmfIK69T/vEUyEUvzYqV8znyCuvU/7xbw3UtPFiD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6f+8bXXISu8cFoaeJz5Bb7p97xtZ/NdQc8Foaad5KBgjMaeYTA00ORSBriBiPMJinccuRQMHXucP1TA072lIHXUZ8wmBppm0/ogmMcRnl8EhTIZjLnxSB17nBTJA0wFLqBngMznyQDroMuZSppqcymDr4BBIDxOfIJg6+AU7MQHtvAOaHNLwSWgioq0uGLQRhUZLorOitnLmltjmLWuc3+NGRIIy+N1TvRu3X3NqRgd0KAXig5vTTXjzUq6+K3TIYxb2R7gGNz4Ru336esZHePYkLh7ReBfN3AEkBbOx7GZFaJxaI4d0ySAOEgtT3Bk4kljEYs5reMTTUvyIbzQVMDTUZfBA5ZnyWy+ysZBLO4EhznR2ZpJBJBDpJDQ4iNhDaHAvlbndcFYmbIgE0MEscO+MzA8QC2NaWCOR0jXvldRxLt3QxkfeQUz5DzKlTxOa9+2WNpZ3iFsJkgvOY3eXbwtFoiqBI5zhVsTNfmvBTTxQAOo7/gnTTwSB1GnyTp4HLkgddfFeyx2EuFXGg0pmfoF5oWXnAcSAfgdVvwOC43a5p4Qu7E2dRlV1V3ftp7e8sAsUf4fN31WewdHN84XXbtoIvOILgByFak96at+z4bkbW8gT8TiVPyMmu3TyzxloczAxNP6dOI8Ru+GeydCLGwULHSHUve/HuYQB4Ly7T6A2d49QXROGQJc9h+IdVw+IPcVYtmSVZTgad2YXrWYnPy7d2Z1J3+eH66IdeJajlmmHDrdYnwyOilF17fa4ciDqDxVu6n/AHk0/lS0HKrF7es6wgxxTatdcceLXBzhX4Fpp/mK8XU/7yafypafCrFscHK9TYpud+/lDv2tK5NL6GZkpKLMlJexxCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vG1n811PJaEDTTUrfdPveVr4711PJaLIYaZ96DYbH2d9ol3Qe1kjg7dA5PeB2IqkgNLzgCdaDVbax9FWvmliNpY1rHQRB4aXNdaZhhEKEdkPbIDJiKR1oahaDZ9pMMscrKFzHse29UjsODhWmlQthsfpBLZnSPjDHbwgkPaXBsjHXo5WCuD2Em6cRiag1QZ7J0dc4Qlz7rn/AGyou1u/ZI94Qe1jepTlzXs2n0VZDY2WgzEGSOJ7WuEIa4ybu8yOkpkLmCQONYwKA4iorr7B0hkiiEQjieWifdyPEhkaLQy5LduvDHEj8TXUJULVt6WWHcvDDHdhaBQ1YYG3Gvbjg8t7LjkRpgKBj2BYPtFphhL7m8e1jnXb1AfvXai98KraS9E3R2izwOkAMzpATd7MbY5Xxukz7bLkZkrhUeK02zLa+CZkzA0yRODm3gS004gEE+K2g6VzgwObcvwwOgiddN4McHNLjjQvDXFoNMtDmgz7N6LCR1pjmtDYDZ5Wxuc9hcHEma9ShwN2FxaPvEtbUVqpWno8yKOaWR85EdotFnaYoGvYDDuw0yPdI0Rl5koBj7JzWvte3pZDKX3CZXWd8jrpBL7O1zWkY0q68S7iThRZJtuuka4SwQSF808wLvtDSx1ouX7u7maKerbQOByQag5HgM+Z4K42jovaY5gx8s7Gutlns8cjmyNDhaN7elZV1DS7iATXeZiuNOIFKaDzK3I6QTb501I9460Q2ki667vIN5cHtVu+sdUVrliEG12Vsgy20QxS2yOW7K5zpI2w2m+GF4ArP7TwaVc5vta1Xog2fI2W1ubbbW7csgdI+zVlmfebiJbk931XaaTfcBQ5ZCts2mGEvhhihrHLGRGZyCJWljj62V5DgCaEU5gr1/4+8umMkMMpnEW9DxK0OdHjf9VI0h7nVc41oS4mmKDVzuqSA5zmVdcL86FxOIqQ0mpJAJFScTmvRJtSd90OnmNwhzb0sjrhGRZV3ZI4heV7gSSAGtcSQBeo2pyF4k0GWJJ4k5o+Pegy2m1SSOvyPfI6lAZHue6gy7TiTTE+KhTTQZqPzOXIKWXwGaCR89Pgl8j5FICmWf6Jnjoc0GWF91wP4SK8xqt8q98xlzC9ljt90AOxbodRy5hcbtE1cYXth7Roxqqrd3hTV39pbZXCwTB8bXDgK/EYFUgWtn4h5g+BXo2f0jELhRpkYSLza3fiQSM/n5idk49dynljjDRZmbi6f1akfid/w6bsyOjK8TXuyXrWgsnTGxPbXfBlNJGuaRyrS6e4lebanTmzRikVZn6AAtYObnuGXwBWXqwsq5dmNOd/j+9EKvKtTvqmqHi6zrY0RRQ1xc++R/0tDmjxLv8AxK8PU/7yFczFL4VYqntHaUlokdJK68ScvuigwAboAD/ypVt6n/eIrnupPmxbPAxfTWKbc9e/lCyLurcmp9CsyUlFmSkvY4hCEIBCEIBYbUOyVmUXCoQfKHWNDc2nagczJXuc1rh81WwfEZ8wvo/rC6t49oESNdupgKXwLzXNrUBzaitKmhBwrqufO6lrXXC0Q4a0f54IOZADIa+QTa7wyA4rpfoWtf8APhx5SfRMdTFrz38PLCT6IOaXdP7T+iYOvc4LpXoYtdKb+Hwkz8E/Q1a6138PPCTHyQc2A01GXMIFNMz5cV0n0M2v+fDywk+iZ6m7Xj66Gpzwk+iDm+HcPMqQHic+QXR/Q3a/50OGWEn0R6HLXT+PDjng/wCiDnLeOgy+qfLvBXRz1PWvCk0OHKTLwS9Dtrx9dDywkw8kHOa668OakMO7EldG9D1rz30Nfg/6I9D1rw9dDTXB+Pkg518ij45DzXRh1QWvWaHwk+iQ6n7XrNDhyf8ARBzz45nyTr4jP4LoZ6oLX/Ohx5P+ifohteHroa/B/wBEHOwNB3/BMHwyAXQ/RDa/50Pg/wCifoitf86Hlg/6IOe008FZobfs/dBr4GXrkbXPDZ7942U7x3t3bwtIZQ3aUJwoSt76IrXT+ND/AOf0R6I7X/Nh54P+iDwsteyd42rGXPXAUjtYc1hMG5vA3hLMGiYEgtaS72hgVCybT2a+4ZomEizwsutbaI6OYA2W+5gdV7qAsc2oHaqQSFsfRHa/50PLB/0TPVHa/wCbDjn7f0QaNtt2eWkGJoDdwKlk5c9oiBn3ZDqMnMt6jn0ZS7TCoPptNt2aKUijeS6MSbtlpYBFvZd4WCR9RJujEKk0vVpxWz9Elr/mw4ZYP+iB1SWv+dDz9v6IKt0itMLzCYTGWxxXCIo5omXjNPIS1stXUo9lSTmcMBhY+pqzOdby4CrWROq7hfc0NHfdd/aV6o+qO1Eis8TRxDXuw/ymlfFdQ6GdE4rBFu46kk1c53tOPPgOA/3KCzMyUkIQCEIQCEIQCEIQIhK4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4FJCEAhCEAhCEH/9k='  alt='first card'
                              alt="second card"
                            />
                            <Carousel.Caption className="card-carousel-caption">
                              <p><strong>Global</strong></p>
                              <p>Available Balance - $40000</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src=
                              'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhMVFhUXGBUbGBgVFhUVFhsaFhgWFhcbGBUYHSggGBolHRUXITEiJSkrLi4uFx8zODMtNygtLisBCgoKDg0NGA8PGzclHyU3NzI3NzI3MC83MzM3Ljc1LTcxNDctNzc3NzcrLy43Nzc3Mi41KzUvNzArKy83Ny0tL//AABEIALABHgMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIGAwUHBAj/xABEEAABAwEFBAYHBgQFBAMAAAABAAIDEQQSITFBBRNRYQYHIoGRoRcjMjVxsdEUQlJjwdMzU3OSFXKj4fBik6LxJCVD/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAUGAwT/xAAoEQEAAQMBBwMFAAAAAAAAAAAAAQIDBBMFESEiMUFxFFGxEjKBkcH/2gAMAwEAAhEDEQA/AO1IQhAIQhAISqi8EDQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQo3gi8EEkKN4IvBBJCjeCLwQSQleCaAQhCAQhCAQhCAScaJrDaj2SgovT/rFh2dRlDJM4VDGmlBiAXu+6KjgSuZy9c9vLjdiswGgLZXGn+YSCvgq11kWgv2nanHGj7tOTWtaPkq2Bpr936IOjembaGHYs1P8kv7qZ65No/y7Nnh2Jf3VztkZcaBpJJoWgEmvIZ1QMKg5jig6J6ZdoVpcs3/AG5c+H8VNvXHtE/cs1a49iX91c5BGWnHnxU66YVOZ5IOh+mTaGPYs1B+XL+6n6Yto4C5Zq69iXD/AFVzoPHcMhxKlyrjm4/og6H6YtoZ3LNy9XLj/qpnrh2h+Czc/Vy4f6q55fGfcAnfAw/u70HQ/S/tHHsWao/Ll/dTHW/tD8Nmofy5f3Vz4cNRlzCV4dx8ig6F6YNoZXLNX+nL+6n6X9ofhs1B+XL+6uf10148kfIZcyg6COt3aGHYs1f6cv7qB1vbQOTLNy9XL+6qABp4oBry4IL/AOl7aH4LNT+nL+6n6XNofhs1f6cv7qoFdfEKVPHRBfh1uW/8Nmp/Tl/dQetvaH4LNy9XL+6qCPI58ipDhr+iC++lu35XbNX+nLSv/dTb1tbQ/BZufq5f3VQa6aceafLXVBffS3b/AMNmp/kl/dQOtnaGV2zV19XL+6qFXXwTA08Sgvvpat/4LPy7Ev7ifpat+rbPz7En7ioVdfAJg0w01QdBs3W5bWntxQPAzDRIwn4OLnfJdW6HdK4rfFvI6gjBzHe008DxHA6+K+aPmMuYV66mrQ5u0C1p7D4X3hza5hb4Vd/cUH0GhRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73jaz+a6vwwW46mWu/xWG42NzrspZvSQAbhNRQElwFcOZOi0/T73la6ZiV3hgtJBKWEOY4tNatc0kOa4cCMQUH1XHBA+2F7GwQbTayr2m4/eRk0reFHOYSKB4uuFBeFOyeSW+NrulNLQyHGVhe0UfCHfZgaVeBeNQCSQO1XALmZtchk3pkeZQal9918njfrWvOqxvN4mpJvEmpxNTiak5oOjz2ewvsLrVAImie12OsLrt+B43u/jbXOE1a5vI0I7K2HWtZrQ0SGKFrbIJO250WzhT1gEW6MXrSw4e1jQ46rlAIz+9lTnxQGDLhmf0QXrpT0ifJs2yGlnvT/at7cs9na/1UrBHi1gMeHClVY+nlkMmz5Hui+ysjETo43M2e+E4taY7LaIKSkmpdU1qGmtFXmdFoHCDef/AB7whYaG9aHSyOjjLnXpDHuTWR7XsDcGEUwqsFl6FQSx32WghpjBY54Y07wki68CoaHAxEAuBpI72rhQWTbO37DAxsL2iWtkhAgZZLLcvvszS132sHeNIc4PqBUEYLPYbLN/hdkNhga+Qw9smPZzoid7IH33T+t3l2mWGXNVk7As/wBu3bIy6NsE8pYC97nEvnELQA9riWh0AIDmk7t1SDVO2dDoDMXRuuwVdTdgTVItJgc2MFxe+kcckpAvEVYO1UFwevZNnsUuzrDZpRHHLM+1GO0GgLZI5GCNkxz3UjXXanIhppTLcCzSXtpf4dDZ3yjaNKFllcBDclrdE3ZAv3cufNVmTonZ29mSUsxeAW3HmrTai4vN66WsZZ2OF1ovb7jiHauhULb1JnSObE53ZjpG4gAsLZHCgY6kl3MOuCjwXBqDy9PrOxs8dxsTXmGL7QILu6baADvmsDDdFAWVDcL17mq1XXwWy6RbNbZpzCxznNDWG88AO7QvCraAsNCOy4VFSMRQnW118EEgNPFOtc9ckmjTQZ8ynUHv8kBXXxUqaeCj8x5hSp4HLkgK6+KdNNdEgdfFOmngUE73/rmlTTxSrrqmBppqgK6+CYGmgzS+Zy5BMeQ8ygfzOXIJg+GqXzPkFKoHdpxQKmngrr1P+8gfypa/GrFSiNPBXXqf95A/lS18WIPoZmSkosyUkAhCEAhCEAsFr9krOsFr9koPlDp8P/sbXTPfOp5LQfI58it90+942s/muoOeC0Rw+GqAx7x5hMU7jlyKQGmoy5hMU7jnyKBg/wB2X+6Yp3DPmUgTlrlXlxTFO4ZcygA3kKnyCd0Z0wHmUU01OfIKTcfh934oHcAGIGOfJF0agVGWGfBFde4hSBphmdEDawZd55JXRnQVGeGfNMDTxKlz1/RAg3+3P/ZP5nyCVNdOHNSyx11+CB00HxHNAPDM58kAaeCAa8uKCXMaZc0/kfIqNdfAKR8tUAPMeYT+Ry5JDzGXMJ/I+RQOvimPIeZRy148kDyGSA+Z8gmPIZcylTTxUm/+kBlnrmgHjmEV101TBp8dEDpp581dOp/3kOO6lr4sVKp4DNXbqg95NOu6l8KsQfQrMlJRZkpIBCEIBCEIBYLX7JWdYLX7JQfJ/T73la+O9dTyWiaaYaDPmt70+95WumZld+i0OHcPMoHTTvaf0TB10yISxyOuXJMHXucEDFfZ8+SYOugwaP1SAOX3c68kwde5o/VBIDTvcf0Tca/A5cik3DuzPE8EyPA+RQOuuuoUgaYDHgVHHvHmFIcBkfLigYp3DzKYrXmfIJDyGXMp001OaB0Gen6p4948wo1/tyTp4jLmEEh5HLkUVrn3pDyPkVL46eaAr4nJSHlqo/M+QUvkPMoCmngnXXQ5pY5HuTrr4hBLl58kA6+CKaaceSAdfBAU08VLP9Ehh8BnzKKeB8igddfEJjh4FL5jPmExwGR/4UB8h5lXXqf95DjupfmxUqvgMldep/3kB+VLXxYg+hmZKSizJSQCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vK1gZmV3hgtDXXQZcyt90+942sfmuqeWC0Nde5oQOmh18imDrqM+YSpTPI58igHXUeYQSA/tz/ANkxXPU5cgkAM/u5058Ewa/E58ggeHcPMqRrmcjny4KF8dwyHNSpTHP8Q+KCWPeMuYUgOGRz5cVA4a/A/opNIORw1QSB18AnTTxKg2QHKlcgFIEZVyzQSrrpw5J008CoF9MSR8OSmBpocQUDB18UfHIZc0r2p70NI4g8EEqeJz5KVddNFHl4lDngajkEEqaH/hTrrqM0uROfzT+YzQOngn8z5BI0GOiQcDhUVOfwQW+ydB5DBLJI9l9sDJY4I5GumJkexse9jpVocHVFDXKtK0PosfV3aBKI7Q5ga5to7UUjSGywxmTdyOeAGnDE4igOOC1o6cWndlgbAHGJkRmEX/yS2ItMdZL2JbdGNMdakArPaOsO1ulbK77PeaJA4XHFrjM245z2OkIJpWgFG4nDEoHB0Kko8vkjoG2Z0U0T2vs7mz2gQEl5AJDe1UYEECuC1XSTYxsdpks+9ZKGOIL48sCQQ5tTcdhi2ppxK9dq6ZWhzXRgQRsdHExjI4yxjGwy79u7BcaOL8SXXqrXbZ2q61zOtD2xMc/F4iaWNLtXULibxriaoPFXXwV16n/eQH5UtfjVipVddTkrr1P+8Rw3UvjViD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6fe8bWPzXV8loQddTkFvun3vG18N66vktDj3nyCCwdAZYo9oWZ8zmCJr+2ZKXB2XYurhSpW527HanTWF9qnstojfNdjdZmxBnZfCJWuuRsr7TM6jE81VNkW1kLy90EVoYWlro5b4FDTtNcxwc12GDgcKle7b+3G2iOGGKzx2eOEyuY1j5ZKumuXyXSuJ/8Azbh8UFo6Z9E3wQ260yWe4TtD1bwRT7O/f0o1poG3jFpXJee0SWi3bKjaWtklZbHMbu4oo3buOyGQjsNbUABxxqcFVGbUIsjrNcbcdNHKXY3g5kckYAGVPWE9ysuzOnDIIhBFYIW0vuDt7aHG/JC6zvfRzyKljiLuXzQe3onJbIrPA8Wyw2ezPc4sZaGwbxzWyESEF0LnHtBwxdh8KKp9InxG12l8FN2Z5ywNFG7syOLLo0F2i2Fk6SRCz2ezz2GKfc7wRudNaI3UkkdIaiJ7Rm7yWitD2l7nMaGC84hgLnAAkm6HOJJAyqccEHs2PtV9lk3sVy9deGmRjZA28KEhrgReGhIPerN03tcpskMNsfGbYJHyvbGyJjo4nxMETJN01rb5Ic67iQHCqpd2uGh8itntnbDrTaX2p7GVLmOLMSwljWtoQTUtNzEV1KC59L5ppoJXWa02OSyMist6GIQ75mEDHFxEV7+MdH68MFrYXz2zZghDRI6O2WeKIMjia+6LNaTdvNaC72QauJOFarXWrpHGYZoobFBA60BgkfHJaHdlsjZaNZI8tYLzBgBgMF7dhdNWWSJrIrDCS1zX3zLaLzpmxvi3hbfoKtkf2R2e1lgKBl6MvtrLK2SK12KzQyPkufaRBee5oZfLS+F7iBeaMSPgtR0ymhfbrS6AsMLpSWFgozIVLRQYF1TzrVZNn7eiZZorNNY4rRuXSuY58s8ZBlul49U8A+w3OuS1FqlY57nMjEbHHBjXOcGcg55Lj3lBl2ZapIpWSRuayRpoHPaxzRe7Bc4Pa5tKOOJBpnorf0wfI7ZgkmnstpkFrDWyWVsXYabPISxzo42Yk0NKfd5KnWGZrJGukibKG1rG5z2NeKECroyHChIOB0W02nt1klnFngssdnj3olNySaQueGOjFTK91AA44DkgtvSvoq4DaVpNnpGI7G6B7SA0U3DZyGtP4Q6tRxovJ0VtErrJFDZLTZIbQ60SgsnEJkeHtgbCGCSJ/wB4PGmap9h2gYobRC1rbs7Yg46jdSCUXacSKY6La7H6RRwGJ5sNnfNEWuZLftEbqsN5rntZJde4EZkY0AQbDZmxLRadmFsEO8eLdJvKCMOAbZ4gO0aUAc52ANMVWLfYpYJHRTMLJWUvNdSuLQ4ZEjFrge9bOw7ejbA6CeyRWhhmdN23zRlr3sax1N08VFGDNeTbu0jarQ+YsawuEYDWkloEcbImgFxJOEYzQYtm20wSsmYGEtNQHtD2V/6mnNWrbW0pjs4/bZIzJO6GSBjYoWSCFm8vyO3TG3WvqwNDjU3SaUVM+Rz5FezaluNo3YeA3dQRQi7q2IOAJr943kHRNtbPps+SxiWymez2eBzoWlm+a+AyyWol12pqwx5nGh44+PortyG7YYG2rclpYyWL7BFMJXvncf47sW3mva2oypVaS2dLxI6aUWOFlotEckb5WyWgmkrLjyI3SFgJHLBaDZlqEM0coaHmJ7H3XEgOLHBwqWmuYCC2PtsllgtkkG7Y87TfFeMUMlIwyd4YBIxwAvNGQWq6ZG9NFIQ0OkstjkkutawOe+BjnOutAAJJ0CjZukDaTMms0c8c05tF1z5o7khDwaOie00o8ihXj23tT7TIJLjY2sZHGxjC4gNiYGMFXkudg3MlB4PmfIK69T/vEUyEUvzYqV8znyCuvU/7xbw3UtPFiD6GZkpKLMlJAIQhAIQhALBa/ZKzrBa/ZKD5P6f+8bXXISu8cFoaeJz5Bb7p97xtZ/NdQc8Foaad5KBgjMaeYTA00ORSBriBiPMJinccuRQMHXucP1TA072lIHXUZ8wmBppm0/ogmMcRnl8EhTIZjLnxSB17nBTJA0wFLqBngMznyQDroMuZSppqcymDr4BBIDxOfIJg6+AU7MQHtvAOaHNLwSWgioq0uGLQRhUZLorOitnLmltjmLWuc3+NGRIIy+N1TvRu3X3NqRgd0KAXig5vTTXjzUq6+K3TIYxb2R7gGNz4Ru336esZHePYkLh7ReBfN3AEkBbOx7GZFaJxaI4d0ySAOEgtT3Bk4kljEYs5reMTTUvyIbzQVMDTUZfBA5ZnyWy+ysZBLO4EhznR2ZpJBJBDpJDQ4iNhDaHAvlbndcFYmbIgE0MEscO+MzA8QC2NaWCOR0jXvldRxLt3QxkfeQUz5DzKlTxOa9+2WNpZ3iFsJkgvOY3eXbwtFoiqBI5zhVsTNfmvBTTxQAOo7/gnTTwSB1GnyTp4HLkgddfFeyx2EuFXGg0pmfoF5oWXnAcSAfgdVvwOC43a5p4Qu7E2dRlV1V3ftp7e8sAsUf4fN31WewdHN84XXbtoIvOILgByFak96at+z4bkbW8gT8TiVPyMmu3TyzxloczAxNP6dOI8Ru+GeydCLGwULHSHUve/HuYQB4Ly7T6A2d49QXROGQJc9h+IdVw+IPcVYtmSVZTgad2YXrWYnPy7d2Z1J3+eH66IdeJajlmmHDrdYnwyOilF17fa4ciDqDxVu6n/AHk0/lS0HKrF7es6wgxxTatdcceLXBzhX4Fpp/mK8XU/7yafypafCrFscHK9TYpud+/lDv2tK5NL6GZkpKLMlJexxCEIQCEIQCwWv2Ss6wWv2Sg+T+n3vG1n811PJaEDTTUrfdPveVr4711PJaLIYaZ96DYbH2d9ol3Qe1kjg7dA5PeB2IqkgNLzgCdaDVbax9FWvmliNpY1rHQRB4aXNdaZhhEKEdkPbIDJiKR1oahaDZ9pMMscrKFzHse29UjsODhWmlQthsfpBLZnSPjDHbwgkPaXBsjHXo5WCuD2Em6cRiag1QZ7J0dc4Qlz7rn/AGyou1u/ZI94Qe1jepTlzXs2n0VZDY2WgzEGSOJ7WuEIa4ybu8yOkpkLmCQONYwKA4iorr7B0hkiiEQjieWifdyPEhkaLQy5LduvDHEj8TXUJULVt6WWHcvDDHdhaBQ1YYG3Gvbjg8t7LjkRpgKBj2BYPtFphhL7m8e1jnXb1AfvXai98KraS9E3R2izwOkAMzpATd7MbY5Xxukz7bLkZkrhUeK02zLa+CZkzA0yRODm3gS004gEE+K2g6VzgwObcvwwOgiddN4McHNLjjQvDXFoNMtDmgz7N6LCR1pjmtDYDZ5Wxuc9hcHEma9ShwN2FxaPvEtbUVqpWno8yKOaWR85EdotFnaYoGvYDDuw0yPdI0Rl5koBj7JzWvte3pZDKX3CZXWd8jrpBL7O1zWkY0q68S7iThRZJtuuka4SwQSF808wLvtDSx1ouX7u7maKerbQOByQag5HgM+Z4K42jovaY5gx8s7Gutlns8cjmyNDhaN7elZV1DS7iATXeZiuNOIFKaDzK3I6QTb501I9460Q2ki667vIN5cHtVu+sdUVrliEG12Vsgy20QxS2yOW7K5zpI2w2m+GF4ArP7TwaVc5vta1Xog2fI2W1ubbbW7csgdI+zVlmfebiJbk931XaaTfcBQ5ZCts2mGEvhhihrHLGRGZyCJWljj62V5DgCaEU5gr1/4+8umMkMMpnEW9DxK0OdHjf9VI0h7nVc41oS4mmKDVzuqSA5zmVdcL86FxOIqQ0mpJAJFScTmvRJtSd90OnmNwhzb0sjrhGRZV3ZI4heV7gSSAGtcSQBeo2pyF4k0GWJJ4k5o+Pegy2m1SSOvyPfI6lAZHue6gy7TiTTE+KhTTQZqPzOXIKWXwGaCR89Pgl8j5FICmWf6Jnjoc0GWF91wP4SK8xqt8q98xlzC9ljt90AOxbodRy5hcbtE1cYXth7Roxqqrd3hTV39pbZXCwTB8bXDgK/EYFUgWtn4h5g+BXo2f0jELhRpkYSLza3fiQSM/n5idk49dynljjDRZmbi6f1akfid/w6bsyOjK8TXuyXrWgsnTGxPbXfBlNJGuaRyrS6e4lebanTmzRikVZn6AAtYObnuGXwBWXqwsq5dmNOd/j+9EKvKtTvqmqHi6zrY0RRQ1xc++R/0tDmjxLv8AxK8PU/7yFczFL4VYqntHaUlokdJK68ScvuigwAboAD/ypVt6n/eIrnupPmxbPAxfTWKbc9e/lCyLurcmp9CsyUlFmSkvY4hCEIBCEIBYbUOyVmUXCoQfKHWNDc2nagczJXuc1rh81WwfEZ8wvo/rC6t49oESNdupgKXwLzXNrUBzaitKmhBwrqufO6lrXXC0Q4a0f54IOZADIa+QTa7wyA4rpfoWtf8APhx5SfRMdTFrz38PLCT6IOaXdP7T+iYOvc4LpXoYtdKb+Hwkz8E/Q1a6138PPCTHyQc2A01GXMIFNMz5cV0n0M2v+fDywk+iZ6m7Xj66Gpzwk+iDm+HcPMqQHic+QXR/Q3a/50OGWEn0R6HLXT+PDjng/wCiDnLeOgy+qfLvBXRz1PWvCk0OHKTLwS9Dtrx9dDywkw8kHOa668OakMO7EldG9D1rz30Nfg/6I9D1rw9dDTXB+Pkg518ij45DzXRh1QWvWaHwk+iQ6n7XrNDhyf8ARBzz45nyTr4jP4LoZ6oLX/Ohx5P+ifohteHroa/B/wBEHOwNB3/BMHwyAXQ/RDa/50Pg/wCifoitf86Hlg/6IOe008FZobfs/dBr4GXrkbXPDZ7942U7x3t3bwtIZQ3aUJwoSt76IrXT+ND/AOf0R6I7X/Nh54P+iDwsteyd42rGXPXAUjtYc1hMG5vA3hLMGiYEgtaS72hgVCybT2a+4ZomEizwsutbaI6OYA2W+5gdV7qAsc2oHaqQSFsfRHa/50PLB/0TPVHa/wCbDjn7f0QaNtt2eWkGJoDdwKlk5c9oiBn3ZDqMnMt6jn0ZS7TCoPptNt2aKUijeS6MSbtlpYBFvZd4WCR9RJujEKk0vVpxWz9Elr/mw4ZYP+iB1SWv+dDz9v6IKt0itMLzCYTGWxxXCIo5omXjNPIS1stXUo9lSTmcMBhY+pqzOdby4CrWROq7hfc0NHfdd/aV6o+qO1Eis8TRxDXuw/ymlfFdQ6GdE4rBFu46kk1c53tOPPgOA/3KCzMyUkIQCEIQCEIQCEIQIhK4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4OCLg4KSEEbg4IuDgpIQRuDgi4OCkhBG4FJCEAhCEAhCEH/9k='  alt='first card'
                              alt="Third card"
                            />

                            <Carousel.Caption className="card-carousel-caption">
                              <p><strong>Global</strong></p>
                              <p>Available Balance - $40000</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
                </Card>
                <Card className="five">
                    <center><Card.Header as="h5" className="card-header-home">Forex Watchlist</Card.Header></center>
                    <Card.Body className="card-body-home">
                        <div className="card-body-form">
                            <button>+</button>
                            <button>My Offers</button>
                            <p>Edit</p>
                        </div>
                        <div className="card-div" style={{margin:'2%'}}>
                            <Table responsive>
                            <tbody>
                                <tr>
                                  <td><button>...</button></td>
                                  <td>GBP/USD</td>
                                  <td>152.43</td>
                                  <td>152.46</td>
                                  <td>1</td>
                                </tr>
                                <tr>
                                  <td><button>...</button></td>
                                  <td>GBP/USD</td>
                                  <td>152.43</td>
                                  <td>152.46</td>
                                  <td>1</td>
                                </tr>
                                <tr>
                                  <td><button>...</button></td>
                                  <td>GBP/USD</td>
                                  <td>152.43</td>
                                  <td>152.46</td>
                                  <td>1</td>
                                </tr>
                            </tbody>
                            </Table>
                        </div>
                    </Card.Body>
                </Card>
                <Card className="six">
                    <center><Card.Header as="h5" className="card-header-home">Current Deposits</Card.Header></center>
                    <Card.Body className="card-body-home">
                      <ul>
                        <div className="row-sixth-card">
                            <div className="row-column1">
                                <p className="c1-p1"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                <p className="c1-p2">balance 40000</p>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>11.4%</strong></p>
                                <img height='30' width='30' src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAH8AdwMBEQACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAQIFAwQGB//EADoQAAEDAgMFBAcHBAMAAAAAAAEAAgMEEQUSIQYTMUFRYZGhwRQiI1KB4fAHMkJxkqKxYtHS8SQ0Q//EABoBAQACAwEAAAAAAAAAAAAAAAADBAECBQb/xAAvEQEAAgECBAMGBwEBAAAAAAAAAQIDBBEFEiExE0FRMnGBseHwIiORocHR8WFT/9oADAMBAAIRAxEAPwD3FAIBAIBAIBAIBAIBAIBAIBAIETZADtQNAIEDdA0AgRPRABA0AgQN0DQCAQIDqgaAQCBAWQNAIEAgaAQBQLRAOcGglxsBxJ5INXDsRpMTpzUUEwmiD3MzgEAlpsbX468+C1raLRvCXNhvhty5I2n+22tkQQCAQCAQCAQCAQCAQUu0Um7ZCb2tmde9rWCHd5VtRtVPVl1JR1Eop7We7Ofadn5Klmzc3SOz0/DuG+D+Zlj8Xp6fX5fL037P6U0myWHscLOc0vcOhJJKs4o5aRDha7J4upvb/vy6OiUiqEAgEAgV9UDQCAQCAQCDjvtGqIYMKmNQfVMJa1oNiXE6W7lHmmIpO65w/HlyaisYp2n19I83k2FYPVYmWuiYDFnAc4m1xfWyp0xTeN4ej1XEMWntyW77f5+r3/CIxFhlMwC1owe/VdB5D3ttAjxQNAIETy5oABA0AgRQNAIKd+Nxsacws4E8roPLdssWk2px6GipD7CL1AQNC7XM74cP9qlknxL8sdnptFjrotJObJ3n7iPj99nTYbQx0lMyGNtmtbYK5WOWNoedy5LZbze/eXoMTckTGe60BZRpoBAIBAIBAIBAIESBx0QQfNGwEue0W7UHl22GLehUhjifaaYWbbkOZUObJyV6d3R4bpPHy729mvf+lZslhW5dBUzN9rK10gv+FugA+Oa/ctcFOWN57yl4tq/Fy+FWfw1+btqWPNPE3q8DxVhyXZIBAIBAroC6AugLoC6BXQaONTbmgfIHWLSCD0sboOMp9qxW0k0++ytic4PzBulufDotK5ItG6zn0mTDkjHaOs7fv9eji6Vsm0mOOmmH/HjNy0jTLfRqq1jxcm89nczWjh+kjHX2rfcz/Dt4o7VY/pi/kj/FXXmlthbc1fCOjr9wQdUgEESeiAHagEAgV0BdAFwHEoKXaDGocJi38hBjDTf1ufRa2tFY3lJhxXzXjHSOsuD2i20ZW4IHU0e7lnu0DPct5G/1zUOTL+XvHm6mj4faNZOO/avX3+jhtzWRBsIDx6QQMg/H0BVaa2p09XYpm0+otNv/ADn7l6HsphrabDIgQMzm53njdxV7HTkrs8zrNTOoyzfy8vctxHlq5XX0yMA+GY+a3VVrgTb19/dYT9d6DoUAgEAgV0CQJBCSTI0usSgo8Txx1JvXu3bImC5LweixMxEby2pS17RWsbzLy/GcVrNq8QMcTRHTxDgOnU9qpzNs1tvJ6OtcXDMHNPW8/f6QrKDDHvxllPKwkMe3OL6Zb6d6xXFPics+STPraxpPGr7Vunx+nf8AR6HHRQAMe+JhdH67SRwNuPiVd2iXl62msTET3bNAwxUdOG8omjwCywlBd0tQSf8A0AHwa35oLzAG+1md0aB3n5ILm6CSBEoFdAri9uaDXqqplK3PLcNvyQchtdtxTYfTugobvrHaAHhH2lQZcvL0r3dTQcOnP+Zk6U+f35uc2c2skmjdBiVQ5srNRI95GYdvaFjDl36W7t+I6CMcxlwxvWfTy+isxfEqrabEW0dDmFK22Z552/EfILS1pzW5a9lzT4sfDsPjZfbny/j+5dDhGFw0UjoY2aNiZqedy7U9ys1rFI2hws+e+e83vPVvS0kLJ4XtjaHvkALgNSAHEeaztG+6PmnlivlDZqfUpZXe7G4+Cy1ZY25WNb0ACCFN9x56yv8ABxHkgv8AAhaGR3V1u4fNBZgoJE2QUGJ7WUmHPfG+CaSVhIyR2Juo75IqtafSZM/bpHrPb79zlaT7Rqk4g81dJBHSPtlsTmi/M8x8Aoa555vxR0dLPwqkYt8MzNo/f3ei4r8fDnxPkmjgiMbnFwfYGxbbX4lWZmIjeXGrjve3LWN5cxje0/pLPR6CSZ78wIkPC46A8VBfNv0o6un4ZNZ59R0j0/v0hrYbgHs5KqsOedwJAdrb5rOPDy9Z7o9dxCcv5ePpX5/QsT2YFayKaBrA4tbmB4Ht+CZcPN1hnQ8RnBHJfrHku8IwmDD4d3ELn8burualpSKRtClqdTfUX5r/AON+KN3pszg023Uf8vWyulOPbU3ZKT+xyB1gvSSt95pb36IM54oMNH/1Y3e8M/6tfNB0GE2bSjtcT9dyCwBQYazO6F7WOcwuaQHN4t7QjMTtO7z+DZx1ISKoyVTw4nNydrzubk9eunRQ0wxXu6Gp4jfLP4I5Y++3p8Gtiez5qxK6OB7JHatIHA9D2LbJji0ItJrL6e2/eJ7/AH6qik2Vr3OaKpsjWDgAbkflfgoa4Jn2pdLLxald4w1+Pb/XQUmAQU7W5KVwcHtdmcbk2IPkp60rXs5ObV5s3t26enktRE8gjdkaW4rdXOKJ0cTGZT6rQPBA4Ynxhwyk5nF3eUGVjS2QvyG5a1tvyv8A3QQdC58kb7fcJPeLeaBywukYW6jUHuN/JBkDHXvZAU9K9sUcd75Wht+tkFvRs3UYBNyg3Wu0QZXtuEGu+na48EGP0Zo5BAjAOiCDqcdEEDTjogiacIImAdEC3AQLchA9yEDEIQZGx2QZ2NQZWoG+vo2PLH1MTXDi0uAKhtqcNbctrRE+9jdk3jDwcpmSzs95Ai5nVBEub18EESWdfBBH1evggRy/QQKw+ggg50bSA5wBOgB5rE2iO8h2ae1ZDAH0EDGX6CCYLevggkHN6+CCtrsNrJquUwshdHM4HO51sug4i2vDRcrPps1sluWI2n9u3l8OjXbq2X4fOZHEbkguJ1j5fqXVbCOhqWWvuS4fdO64fuQWERmbGBK3O/m5oAB+F0Ew5xIvGQO0hBkQCAQCCEsTJWlsjQ4HkVrasWjaRJrQ1oa0WAWYiI6QGsjQNPIamd+6BDrWO9Lb6dihpWYyWtt3GZgmjaAyNhHPNKTbvCmGVjpCfaMa0dQ6/kg//9k='/>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>Gains</strong></p>
                                <button>Deposits</button>
                                <button>Withdraw</button>
                            </div>
                            <div>
                                <p className="c2-p1">2.5% Fixed</p>
                                <p>1 Month</p>
                            </div>
                        </div>
                        <div className="row-sixth-card">
                            <div className="row-column1">
                                <p className="c1-p1"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                <p className="c1-p2">balance 40000</p>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>11.4%</strong></p>
                                <img height='30' width='30' src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAH8AdwMBEQACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAQIFAwQGB//EADoQAAEDAgMFBAcHBAMAAAAAAAEAAgMEEQUSIQYTMUFRYZGhwRQiI1KB4fAHMkJxkqKxYtHS8SQ0Q//EABoBAQACAwEAAAAAAAAAAAAAAAADBAECBQb/xAAvEQEAAgECBAMGBwEBAAAAAAAAAQIDBBEFEiExE0FRMnGBseHwIiORocHR8WFT/9oADAMBAAIRAxEAPwD3FAIBAIBAIBAIBAIBAIBAIBAIETZADtQNAIEDdA0AgRPRABA0AgQN0DQCAQIDqgaAQCBAWQNAIEAgaAQBQLRAOcGglxsBxJ5INXDsRpMTpzUUEwmiD3MzgEAlpsbX468+C1raLRvCXNhvhty5I2n+22tkQQCAQCAQCAQCAQCAQUu0Um7ZCb2tmde9rWCHd5VtRtVPVl1JR1Eop7We7Ofadn5Klmzc3SOz0/DuG+D+Zlj8Xp6fX5fL037P6U0myWHscLOc0vcOhJJKs4o5aRDha7J4upvb/vy6OiUiqEAgEAgV9UDQCAQCAQCDjvtGqIYMKmNQfVMJa1oNiXE6W7lHmmIpO65w/HlyaisYp2n19I83k2FYPVYmWuiYDFnAc4m1xfWyp0xTeN4ej1XEMWntyW77f5+r3/CIxFhlMwC1owe/VdB5D3ttAjxQNAIETy5oABA0AgRQNAIKd+Nxsacws4E8roPLdssWk2px6GipD7CL1AQNC7XM74cP9qlknxL8sdnptFjrotJObJ3n7iPj99nTYbQx0lMyGNtmtbYK5WOWNoedy5LZbze/eXoMTckTGe60BZRpoBAIBAIBAIBAIESBx0QQfNGwEue0W7UHl22GLehUhjifaaYWbbkOZUObJyV6d3R4bpPHy729mvf+lZslhW5dBUzN9rK10gv+FugA+Oa/ctcFOWN57yl4tq/Fy+FWfw1+btqWPNPE3q8DxVhyXZIBAIBAroC6AugLoC6BXQaONTbmgfIHWLSCD0sboOMp9qxW0k0++ytic4PzBulufDotK5ItG6zn0mTDkjHaOs7fv9eji6Vsm0mOOmmH/HjNy0jTLfRqq1jxcm89nczWjh+kjHX2rfcz/Dt4o7VY/pi/kj/FXXmlthbc1fCOjr9wQdUgEESeiAHagEAgV0BdAFwHEoKXaDGocJi38hBjDTf1ufRa2tFY3lJhxXzXjHSOsuD2i20ZW4IHU0e7lnu0DPct5G/1zUOTL+XvHm6mj4faNZOO/avX3+jhtzWRBsIDx6QQMg/H0BVaa2p09XYpm0+otNv/ADn7l6HsphrabDIgQMzm53njdxV7HTkrs8zrNTOoyzfy8vctxHlq5XX0yMA+GY+a3VVrgTb19/dYT9d6DoUAgEAgV0CQJBCSTI0usSgo8Txx1JvXu3bImC5LweixMxEby2pS17RWsbzLy/GcVrNq8QMcTRHTxDgOnU9qpzNs1tvJ6OtcXDMHNPW8/f6QrKDDHvxllPKwkMe3OL6Zb6d6xXFPics+STPraxpPGr7Vunx+nf8AR6HHRQAMe+JhdH67SRwNuPiVd2iXl62msTET3bNAwxUdOG8omjwCywlBd0tQSf8A0AHwa35oLzAG+1md0aB3n5ILm6CSBEoFdAri9uaDXqqplK3PLcNvyQchtdtxTYfTugobvrHaAHhH2lQZcvL0r3dTQcOnP+Zk6U+f35uc2c2skmjdBiVQ5srNRI95GYdvaFjDl36W7t+I6CMcxlwxvWfTy+isxfEqrabEW0dDmFK22Z552/EfILS1pzW5a9lzT4sfDsPjZfbny/j+5dDhGFw0UjoY2aNiZqedy7U9ys1rFI2hws+e+e83vPVvS0kLJ4XtjaHvkALgNSAHEeaztG+6PmnlivlDZqfUpZXe7G4+Cy1ZY25WNb0ACCFN9x56yv8ABxHkgv8AAhaGR3V1u4fNBZgoJE2QUGJ7WUmHPfG+CaSVhIyR2Juo75IqtafSZM/bpHrPb79zlaT7Rqk4g81dJBHSPtlsTmi/M8x8Aoa555vxR0dLPwqkYt8MzNo/f3ei4r8fDnxPkmjgiMbnFwfYGxbbX4lWZmIjeXGrjve3LWN5cxje0/pLPR6CSZ78wIkPC46A8VBfNv0o6un4ZNZ59R0j0/v0hrYbgHs5KqsOedwJAdrb5rOPDy9Z7o9dxCcv5ePpX5/QsT2YFayKaBrA4tbmB4Ht+CZcPN1hnQ8RnBHJfrHku8IwmDD4d3ELn8burualpSKRtClqdTfUX5r/AON+KN3pszg023Uf8vWyulOPbU3ZKT+xyB1gvSSt95pb36IM54oMNH/1Y3e8M/6tfNB0GE2bSjtcT9dyCwBQYazO6F7WOcwuaQHN4t7QjMTtO7z+DZx1ISKoyVTw4nNydrzubk9eunRQ0wxXu6Gp4jfLP4I5Y++3p8Gtiez5qxK6OB7JHatIHA9D2LbJji0ItJrL6e2/eJ7/AH6qik2Vr3OaKpsjWDgAbkflfgoa4Jn2pdLLxald4w1+Pb/XQUmAQU7W5KVwcHtdmcbk2IPkp60rXs5ObV5s3t26enktRE8gjdkaW4rdXOKJ0cTGZT6rQPBA4Ynxhwyk5nF3eUGVjS2QvyG5a1tvyv8A3QQdC58kb7fcJPeLeaBywukYW6jUHuN/JBkDHXvZAU9K9sUcd75Wht+tkFvRs3UYBNyg3Wu0QZXtuEGu+na48EGP0Zo5BAjAOiCDqcdEEDTjogiacIImAdEC3AQLchA9yEDEIQZGx2QZ2NQZWoG+vo2PLH1MTXDi0uAKhtqcNbctrRE+9jdk3jDwcpmSzs95Ai5nVBEub18EESWdfBBH1evggRy/QQKw+ggg50bSA5wBOgB5rE2iO8h2ae1ZDAH0EDGX6CCYLevggkHN6+CCtrsNrJquUwshdHM4HO51sug4i2vDRcrPps1sluWI2n9u3l8OjXbq2X4fOZHEbkguJ1j5fqXVbCOhqWWvuS4fdO64fuQWERmbGBK3O/m5oAB+F0Ew5xIvGQO0hBkQCAQCCEsTJWlsjQ4HkVrasWjaRJrQ1oa0WAWYiI6QGsjQNPIamd+6BDrWO9Lb6dihpWYyWtt3GZgmjaAyNhHPNKTbvCmGVjpCfaMa0dQ6/kg//9k='/>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>Gains</strong></p>
                                <button>Deposits</button>
                                <button>Withdraw</button>
                            </div>
                            <div>
                                <p className="c2-p1">2.5% Fixed</p>
                                <p>1 Month</p>
                            </div>
                        </div>
                        <div className="row-sixth-card">
                            <div className="row-column1">
                                <p className="c1-p1"><img src={logo}/> <strong>USD</strong> United States Dollars</p>
                                <p className="c1-p2">balance 40000</p>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>11.4%</strong></p>
                                <img height='30' width='30' src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAH8AdwMBEQACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAAAQIFAwQGB//EADoQAAEDAgMFBAcHBAMAAAAAAAEAAgMEEQUSIQYTMUFRYZGhwRQiI1KB4fAHMkJxkqKxYtHS8SQ0Q//EABoBAQACAwEAAAAAAAAAAAAAAAADBAECBQb/xAAvEQEAAgECBAMGBwEBAAAAAAAAAQIDBBEFEiExE0FRMnGBseHwIiORocHR8WFT/9oADAMBAAIRAxEAPwD3FAIBAIBAIBAIBAIBAIBAIBAIETZADtQNAIEDdA0AgRPRABA0AgQN0DQCAQIDqgaAQCBAWQNAIEAgaAQBQLRAOcGglxsBxJ5INXDsRpMTpzUUEwmiD3MzgEAlpsbX468+C1raLRvCXNhvhty5I2n+22tkQQCAQCAQCAQCAQCAQUu0Um7ZCb2tmde9rWCHd5VtRtVPVl1JR1Eop7We7Ofadn5Klmzc3SOz0/DuG+D+Zlj8Xp6fX5fL037P6U0myWHscLOc0vcOhJJKs4o5aRDha7J4upvb/vy6OiUiqEAgEAgV9UDQCAQCAQCDjvtGqIYMKmNQfVMJa1oNiXE6W7lHmmIpO65w/HlyaisYp2n19I83k2FYPVYmWuiYDFnAc4m1xfWyp0xTeN4ej1XEMWntyW77f5+r3/CIxFhlMwC1owe/VdB5D3ttAjxQNAIETy5oABA0AgRQNAIKd+Nxsacws4E8roPLdssWk2px6GipD7CL1AQNC7XM74cP9qlknxL8sdnptFjrotJObJ3n7iPj99nTYbQx0lMyGNtmtbYK5WOWNoedy5LZbze/eXoMTckTGe60BZRpoBAIBAIBAIBAIESBx0QQfNGwEue0W7UHl22GLehUhjifaaYWbbkOZUObJyV6d3R4bpPHy729mvf+lZslhW5dBUzN9rK10gv+FugA+Oa/ctcFOWN57yl4tq/Fy+FWfw1+btqWPNPE3q8DxVhyXZIBAIBAroC6AugLoC6BXQaONTbmgfIHWLSCD0sboOMp9qxW0k0++ytic4PzBulufDotK5ItG6zn0mTDkjHaOs7fv9eji6Vsm0mOOmmH/HjNy0jTLfRqq1jxcm89nczWjh+kjHX2rfcz/Dt4o7VY/pi/kj/FXXmlthbc1fCOjr9wQdUgEESeiAHagEAgV0BdAFwHEoKXaDGocJi38hBjDTf1ufRa2tFY3lJhxXzXjHSOsuD2i20ZW4IHU0e7lnu0DPct5G/1zUOTL+XvHm6mj4faNZOO/avX3+jhtzWRBsIDx6QQMg/H0BVaa2p09XYpm0+otNv/ADn7l6HsphrabDIgQMzm53njdxV7HTkrs8zrNTOoyzfy8vctxHlq5XX0yMA+GY+a3VVrgTb19/dYT9d6DoUAgEAgV0CQJBCSTI0usSgo8Txx1JvXu3bImC5LweixMxEby2pS17RWsbzLy/GcVrNq8QMcTRHTxDgOnU9qpzNs1tvJ6OtcXDMHNPW8/f6QrKDDHvxllPKwkMe3OL6Zb6d6xXFPics+STPraxpPGr7Vunx+nf8AR6HHRQAMe+JhdH67SRwNuPiVd2iXl62msTET3bNAwxUdOG8omjwCywlBd0tQSf8A0AHwa35oLzAG+1md0aB3n5ILm6CSBEoFdAri9uaDXqqplK3PLcNvyQchtdtxTYfTugobvrHaAHhH2lQZcvL0r3dTQcOnP+Zk6U+f35uc2c2skmjdBiVQ5srNRI95GYdvaFjDl36W7t+I6CMcxlwxvWfTy+isxfEqrabEW0dDmFK22Z552/EfILS1pzW5a9lzT4sfDsPjZfbny/j+5dDhGFw0UjoY2aNiZqedy7U9ys1rFI2hws+e+e83vPVvS0kLJ4XtjaHvkALgNSAHEeaztG+6PmnlivlDZqfUpZXe7G4+Cy1ZY25WNb0ACCFN9x56yv8ABxHkgv8AAhaGR3V1u4fNBZgoJE2QUGJ7WUmHPfG+CaSVhIyR2Juo75IqtafSZM/bpHrPb79zlaT7Rqk4g81dJBHSPtlsTmi/M8x8Aoa555vxR0dLPwqkYt8MzNo/f3ei4r8fDnxPkmjgiMbnFwfYGxbbX4lWZmIjeXGrjve3LWN5cxje0/pLPR6CSZ78wIkPC46A8VBfNv0o6un4ZNZ59R0j0/v0hrYbgHs5KqsOedwJAdrb5rOPDy9Z7o9dxCcv5ePpX5/QsT2YFayKaBrA4tbmB4Ht+CZcPN1hnQ8RnBHJfrHku8IwmDD4d3ELn8burualpSKRtClqdTfUX5r/AON+KN3pszg023Uf8vWyulOPbU3ZKT+xyB1gvSSt95pb36IM54oMNH/1Y3e8M/6tfNB0GE2bSjtcT9dyCwBQYazO6F7WOcwuaQHN4t7QjMTtO7z+DZx1ISKoyVTw4nNydrzubk9eunRQ0wxXu6Gp4jfLP4I5Y++3p8Gtiez5qxK6OB7JHatIHA9D2LbJji0ItJrL6e2/eJ7/AH6qik2Vr3OaKpsjWDgAbkflfgoa4Jn2pdLLxald4w1+Pb/XQUmAQU7W5KVwcHtdmcbk2IPkp60rXs5ObV5s3t26enktRE8gjdkaW4rdXOKJ0cTGZT6rQPBA4Ynxhwyk5nF3eUGVjS2QvyG5a1tvyv8A3QQdC58kb7fcJPeLeaBywukYW6jUHuN/JBkDHXvZAU9K9sUcd75Wht+tkFvRs3UYBNyg3Wu0QZXtuEGu+na48EGP0Zo5BAjAOiCDqcdEEDTjogiacIImAdEC3AQLchA9yEDEIQZGx2QZ2NQZWoG+vo2PLH1MTXDi0uAKhtqcNbctrRE+9jdk3jDwcpmSzs95Ai5nVBEub18EESWdfBBH1evggRy/QQKw+ggg50bSA5wBOgB5rE2iO8h2ae1ZDAH0EDGX6CCYLevggkHN6+CCtrsNrJquUwshdHM4HO51sug4i2vDRcrPps1sluWI2n9u3l8OjXbq2X4fOZHEbkguJ1j5fqXVbCOhqWWvuS4fdO64fuQWERmbGBK3O/m5oAB+F0Ew5xIvGQO0hBkQCAQCCEsTJWlsjQ4HkVrasWjaRJrQ1oa0WAWYiI6QGsjQNPIamd+6BDrWO9Lb6dihpWYyWtt3GZgmjaAyNhHPNKTbvCmGVjpCfaMa0dQ6/kg//9k='/>
                            </div>
                            <div>
                                <p className="c2-p1"><strong>Gains</strong></p>
                                <button>Deposits</button>
                                <button>Withdraw</button>
                            </div>
                            <div>
                                <p className="c2-p1">2.5% Fixed</p>
                                <p>1 Month</p>
                            </div>
                        </div>
                        
                      </ul>
                    </Card.Body>
                </Card>
            </Masonry>
           
            {/* <Breadcrumb title="Sterling Payment Services" parent="Dashboard" /> */}
            {/*<div className="container-fluid ">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header span">
                                <h5>My Account</h5>
                            </div>
                            <div className="card-body datatable-react pt-0 sudata-table">
                                <button onClick={this.gotoTransferForm} disabled={this.state.isDisabled} className="btn btn-primary float-right" type="button" >Transfer</button>
                                <DataTable
                                    columns={columns}
                                    data={userDetails}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>*/}
        </Fragment>
        )
    }
}
export default MyAccount;