import React, { Component, Fragment } from 'react';
import DataTable from 'react-data-table-component';



var columns,axz=1;


class UserMtMessage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetailsStatic: [
                {id: 1,suSuType: "test1",suCrDate: "19/02/2020",suSuMsg: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"},
                {id: 2,suSuType: "test3",suCrDate: "18/02/2020",suSuMsg: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"},
                {id: 3,suSuType: "test2",suCrDate: "20/02/2020",suSuMsg: "Lorem ipsum dolor sit amet, consectetur adipisicing elit"}
            ],
            msgBtn: false
        }
    }
    shrinkExpandFn=(event) => {
        if(axz)
        {
            event.currentTarget.parentNode.classList.add('activeMsg');
            event.currentTarget.text= 'Show Less';
            axz=0;
        }
        else
        {
            event.currentTarget.parentNode.classList.remove('activeMsg');
            event.currentTarget.text= 'Show More';
            axz=1;
        }
    }
    componentDidMount () {
       
    }

    gotoTransferForm = () => {
        this.props.history.push('./transfer-form');
    }

    render() {
        columns = [
            {
                name: 'Type',
                selector: "suSuType",
                maxWidth: '20%',
                sortable: true,
                cell: row => 
                <span >
                    {row.suSuType ? <span>{row.suSuType}</span> :<span>-</span> }
                </span>
            },{
                name: 'Created At',
                selector: "suCrDate",
                maxWidth: '20%',
                sortable: true,
                cell: row => 
                <span>
                    {row.suCrDate ? <span>{row.suCrDate}</span> :<span>-</span> }
                </span>
            },
            {
                name: 'Message',
                selector: "suSuMsg",
                sortable: true,
                maxWidth: '60%',
                cell: row => 
                <span >
                    {row.suSuMsg ? <span className='mt-messageWrap'><span >{row.suSuMsg}</span><span onClick={this.shrinkExpandFn}><span>Show More</span><span>Show Less</span></span></span> :<span>-</span> }
                </span>
            }
        ]
        return(
        <Fragment>
            <div className="container-fluid ">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header span">
                                <h5>MT Message</h5>
                            </div>
                            <div className="card-body datatable-react sudata-table">
                                <DataTable
                                    // title=""
                                    columns={columns} 
                                    data={this.state.userDetailsStatic} 
                                    defaultSortField={"suCrDate" }
                                    defaultSortAsc = {true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}

export default UserMtMessage;