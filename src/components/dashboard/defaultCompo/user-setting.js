import React, { Component, Fragment } from 'react';
import axios from 'axios';


import { ToastContainer, toast } from 'react-toastify';
import {updatePassword} from '../../../constant/actionTypes'

class UserSettings extends Component {
    constructor(props) {
        super(props);
        // this.handleUpdate = this.handleUpdate.bind(this);
        this.state ={
            isDisabled: true,
            oldpassword: '',
            newpassword: '',
            confirmpassword: '',
            newPassState: false,
            oldPassState: false,
            confPassState: false,            
            notMatMsg: '',
            passLength: false,
            passNo: false,
            passSpcl: false,
            passUper: false,
            passLwr: false,
            oPassLength: false,
            oPassNo: false,
            oPassSpcl: false,
            oPassUper: false,
            oPassLwr: false,
        }
    }
    UNSAFE_componentWillMount() {        
    }
    componentDidMount() {        
    }  
    resetFormFieldVals=(event)=>{
        document.querySelectorAll('form input').forEach(function(userItem) {
            userItem.value='';
        })
    }
    handleUpdateBtnState=()=>{
        const {confPassState,newPassState,oldPassState,oldpassword,newpassword,confirmpassword} = this.state;
        // console.log(this.state)
        if(confPassState && newPassState && oldPassState && oldpassword != '' && newpassword != '' && confirmpassword != ''){
            this.setState({
                isDisabled: false
            })
        }
        else{
            this.setState({
                isDisabled: true
            })
        }
    }
    handlePasswordState=(inputNo)=>{
        const {passLength,passNo,passSpcl,passUper,passLwr,
            oPassLength,oPassNo,oPassSpcl,oPassUper,oPassLwr,
            confirmpassword,newpassword} = this.state;
        if(inputNo == 1){
            if(oPassLength && oPassNo && oPassSpcl && oPassUper && oPassLwr){
                this.setState({oldPassState : true},()=>{this.handleUpdateBtnState()});
            }
            else{
                this.setState({oldPassState : false},()=>{this.handleUpdateBtnState()});
            }
        }
        else if( inputNo == 2){
            //new password
            if(passLength && passNo && passSpcl && passUper && passLwr){
                this.setState({newPassState : true},()=>{this.handleUpdateBtnState()});
            }
            else{
                this.setState({newPassState : false},()=>{this.handleUpdateBtnState()});
            }
            //confirm pass
            if(confirmpassword !== ''){
                if(confirmpassword === newpassword){
                    this.setState({confPassState : true,notMatMsg: ''},()=>{this.handleUpdateBtnState()})
                }
                else{
                    this.setState({confPassState : false,notMatMsg: 'Password Not Matched'},()=>{this.handleUpdateBtnState()})
                }
            }
            else{
                this.setState({confPassState : false,notMatMsg: ''},()=>{this.handleUpdateBtnState()})
            }
        }
        else{
            //do nothing
        }
    }
    handlePasswordChange = (val,no) => {
        let newVal=val;
        newVal=newVal != null? newVal.toString() : newVal;
        // let regex='';
        if(no == 1)
        {
            this.setState({oPassLength: false,oPassNo: false,oPassSpcl: false,oPassUper: false,oPassLwr: false,},
                ()=>{
                        if((/[0-9]+/.test(newVal)))
                        {
                            //check one digit
                            this.setState({oPassNo: true},()=>{this.handlePasswordState(1)});
                        }
                        if((/[a-z]+/.test(newVal)))
                        {
                            //check one lower
                            this.setState({oPassLwr: true,},()=>{this.handlePasswordState(1)});
                        }
                        if((/[A-Z]+/.test(newVal)))
                        {
                            //check one upper
                            this.setState({oPassUper: true,},()=>{this.handlePasswordState(1)});
                        }
                        if(newVal)
                        {
                            if(newVal.length >= 8 && newVal.length <= 20){
                                this.setState({oPassLength: true,},()=>{this.handlePasswordState(1)});
                            }
                        }
                        if((/[@#$%^&+=]+/.test(newVal)))
                        {
                            this.setState({oPassSpcl: true,},()=>{this.handlePasswordState(1)});
                        }
                });            
            
        }
        if(no == 2){
            this.setState({passLength: false,passNo: false,passSpcl: false,passUper: false,passLwr: false,},
                ()=>{
                    if((/[0-9]+/.test(newVal)))
                        {
                            //check one digit
                            this.setState({passNo: true},()=>{this.handlePasswordState(2)});
                        }
                        if((/[a-z]+/.test(newVal)))
                        {
                            //check one lower
                            this.setState({passLwr: true,},()=>{this.handlePasswordState(2)});
                        }
                        if((/[A-Z]+/.test(newVal)))
                        {
                            //check one upper
                            this.setState({passUper: true,},()=>{this.handlePasswordState(2)});
                        }
                        if(newVal)
                        {
                            if(newVal.length >= 8 && newVal.length <= 20){
                                this.setState({passLength: true,},()=>{this.handlePasswordState(2)});
                            }
                        }
                        if((/[@#$%^&+=]+/.test(newVal)))
                        {
                            this.setState({passSpcl: true,},()=>{this.handlePasswordState(2)});
                        }
                });
        }
    };
    handlePasswordMatch = (val) => {
        const {newpassword} = this.state;
        if(newpassword === val){
            this.setState({confPassState : true,notMatMsg: ''},()=>{this.handleUpdateBtnState()});
        }
        else{
            this.setState({confPassState : false,notMatMsg: 'Password Not Matched'},()=>{this.handleUpdateBtnState()})
        }
    }
    detectChngedField=(fieldname,fieldvalue)=>{
        if(fieldname == 'oldpassword'){
            this.handlePasswordChange(fieldvalue,1);
        }
        else if(fieldname == 'newpassword'){
            this.handlePasswordChange(fieldvalue,2);
        }
        else if(fieldname == 'confirmpassword'){
            this.handlePasswordMatch(fieldvalue);
        }
        else{
            //do nothing
        }
        // console.log(this.state)
    }
    handleChange=(event)=>{
        let fieldname=event.target.name;
        let fieldvalue=event.target.value;
        
        this.setState({[fieldname]: fieldvalue},
            ()=>{
                // console.log(this.state)
                this.detectChngedField(fieldname,fieldvalue)
            }
        );  
    }
    handlePasswordUpdate=(event)=>{
        event.preventDefault();
        const {oldpassword,newpassword,confirmpassword}=this.state;
        let userdetail=localStorage.getItem('user');
        userdetail=JSON.parse(userdetail);
        // console.log(userdetail)
        let userId=userdetail.id;
        event.target.classList.add('spin');
        var $this=this,$target = event.target,$event=event;
        axios.post(updatePassword,{
            "id": userId,
            "old_password": oldpassword,
            "new_password": newpassword,
            "confirm_password": confirmpassword
        })
        .then(res=>{
            console.log(res)
            toast.success(res.data.message)
            $target.classList.remove('spin');
            $this.setState({
                isDisabled: true,
                oldpassword: '',
                newpassword: '',
                confirmpassword: '',
                newPassState: false,
                oldPassState: false,
                confPassState: false,            
                notMatMsg: '',
                passLength: false,
                passNo: false,
                passSpcl: false,
                passUper: false,
                passLwr: false,
                oPassLength: false,
                oPassNo: false,
                oPassSpcl: false,
                oPassUper: false,
                oPassLwr: false,
            })
            $this.resetFormFieldVals($event);
        })
        .catch(err=>{
            console.log(err)
            toast.error('fail!')
            $target.classList.remove('spin');
            $this.setState({
                isDisabled: true,
                oldpassword: '',
                newpassword: '',
                confirmpassword: '',
                newPassState: false,
                oldPassState: false,
                confPassState: false,            
                notMatMsg: '',
                passLength: false,
                passNo: false,
                passSpcl: false,
                passUper: false,
                passLwr: false,
                oPassLength: false,
                oPassNo: false,
                oPassSpcl: false,
                oPassUper: false,
                oPassLwr: false,
            })
            $this.resetFormFieldVals($event);
        })
    }

    render() {
        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Setting</h5>
                                </div>
                                <div className="card-body">
                                    <form className="form-horizontal custom-form" >
                                        <div className="row">
                                            <div className="col mx-351">
                                                <div className=" form-group relative">
                                                    <label className="cs-label" htmlFor="oldpassword">Old Password&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                                    <input className="form-control" id="oldpassword" type="password" autoComplete="off" name="oldpassword" onChange={event => this.handleChange(event)} onCut={event => this.handleChange(event)} />
                                                    <ul className="passwordError">
                                                        <li className={this.state.oPassLength == true? 'text-success' : ''}>Min 8 ,Max 20 characters</li>
                                                        <li className={this.state.oPassUper == true? 'text-success' : ''}>At least one uppercase character </li>
                                                        <li className={this.state.oPassLwr == true? 'text-success' : ''}>At least one lowercase character</li>
                                                        <li className={this.state.oPassNo == true? 'text-success' : ''}>At least one digit </li>
                                                        <li className={this.state.oPassSpcl == true? 'text-success' : ''}>At least one special character</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="col mx-351"> 
                                                <div className="form-group relative">
                                                    <label className="cs-label" htmlFor="newpassword">New Password&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                                    <input className="form-control" id="newpassword" type="password" autoComplete="off" name="newpassword" onChange={event => this.handleChange(event)} onCut={event => this.handleChange(event)}/>
                                                    <ul className="passwordError">
                                                        <li className={this.state.passLength == true? 'text-success' : ''}>Min 8 ,Max 20 characters</li>
                                                        <li className={this.state.passUper == true? 'text-success' : ''}>At least one uppercase character </li>
                                                        <li className={this.state.passLwr == true? 'text-success' : ''}>At least one lowercase character</li>
                                                        <li className={this.state.passNo == true? 'text-success' : ''}>At least one digit </li>
                                                        <li className={this.state.passSpcl == true? 'text-success' : ''}>At least one special character</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="col mx-351"> 
                                                <div className=" form-group">
                                                    <label className="cs-label" htmlFor="confirmpassword">Confirm Password&nbsp;<span style={{ "color": "#e4566e" }}>*</span></label>
                                                    <input className="form-control" id="confirmpassword" autoComplete="off" type="password" name="confirmpassword" onChange={event => this.handleChange(event)} onCut={event => this.handleChange(event)}/>
                                                    <small style={{ "color": "#e4566e" }}>{this.state.notMatMsg}</small>
                                                </div>
                                            </div>
                                            <div className="col-12"> 
                                                <div className=" form-group">
                                                    <button disabled={this.state.isDisabled} type="button" className="btn btn-primary loader-btn" onClick={this.handlePasswordUpdate}><i className="fa fa-spinner fa-spin"></i> Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default UserSettings;