import React, { Component } from 'react';

import InternalForm from './reusableComponents/InternalForm';
import ExternalForm from './reusableComponents/ExternalForm';
import LoaderBackdrop from './reusableComponents/LoaderBackdrop';
import { connect } from 'react-redux';
import './styles/Transfer.css';

export class Transfer extends Component {
  state = {
    transferType: 'external',
    account_number: '',
    istransferring: true,
  };

  changeTransferType = (type) => {
    this.setState({ transferType: type });
  };
  handleChange = (date, type) => {
    type === 'startDate'
      ? this.setState({
          startDate: date,
        })
      : this.setState({
          endDate: date,
        });
  };

  render() {
    return (
      <div>
        {/* <div className="transfer-card-container"> */}
        {this.state.transferType === 'internal' ? (
          this.props.transferLoader ? (
            <LoaderBackdrop message="Transferring the amount.Please wait.." />
          ) : (
            <InternalForm
              account_number={this.props.accountNumber}
              handleChange={this.handleChange}
              changeTransferType={this.changeTransferType}
              currentCurrency={this.props.currentCurrency}
              accountId={this.props.accountId}
            />
          )
        ) : this.props.transferLoader ? (
          <LoaderBackdrop message="Transferring the amount.Please wait.." />
        ) : (
          <ExternalForm
            account_number={this.props.accountNumber}
            handleChange={this.handleChange}
            changeTransferType={this.changeTransferType}
            allBeneficiaries={this.props.allBeneficiaries}
            accountId={this.props.accountId}
            currentCurrency={this.props.currentCurrency}
            accountHolderName={this.props.accountHolderName}
            accountHolderAddress={this.props.accountHolderAddress}
          />
        )}
        {/* </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  transferLoader: state.loadingReducer.transferLoader,
});

export default connect(mapStateToProps, {})(Transfer);
