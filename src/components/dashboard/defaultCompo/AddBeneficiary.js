import React, { Component } from 'react';
// import Header from '../components/reusableComponents/Header';
import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
// import { getAccounts } from '../actions/AccountsActions';
import { addBeneficiary } from '../../../actions/BeneficiaryActions/Beneficiary';
import { OnGetAccounts } from '../../../actions';
import Header from '../../common/header-component/header';
import Footer from '../../..//components/common/footer';
import { Link } from 'react-router-dom';
import '../defaultCompo/styles/AddBeneficiary.css';

export class AddBeneficiary extends Component {
  state = {
    countries: {
      AF: 'AFGHANISTAN',
      AL: 'ALBANIA',
      DZ: 'ALGERIA',
      AU: 'AUSTRALIA ',
      IN: 'INDIA',
      US: 'UNITED STATES',
      GB: 'UNITED KINGDOM',
    },
    required: '* Required',
    currencyId: '',
    beneficiaryName: '',
    beneficiaryType: 'corporate',
    beneficiaryFirstName: '',
    beneficiaryLastName: '',
    beneficiaryBirthDate: '',
    beneficiaryCompanyName: '',
    beneficiaryCountry: '',
    beneficiaryState: '',
    beneficiaryPostalCode: '',
    beneficiaryCity: '',
    beneficiaryAddressLine: '',
    beneficiaryAccountNumber: '',
    iban: '',
    swift: '',
    bic: '',
    currency: '',
    bankName: '',
    bankAccountCountry: '',
    bankAccountHolderName: '',
    bankAddress: '',
    isSepa: false,
    isNonSepa: true,
    chaps: false,
    aba: '',
    sort_code: '',
    isChaps: false,
    is_retail: false,
    beneficiaryAddressLine1: '',
    beneficiaryAddressLine2: '',
  };
  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onToggleSepa = (type) => {
    if (type === 'sepa') {
      this.setState({
        isSepa: true,
        isNonSepa: false,
        currency: 'EUR',
        chaps: false,
      });
    } else if (type === 'swift') {
      this.setState({
        isSepa: false,
        isNonSepa: true,
        currency: '',
        chaps: false,
      });
    } else {
      this.setState({
        isSepa: false,
        isNonSepa: false,
        currency: '',
        chaps: true,
      });
    }
  };

  onDropdownValueChange = (type, id) => {
    const {
      countries,
      bankAccountCountry,

      currency,
    } = this.state;
    // const { currencyAccount } = this.props;
    var val = document.getElementById(id).value;
    if (type === 'countries' || type === 'bankAccountCountries') {
      for (let key in countries) {
        if (countries.hasOwnProperty(key)) {
          if (countries[key] === val)
            type === 'countries'
              ? this.setState({ beneficiaryCountry: key })
              : this.setState({ bankAccountCountry: key }, () => {
                  if (currency === 'GBP' && key === 'GB') this.setChaps(true);
                  else {
                    this.setChaps(false);
                  }
                });
        }
      }
    }
    // else if (type === 'currency') {
    //   for (let i in currencyAccount) {
    //     if (currencyAccount[i].currency === val) {
    //       this.setState({ currency: val }, () => {
    //         if (val === 'GBP' && bankAccountCountry === 'GB')
    //           this.setChaps(true);
    //         else {
    //           this.setChaps(false);
    //         }
    //       });
    //     }
    //   }
    // }
    else if (type === 'beneficiaryType') {
      if (val === 'retail') {
        this.setState({
          is_retail: true,
          beneficiaryType: 'retail',
          beneficiaryCompanyName: '',
        });
      } else {
        this.setState({
          is_retail: false,
          beneficiaryType: 'corporate',
          beneficiaryFirstName: '',
          beneficiaryLastName: '',
        });
      }
    }
  };

  setChaps = (value) => {
    this.setState({ isChaps: value, aba: '', sort_code: '' });
  };

  onButtonCLick = () => {
    const {
      beneficiaryName,
      beneficiaryType,
      beneficiaryFirstName,
      beneficiaryLastName,
      beneficiaryBirthDate,
      beneficiaryCompanyName,
      beneficiaryCountry,
      beneficiaryState,
      beneficiaryPostalCode,
      beneficiaryCity,
      beneficiaryAddressLine,
      beneficiaryAccountNumber,
      iban,
      bic,
      currency,
      bankName,
      bankAccountCountry,
      bankAccountHolderName,
      bankAddress,
      isSepa,
      swift,
      aba,
      isChaps,
      sort_code,
      is_retail,
      beneficiaryAddressLine1,
      beneficiaryAddressLine2,
    } = this.state;
    let data = isSepa
      ? {
          acc_id: '5',
          beneficiaryName,
          beneficiaryType,
          beneficiaryFirstName,
          beneficiaryLastName,
          beneficiaryBirthDate,
          beneficiaryCompanyName,
          // beneficiaryCountry,
          // beneficiaryState,
          beneficiaryPostalCode,
          beneficiaryCity,
          beneficiaryAddressLine,
          beneficiaryAccountNumber,
          iban,
          bic,
          currency,
          bankName,
          bankAccountCountry,
          bankAccountHolderName,
          bankAddress,
          isSepa,
          is_retail,
        }
      : {
          acc_id: this.props.location.state.currencyAccountId,
          beneficiaryName,
          // beneficiaryType,
          // beneficiaryFirstName,
          // beneficiaryLastName,
          // beneficiaryBirthDate,
          // beneficiaryCompanyName,
          beneficiaryCountry,
          // beneficiaryPostalCode,
          // beneficiaryCity,
          // beneficiaryAddressLine,
          beneficiaryAccountNumber,
          // aba,
          swiftcode: bic,
          // currency,
          // bankName,
          // bankAccountCountry,
          // bankAccountHolderName,
          // bankAddress,
          beneficiary_client_address_line_1: beneficiaryAddressLine1,
          beneficiary_client_address_line_2: beneficiaryAddressLine2,
          // isSepa,
          // is_chaps: isChaps,
          // sort_code,
          // is_retail,
        };

    if (!beneficiaryName) {
      alert('Recipient Name required');
    }
    // else if (!beneficiaryType) {
    //   alert('Beneficiar Type required');
    // }
    // else if (!beneficiaryCountry) {
    //   alert('Beneficiary country required');
    // }
    // else if (!beneficiaryPostalCode) {
    //   alert('Beneficiary Postal Code required');
    // }
    // else if (!beneficiaryCity) {
    //   alert('Beneficiary City required');
    // }
    // else if (!beneficiaryAddressLine) {
    //   alert('Beneficiary address required');
    // }
    // else if (!currency) {
    //   alert('Currency required');
    // }
    else if (!beneficiaryAddressLine1) {
      alert('Recipient Address line 1 required');
    } else if (!beneficiaryAddressLine2) {
      alert('Recipient Address line 2 required');
    } else {
      this.props.addBeneficiary(
        data,
        localStorage.getItem('access'),
        this.props.history
      );
    }
    // else if (!bankAccountCountry) {
    //   alert('Bank Account country required');
    // }
    // else if (!bankAccountHolderName) {
    //   alert('Bank Account Holder name required');
    // }

    // if (isSepa) {
    //   if (is_retail) {
    //     if (!beneficiaryFirstName) {
    //       alert('Beneficiary First name required');
    //     } else if (!beneficiaryLastName) {
    //       alert('Beneficiary Last name required');
    //     } else if (!iban) {
    //       alert('iban name required');
    //     } else {
    //       //   this.props.addBeneficiary(
    //       //     data,
    //       //     localStorage.getItem('access'),
    //       //     this.props.history,
    //       //     this.props.location.state.currency
    //       //   );
    //     }
    //   } else {
    //     if (!beneficiaryCompanyName) {
    //       alert('Beneficiary Company name required');
    //     } else if (!iban) {
    //       alert('iban name required');
    //     } else {
    //       //   this.props.addBeneficiary(
    //       //     data,
    //       //     localStorage.getItem('access'),
    //       //     this.props.history,
    //       //     this.props.location.state.currency
    //       //   );
    //     }
    //   }
    // }
    // else {
    //   if (!bic) {
    //     alert('Bic name required');
    //   }
    //   if (is_retail) {
    //     if (isChaps) {
    //       if (!sort_code) {
    //         alert('Sort code required');
    //       } else if (!beneficiaryAccountNumber) {
    //         alert('Beneficiary Account number required');
    //       } else if (sort_code.length > 6 || sort_code.length < 6) {
    //         alert('Sort code should be a 6 digit code');
    //       } else if (!beneficiaryFirstName) {
    //         alert('Beneficiary First name required');
    //       } else if (!beneficiaryLastName) {
    //         alert('Beneficiary Last name required');
    //       } else {
    //         // this.props.addBeneficiary(
    //         //   data,
    //         //   localStorage.getItem('access'),
    //         //   this.props.history,
    //         //   this.props.location.state.currency
    //         // );
    //       }
    //     } else if (!isChaps) {
    //       if (!aba) {
    //         alert('aba code required');
    //       } else if (!beneficiaryAccountNumber) {
    //         alert('Beneficiary Account number required');
    //       } else if (aba.length > 9 || aba.length < 9) {
    //         alert('aba code should be a 9 digit code');
    //       } else if (!beneficiaryFirstName) {
    //         alert('Beneficiary First name required');
    //       } else if (!beneficiaryLastName) {
    //         alert('Beneficiary Last name required');
    //       } else {
    //         // this.props.addBeneficiary(
    //         //   data,
    //         //   localStorage.getItem('access'),
    //         //   this.props.history,
    //         //   this.props.location.state.currency
    //         // );
    //       }
    //     }
    //   } else {
    //     if (isChaps) {
    //       if (!sort_code) {
    //         alert('Sort code required');
    //       } else if (!beneficiaryAccountNumber) {
    //         alert('Beneficiary Account number required');
    //       } else if (sort_code.length > 6 || sort_code.length < 6) {
    //         alert('Sort code should be a 6 digit code');
    //       } else if (!beneficiaryCompanyName) {
    //         alert('Beneficiary Last name required');
    //       } else {
    //         // this.props.addBeneficiary(
    //         //   data,
    //         //   localStorage.getItem('access'),
    //         //   this.props.history,
    //         //   this.props.location.state.currency
    //         // );
    //       }
    //     } else if (!isChaps) {
    //       if (!aba) {
    //         alert('aba code required');
    //       } else if (!beneficiaryAccountNumber) {
    //         alert('Beneficiary Account number required');
    //       } else if (aba.length > 9 || aba.length < 9) {
    //         alert('aba code should be a 9 digit code');
    //       } else if (!beneficiaryCompanyName) {
    //         alert('Beneficiary Last name required');
    //       } else {
    //         // this.props.addBeneficiary(
    //         //   data,
    //         //   localStorage.getItem('access'),
    //         //   this.props.history,
    //         //   this.props.location.state.currency
    //         // );
    //       }
    //     }
    //   }

    //   // else if (!beneficiaryCompanyName) {
    //   //   alert('Beneficiary company name required');
    //   // }
    // }
  };

  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });
      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        // this.props.OnGetAccounts();
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (
  //     this.state.currency === 'GBP' &&
  //     this.state.bankAccountCountry === 'GB'
  //   ) {
  //     this.setState({ isChaps: true });
  //   }
  // }
  render() {
    const {
      beneficiaryName,
      beneficiaryType,
      beneficiaryFirstName,
      beneficiaryLastName,
      beneficiaryBirthDate,
      beneficiaryCompanyName,
      beneficiaryCountry,
      beneficiaryState,
      beneficiaryPostalCode,
      beneficiaryCity,
      beneficiaryAddressLine,
      beneficiaryAccountNumber,
      iban,
      bic,
      currency,
      bankName,
      bankAccountCountry,
      bankAccountHolderName,
      bankAddress,
      countries,
      isSepa,
      isNonSepa,
      chaps,
      required,
      swift,
      aba,
      isChaps,
      sort_code,
      is_retail,
      beneficiaryAddressLine1,
      beneficiaryAddressLine2,
    } = this.state;
    const { accountData } = this.props;
    const currencies = [];
    for (let i in accountData) {
      currencies.push(accountData[i].currency);
    }
    const country = Object.values(countries);
    const beneficiaryTypeDropdown = ['corporate', 'retail'];

    return (
      <div style={{ background: '#FFF' }}>
        <Header />

        <div className="add-beneficiary-container">
          <div className="choose-sepa-type-button-div">
            <div className="go-back-home">
              <Link to="/home">Go Back to Home</Link>
            </div>
            <div className="toggle-button-container">
              {/* <button
                onClick={() => this.onToggleSepa('sepa')}
                className={
                  isSepa ? 'transfer-type-active' : 'transfer-type-inactive'
                }
              >
                Sepa
              </button> */}
              <button
                onClick={() => this.onToggleSepa('swift')}
                className={
                  isNonSepa ? 'transfer-type-active' : 'transfer-type-inactive'
                }
              >
                Swift
              </button>
            </div>
            {/* <button
              onClick={() => this.onToggleSepa('chaps')}
              className={
                chaps ? 'transfer-type-active' : 'transfer-type-inactive'
              }
            >
              Chaps
            </button> */}
          </div>
          <p className="Add-Beneficiary-Form-title-text">
            Add Recipient for {this.props.location.state.currency}
            {/* {this.props.location.state.currency} */}
          </p>
          <div className="beneficiary-text-box">Recipient Details</div>
          <hr />
          <section className="beneficiary-details-section">
            {/* <div className="add-beneficiary-fields-div">
              <label>Beneficiary Type</label>
              <input
                id="beneficiaryType"
                className="signup-input"
                onChange={() => {
                  this.onDropdownValueChange(
                    'beneficiaryType',
                    'beneficiaryType'
                  );
                }}
                // name="beneficiaryType"
                // value={beneficiaryType}
                list="beneficiaryTypelist"
              />
              <datalist id="beneficiaryTypelist">
                {beneficiaryTypeDropdown.map((item, key) => (
                  <option key={key} value={item} />
                ))}
              </datalist>
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            <div className="add-beneficiary-fields-div">
              <label>Recipient Name</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryName"
                value={beneficiaryName}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            <div className="add-beneficiary-fields-div">
              <label>Swift Code</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="bic"
                value={bic}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            {!isSepa && (
              <div className="add-beneficiary-fields-div">
                <label>Recipient Account Number</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryAccountNumber"
                  value={beneficiaryAccountNumber}
                />
                <span style={{ color: 'red' }}>{required}</span>
              </div>
            )}

            {/* {is_retail && (
              <>
                <div className="add-beneficiary-fields-div">
                  <label>Beneficiary First Name</label>
                  <input
                    className="signup-input"
                    onChange={this.onInputChange}
                    name="beneficiaryFirstName"
                    value={beneficiaryFirstName}
                  />
                </div>
                <div className="add-beneficiary-fields-div">
                  <label>Beneficiary Last Name</label>
                  <input
                    className="signup-input"
                    onChange={this.onInputChange}
                    name="beneficiaryLastName"
                    value={beneficiaryLastName}
                    required
                  />
                </div>
              </>
            )} */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Birth Date</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryBirthDate"
                value={beneficiaryBirthDate}
                type="date"
              />
            </div> */}
            {/* {!is_retail && (
              <div className="add-beneficiary-fields-div">
                <label>Company Name</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name="beneficiaryCompanyName"
                  value={beneficiaryCompanyName}
                />
                <span style={{ color: 'red' }}>{required}</span>
              </div>
            )} */}
            <div className="add-beneficiary-fields-div">
              <label>Beneficiary Country</label>
              <input
                id="countryListInput"
                className="signup-input"
                onChange={(e) =>
                  this.onDropdownValueChange('countries', 'countryListInput', e)
                }
                list="countryList"
              />
              <datalist id="countryList">
                {country.map((item, key) => (
                  <option key={key} value={item} />
                ))}
              </datalist>
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            {/* <div className="add-beneficiary-fields-div">
              <label>Beneficiary State</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryState"
                value={beneficiaryState}
              />
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Postal Code</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryPostalCode"
                value={beneficiaryPostalCode}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>City</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryCity"
                value={beneficiaryCity}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Address Line</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryAddressLine"
                value={beneficiaryAddressLine}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            <div className="add-beneficiary-fields-div">
              <label>Recipient Address Line 1</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryAddressLine1"
                value={beneficiaryAddressLine1}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            <div className="add-beneficiary-fields-div">
              <label>Recipient Address Line 2</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="beneficiaryAddressLine2"
                value={beneficiaryAddressLine2}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>

            {/* {isSepa && (
              <div className="add-beneficiary-fields-div">
                <label>IBAN</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name={'iban'}
                  value={iban}
                />
                <span style={{ color: 'red' }}>{required}</span>
              </div>
            )} */}

            {/* <div className="add-beneficiary-fields-div">
              <label>Currency </label>
              <input
                id="currencyListInput"
                className="signup-input"
                value={isSepa ? 'EUR' : currency}
                name={isSepa ? 'EUR' : 'currency'}
                readOnly={isSepa && true}
                onChange={(e) => {
                  this.onInputChange(e);
                  this.onDropdownValueChange('currency', 'currencyListInput');
                }}
                list="currencyList"
              />
              {isNonSepa && (
                <datalist id="currencyList">
                  {currencies.map((item, key) => (
                    <option key={key} value={item} />
                  ))}
                </datalist>
              )}
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Bank Name</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="bankName"
                value={bankName}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Bank Account Country</label>
              <input
                id="bankAccountCountryListInput"
                className="signup-input"
                onChange={() =>
                  this.onDropdownValueChange(
                    'bankAccountCountries',
                    'bankAccountCountryListInput'
                  )
                }
                list="bankAccountCountryList"
              />
              <datalist id="bankAccountCountryList">
                {country.map((item, key) => (
                  <option key={key} value={item} />
                ))}
              </datalist>
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* {isNonSepa && (
              <div className="add-beneficiary-fields-div">
                <label>{isChaps ? 'Sort Code' : 'ABA Code'}</label>
                <input
                  className="signup-input"
                  onChange={this.onInputChange}
                  name={isChaps ? 'sort_code' : 'aba'}
                  value={isChaps ? sort_code : aba}
                />
                <span style={{ color: 'red' }}>{required}</span>
              </div>
            )} */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Bank Account Holder Name</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="bankAccountHolderName"
                value={bankAccountHolderName}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            {/* <div className="add-beneficiary-fields-div">
              <label>Bank Address</label>
              <input
                className="signup-input"
                onChange={this.onInputChange}
                name="bankAddress"
                value={bankAddress}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div> */}
            <div></div>
            <div></div>
            <div className="add-beneficiary-fields-div">
              {this.state.isChaps && (
                <span
                  style={{
                    color: '#6A5497',
                    fontWeight: '600',
                    fontSize: '12px',
                  }}
                >
                  Beneficiary will be added as Chap's
                </span>
              )}
            </div>
          </section>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <button
              className="add-beneficiary-button"
              onClick={this.onButtonCLick}
            >
              Add Recipient
            </button>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  beneficiaryMessage: state.beneficiary.beneficiaryMessage,
  accountData: state.getAccounts.accountData,
});

export default connect(mapStateToProps, { OnGetAccounts, addBeneficiary })(
  AddBeneficiary
);
