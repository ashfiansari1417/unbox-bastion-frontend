import React, { Component } from 'react';
import Header from '../../common/header-component/header';
import ReactTable from 'react-table';
import { Tab, Row, Col, Nav } from 'react-bootstrap';
import {
  getBeneficiaries,
  deleteRecipient,
} from '../../../actions/BeneficiaryActions/Beneficiary';
import { OnGetAccounts } from '../../../actions';

import { connect } from 'react-redux';
import jwt_decode from 'jwt-decode';
import './styles/CustomLoader.css';

export class AddressBook extends Component {
  state = {
    eurBenef: [],
    usdBenef: [],
    gbpBenef: [],
    rubBenef: [],
    currentAccountId: '',
    eurCurrencyId: '',
    usdCurrencyId: '',
    gbpCurrencyId: '',
    rubCurrencyId: '',
    isloading: false,
  };
  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });

      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        this.props.OnGetAccounts(localStorage.getItem('access'));

        // this.props.getExchangeRates(localStorage.getItem('access'));
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.accountData !== this.props.accountData) {
      const { accountData } = this.props;
      for (let i in accountData) {
        if (accountData[i].currency === 'EUR') {
          this.setState({
            eurCurrencyId: accountData[i].account_id,
            currentAccountId: accountData[i].account_id,
          });
        } else if (accountData[i].currency === 'USD') {
          this.setState({ usdCurrencyId: accountData[i].account_id });
        } else if (accountData[i].currency === 'RUB') {
          this.setState({ rubCurrencyId: accountData[i].account_id });
        } else {
          this.setState({ gbpCurrencyId: accountData[i].account_id });
        }
      }
    }
    if (prevState.currentAccountId !== this.state.currentAccountId) {
      // getbenefApicall
      this.props.getBeneficiaries(
        this.state.currentAccountId,
        localStorage.getItem('access')
      );
    }
    if (prevProps.allBeneficiaries !== this.props.allBeneficiaries) {
      let data = [];
      for (let i in this.props.allBeneficiaries) {
        data.push(this.props.allBeneficiaries[i]);
      }
      if (this.state.currentAccountId === this.state.eurCurrencyId) {
        // data.push(this.props.allBeneficiaries);
        this.setState({ eurBenef: data });
      } else if (this.state.currentAccountId === this.state.usdCurrencyId) {
        this.setState({ usdBenef: data });
      } else if (this.state.currentAccountId === this.state.gbpCurrencyId) {
        this.setState({ gbpBenef: data });
      } else {
        this.setState({ rubBenef: data });
      }
    }
  }
  // changeCurrency = () => {

  // }
  render() {
    const {
      eurBenef,
      usdBenef,
      gbpBenef,
      rubBenef,
      eurCurrencyId,
      usdCurrencyId,
      gbpCurrencyId,
      rubCurrencyId,
      isloading,
    } = this.state;

    var beneficiaryColumn = [
      {
        Header: (
          <b>
            Name <i className="fa fa-sort"></i>
          </b>
        ),
        // width: 120,
        accessor: 'beneficiary_client_name',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Account Number <i className="fa fa-sort"></i>
          </b>
        ),

        // width: 120,
        accessor: 'beneficiary_client_account',
        style: {
          textAlign: 'center',
        },
      },

      {
        Header: <b>Swift Code</b>,
        // width: 120,
        accessor: 'swift_code',
        style: {
          textAlign: 'center',
        },
      },
    ];
    let deleteAction = {
      Header: <b>Action</b>,
      id: 'delete',
      accessor: (str) => 'delete',
      width: 150,
      Cell: (row) => (
        <div>
          <span
            title="Delete"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              this.props.deleteRecipient(
                {
                  beneficiary_id: row.original.id,
                  acc_id: this.state.currentAccountId,
                },
                localStorage.getItem('access')
              );
            }}
          >
            <i
              className="fa fa-trash-o"
              style={{ width: 35, fontSize: 18, padding: 11 }}
            ></i>
          </span>
        </div>
      ),
      style: {
        textAlign: 'center',
      },
      sortable: false,
    };
    return (
      <div>
        <Header />
        <div
          className="col-sm-12 mx-auto table--body "
          style={{ marginTop: '46px' }}
        >
          {isloading ? (
            <div style={{ height: '100vh' }}>
              <div className="customLoader-ring" id="spinner-id">
                <div></div>
                <div></div>
              </div>
            </div>
          ) : (
            <Tab.Container id="left-tabs-example" defaultActiveKey="EUR">
              <Row>
                <Col>
                  <Nav
                    variant="pills"
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      marginBottom: 20,
                      marginTop: -50,
                    }}
                  >
                    <Nav.Item
                      onClick={() => {
                        this.setState({ currentAccountId: eurCurrencyId });
                      }}
                    >
                      <Nav.Link eventKey="EUR">EUR</Nav.Link>
                    </Nav.Item>
                    <Nav.Item
                      onClick={() => {
                        this.setState({ currentAccountId: usdCurrencyId });
                      }}
                    >
                      <Nav.Link eventKey="USD">USD</Nav.Link>
                    </Nav.Item>
                    <Nav.Item
                      onClick={() => {
                        this.setState({ currentAccountId: gbpCurrencyId });
                      }}
                    >
                      <Nav.Link eventKey="GBP">GBP</Nav.Link>
                    </Nav.Item>
                    <Nav.Item
                      onClick={() => {
                        this.setState({ currentAccountId: rubCurrencyId });
                      }}
                    >
                      <Nav.Link eventKey="RUB">RUB</Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
                <Col sm={12}>
                  <Tab.Content>
                    <Tab.Pane eventKey="EUR">
                      <div className="card-body datatable-react pt-0 ">
                        <ReactTable
                          data={eurBenef}
                          columns={[
                            ...beneficiaryColumn,
                            deleteAction,
                            // undercomplianceActions,
                          ]}
                          defaultPageSize={10}
                        />
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="USD">
                      <div className="card-body datatable-react pt-0 ">
                        <ReactTable
                          data={usdBenef}
                          columns={[
                            ...beneficiaryColumn,
                            deleteAction,
                            // undercomplianceActions,
                          ]}
                          defaultPageSize={10}
                        />
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="GBP">
                      <div className="card-body datatable-react pt-0 ">
                        <ReactTable
                          data={gbpBenef}
                          columns={[
                            ...beneficiaryColumn,
                            deleteAction,
                            // undercomplianceActions,
                          ]}
                          defaultPageSize={10}
                        />
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="RUB">
                      <div className="card-body datatable-react pt-0 ">
                        <ReactTable
                          data={rubBenef}
                          columns={[
                            ...beneficiaryColumn,
                            deleteAction,
                            // undercomplianceActions,
                          ]}
                          defaultPageSize={10}
                        />
                      </div>
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  // const { getAccounts, transactions } = state;
  // return { getAccounts, transactions };
  accountData: state.getAccounts.accountData,
  allBeneficiaries: state.beneficiary.allBeneficiaries,
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToProps, {
  // getBeneficiaries,
  OnGetAccounts,
  getBeneficiaries,
  deleteRecipient,
})(AddressBook);
