import React, { Component, Fragment } from 'react';
import { Tab, Row, Col, Nav } from 'react-bootstrap';
import ReactTable from 'react-table';

import DataTable from 'react-data-table-component';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../../../assets/scss/Components/allTransaction.scss';
import printIcon from '../../../assets/images/payment-history/payment-history.png';
import eurIcon from '../../../assets/images/HomePage/eur.png';
import gbpIcon from '../../../assets/images/gbp.png';
import usaIcon from '../../../assets/images/usaIcon.png';
import russiaIcon from '../../../assets/images/russiaIcon.png';
import pageIcon from '../../../assets/images/HomePage/page.png';
import sepaIcon from '../../../assets/images/sepaIcon.png';
import swiftIcon from '../../../assets/images/payment-history/swifticon.png';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Header from '../../common/header-component/header';
import Footer from '../../..//components/common/footer';
import { connect } from 'react-redux';
import { OnGetAccounts } from '../../../actions';
import {
  getTransactions,
  getExchangeRates,
  onExchangeCurrency,
} from '../../../actions/Transaction/transactions';
import { getBeneficiaries } from '../../../actions/BeneficiaryActions/Beneficiary';
import '../../../assets/scss/Components/home.scss';

import './styles/Home.css';
import './styles/CustomLoader.css';

import ExhangeForm from './reusableComponents/ExchangeForm';
import Transfer from './Transfer';
import { Link } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

var columns;

class DefaultCompo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '5',
      comanyName: '',
      // userDetails: {},

      startDate: '',
      endDate: '',
      account_number: null,
      transactionTab: true,
      transferTab: false,
      exchangeTab: false,
      internal: true,
      external: false,

      transferType: 'external',
      selectedCurrency: 'EUR',
      selectedAccount: 'Business',
      accountCardsData: {
        Business: [],
        Merchant: [],
      },
      accounts: [
        { label: 'Business Accounts', active: true, account: 'Business' },
      ],
      transactionDetails: {},
      externalTransaction: [],
      internalTransaction: [],
      exchangeTransaction: [],
      currentCurrency: {},
      toCurrency: '',
      currentExchangeRate: '',
      exchangeToAccount: '',
      toEURCommission: '',
      transactionType: 'External',
    };
  }

  handleTab = (e, type) => {
    if (type === 'transactions') {
      this.setState({
        transactionTab: true,
        transferTab: false,
        exchangeTab: false,
      });
    } else if (type === 'transfer') {
      this.setState({
        transactionTab: false,
        transferTab: true,
        exchangeTab: false,
      });
    } else if (type === 'exchange') {
      this.setState({
        transactionTab: false,
        transferTab: false,
        exchangeTab: true,
      });
    }
  };
  componentDidUpdate(prevProps, prevState) {
    var decode = jwt_decode(localStorage.getItem('access'));
    if (localStorage.getItem('access')) {
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        if (prevProps.accountData !== this.props.accountData) {
          const { accountData } = this.props;
          const data = [];

          for (let d in accountData) {
            if (d === '0') {
              data.push({
                countryCurrency: accountData[d].currency,
                totalBalance: accountData[d].total_balance,
                availableBalance: accountData[d].available_balance,
                blockedBalance: accountData[d].blocked_balance,
                icon: eurIcon,
                active: true,
                currencyIcon: '€',
                account_id: accountData[d].account_id,
                account_holder_name: accountData[d].account_holder_name,
                account_holder_address: accountData[d].account_holder_address,
                account_no: accountData[d].account_no,
              });

              this.setState({
                currentCurrency: {
                  countryCurrency: accountData[d].currency,
                  totalBalance: accountData[d].total_balance,
                  availableBalance: accountData[d].available_balance,
                  blockedBalance: accountData[d].blocked_balance,
                  icon: eurIcon,
                  active: true,
                  currencyIcon: '€',
                  account_id: accountData[d].account_id,
                  account_holder_name: accountData[d].account_holder_name,
                  account_holder_address: accountData[d].account_holder_address,
                  account_no: accountData[d].account_no,
                },
              });
            } else {
              data.push({
                countryCurrency: accountData[d].currency,
                totalBalance: accountData[d].total_balance,
                availableBalance: accountData[d].available_balance,
                blockedBalance: accountData[d].blocked_balance,
                icon: eurIcon,
                active: false,
                currencyIcon: '€',
                account_id: accountData[d].account_id,
                account_holder_name: accountData[d].account_holder_name,
                account_holder_address: accountData[d].account_holder_address,
                account_no: accountData[d].account_no,
              });
            }
          }

          const accountCardsData = {
            Business: data,
            Merchant: data,
          };
          this.setState({ accountCardsData });
        }

        if (this.props.transactionData !== prevProps.transactionData) {
          const { transactionData } = this.props;
          const externalTransaction = [];
          const exchangeTransaction = [];
          const internalTransaction = [];
          for (let i in transactionData.alltransaction['external']) {
            externalTransaction.push(
              transactionData.alltransaction['external'][i]
            );
          }
          for (let i in transactionData.alltransaction['exchange']) {
            exchangeTransaction.push(
              transactionData.alltransaction['exchange'][i]
            );
          }
          for (let i in transactionData.alltransaction['internal']) {
            internalTransaction.push(
              transactionData.alltransaction['internal'][i]
            );
          }
          // console.log(transactionData.alltransaction['external']);
          this.setState({
            externalTransaction: externalTransaction,
            internalTransaction: internalTransaction,
            exchangeTransaction,
          });
        }
        if (prevProps.exchangeRate !== this.props.exchangeRate) {
          this.setState({ exchangeRateInfo: this.props.exchangeRate });
        }
        if (prevState.currentCurrency !== this.state.currentCurrency) {
          this.props.getBeneficiaries(
            this.state.currentCurrency.account_id,
            localStorage.getItem('access')
          );
          this.props.getTransactions(
            this.state.currentCurrency.account_id,
            localStorage.getItem('access')
          );
        }
      }
    }
  }

  componentDidMount() {
    if (localStorage.getItem('access')) {
      var decode = jwt_decode(localStorage.getItem('access'));
      this.setState({ token: localStorage.getItem('access') });

      //checking if the token is expired or not
      if (Date.now() > decode.exp * 1000) {
        //if token is expired push to login page
        localStorage.removeItem('access');
        this.props.history.push('/login', {
          message: 'Token expired please login again',
        });
      } else {
        this.props.OnGetAccounts(localStorage.getItem('access'));
        this.props.getExchangeRates(localStorage.getItem('access'));
      }
    } else {
      this.props.history.push('/login', {
        message: 'No token present, please login',
      });
    }
  }

  changeCurrency = (index) => {
    const cards = this.state.accountCardsData;
    const currentCard = {};
    const selectedCurrency =
      cards[this.state.selectedAccount][index].countryCurrency;
    cards[this.state.selectedAccount].map((el) => (el.active = false));
    cards[this.state.selectedAccount][index].active = true;
    currentCard['active'] = cards[this.state.selectedAccount][index].active;
    currentCard['account_id'] =
      cards[this.state.selectedAccount][index].account_id;
    currentCard['countryCurrency'] =
      cards[this.state.selectedAccount][index].countryCurrency;
    currentCard['totalBalance'] =
      cards[this.state.selectedAccount][index].totalBalance;
    currentCard['availableBalance'] =
      cards[this.state.selectedAccount][index].availableBalance;
    currentCard['blockedBalance'] =
      cards[this.state.selectedAccount][index].blockedBalance;
    currentCard['id'] = cards[this.state.selectedAccount][index].id;
    currentCard['account_holder_name'] =
      cards[this.state.selectedAccount][index].account_holder_name;
    currentCard['account_holder_address'] =
      cards[this.state.selectedAccount][index].account_holder_address;
    currentCard['account_no'] =
      cards[this.state.selectedAccount][index].account_no;
    this.setState({
      accountCardsData: cards,
      selectedCurrency,
      currentCurrency: currentCard,
    });
  };

  accountCards = (accountCardsData) => {
    return accountCardsData.map((item, index) => {
      return (
        <React.Fragment>
          <div className="row">
            <div
              style={{ height: 130 }}
              onClick={() => this.changeCurrency(index)}
              className="card"
            >
              <div
                className={`card-body text-center ${
                  item.active ? 'current-card' : ''
                }`}
                style={{
                  background: item.active && '#f3e3ff',
                  cursor: 'pointer',
                }}
              >
                <div className="row">
                  <div className="col-md-6 ">
                    <div className="row ">Total Funds</div>
                    <div className="row pt-2">
                      <img
                        src={
                          item.countryCurrency === 'EUR'
                            ? eurIcon
                            : item.countryCurrency === 'USD'
                            ? usaIcon
                            : item.countryCurrency === 'RUB'
                            ? russiaIcon
                            : gbpIcon
                        }
                      />
                      <span className="pl-2">{item.countryCurrency}</span>
                    </div>
                  </div>
                  <div className="col-md-6 ">
                    <div className="row pull-right">
                      <img src={pageIcon} />
                    </div>
                    <div className="row pt-4 pull-right">
                      <img src={item.sepaIcon ? sepaIcon : swiftIcon} />
                      <span className="pl-1">
                        {item.sepaIcon ? '' : 'Swift'}
                      </span>
                    </div>
                  </div>
                </div>
                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span style={{ textAlign: 'left' }}>Total Balance</span>
                    <span style={{ textAlign: 'right' }}>
                      {item.totalBalance.toFixed(2)}
                    </span>
                  </div>
                </div>
                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span style={{ textAlign: 'left' }}>Available Balance</span>
                    <span>{item.availableBalance.toFixed(2)}</span>
                  </div>
                </div>
                <div
                  style={{ marginTop: 8 }}
                  className={`row text-center amount-text p-1 ${
                    item.active ? 'amount-text-active' : 'amount-text-disable'
                  }`}
                >
                  <div
                    className="col-md-12"
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span>Blocked Balance</span>
                    <span>{item.blockedBalance.toFixed(2)}</span>
                  </div>
                </div>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}></div>
              <div className="row ml-1 tabs" style={{ width: '900px' }}>
                <div
                  className={`item ${
                    this.state.transactionTab && item.active ? 'active' : ''
                  }`}
                  onClick={(e) => this.handleTab(e, 'transactions')}
                >
                  Transactions
                  {/* <img src={transactionIcon}/> */}
                </div>
                <div
                  className={`item ${
                    this.state.transferTab && item.active ? 'active' : ''
                  }`}
                  onClick={(e) => this.handleTab(e, 'transfer')}
                >
                  Transfer
                  {/* <img src={transferIcon} /> */}
                </div>

                <div
                  className={`item ${
                    this.state.exchangeTab && item.active ? 'active' : ''
                  }`}
                  onClick={(e) => this.handleTab(e, 'exchange')}
                >
                  Exchange
                  {/* <img src={exchangeIcon}/> */}
                </div>
              </div>
              {item.active ? <div className="arrow-down "></div> : null}
            </div>
          </div>
        </React.Fragment>
      );
    });
  };
  getUserTransactionHistory = () => {
    this.setState({
      userDetails: this.state.userDetails,
    });
  };

  changeAccount = (index) => {
    const accounts = this.state.accounts;
    const selectedAccount = accounts[index].account;
    accounts.map((el) => (el.active = false));
    accounts[index].active = true;
    this.setState({ accounts, selectedAccount });
  };

  renderExpandableRow = ({ data }) => {
    return (
      <div>
        <div className="expandable-row">
          {/* <div className="field-container">
            <label>Entity Type</label>
            <input
              readOnly
              id="entity_type_exp"
              value={data.entity_type ? data.entity_type : 'NOT PRESENT'}
            />
          </div> */}
          <div className="field-container">
            <label>Account Number </label>
            <input
              readOnly
              id="account_number_exp"
              value={data.CounterPartyAccountNo}
            />
          </div>
          <div className="field-container">
            <label>Swift Code</label>
            <input readOnly id="swift_exp" value={data.swift_code} />
          </div>
          <div className="field-container">
            <label>Entity Name</label>
            <input readOnly id="entity_name_exp" value={data.Entity_Name} />
          </div>
          <div className="field-container">
            <label>Address line</label>
            <input readOnly id="address_exp" value={data.address} />
          </div>
        </div>
      </div>
    );
  };

  //when a currency is selected
  onDropdownValueChange = (from_currency) => {
    var val = document.getElementById('dlistInput').value;
    this.setState({ toCurrency: val });
    this.findExchangeRate(from_currency, val);
  };
  findExchangeRate = (from_currency, to_currency) => {
    const { accountCardsData } = this.state;
    const objects = this.state.exchangeRateInfo;
    let toEURComm = null;
    var size = Object.keys(objects).length;
    const results = [];
    for (let i = 1; i <= size; i++) {
      for (let key in objects[i]) {
        if (
          objects[i][key] === from_currency &&
          objects[i].to_currency === to_currency
        ) {
          results.push(objects[i]);
        }
        if (
          objects[i].from_currency === from_currency &&
          objects[i].to_currency === 'EUR'
        ) {
          toEURComm = objects[i].exchange_rate;
        }
      }
      this.setState({ toEURCommission: toEURComm });
    }

    for (let c in accountCardsData['Business']) {
      if (accountCardsData['Business'][c].countryCurrency === to_currency) {
        this.setState({
          exchangeToAccount: accountCardsData['Business'][c].account_no,
        });
      }
    }

    this.setState({ currentExchangeRate: results[0] });
  };

  exportPDF = () => {
    const { transactionType } = this.state;
    const unit = 'pt';
    const size = 'A4'; // Use A1, A2, A3 or A4
    const orientation = 'portrait'; // portrait or landscape
    const fileName =
      transactionType === 'External'
        ? `External-Transaction-Report ${new Date().toLocaleDateString()}`
        : transactionType === 'Internal'
        ? `Internal-Transaction-Report ${new Date().toLocaleDateString()}`
        : `Exchange-Transaction-Report ${new Date().toLocaleDateString()}`;

    const marginLeft = 10;
    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(12);

    const title =
      transactionType === 'External'
        ? `External Transaction Report - ${new Date().toLocaleDateString()}`
        : transactionType === 'Internal'
        ? `Internal Transaction Report - ${new Date().toLocaleDateString()}`
        : `Exchange Transaction Report - ${new Date().toLocaleDateString()}`;
    const headers = [
      [
        'Date',
        'CounterParty Name',
        'CounterParty Account Number',
        'Payment Description',
        'Amount',
        'Status',
      ],
    ];
    const transactionDataType =
      transactionType === 'External'
        ? this.state.externalTransaction
        : transactionType === 'Internal'
        ? this.state.internalTransaction
        : this.state.exchangeTransaction;

    const data = transactionDataType.map((elt) => [
      elt.date,
      elt.CounterPartyName,
      elt.CounterPartyAccountNo,
      elt.Description,
      elt.Amount.toFixed(2),
      elt.Status,
    ]);
    // const data = this.state.people;

    let content = {
      startY: 50,
      // tableWidth: 300,
      cellWidth: 50,
      margin: 10,
      head: headers,
      body: data,
      // headStyles:{cellWidth:},
      bodyStyles: { halign: 'center', fontSize: 9 },
      // columnStyles: { halign: 'center' },
    };

    doc.text(title, marginLeft, 20);
    doc.autoTable(content);
    doc.save(fileName);
  };

  render() {
    const {
      transactionDetails,
      externalTransaction,
      internalTransaction,
      exchangeTransaction,
      currentCurrency,
      accountCardsData,
      toEURCommission,
    } = this.state;
    var externalTransactionColumn = [
      {
        Header: '',
        accessor: '',
        width: 50,
        Cell: (row) => (
          <div>
            <a
              href={row.value.invoice_url}
              download
              style={{ color: 'black' }}
              title="Download Invoice"
            >
              <i
                style={{ width: 35, fontSize: 18, padding: 11 }}
                className="fa fa-envelope-open"
              ></i>
            </a>
          </div>
        ),
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        width: 120,
        accessor: 'date',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: <b>CounterParty Name</b>,
        width: 180,
        accessor: 'CounterPartyName',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: <b>CounterParty Account Number</b>,
        width: 260,
        accessor: 'CounterPartyAccountNo',
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: <b>Payment Description</b>,
        accessor: 'Description',
        width: 260,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: <b>Amount</b>,
        // accessor: 'Amount',
        Cell: (row) => <span>{row.original.Amount.toFixed(2)}</span>,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: <b>Status</b>,
        // accessor: '',
        Cell: (row) => (
          <span className="home-table-status">
            {row.original.Status === 'P' ? (
              <div className="pending">Pending</div>
            ) : row.original.Status === 'AP' ? (
              <div className="approved">Approved</div>
            ) : row.original.Status === 'R' ? (
              <div className="rejected">Rejected</div>
            ) : (
              <div className="executed">Executed</div>
            )}
          </span>
        ),
        style: {
          textAlign: 'center',
        },

        //
      },
    ];
    var exchangeTransactionColumn = [
      {
        Header: (
          <b>
            Date <i className="fa fa-sort"></i>
          </b>
        ),
        // width: 120,
        accessor: 'date',
        style: {
          textAlign: 'center',
        },
      },
      // {
      //   Header: <b>CounterParty Name</b>,
      //   width: 180,
      //   accessor: 'CounterPartyName',
      //   style: {
      //     textAlign: 'left',
      //   },
      // },
      {
        Header: <b>CounterParty Account Number</b>,
        // width: 260,
        accessor: 'CounterPartyAccountNo',
        style: {
          textAlign: 'center',
        },
      },
      // {
      //   Header: <b>Payment Description</b>,
      //   accessor: 'Description',
      //   width: 260,
      //   style: {
      //     textAlign: 'center',
      //   },
      // },
      {
        Header: <b>Amount</b>,
        // accessor: 'Amount',
        Cell: (row) => <span>{row.original.Amount.toFixed(2)}</span>,
        style: {
          textAlign: 'center',
        },
      },
      // {
      //   Header: <b>Status</b>,
      //   accessor: 'Status',
      //   cell: (row) => (
      //     // <span className="home-table-status">
      //     //   {console.log('rowwww', row)}
      //     //   {row.Status === 'P' ? (
      //     //     <div className="pending">Pending</div>
      //     //   ) : row.Status === 'AP' ? (
      //     //     <div className="approved">Approved</div>
      //     //   ) : (
      //     //     <div className="rejected">Rejected</div>
      //     //   )}
      //     // </span>
      //     <div>{console.log('row', row)}</div>
      //   ),
      //   style: {
      //     textAlign: 'center',
      //   },

      //   //
      // },
    ];

    return (
      <Fragment>
        <Header />
        <div
          className="container-fluid"
          style={{ background: 'white', width: '100%' }}
        >
          <div className="row home-header-body">
            <div
              className="col-md-11 header-body-title mt-2 p-0 mx-5"
              style={{ display: 'flex', justifyContent: 'space-between' }}
            >
              <h6 style={{ fontWeight: 'bold' }}>
                {localStorage.getItem('CompanyName')}
              </h6>
              <Link
                to={{
                  pathname: '/beneficiary',
                  state: {
                    currencyAccountId: currentCurrency.account_id,
                    currency: currentCurrency.countryCurrency,
                  },
                }}
              >
                {currentCurrency.countryCurrency
                  ? `Add Recipient for ${currentCurrency.countryCurrency}`
                  : ''}
              </Link>
            </div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                // marginBottom: '-50px',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  margin: '5px 45px',
                  marginBottom: -20,
                }}
                className="topTabContainer"
              >
                {this.state.accounts.map((el, index) => {
                  return (
                    <div
                      onClick={() => this.changeAccount(index)}
                      style={{
                        cursor: 'pointer',
                        backgroundColor: el.active ? '#F3E3FF' : '#fff',
                        padding: '5px 15px',
                        fontSize: 13,
                        fontWeight: 'bold',
                        marginRight: 15,
                        boxShadow: '0 3px 5px rgba(0, 0, 0, 0.2)',
                      }}
                      className="topTab"
                    >
                      {el.label}
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          {this.props.isloading ? (
            <div style={{ height: '100vh' }}>
              <div className="customLoader-ring" id="spinner-id">
                <div></div>
                <div></div>
              </div>
            </div>
          ) : (
            <>
              <div className="carousel-home">
                <div className="row   header-body-content my-3 currency-card-container ">
                  {this.accountCards(
                    this.state.accountCardsData[this.state.selectedAccount]
                  )}
                </div>
              </div>
              <div className="row">
                {this.state.transactionTab ? (
                  <div
                    className="col-sm-11 mx-auto table--body "
                    style={{ marginTop: '46px' }}
                  >
                    <div className="card transaction-table-card">
                      <div className="transaction-table-header">
                        <div className="transaction-history">
                          <span>Transaction</span>
                          <p className="blue-text">
                            Account no. {currentCurrency.account_no}
                          </p>
                        </div>
                        <div className="date-picker-fields-print-logo">
                          <div className="datepicker-div">
                            <DatePicker
                              selected={this.state.startDate}
                              onChange={(date) =>
                                this.setState({ startDate: date })
                              }
                              placeholderText="From"
                              customInput={
                                <input className="example-custom-input" />
                              }
                            />

                            <DatePicker
                              selected={this.state.endDate}
                              onChange={(date) =>
                                this.setState({ endDate: date })
                              }
                              placeholderText="To"
                              customInput={
                                <input
                                  className="example-custom-input"
                                  // onChange={this.setState({ startDate: value })}
                                  // value={value}
                                />
                              }
                            />
                          </div>
                          <img
                            className="printer-icon"
                            src={printIcon}
                            alt=""
                            onClick={this.exportPDF}
                            style={{ cursor: 'pointer' }}
                            title="Download PDF"
                          />
                          <span className="image-icon-block">
                            <img
                              className="image-icon-swift"
                              src={swiftIcon}
                              alt=""
                            />
                          </span>
                          <span className="swift-text-table-header">SWIFT</span>
                        </div>
                      </div>

                      <Tab.Container
                        id="left-tabs-example"
                        defaultActiveKey="externalTransaction"
                      >
                        <Row>
                          <Col>
                            <Nav
                              variant="pills"
                              style={{
                                display: 'flex',
                                justifyContent: 'center',
                                marginBottom: 20,
                                marginTop: -50,
                              }}
                            >
                              <Nav.Item
                                onClick={() =>
                                  this.setState({ transactionType: 'External' })
                                }
                              >
                                <Nav.Link eventKey="externalTransaction">
                                  External Transactions
                                </Nav.Link>
                              </Nav.Item>
                              <Nav.Item
                                onClick={() =>
                                  this.setState({ transactionType: 'Internal' })
                                }
                              >
                                <Nav.Link eventKey="internalTransaction">
                                  Internal Transactions
                                </Nav.Link>
                              </Nav.Item>
                              <Nav.Item
                                onClick={() =>
                                  this.setState({ transactionType: 'Exchange' })
                                }
                              >
                                <Nav.Link eventKey="exchangeTransaction">
                                  Exchange Transactions
                                </Nav.Link>
                              </Nav.Item>
                            </Nav>
                          </Col>
                          <Col sm={12}>
                            <Tab.Content>
                              <Tab.Pane eventKey="externalTransaction">
                                <div className="card-body datatable-react pt-0 rdt_Table">
                                  <ReactTable
                                    // id="externalTable"
                                    data={externalTransaction}
                                    columns={[
                                      ...externalTransactionColumn,
                                      // undercomplianceActions,
                                    ]}
                                    // showPagination={false}
                                    defaultPageSize={20}
                                    SubComponent={(data) => (
                                      <div>
                                        {
                                          <this.renderExpandableRow
                                            data={data.row._original}
                                          />
                                        }
                                      </div>
                                    )}
                                  />
                                </div>
                              </Tab.Pane>
                              <Tab.Pane eventKey="internalTransaction">
                                <div className="card-body datatable-react pt-0 rdt_Table">
                                  <ReactTable
                                    data={internalTransaction}
                                    columns={[
                                      ...externalTransactionColumn,
                                      // undercomplianceActions,
                                    ]}
                                    // showPagination={false}
                                    defaultPageSize={20}
                                  />
                                </div>
                              </Tab.Pane>
                              <Tab.Pane eventKey="exchangeTransaction">
                                <div className="card-body datatable-react pt-0 rdt_Table">
                                  <ReactTable
                                    data={exchangeTransaction}
                                    columns={[
                                      ...exchangeTransactionColumn,
                                      // undercomplianceActions,
                                    ]}
                                    // showPagination={false}
                                    defaultPageSize={20}
                                  />
                                </div>
                              </Tab.Pane>
                            </Tab.Content>
                          </Col>
                        </Row>
                      </Tab.Container>
                      {/* </div> */}
                    </div>
                  </div>
                ) : (
                  ''
                )}
                {this.state.transferTab ? (
                  <div className="col-sm-11 mx-auto table--body">
                    <Transfer
                      allBeneficiaries={this.props.allBeneficiaries}
                      currentCurrency={currentCurrency.countryCurrency}
                      accountNumber={currentCurrency.account_no}
                      accountId={currentCurrency.account_id}
                      accountHolderName={currentCurrency.account_holder_name}
                      accountHolderAddress={
                        currentCurrency.account_holder_address
                      }
                    />
                  </div>
                ) : (
                  ''
                )}
                {this.state.exchangeTab ? (
                  <div className="col-sm-11 mx-auto table--body">
                    {/* <ExhangeForm
                      token={localStorage.getItem('access')}
                      accountNumber={currentCurrency.account_no}
                      currency={currentCurrency.countryCurrency}
                      onDropdownValueChange={this.onDropdownValueChange}
                      currentExchangeRate={this.state.currentExchangeRate}
                      toCurrency={this.state.toCurrency}
                      currencyAccount={accountCardsData}
                      exchangeToAccount={this.state.exchangeToAccount}
                      toEURCommission={toEURCommission}
                      onExchangeCurrency={this.props.onExchangeCurrency}
                      // account_id={accountId}
                    /> */}
                    <div style={{ margin: '70px auto', width: '40%' }}>
                      <h2>
                        This page is under maintenance...
                        <br />
                        Please try again after some time
                      </h2>
                    </div>
                  </div>
                ) : (
                  ''
                )}
              </div>
            </>
          )}
        </div>
        <Footer />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  // const { getAccounts, transactions } = state;
  // return { getAccounts, transactions };
  accountData: state.getAccounts.accountData,
  transactionData: state.transactions.transactionData,
  exchangeRate: state.transactions.exchangeRate,
  allBeneficiaries: state.beneficiary.allBeneficiaries,
  isloading: state.loadingReducer.isloading,
});

export default connect(mapStateToProps, {
  OnGetAccounts,
  getTransactions,
  getBeneficiaries,
  getExchangeRates,
  onExchangeCurrency,
})(DefaultCompo);
