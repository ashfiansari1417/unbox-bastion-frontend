import React, { Component, Fragment } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../../../assets/scss/Components/allTransaction.scss" 
import printIcon from  "../../../assets/images/payment-history/payment-history.png"
import eurIcon from  "../../../assets/images/payment-history/eur.png"
import gbpIcon from  "../../../assets/images/payment-history/gbp.png"
import swiftIcon from  "../../../assets/images/payment-history/swifticon.png"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { GET_ALL_TRANSACTION_REQUEST } from '../../../constant/actionTypes';

// import Breadcrumb from '../../common/breadcrumb';

var columns

class DefaultCompo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "5",
            comanyName: '',
            userDetails: [
                { id: 1, date: '24 April', client_type: '198289', transferred_to:'Name', message: 'Barbarian', total_availablebal_after_transfer: '1982' },
                { id: 2, date: '24 April', client_type: '198289', transferred_to:'Name', message: 'Barbarian', total_availablebal_after_transfer: '1982' },
                { id: 3, date: '24 April', client_type: '198289', transferred_to:'Name', message: 'Barbarian', total_availablebal_after_transfer: '1982' }
            ],
            settings: 
            {
                // centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                focusOnSelect:true,
                slidesToScroll:1,
                centerMode:true,
                // draggable:true,
                responsive: [
                  {
                    breakpoint: 768,
                    settings: {
                      arrows: false,
                      centerMode: true,
                      centerPadding: '40px',
                      slidesToShow: 3
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      arrows: false,
                      centerMode: true,
                      centerPadding: '40px',
                      slidesToShow: 3
                    }
                  }
                ]
              },
              startDate: new Date(),
              endDate: new Date()

        }
    }

    UNSAFE_componentWillMount() {
        const getID = localStorage.getItem('user');
        const userID = JSON.parse(getID)
        this.setState({
            // id: userID.id,
            // comanyName: userID.company_name
        })
    }

    componentDidMount() {
        // this.getUserTransactionHistory();

        columns = [
            {
                name: 'Date',
                selector: "date",
                maxWidth: '10%',
                sortable: true,
                cell: row =>
                    <span>
                        {row.date ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}>{row.date}</span> : <span>-</span>}
                    </span>
            },
            {
                name: 'Transferred To',
                selector: "transferred_to",
                maxWidth: '30%',
                sortable: true,
                cell: row =>
                    <span>
                        {row.transferred_to ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}>{row.transferred_to}</span> : <span>-</span>}
                    </span>
            },
            {
                name: 'Message',
                selector: "message",
                sortable: true,
                maxWidth: '10%',
                cell: row =>
                    <span >
                        {row.message ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}> {row.message}</span> : <span>-</span>}
                        {/* {row.message ? <span>{new Intl.NumberFormat('en-IN').format(row.amount)} {row.currency}</span> : <span>-</span>} */}
                    </span>
            },
             {
                name: 'Balance',
                selector: "total_availablebal_after_transfer",
                sortable: true,
                maxWidth: '20%',
                cell: row =>
                <span>
                    {row.total_availablebal_after_transfer  ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}> {row.total_availablebal_after_transfer}</span> : <span>-</span>}
                </span>
            },
             {
                name: 'Total Balance',
                selector: "total_availablebal_after_transfer",
                sortable: true,
                maxWidth: '20%',
                cell: row =>
                    <span>
            {row.total_availablebal_after_transfer ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}>{new Intl.NumberFormat('en-IN').format(parseFloat(row.total_availablebal_after_transfer))} {row.currency}</span> : <span>-</span>}
                    </span>
            },
            {
                name: 'Available Balance',
                selector: "AvailableBalence",
                maxWidth: '20%',
                cell: row =>
                    <span>
                        {row.total_availablebal_after_transfer ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}>{new Intl.NumberFormat('en-IN').format(row.total_availablebal_after_transfer)} {row.currency}</span> : <span>-</span>}
                    </span>
            },
             {
                name: 'Status',
                sortable: true,
                selector: "account_status",
                maxWidth: '10%',
                cell: row =>
                    <span>
                        {(row.account_status==0) ? <span style={{ 'fontSize': '22px', 'color': 'rgba(0, 0, 0, 0.5)'}}>Pending</span> :(row.account_status==1)? <span>Approved</span>:<span>Rejected</span>}
                    </span>
            },
        ]

        
    }
    transactionCardslider = (carouselData) => {
        return (<div className="row">
            <div className="col-md-11 ecommerce-slider mx-auto mt50" >
                <Slider {...this.state.settings}>
                    {carouselData.map((item, index) => {
                        return (
                            <div className="item" id={index} key={`carousel-${index}`} >
                                <div className="card">
                                    <div className="card-body ecommerce-icons text-center" style={{'background' : item.color}}>
                                        <div className='opaque-circle-top'></div>
                                        <div className='opaque-circle-bottom'></div>
                                        <div className="col-md-1 pull-left currency--sign">
                                            <img src={item.currency} alt=''/>
                                        </div>
                                        <div className="col-md-10 pull-left">
                                            <h4 className="p-0 title">Account number</h4>
                                            <h5 className="p-0 title-data">{item.account_number}</h5>
                                            <h4 className="p-0 title">Balance</h4>
                                            <h5 className="p-0 title-data">${item.balance}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>)
                    })
                    }
                </Slider>
            </div>
        </div>)
    }
    getUserTransactionHistory = () => {
        axios.post(GET_ALL_TRANSACTION_REQUEST, {
            "user_id": this.state.id

        })
            .then(res => {
                this.setState({
                    userDetails: res.data.result.Items
                })

            })
    }
    handleChange = (date, type) => {
        type === "startDate" ? this.setState({
            startDate: date
        })
        : this.setState({
            endDate: date
        });
    };

    render() {
        const { userDetails } = this.state;
        const carouselData = [
            { data: 'aman', color:'#CB7F69', account_number:'12345678', balance:'120000000', currency: eurIcon },
            { data: 'body', color:'#27719D', account_number:'12345678', balance:'120000000', currency: gbpIcon },
            { data: 'builder', color:'#B48EB4', account_number:'12345678', balance:'120000000', currency: eurIcon},
            { data: 'rocking', color:'#CB7F69', account_number:'12345678', balance:'120000000', currency: gbpIcon},
            // { data: 'guy', color:'27719D', account_number:'12345678', balance:'120000000', currency: eurIcon}
        ]
        return (
            <Fragment>
                {/* <Breadcrumb title="Sterling Payment Services" parent="Dashboard" /> */}
                <div className="container-fluid">
                    {this.transactionCardslider(carouselData)}
                    <div className="row">
                        <div className="col-sm-11 mx-auto">
                            <div className="card">
                                <div className="card-header span">
                                <div className="row"> 
                                    <div className="col-md-2">
                                        <span className="image-icon-block">
                                            <img className="image-icon-swift" src={swiftIcon} alt="" />
                                        </span>
                                        <span className="text-block">
                                            SWIFT
                                        </span>
                                    </div>
                                    <div className="col-md-3 text-block">Business Account</div>
                                    <div className="col-md-7">
                                    <div className="row">
                                    <div className="col-md-4">
                                        <div className="date-label">From:</div>
                                        <DatePicker 
                                             selected={this.state.startDate}
                                             onChange={(date) => this.handleChange(date,'startDate')}
                                             placeholderText={"From"}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                    <div className="date-label">To:</div>
                                        <DatePicker 
                                             selected={this.state.endDate}
                                             onChange={(date) => this.handleChange(date, 'endDate')}
                                             placeholderText={"To"}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <img className="pull-right" src={printIcon} alt=""/>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <div className="card-body datatable-react pt-0">
                                    <DataTable
                                        noHeader                                     
                                        // defaultSortField="created_at"
                                        defaultSortAsc={true}
                                        columns={columns}
                                        data={userDetails}
                                        pagination
                                        head={`fontSize : '24px`}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default DefaultCompo;