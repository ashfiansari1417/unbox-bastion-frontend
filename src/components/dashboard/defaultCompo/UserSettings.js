import React, { Component } from 'react';
import Header from '../../common/header-component/header';
import { Link } from 'react-router-dom';
import './styles/UserSettings.css';

export default class UserSettings extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="user-settings-container">
          <Link to="user-profile" className="user-settings-card">
            <div style={{ height: 150 }}>
              <img
                src={require('../../../assets/images/resetPassword.png')}
                className="setting-icon"
              />
            </div>
            <h3>Edit Profile</h3>
          </Link>

          <Link to="reset-password" className="user-settings-card">
            <div style={{ height: 150 }}>
              <img
                src={require('../../../assets/images/resetPassword.png')}
                className="setting-icon"
              />
            </div>
            <h3>Reset Password</h3>
          </Link>
        </div>
      </div>
    );
  }
}
