import React from 'react';
import { Link } from 'react-router-dom';

import '../styles/LoaderBackdrop.css';

const LoaderBackdrop = (props) => {
  const off = () => {
    document.getElementById('overlay').style.display = 'none';
  };
  return (
    <div id="overlay" onClick={off}>
      <h3 className="modal1">{props.message}</h3>
    </div>
  );
};

export default LoaderBackdrop;

// LoaderBackdrop
