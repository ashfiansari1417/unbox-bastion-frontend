import React, { Component } from 'react';
import '../styles/ExternalForm.css';
import { internalTransaction } from '../../../../actions/Transaction/transactions';
import { connect } from 'react-redux';

export class InternalForm extends Component {
  state = {
    companyName: '',
    recipientName: '',
    recipientAccNumber: '', //98765432
    amount: '',
    invoice: '',
    paymentDesc: '',
    messageToOperator: '',
    pin: '',
  };
  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  invoiceFileChange = (e) => {
    // console.log('call this function successfully for file change');
    const invoiceFile = e.target.files[0];
    console.log(invoiceFile);
    console.log(invoiceFile.type);
    if (invoiceFile) {
      if (
        invoiceFile.type === 'image/jpeg' ||
        invoiceFile.type === 'application/pdf' ||
        invoiceFile.type === 'image/jpg'
      ) {
        console.log(invoiceFile.type);
        this.setState({
          invoice: invoiceFile,
        });
      } else {
        alert('Only files with .jpeg or .pdf accepted');
        this.clearFileInput('myInvoice');
      }
    }
  };
  clearFileInput = (id) => {
    var oldInput = document.getElementById(id);

    var newInput = document.createElement('input');

    newInput.addEventListener('change', this.invoiceFileChange);
    newInput.type = 'file';
    newInput.id = oldInput.id;
    newInput.name = oldInput.name;
    newInput.className = oldInput.className;
    newInput.accept = oldInput.accept;
    // newInput.onchange = oldInput.onchange;
    newInput.style.cssText = oldInput.style.cssText;
    // TODO: copy any other relevant attributes

    oldInput.parentNode.replaceChild(newInput, oldInput);
  };

  transferClick = () => {
    const {
      companyName,
      recipientName,
      recipientAccNumber,
      amount,
      invoice,
      paymentDesc,
      messageToOperator,
      pin,
    } = this.state;
    const data = {
      // companyName,
      // recipientName,
      // recipientAccNumber,
      // amount,
      // invoice,
      // paymentDesc,
      // messageToOperator,
      // pin,
      account_id: this.props.accountId,
      beneficiary_client_account: recipientAccNumber,
      amount,
      remark: messageToOperator,
    };
    console.log('from btn click', data);
    let formData = new FormData();
    if (!recipientAccNumber) {
      alert('Recipient account number required');
    } else if (!amount) {
      alert('Please enter amount');
    } else if (!invoice) {
      alert('Please upload an invoice');
    } else {
      formData.append('transaction_data', JSON.stringify(data));
      formData.append('file', this.state.invoice);
      // formData.append('pin', this.state.pin);
      console.log(this.state.invoice);
      this.props.internalTransaction(formData, localStorage.getItem('access'));
    }
  };
  render() {
    const {
      companyName,
      recipientName,
      recipientAccNumber,
      amount,
      invoice,
      paymentDesc,
      messageToOperator,
      pin,
    } = this.state;
    return (
      <div className="transfer-card-container-internal">
        <div style={{ margin: '20px 10px 20px 50px' }}>
          <div className="top-row-element-container">
            <div className="top-element-div">
              <h3 className="tranfer-form-text">Transfer Form</h3>
              <p className="blue-text">
                Account no. {this.props.account_number}
              </p>
            </div>
            <div className="top-element-div">
              <span>Choose Transfer Type</span>
              <div className="choose-transfer-type-button-div">
                <button
                  onClick={() => this.props.changeTransferType('internal')}
                  className={'transfer-type-active'}
                  // style={{ cursor: 'not-allowed' }}
                >
                  Internal
                </button>
                <button
                  onClick={() => this.props.changeTransferType('external')}
                  className={'transfer-type-inactive'}
                >
                  External
                </button>
              </div>
            </div>
          </div>
          <p className="beneficicary-information-text">Recipient Information</p>
          <section className="internal-form-beneficiary-information">
            <div className="internal-form-user-input-container">
              {/* <div>
                <label>Company Name</label>
                <input
                  value={companyName}
                  name="companyName"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div> */}
              {/* <div>
                <label>Recipient Name</label>
                <input
                  value={recipientName}
                  name="recipientName"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div> */}
              <div>
                <label>Account Number </label>
                <input
                  value={recipientAccNumber}
                  name="recipientAccNumber"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div>
              <div>
                <label>Amount {`(${this.props.currentCurrency})`}</label>
                <input
                  value={amount}
                  name="amount"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div>
              <div>
                <label>Upload document (Invoice)</label>
                <input
                  className="upload-file"
                  type="file"
                  id="myInvoice"
                  name="invoice"
                  accept=".pdf,.jpg,.jpeg"
                  onChange={this.invoiceFileChange}
                />
              </div>
              {/* <div>
                <label>Payment Description</label>
                <input
                  value={paymentDesc}
                  name="paymentDesc"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div> */}
              <div className="msg-to-operator-div">
                <label>Message to operator</label>
                <input
                  value={messageToOperator}
                  name="messageToOperator"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div>

              {/* <div style={{ marginTop: '16px' }}>
                <label>Enter Google Authentication Code</label>
                <input
                  value={pin}
                  name="pin"
                  onChange={(e) => this.onInputChange(e)}
                />
              </div> */}
              {/* <div>
                <label style={{ marginTop: '20px' }}>
                  Transaction Fee: € 0.00
                </label>
              </div> */}
              <div>
                <button
                  onClick={this.transferClick}
                  id="internal-transfer-submit"
                >
                  Transfer
                </button>
              </div>
            </div>

            <div>
              <h5>Sterling Payment Services Limited</h5>
              <p>
                Unit D, 9/F, Neich Tower, 128 Gloucester Road, Wanchai, Hong
                Kong
              </p>
              <p>
                <a href="">https://sterlingsafepayment.com</a>
                <span> info@sterlingsafepayment.com</span>
              </p>
              <p>
                <span>Phone: +852 5801 4396</span> <br />
                <span>BIC/SWIFT code: STPVHKHH</span>
              </p>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  internalTransactionMessage: state.transactions.internalTransactionMessage,
});

export default connect(mapStateToProps, { internalTransaction })(InternalForm);
