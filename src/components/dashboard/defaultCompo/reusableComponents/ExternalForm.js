import React, { Component } from 'react';
import '../styles/ExternalForm.css';
import { externalOutgoingTransaction } from '../../../../actions/Transaction/transactions';
import resetIcon from '../../../../assets/images/resetIcon.png';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

export class ExternalForm extends Component {
  state = {
    isSepa: false,
    isSwift: true,
    beneficiaryName: '',
    beneficiaryId: '',
    sepaCommission: '15',
    swiftCommission: '35',
    amount: '',
    pin: '',
    remark: '',
    reference_message: '',
    operatorMessage: '',
    invoice: '',
    required: '* Required',

    // noBeneficiaryText: 'No beneficiary added',
  };

  onDropdownValueChange = (type, id, e) => {
    const { allBeneficiaries, currentCurrency } = this.props;
    const { beneficiaryName } = this.state;
    // var val = document.getElementById(id).value;
    // this.setState({ beneficiaryName: document.getElementById(id).value });
    //   currency
    if (type === 'beneficiary') {
      for (let i in allBeneficiaries) {
        if (
          allBeneficiaries[i].beneficiary_client_name ===
          document.getElementById(id).value
        ) {
          // if (currentCurrency !== allBeneficiaries[i].currency) {
          //   alert('Please Change the currency ');
          //   // document.getElementById(id).value = '';
          //   this.setState({ beneficiaryName: '' });
          // } else {
          this.setState({
            beneficiaryId: allBeneficiaries[i].id,
            beneficiaryName: allBeneficiaries[i].beneficiary_client_name,
            beneficiaryAccount: allBeneficiaries[i].beneficiary_client_account,
            beneficiaryAddress1:
              allBeneficiaries[i].beneficiary_client_address_line_1,
            beneficiaryAddress2:
              allBeneficiaries[i].beneficiary_client_address_line_2,
          });
          // }
        }
      }
    }
  };

  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    // console.log(event);-
  };
  invoiceFileChange = (e) => {
    // console.log('call this function successfully for file change');
    const invoiceFile = e.target.files[0];
    console.log(invoiceFile);
    if (invoiceFile) {
      if (
        invoiceFile.type === 'image/jpeg' ||
        invoiceFile.type === 'application/pdf' ||
        invoiceFile.type === 'image/jpg'
      ) {
        console.log(invoiceFile.type);
        this.setState({
          invoice: invoiceFile,
        });
      } else {
        alert('Only files with .jpeg or .pdf accepted');
        this.clearFileInput('myInvoice');
      }
    }
  };
  clearFileInput = (id) => {
    var oldInput = document.getElementById(id);

    var newInput = document.createElement('input');

    // new input onchange
    newInput.addEventListener('change', this.invoiceFileChange);

    newInput.type = 'file';
    newInput.id = oldInput.id;
    newInput.name = oldInput.name;
    newInput.className = oldInput.className;
    newInput.accept = oldInput.accept;
    // newInput.onchange = oldInput.onchange;
    newInput.style.cssText = oldInput.style.cssText;

    oldInput.parentNode.replaceChild(newInput, oldInput);
  };

  onTransferClick = () => {
    // let data = isSepa
    //   ? {
    //       account_id: this.props.accountId,
    //       beneficiary_id: this.state.beneficiaryId,
    //       amount: this.state.amount,
    //       commision: this.state.sepaCommission,
    //       remark: this.state.remark,
    //       pin: this.state.pin,
    //     }
    //   : {
    //       account_id: this.props.accountId,
    //       beneficiary_id: this.state.beneficiaryId,
    //       amount: this.state.amount,
    //       commision: this.state.swiftCommission,
    //       remark: this.state.remark,
    //       pin: this.state.pin,
    //     };
    let formData = new FormData();

    let data = {
      account_id: this.props.accountId,
      amount: this.state.amount,
      dest_swift: '',

      beneficiary_id: this.state.beneficiaryId,
      beneficiary_client_account: this.state.beneficiaryAccount,
      beneficiary_client_name: this.state.beneficiaryName,
      beneficiary_client_address_line_1: this.state.beneficiaryAddress1,
      beneficiary_client_address_line_2: this.state.beneficiaryAddress2,
      ordering_customer_name:
        this.props.accountHolderName === null
          ? ''
          : this.props.accountHolderName,
      ordering_customer_address:
        this.props.accountHolderAddress === null
          ? ''
          : this.props.accountHolderAddress,
      reference_message: this.state.reference_message,
      message_for_operator: this.state.operatorMessage,

      // file: this.state.invoice,
    };

    // console.log(data);
    if (!this.state.beneficiaryName) {
      alert('Please select a Recipient from the dropdown');
    } else if (!this.state.amount) {
      alert('Please enter the amount to be transferred');
    } else if (this.state.amount <= 1) {
      alert('Amount should be greater than 1');
    } else if (!this.state.invoice) {
      alert('Please Upload the invoice');
    } else if (!this.state.reference_message) {
      alert('Reference message required');
    } else if (this.state.reference_message.length > 136) {
      alert('Max characters for reference message is 136');
    } else if (!this.state.pin) {
      alert('Enter Google Authenticator Pin');
    } else {
      formData.append('transaction_data', JSON.stringify(data));
      formData.append('file', this.state.invoice);
      formData.append('pin', this.state.pin);
      console.log(this.state.invoice);
      // for (var pair of formData.entries()) {
      //   console.log(pair[0] + ', ' + pair[1]);
      // }
      this.props.externalOutgoingTransaction(
        formData,
        localStorage.getItem('access')
      );
    }
  };

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevProps.currentCurrency !== this.props.currentCurrency) {
  //     this.setState({ beneficiaryName: '' });
  //   }
  // }

  render() {
    const { allBeneficiaries, currentCurrency } = this.props;
    const { isSepa, beneficiaryName, isSwift, required } = this.state;
    // console.log('from ext form', allBeneficiaries);

    // const sepaBeneficiaries = [];
    const swiftBeneficiaries = [];
    for (let i in allBeneficiaries) {
      // if (allBeneficiaries[i].is_sepa === true)
      //   sepaBeneficiaries.push(allBeneficiaries[i].beneficiary_name);
      // else {
      swiftBeneficiaries.push(allBeneficiaries[i].beneficiary_client_name);
      // }
    }

    return (
      <div className="transfer-card-container-external">
        <div style={{ margin: '20px 10px 20px 50px' }}>
          <div className="top-row-element-container">
            <div className="top-element-div">
              <h3 className="tranfer-form-text">Transfer Form</h3>
              <p className="blue-text">
                Account no. {this.props.account_number}
              </p>
            </div>
            <div className="top-element-div">
              <span>Choose Transfer Type</span>
              <div className="choose-transfer-type-button-div">
                <button
                  onClick={() => this.props.changeTransferType('internal')}
                  className={'transfer-type-inactive'}
                  // style={{ cursor: 'not-allowed' }}
                >
                  Internal
                </button>
                <button
                  onClick={() => this.props.changeTransferType('external')}
                  className={'transfer-type-active'}
                >
                  External
                </button>
              </div>
            </div>
            <div className="transfer-type">
              <div className="choose-transfer-type-button-div">
                {/* <div className="radio-button-div">
                <input
                  type="radio"
                  checked={isSepa}
                  // onChange={() =>
                  //   this.setState({ isSepa: true, isSwift: false })
                  // }
                  style={{ cursor: 'not-allowed' }}
                />
                Sepa
              </div> */}

                {/* <div className="radio-button-div">
                <input
                  type="radio"
                  checked={isSwift}
                  onChange={() =>
                    this.setState({ isSepa: false, isSwift: true })
                  }
                />
                Swift
              </div> */}
              </div>
            </div>
            {/* <div className="top-element-div">
          <p>Or</p>
        </div>
        <div className="top-element-div">
          <label>Save in Address Book</label>
          <input placeholder="Search..." />
        </div> */}
          </div>
          <p className="beneficicary-information-text">Recipient Information</p>

          <section className="beneficicary-information">
            <div
              className="user-inputs-div"
              style={{ display: 'flex', flexDirection: 'column' }}
            >
              <label>Recipient Name</label>
              <div style={{ display: 'flex' }}>
                {isSepa ? (
                  <>
                    <input
                      id="beneficiaryListInput"
                      // placeholder={
                      //   sepaBeneficiaries.length > 0
                      //     ? 'Select Beneficiary'
                      //     : 'No beneficiaries available'
                      // }
                      value={beneficiaryName}
                      onChange={() => {
                        this.onDropdownValueChange(
                          'beneficiary',
                          'beneficiaryListInput'
                        );
                      }}
                      list="beneficiaryList"
                    />
                    <datalist id="beneficiaryList">
                      {/* {sepaBeneficiaries.map((item, key) => (
                    <option key={key} value={item} />
                  ))} */}
                    </datalist>
                  </>
                ) : (
                  <>
                    <input
                      id="beneficiaryListInput"
                      placeholder={
                        swiftBeneficiaries.length > 0
                          ? 'Select Recipient'
                          : 'No Recipients available'
                      }
                      name="beneficiaryName"
                      value={beneficiaryName}
                      // onKeyDown={(e) => this.handleKeyPress(e)}
                      onChange={(e) => {
                        // this.onInputChange(e);
                        this.onDropdownValueChange(
                          'beneficiary',
                          'beneficiaryListInput',
                          e
                        );
                      }}
                      list="beneficiaryList"
                      // style={{ width: '150px' }}
                    />
                    <datalist id="beneficiaryList">
                      {swiftBeneficiaries.map((item, key) => (
                        <option key={key} value={item} />
                      ))}
                    </datalist>
                  </>
                )}
                {beneficiaryName && (
                  <div
                    style={{
                      cursor: 'pointer',
                      alignSelf: 'center',
                      paddingLeft: 10,
                    }}
                    onClick={() => {
                      this.setState({ beneficiaryName: '' });
                    }}
                  >
                    <span style={{ color: '#3498DB' }}>
                      Select another recipient
                    </span>
                    <img
                      src={resetIcon}
                      alt="reset"
                      style={{
                        width: 16,
                        height: 16,
                        paddingLeft: 4,
                        alignSelf: 'center',
                      }}
                    />
                  </div>
                )}
              </div>
              <span style={{ color: 'red' }}>{required}</span>
              <Link
                style={{ textAlign: 'left', width: '100px', marginTop: '4px' }}
                to={{
                  pathname: '/beneficiary',
                  state: {
                    currencyAccountId: this.props.accountId,
                    currency: currentCurrency,
                  },
                }}
                // className="add-beneficiary"
              >
                {this.props.accountId ? `Add Recipient` : 'Add Recipient'}
              </Link>

              {/* <Link
              style={{ textAlign: 'left', width: '100px', fontSize: '18px' }}
              to={{
                pathname: '/add-benificiary',
                state: {
                  currencyAccountId: this.props.accountId,
                  // currency: currentCurrency.currency,
                },
              }}
              // className="add-beneficiary"
            >
              {this.props.accountId ? `Add Benificiary` : ''}
            </Link> */}
              {/* <span>OR</span> */}
            </div>
            <div className="user-inputs-div">
              <label>Amount ({currentCurrency})</label>
              <input
                value={this.state.amount}
                name="amount"
                onChange={(e) => this.onInputChange(e)}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            <div className="user-inputs-div">
              <label>Upload Invoice</label>
              <input
                type="file"
                id="myInvoice"
                name="invoice"
                accept=".pdf,.jpg,.jpeg"
                onChange={this.invoiceFileChange}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>
            <div className="user-inputs-div" style={{ marginTop: '26px' }}>
              <label>Reference Message</label>
              <input
                value={this.state.reference_message}
                name="reference_message"
                onChange={(e) => this.onInputChange(e)}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>

            <div className="user-inputs-div" style={{ marginTop: '26px' }}>
              <label>Message to operator</label>
              <input
                value={this.state.operatorMessage}
                name="operatorMessage"
                onChange={(e) => this.onInputChange(e)}
              />
              <span>(Optional)</span>
            </div>
            {/* <div></div> */}
            <div className="user-inputs-div" style={{ marginTop: '26px' }}>
              <label>Enter Google Authentication Code</label>
              <input
                value={this.state.pin}
                name="pin"
                onChange={(e) => this.onInputChange(e)}
              />
              <span style={{ color: 'red' }}>{required}</span>
            </div>

            {/* <div className="user-inputs-div">
          <label>Beneficiary Type</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div">
          <label>Address Line</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div">
          <label>Account Number (IBAN)</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div">
            <label>Payment description</label>
            <input
              name="remark"
              value={this.state.remark}
              onChange={(e) => this.onInputChange(e)}
            />
          </div> */}
            {/* <div className="user-inputs-div">
          <span>Beneficiary Bank</span>
          <label style={{ marginTop: '6px' }}>Swift Code</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div">
          <span>Intermediary Bank</span>
          <label style={{ marginTop: '6px' }}>Swift Code</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div" style={{ marginTop: '60px' }}>
            <label>Upload document (Invoice)</label>
            <input
              className="upload-file"
              type="file"
              id="myfile"
              name="myfile"
            />
          </div> */}
            {/* <div className="user-inputs-div">
          <label>Country</label>
          <input />
        </div> */}
            {/* <div className="user-inputs-div" style={{ marginTop: '18px' }}>
          <label>Bank Name</label>
          <label>Sterling Bank Systems</label>
        </div> */}
            {/* <div className="user-inputs-div" style={{ marginTop: '18px' }}>
          <label>Bank Name</label>
          <label>Sterling Bank Systems</label>
        </div> */}

            {/* <div className="user-inputs-div">
            <Link
              style={{ textAlign: 'left', width: '100px' }}
              to={{
                pathname: '/beneficiary',
                state: {
                  currencyAccountId: this.props.accountId,
                  // currency: currentCurrency.currency,
                },
              }}
              // className="add-beneficiary"
            >
              {this.props.accountId ? `Add Benificiary` : 'Add Benificiary'}
            </Link>
          </div> */}

            {/* <div className="user-inputs-div">
            <label>Enter Google Authentication Code</label>
            <input
              value={this.state.pin}
              name="pin"
              onChange={(e) => this.onInputChange(e)}
            />
          </div> */}
            {/* <div className="user-inputs-div">
             <label style={{ marginTop: '20px' }}>
              {isSepa
                ? `Transaction Fee: € ${this.state.sepaCommission}.00`
                : `Transaction Fee: € ${this.state.swiftCommission}.00`}
            </label> 
          </div> */}

            {/* <div></div>
        <div></div> */}

            {/* <div className="user-inputs-div">
          <button className="save">Save to address book</button>
        </div> */}
          </section>
          <div style={{ display: 'flex' }}>
            <div className="transfer-button-div">
              <button
                className="transfer"
                onClick={() => this.onTransferClick()}
              >
                Transfer
              </button>
            </div>
            <span className="instructionText">
              Message: ** you have to add a recipient if you have not already
              and select it from the drop down
            </span>
          </div>
          {/* <div className="message-to-operator">
          <label>Message To operator</label>
          <input />
        </div> */}
          {/* <p className="urgentHelpText">
          For urgent help on external payments, please call at
          <span className="blue-text"> +7-977-110-06-43 </span>
          or write to
          <span className="blue-text"> externals@sterlingsafepayment.com</span>
        </p> */}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  transactionMessage: state.transactions.transactionMessage,
});

export default connect(mapStateToProps, { externalOutgoingTransaction })(
  ExternalForm
);
