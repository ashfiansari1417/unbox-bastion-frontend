import React, { Component } from 'react';
import { connect } from 'react-redux';
import { exchangeCurrency } from '../../../../actions/Transaction/transactions';
import '../styles/ExchangeForm.css';

export default class ExchangeForm extends Component {
  state = {
    toCurrency: '',
    toAccount: '',
    amount: '',
    // exchangeRate: this.props.currentExchangeRate
    //   ? this.props.currentExchangeRate.exchange_rate
    //   : '0.0',
    exchangeAmount: '',
    commisionAmount: '0.0',
    currentExchangeRate: '0.0',
    exchangeDescription: '',
    pin: '',
  };

  // currentExchangeRate = ()

  onInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
    if (event.target.name === 'amount') {
      const exAmt = this.props.currentExchangeRate
        ? this.props.currentExchangeRate.exchange_rate * event.target.value
        : '';
      const commAmt =
        this.props.currency === 'EUR'
          ? 0.006 * event.target.value
          : this.props.toEURCommission * 0.006 * event.target.value;
      // this.props.toEURCommission * 0.006 * event.target.value
      this.setState({
        exchangeAmount: exAmt.toFixed(4),
        commisionAmount: commAmt.toFixed(4),
      });
    }
  };

  // onDropdownValueChange = () => {
  //   var val = document.getElementById('dlistInput').value;
  //   this.setState({ toCurrency: val });
  // };

  onExchangeClicked = () => {
    const token = this.props.token;
    // const decode = jwt_decode(token);
    let data = {
      from_account_number: this.props.accountNumber,
      to_account_number: this.state.toAccount,
      exchange_amt: this.state.amount,
      commission: this.state.commisionAmount,
      converted_amt: this.state.exchangeAmount,
      desc: this.state.exchangeDescription,
      pin: this.state.pin,
    };

    if (!this.state.amount) {
      alert('Amount Field required');
    } else if (!this.state.pin) {
      alert('Enter Google authentication Pin');
    } else {
      this.props.onExchangeCurrency(data, localStorage.getItem('access'));
      // document.getElementById('dlistInput').value = '';
      this.setState({
        amount: '',
        commisionAmount: '0.0',
        exchangeAmount: '',
        toCurrency: '',
        toAccount: '',
        pin: '',
        exchangeDescription: '',
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.accountNumber !== this.props.accountNumber) {
      document.getElementById('dlistInput').value = '';
      this.setState({
        amount: '',
        commisionAmount: '0.0',
        exchangeAmount: '',
        toCurrency: '',
        toAccount: '',
        pin: '',
        exchangeDescription: '',
      });
    }
    // this.props.currentExchangeRate.exchange_rate
    if (prevProps.toCurrency !== this.props.toCurrency) {
      this.setState({ toCurrency: this.props.toCurrency });
    }
    if (prevProps.exchangeToAccount !== this.props.exchangeToAccount) {
      this.setState({ toAccount: this.props.exchangeToAccount });
    }
  }

  render() {
    const { currencyAccount, toEURCommission } = this.props;
    const currency = [];
    for (let i in currencyAccount['Business']) {
      if (
        currencyAccount['Business'][i].countryCurrency !== this.props.currency
      )
        currency.push(currencyAccount['Business'][i].countryCurrency);
    }

    return (
      <div>
        {/* <div className="card"> */}
        <div className="exchange-card-container ">
          <div className="exchange-form-container">
            <div className="field-div">
              <h5 className="bold-text">Exchange Form</h5>
              <p className="blue-text">
                Account no. {this.props.accountNumber}
              </p>
              <p>From Currency: </p>
              <p style={{ marginTop: '-10px', color: '#7800c0' }}>
                {this.props.currency}
              </p>
              <p>Currency To:</p>
              <input
                id="dlistInput"
                type="text"
                list="dlist"
                onChange={() =>
                  this.props.onDropdownValueChange(this.props.currency)
                }
              />

              <datalist id="dlist">
                {currency.map((item, key) => (
                  <option key={key} value={item} />
                ))}
              </datalist>
              <p>Current Exchange Rate</p>
              <p style={{ marginTop: '0px' }}>
                {this.props.currentExchangeRate
                  ? this.props.currentExchangeRate.exchange_rate
                  : '0.0'}
              </p>
              <p>Commission 0.6%</p>
              <p
                style={{
                  textAlign: 'left',
                  marginTop: '0px',
                  // marginRight: '40px',
                }}
              >
                € {this.state.commisionAmount}
              </p>
            </div>
            <div className="field-div">
              <h5 className="bold-text">Exchange To</h5>
              <p className="blue-text">
                Account no. {`${this.state.toAccount}`}
              </p>
              <p>Amount {`(${this.props.currency})`} </p>
              <input
                value={this.state.amount}
                name="amount"
                onChange={this.onInputChange}
                readOnly={this.props.toCurrency !== '' ? false : true}
              />
              <p style={{ marginTop: '18px' }}>Exchange Description</p>
              <input
                value={this.state.exchangeDescription}
                name="exchangeDescription"
                onChange={this.onInputChange}
              />
              <p>Exchanged Amount in {this.state.toCurrency}</p>
              <p style={{ marginTop: '0px' }}>{this.state.exchangeAmount}</p>
              <p>Enter Google Authentication code</p>

              <input
                name="pin"
                value={this.state.pin}
                onChange={this.onInputChange}
              />
              <button
                className="exchange-form-button exchange"
                onClick={this.onExchangeClicked}
              >
                Exchange
              </button>
            </div>
          </div>

          <div className="Bastion-financial-group-card">
            <h5>Sterling Payment Services Limited</h5>
            <p>
              Unit D, 9/F, Neich Tower, 128 Gloucester Road, Wanchai, Hong Kong
            </p>
            <p>
              <a href="">https://sterlingsafepayment.com</a>
              <span> info@sterlingsafepayment.com</span>
            </p>
            <p>
              <span>Phone: +852 5801 4396</span> <br />
              <span>BIC/SWIFT code: STPVHKHH</span>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
