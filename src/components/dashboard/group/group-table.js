import React, { Fragment } from 'react';
import { withApollo } from "react-apollo";

import { getGroups } from '../../../services/queries';

import Datatable from '../../common/datatable';
    
class GroupTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nametable: []
        }
    }

    componentDidMount() {
        this.getGroups();
    }

    getGroups = () => {
        this.props.client
        .query({
            query: getGroups,
            fetchPolicy: "network-only"
         })
          .then(res => {       
              this.setState({
                  nametable : res.data.getGroups
                })
          });          
    };

    gotoGroupForm = () => {
        this.props.history.push("/dashboard/group/group-form");
    }

    render() {
        const { nametable } = this.state;
        
        return(
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Group List</h5>
                                    <button className="btn btn-primary float-right" type="button" onClick={this.gotoGroupForm}>Add Group</button>
                                </div>
                                <div className="card-body datatable-react">
                                    <Datatable
                                        history={this.props.history}
                                        multiSelectOption={false}
                                        myData={nametable}
                                        pageSize={10}
                                        pagination={true}
                                        class="-striped -highlight"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default withApollo(GroupTable);