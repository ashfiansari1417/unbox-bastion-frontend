import React, {Fragment, Component} from "react";
import { withApollo } from "react-apollo";
import TagsInput from "react-tagsinput";
import ImageUploader from 'react-images-upload';
import { ToastContainer, toast } from 'react-toastify';

import 'react-tagsinput/react-tagsinput.css';

import { getGroupByID, getCommunityNames } from '../../../services/queries';
import { addGroups } from '../../../services/mutation';

import Breadcrumb from '../../common/breadcrumb';
import GroupQuestion from './group-question';

class EditCommunity extends Component {

  closeButton = {
    right: "15px",
    position: "absolute",
    top: "-9px",
    color: "#fff",
    background: "#ff4081",
    borderRadius: "50%",
    textAlign: "center",
    cursor: "pointer",
    fontSize: "26px",
    fontWeight: "bold",
    lineHeight: "30px",
    width: "30px",
    height: "30px",
  };

  constructor(props) {
    super(props);
    this.state = {
      gid: this.props.match.params.id,
      community: [],
      id : "",
      name: "",
      description: "",
      tags: [],
      featureImage: "",
      images: [],
      imagesAdd: [],
      featureImageAdd: ""
    }
  }

  static getDerivedStateFromProps(props, state) {
      if (props.match.params.id !== state.id) {
        return {
            gid: props.match.params.id,
        };
    }
    return null;
  }

  componentDidMount() {
      this.getSingleGroup();
      this.getCommunityNames()
    }

  getCommunityNames = () => {
    this.props.client
    .query({
      query: getCommunityNames,
      fetchPolicy: "no-cache"
    })
    .then(res=> {
      this.setState({
          community: res.data.getCommunity
        });
    });
  };

  getSingleGroup = () => {
    this.props.client
      .query({
        query: getGroupByID,
        variables: {
            GroupID: parseInt(this.state.gid)
        },
        options: {
          fetchPolicy: "network-only"
        }
      })
      .then(res => {
        this.setState({
            id: res.data.getGroupByID.CommunityID,
            name: res.data.getGroupByID.Name,
            description: res.data.getGroupByID.Description,
            tags: res.data.getGroupByID.Hashtag,
            featureImage: res.data.getGroupByID.FeaturedImage,
            images: res.data.getGroupByID.Images
          });
      });
  };

  handleCID = event => {
    this.setState({
      id: event.target.value
    });
  }

  handleName = event => {
    this.setState({
      name: event.target.value
    });
  }

  handleHashTags = (tags) => {
    this.setState({
      tags
    });
  }

  handleDescription = event => {
    this.setState({
      description: event.target.value
    })
  }

  onDropFeaturedImage = (pictureFiles, pictureDataURLs) => {
    let rem = pictureFiles[0].name
    let str = pictureDataURLs;
    let base64 = str.toString().replace(";name=", "");
    let add = base64.toString().replace( rem, "");
    this.setState({
        featureImage: add
    });
  }

  onDropImageGallery = (pictureFiles,pictureDataURLs) => {
    let rem = pictureFiles.map((item) => item.name)
    var str = pictureDataURLs;
    var base64 = str.map((item) => item.replace(";name=", ""));
    var i;
    var imagesGallery = []
    for (i = 0; i < rem.length; i++) {
      var add = base64.map((item) => item.replace( rem[i], ""));
      imagesGallery.push(add[i])
    }
    this.setState({
        imagesAdd: imagesGallery
    });
  }

  removeImage(name) {
    const { images } = this.state
    while (images.indexOf(name) !== -1) {
      images.splice(images.indexOf(name), 1);
    }
    this.setState({
      images
    });
  }

  removeFeaturedImage = () => {
    this.setState({
      featureImage: ""
    });
  }

  renderImage(imageUrl) {
    return (
      <figure className="col-xl-3 col-sm-6">
        <div style={this.closeButton} onClick={() => this.removeImage(imageUrl)}>X</div>
        <img
            src={imageUrl}
            alt="Gallery"
            className="img-thumbnail"
        />
      </figure>
    );
  }

  editGroup = () => {
    this.props.client
    .mutate({
      mutation: addGroups,
      variables: {
        GroupID: parseInt(this.state.gid),
        CommunityID: this.state.id,
        Name: this.state.name,
        Description: this.state.description,
        HashTag: this.state.tags,
        FeaturedImage: this.state.featureImage.concat(this.state.featureImageAdd),
        Images: this.state.imagesAdd.concat((this.state.images && this.state.images))
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      toast.success("Group updated !")
      this.props.history.push('/dashboard/group')
      // window.location.reload();
    });
  }

  render() {      
    const { name, description, tags, featureImage, images, community } = this.state;
    
    let optionItems = community.map((community, index) =>
      <option key={index} value={community.CommunityID}>{community.Name}</option>);

    return( 
      <Fragment>
          <Breadcrumb title="Community/ Edit Group" parent="Dashboard" />
          <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Edit Group</h5>
                        </div>
                        <form className="form theme-form">
                        <div className="card-body">
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlSelect9">Select Community</label>
                                    <select value={this.state.id} className="selectpicker show-tick form-control"  id="id" onChange={this.handleCID}>
                                        {optionItems}
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Group Name</label>
                                    <input className="form-control" id="exampleFormControlInput1" type="text" value={name} onChange={this.handleName}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword2">HashTag</label>
                                    <TagsInput name="tags" value={tags} onChange={this.handleHashTags}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea4">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea4" rows="3" value={description} onChange={this.handleDescription}></textarea>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Featured Image</label>
                                    { !featureImage ? 
                                    <ImageUploader
                                        singleImage={true}
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Featured Image"
                                        onChange={this.onDropFeaturedImage}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    /> : null }
                                    <div className="my-gallery card-body row">
                                      { featureImage ? 
                                      <figure className="col-xl-3 col-sm-6">
                                          <div style={this.closeButton} onClick={this.removeFeaturedImage}>X</div>
                                          <img
                                              src={featureImage}
                                              alt="Gallery"
                                              className="img-thumbnail"
                                          />
                                      </figure> : null }
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Images Gallery</label>
                                    <ImageUploader
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Images"
                                        onChange={this.onDropImageGallery}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />
                                    <div className="my-gallery card-body row">
                                    {
                                      (images && images.length >= 1) ? 
                                          images.map(imageUrl => this.renderImage(imageUrl))
                                          : null
                                    }
                                    </div>
                                </div>
                                </div>
                            </div>
                            <GroupQuestion/>       
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-primary mr-1" type="button" onClick={this.editGroup}>Update Group</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          <ToastContainer />
      </Fragment>
    );
  }
}

export default withApollo(EditCommunity);