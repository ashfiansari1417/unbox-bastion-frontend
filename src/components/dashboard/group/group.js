import React, {Fragment} from 'react';

import Breadcrumb from '../../common/breadcrumb';

import GroupTable from './group-table';

function Group(props) {
    return(
        <Fragment>
            <Breadcrumb title="Community Manegement" parent="Dashboard" />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-body btn-showcase">
                                <GroupTable history={props.history}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Group;