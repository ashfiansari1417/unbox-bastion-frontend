import React, { Fragment, Component } from 'react'

class GroupQuestions extends Component {
    state = {
        question: "",
        questionArray: []
    }

    handleQue = (event) => {
        this.setState({
            question: event.target.value
        })
    }

    addQuestion = () => {
        this.setState(prevState => ({
            questionArray: prevState.questionArray.concat(this.state.question)
        }))
        this.setState({ question: "" })
    }

    removeQuestion(que) {
        const { questionArray } = this.state
        while (questionArray.indexOf(que) !== -1) {
            questionArray.splice(questionArray.indexOf(que), 1);
        }
        this.setState({ questionArray });
      }

    render() {
        const { questionArray } = this.state;

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header">
                                    <h6>Group Questions</h6>
                                </div>
                                <div className="card-body">
                                    <div className="todo">
                                    <div className="todo-list-wrapper">
                                        <div className="todo-list-container">
                                                <div className="new-task-wrapper">
                                                    <input className="col-sm-6" type="text" value={this.state.question} onChange={this.handleQue}/>
                                                    <span className="btn btn-success ml-3 add-new-task-btn" id="add-task" onClick={this.addQuestion}>Add</span>
                                                </div><br/>
                                                <div className="todo-list-body">
                                                    <ul id="todo-list">
                                                        {questionArray.length > 0 ?
                                                            questionArray.map((todo, index) =>
                                                                <li className="task" key={index} >
                                                                    <div className="task-container">
                                                                        <h4 className="task-label">{todo}</h4>
                                                                        <span className="task-action-btn">
                                                                            <span className="action-box large delete-btn" title="Delete Que" onClick={() => this.removeQuestion(todo)}>
                                                                                <i className="icon"><i className="icon-trash"></i></i></span>
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                            ) : ''}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default GroupQuestions