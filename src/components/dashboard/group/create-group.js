import React, { Fragment, Component} from 'react';
import { withApollo } from 'react-apollo';
import ImageUploader from 'react-images-upload';
import TagsInput from 'react-tagsinput';
import { ToastContainer, toast } from 'react-toastify';

import 'react-tagsinput/react-tagsinput.css';

import { addGroups } from '../../../services/mutation';
import { getCommunityNames } from '../../../services/queries';

import Breadcrumb from '../../common/breadcrumb';
import GroupQuestion from './group-question';

class CreateGroup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cid: "",
      community: [],  
      gname: "",
      description: "",
      tags: [],
      featureImage: "",
      images:[],
      scope: "0",
      questions: "no",
      childQue: [],
      question: "",
      status: 1,
      questionArray: [],
      isDisabled: true
    }
  }

  handleQue = (event) => {
    this.setState({
        question: event.target.value,
        status: 1
    })
}

addQuestion = () => {

    const newQuestion = this.state.question;
    const newStatus = this.state.status;

    const obj = { 'Question': newQuestion, 'Status': newStatus };

    const newArray = this.state.questionArray.slice();

    newArray.push(obj)
    
    this.setState({
        questionArray: newArray
    })
    this.setState({ question: "" })
}

removeQuestion(que) {
    const { questionArray } = this.state
    while (questionArray.indexOf(que) !== -1) {
        questionArray.splice(questionArray.indexOf(que), 1);
    }
    this.setState({ questionArray });
  }

  renderGroupQuestion = () => {
    return <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header">
                                    <h6>Group Questions</h6>
                                </div>
                                <div className="card-body">
                                    <div className="todo">
                                    <div className="todo-list-wrapper">
                                        <div className="todo-list-container">
                                                <div className="new-task-wrapper">
                                                    <input className="col-sm-6" type="text" value={this.state.question} onChange={this.handleQue}/>
                                                    <span className="btn btn-success ml-3 add-new-task-btn" id="add-task" onClick={this.addQuestion}>Add</span>
                                                </div><br/>
                                                <div className="todo-list-body">
                                                    <ul id="todo-list">
                                                        {this.state.questionArray.length > 0 ?
                                                            this.state.questionArray.map((todo, index) =>
                                                                <li className="task" key={index} >
                                                                    <div className="task-container">
                                                                        <h4 className="task-label">{todo.Question}</h4>
                                                                        <span className="task-action-btn">
                                                                            <span className="action-box large delete-btn" title="Delete Que" onClick={() => this.removeQuestion(todo)}>
                                                                                <i className="icon"><i className="icon-trash"></i></i></span>
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                            ) : ''}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
  }

  componentDidMount(){
    this.getCommunityNames();
  }

  getCommunityNames = () => {
    this.props.client
    .query({
      query: getCommunityNames,
      fetchPolicy: "no-cache"
    })
    .then(res=> {
      this.setState({
        community: res.data.getCommunity
      });
    });
  };

  handleCID = event => {
    this.setState({
      cid: event.target.value
    });
    this.handleError();
  }

  handleName = event => {
    this.setState({
      cname: event.target.value
    });
    this.handleError();
  }

  handleHashTags = (tags) => {
    this.setState({
      tags
    });
  }

  handleDescription = event => {
    this.setState({
      description: event.target.value
    })
  }

  onDropFeaturedImage = (pictureFiles, pictureDataURLs) => {
    let rem = pictureFiles[0].name
    let str = pictureDataURLs;
    let base64 = str.toString().replace(";name=", "");
    let add = base64.toString().replace( rem, "");
    this.setState({
        featureImage: add
    });
  }
  
  onDropImageGallery = (pictureFiles, pictureDataURLs) => {
    let rem = pictureFiles.map((item) => item.name)
    var str = pictureDataURLs;
    var base64 = str.map((item) => item.replace(";name=", ""));
    var i;
    var imagesGallery = []
    for (i = 0; i < rem.length; i++) {
      var add = base64.map((item) => item.replace( rem[i], ""));
      imagesGallery.push(add[i])
    }
    this.setState({
        images: imagesGallery
    });
  }

  groupScope = (event) => {
    
    this.setState({
      scope: event.target.value
    })
  }

  addQuestions = (event) => {
    
    this.setState({
      questions: event.target.value
    })
  }

  handleError = () => {
    if (this.state.cname) {
      this.setState({ isDisabled: false });
    } else {
      this.setState({ isDisabled: true });
    }
  }

  createGroup = () => {
    this.props.client
    .mutate({
      mutation: addGroups,
      variables: {
        CommunityID: parseInt(this.state.cid),
        Name: this.state.cname,
        Description: this.state.description,
        Hashtag: this.state.tags,
        FeaturedImage: this.state.featureImage,
        Images: this.state.images,
        isScope: parseInt(this.state.scope),
        GroupQuestions: this.state.questionArray
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
        toast.success("Group created !")
        this.props.history.push('/dashboard/group')
    });
  }

  render() {
    const { description, tags, community, scope, questions } = this.state

    let optionItems = community.map((community, index) =>
      <option key={index} value={community.CommunityID}>{community.Name}</option>);
    
    return(
        <Fragment>
          <Breadcrumb title="Group/ Create Group" parent="Dashboard" />
          <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Create Group</h5>
                        </div>
                        <form className="form theme-form">
                            <div className="card-body">
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlSelect9">Select Community</label>
                                    <select defaultValue="1" className="form-control digits" id="cid" onChange={this.handleCID}>
                                       {optionItems}
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Group Name</label>
                                    <input className="form-control" id="exampleFormControlInput1" type="text" onChange={this.handleName}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword2">HashTag</label>
                                    <TagsInput name="tags" value={tags} onChange={this.handleHashTags}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea4">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea4" rows="3" value={description} onChange={this.handleDescription}></textarea>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Featured Image</label>
                                    <ImageUploader
                                        singleImage={true}
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Featured Image"
                                        onChange={this.onDropFeaturedImage}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />:
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Images Gallery</label>
                                    <ImageUploader
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Images"
                                        onChange={this.onDropImageGallery}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Scope</label>
                                    <div className="col">
                                      <div className="form-group m-t-15 m-checkbox-inline mb-0 custom-radio-ml">
                                        <div className="radio radio-primary">
                                          <input id="radioinline1" type="radio" name="scope" value="1" onChange={this.groupScope} />
                                          <label className="mb-0" htmlFor="radioinline1">Private</label>
                                        </div>
                                        <div className="radio radio-primary">
                                          <input id="radioinline2" type="radio" name="scope" value="0" onChange={this.groupScope} defaultChecked />
                                          <label className="mb-0" htmlFor="radioinline2">Public</label>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            { scope === "1" ? 
                              <div className="row">
                                <div className="col">
                                  <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Would you like to add questions for this groups?</label>
                                    <div className="col">
                                      <div className="form-group m-t-15 m-checkbox-inline mb-0 custom-radio-ml">
                                        <div className="radio radio-primary">
                                          <input id="radioinline3" type="radio" name="grpque" value="yes" onChange={this.addQuestions}  />
                                          <label className="mb-0" for="radioinline3">Yes</label>
                                        </div>
                                        <div className="radio radio-primary">
                                          <input id="radioinline4" type="radio" name="grpque" value="no" onChange={this.addQuestions} defaultChecked />
                                          <label className="mb-0" for="radioinline4">No</label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div> : null 
                            }
                            { questions === "yes" ? this.renderGroupQuestion() : null }
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-primary mr-1" type="button" onClick={this.createGroup}>Create Group</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          <ToastContainer />
      </Fragment>
    );
  }
}

export default withApollo(CreateGroup);