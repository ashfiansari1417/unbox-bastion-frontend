import React, { Fragment } from 'react';
import { withApollo } from "react-apollo";

import { getUsers } from '../../../services/queries';

import Datatable from '../../common/datatable';
    
class UserTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nametable: []
        }
    }

    componentDidMount() {
        this.getUsers();
    }

    getUsers = () => {
        this.props.client
        .query({
            query: getUsers,
            fetchPolicy: "network-only"
         })
          .then(res => {       
              this.setState({
                  nametable : res.data.getUsers
                })
          });          
    };

    render() {
        const { nametable } = this.state;
        
        return(
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>User List</h5>
                                </div>
                                <div className="card-body datatable-react">
                                    <Datatable
                                        history={this.props.history}
                                        multiSelectOption={false}
                                        myData={nametable}
                                        pageSize={10}
                                        pagination={true}
                                        class="-striped -highlight"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default withApollo(UserTable);