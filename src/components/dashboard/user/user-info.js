import React, { Fragment, useState } from 'react';
import { withApollo } from "react-apollo";
import { useQuery } from "@apollo/react-hooks";
import { TabContent, TabPane, Nav, NavItem, NavLink, Spinner } from 'reactstrap';

import { getUserByID } from '../../../services/queries';

import one from "../../../assets/images/user/user.png";

import AboutTab from '../../../components/social-app/aboutTab';
import ImpactTab from '../../../components/social-app/impactTab';

var sectionStyle = {
    backgroundRepeat: "no-repeat",
    backgroundSize: "100% 100%",
    backgroundImage: `url(https://images.unsplash.com/photo-1506869640319-fe1a24fd76dc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80)`
  };

const UserInfo = (props) => {
    const [activeTab, setActiveTab] = useState('1');
    const [id, setID] = useState(props.match.params.id);
    

    const userInfo = useQuery(getUserByID, {
        variables: { ID: parseInt(id) },
        fetchPolicy: "network-only"
      });
    
      if (userInfo.loading) return <Spinner color="dark" />;
      const UserDetails = userInfo.data.getUserByID;

      console.log("props",UserDetails);

    return (
        <Fragment>
            <div className="container-fluid">
                <div className="user-profile social-app-profile">
                    <div className="row">
                        {/* <!-- user profile first-style start--> */}
                        <div className="col-sm-12">
                            <div className="card hovercard text-center">
                                <div style={ sectionStyle } className="cardheader socialheader"></div>
                                <div className="user-image">
                                    <div className="avatar">
                                        {UserDetails.Avatar ? <img alt="user" src={UserDetails.Avatar}/> : <img alt="user" src={one}/> }
                                    </div>
                                </div>
                                <div className="info market-tabs p-0">
                                    <Nav tabs className="tabs-scoial borderb-tab-primary market-tabs">
                                        <NavItem className="nav nav-tabs " id="myTab" role="tablist">
                                            <NavLink className={activeTab == '1' ? 'active' : ''} onClick={() => setActiveTab('1')}>
                                                About
                                        </NavLink>
                                        </NavItem>
                                        <li className="nav-item">
                                            <div className="user-designation"></div>
                                            <div className="title"><a target="_blank" href="#javascripts">{UserDetails.Name}</a></div>
                                            <div className="desc mt-2">{UserDetails.Description}</div>
                                        </li>
                                        <NavItem className="nav nav-tabs" id="myTab" role="tablist">
                                            <NavLink className={activeTab == '2' ? 'active' : ''} onClick={() => setActiveTab('2')}>
                                                Impact 
                                        </NavLink>
                                        </NavItem>
                                    </Nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <TabContent activeTab={activeTab} className="tab-content">
                        <TabPane tabId="1">
                            <AboutTab UserDetails={UserDetails} />
                        </TabPane>
                        <TabPane tabId="2">
                            <ImpactTab UserDetails={UserDetails} />
                        </TabPane>
                    </TabContent>
                </div>
            </div>
        </Fragment>
    );
};

export default withApollo(UserInfo);