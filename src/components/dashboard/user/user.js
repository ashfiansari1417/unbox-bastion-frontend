import React, {Fragment} from 'react';

import Breadcrumb from '../../common/breadcrumb';

import UserTable from './user-table';

function User(props) {
    return(
        <Fragment>
            <Breadcrumb title="User Manegement" parent="Dashboard" />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-body btn-showcase">
                                <UserTable history={props.history}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default User;