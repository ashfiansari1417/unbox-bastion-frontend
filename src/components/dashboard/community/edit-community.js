import React, {Fragment, Component} from "react";
import { withApollo } from "react-apollo";
import Lightbox from "react-image-lightbox";
import TagsInput from "react-tagsinput";
import { ToastContainer, toast } from 'react-toastify';

import 'react-tagsinput/react-tagsinput.css';

import { getCommunityByID } from '../../../services/queries';
import { addCommunity } from '../../../services/mutation';

import Breadcrumb from '../../common/breadcrumb';

import ImageUploader from 'react-images-upload';

class EditCommunity extends Component {

  closeButton = {
    right: "15px",
    position: "absolute",
    top: "-9px",
    color: "#fff",
    background: "#ff4081",
    borderRadius: "50%",
    textAlign: "center",
    cursor: "pointer",
    fontSize: "26px",
    fontWeight: "bold",
    lineHeight: "30px",
    width: "30px",
    height: "30px",
  };

  constructor(props) {
    super(props);
    this.state = {
      id : this.props.match.params.id,
      cname: "",
      description: "",
      tags: [],
      featureImage: "",
      images:[],
      imagesAdd:[],
      featureImageAdd: ""
    }
    this.onDropFeaturedImage = this.onDropFeaturedImage.bind(this);
    this.onDropImageGallery = this.onDropImageGallery.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
      if (props.match.params.id !== state.id) {
        return {
            id: props.match.params.id,
        };
    }
    return null;
  }

  componentDidMount() {
      this.getSingleCommunity()
  }

  getSingleCommunity = () => {
    this.props.client
      .query({
        query: getCommunityByID,
        variables: {
            CommunityID: parseInt(this.state.id)
        },
        options: {
          fetchPolicy: "network-only"
        }
      })
      .then(res => {
        this.setState({
            cname: res.data.getCommunityByID.Name,
            description: res.data.getCommunityByID.Description,
            tags: res.data.getCommunityByID.HashTag,
            featureImage: res.data.getCommunityByID.FeaturedImage,
            images: res.data.getCommunityByID.Images
          });
      });
  };

  handleCName = event => {
    this.setState({
      cname: event.target.value
    });
  }

  handleHashTags = (tags) => {
    this.setState({
      tags
    });
  }

  handleDescription = event => {
    this.setState({
      description: event.target.value
    })
  }

  onDropFeaturedImage(pictureFiles, pictureDataURLs) {
    let rem = pictureFiles[0].name
    let str = pictureDataURLs;
    let base64 = str.toString().replace(";name=", "");
    let add = base64.toString().replace( rem, "");
    this.setState({
        featureImage: add
    });
  }

  onDropImageGallery(pictureFiles,pictureDataURLs) {
    let rem = pictureFiles.map((item) => item.name)
    var str = pictureDataURLs;
    var base64 = str.map((item) => item.replace(";name=", ""));
    var i;
    var imagesGallery = []
    for (i = 0; i < rem.length; i++) {
      var add = base64.map((item) => item.replace( rem[i], ""));
      imagesGallery.push(add[i])
    }
    this.setState({
        imagesAdd: imagesGallery
    });
  }

  removeImage(name) {
    const { images } = this.state
    while (images.indexOf(name) !== -1) {
      images.splice(images.indexOf(name), 1);
    }
    this.setState({
      images
    });
  }

  removeFeaturedImage = () => {
    this.setState({
      featureImage: ""
    });
  }

  renderImage(imageUrl) {
    return (
      <figure className="col-xl-3 col-sm-6">
        <div style={this.closeButton} onClick={() => this.removeImage(imageUrl)}>X</div>
        <img
            src={imageUrl}
            alt="Gallery"
            className="img-thumbnail"
        />
      </figure>
    );
  }

  editCommunity = () => {
    this.props.client
    .mutate({
      mutation: addCommunity,
      variables: {
        CommunityID: parseInt(this.state.id),
        Name: this.state.cname,
        Description: this.state.description,
        HashTag: this.state.tags,
        FeaturedImage: this.state.featureImage.concat(this.state.featureImageAdd),
        Images: [...this.state.images, ...this.state.imagesAdd]
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
      toast.success("Community updated !")
      this.props.history.push('/dashboard/community')
      // window.location.reload();
    });
  }

  render() {      
    const { cname, description, tags, featureImage, images } = this.state;   

    return( 
      <Fragment>
          <Breadcrumb title="Community/ Edit Community" parent="Dashboard" />
          <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Edit Community</h5>
                        </div>
                        <form className="form theme-form">
                            <div className="card-body">
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Community Name</label>
                                    <input className="form-control" id="exampleFormControlInput1" type="text" value={cname} onChange={this.handleCName}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword2">HashTag</label>
                                    <TagsInput name="tags" value={tags} onChange={this.handleHashTags}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea4">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea4" rows="3" value={description} onChange={this.handleDescription}></textarea>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Featured Image</label>
                                    { !featureImage ? 
                                    <ImageUploader
                                        singleImage={true}
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Featured Image"
                                        onChange={this.onDropFeaturedImage}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    /> : null }
                                    <div className="my-gallery card-body row">
                                      { featureImage ? 
                                      <figure className="col-xl-3 col-sm-6">
                                          <div style={this.closeButton} onClick={this.removeFeaturedImage}>X</div>
                                          <img
                                              src={featureImage}
                                              alt="Gallery"
                                              className="img-thumbnail"
                                          />
                                      </figure> : null }
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Images Gallery</label>
                                    <ImageUploader
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Images"
                                        onChange={this.onDropImageGallery}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />
                                    <div className="my-gallery card-body row">
                                    {
                                      (images && images.length >= 1) ? 
                                          images.map(imageUrl => this.renderImage(imageUrl))
                                          : null
                                    }
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-primary mr-1" type="button" onClick={this.editCommunity}>Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          <ToastContainer />
      </Fragment>
    );
  }
}

export default withApollo(EditCommunity);