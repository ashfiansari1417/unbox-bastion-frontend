import React, { Fragment, Component} from 'react';
import { withApollo } from 'react-apollo';
import ImageUploader from 'react-images-upload';
import TagsInput from 'react-tagsinput';
import { ToastContainer, toast } from 'react-toastify';

import 'react-tagsinput/react-tagsinput.css';

import { addCommunity } from '../../../services/mutation'

import Breadcrumb from '../../common/breadcrumb';

class CreateCommunity extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cname: "",
      description: "",
      tags: [],
      featureImage: "",
      images:[],
      isDisabled: true
    }
    this.onDropFeaturedImage = this.onDropFeaturedImage.bind(this);
    this.onDropImageGallery = this.onDropImageGallery.bind(this);
  }

  handleCName = event => {
    this.setState({
      cname: event.target.value
    });
    this.handleError();
  }

  handleHashTags = (tags) => {
    this.setState({
      tags
    });
  }

  handleDescription = event => {
    this.setState({
      description: event.target.value
    })
  }

  onDropFeaturedImage(pictureFiles, pictureDataURLs) {
    let rem = pictureFiles[0].name
    let str = pictureDataURLs;
    let base64 = str.toString().replace(";name=", "");
    let add = base64.toString().replace( rem, "");
    this.setState({
        featureImage: add
    });
  }
  
  onDropImageGallery(pictureFiles, pictureDataURLs) {
    let rem = pictureFiles.map((item) => item.name)
    var str = pictureDataURLs;
    var base64 = str.map((item) => item.replace(";name=", ""));
    var i;
    var imagesGallery = []
    for (i = 0; i < rem.length; i++) {
      var add = base64.map((item) => item.replace( rem[i], ""));
      imagesGallery.push(add[i])
    }
    this.setState({
        images: imagesGallery
    });
  }

  handleError = () => {
    if (this.state.cname) {
      this.setState({ isDisabled: false });
    } else {
      this.setState({ isDisabled: true });
    }
  }

  createCommunity = () => {
    this.props.client
    .mutate({
      mutation: addCommunity,
      variables: {
        Name: this.state.cname,
        Description: this.state.description,
        HashTag: this.state.tags,
        FeaturedImage: this.state.featureImage,
        Images: this.state.images
      },
      fetchPolicy: "no-cache"
    })
    .then(res => {
        toast.success("Community created !")
        this.props.history.push('/dashboard/community')
    });
  }

  render() {
    const { description, tags, featureImage } = this.state
    
    return(
        <Fragment>
          <Breadcrumb title="Community/ Create Community" parent="Dashboard" />
          <div className="container-fluid">
            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Create Community</h5>
                        </div>
                        <form className="form theme-form">
                            <div className="card-body">
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Community Name</label>
                                    <input className="form-control" id="exampleFormControlInput1" type="text" onChange={this.handleCName}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword2">HashTag</label>
                                    <TagsInput name="tags" value={tags} onChange={this.handleHashTags}/>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlTextarea4">Description</label>
                                    <textarea className="form-control" id="exampleFormControlTextarea4" rows="3" value={description} onChange={this.handleDescription}></textarea>
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Featured Image</label>
                                    <ImageUploader
                                        singleImage={true}
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Featured Image"
                                        onChange={this.onDropFeaturedImage}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />:
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlInput1">Images Gallery</label>
                                    <ImageUploader
                                        withIcon={false}
                                        withPreview={true}
                                        label=""
                                        buttonText="Upload Images"
                                        onChange={this.onDropImageGallery}
                                        imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                                        maxFileSize={1048576}
                                        fileSizeError=" file size is too big"
                                    />
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="card-footer">
                                <button className="btn btn-primary mr-1" type="button" onClick={this.createCommunity}>Create Community</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          <ToastContainer />
      </Fragment>
    );
  }
}

export default withApollo(CreateCommunity);