import React, { Fragment } from 'react';
import { withApollo } from "react-apollo";

import { getCommunity } from '../../../services/queries';

import Datatable from '../../common/datatable';
    
class CommunityTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            nametable: []
        }
    }

    componentDidMount() {
        this.getCommunity();
    }

    getCommunity = () => {
        this.props.client
        .query({
            query: getCommunity,
            fetchPolicy: "network-only"
         })
          .then(res => {       
              this.setState({
                  nametable : res.data.getCommunity
                })
          });          
    };

    gotoCommunityForm = () => {
        this.props.history.push("/dashboard/community/community-form");
    }

    render() {
        const { nametable } = this.state;   
        
        return(
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-header span">
                                    <h5>Community List</h5>
                                    <button className="btn btn-primary float-right" type="button" onClick={this.gotoCommunityForm}>Add Community</button>
                                </div>
                                <div className="card-body datatable-react">
                                    <Datatable
                                        history={this.props.history}
                                        multiSelectOption={false}
                                        myData={nametable}
                                        pageSize={10}
                                        pagination={true}
                                        class="-striped -highlight"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default withApollo(CommunityTable);