import React, {Fragment} from 'react';

import Breadcrumb from '../../common/breadcrumb';

import CommunityTable from './community-table';

class Community extends React.Component {

    render() {
        return(
            <Fragment>
                <Breadcrumb title="Community Manegement" parent="Dashboard" />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card">
                                <div className="card-body btn-showcase">
                                    <CommunityTable history={this.props.history}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Community;